<?php

namespace App\Exceptions;

use Illuminate\Database\Events\QueryExecuted;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;
use Illuminate\Database\QueryException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Auth\Access\AuthorizationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Modules\Core\Enums\HttpStatusCode;
use Illuminate\Http\Exceptions\ThrottleRequestsException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Throwable $exception)
    {
        if ($exception instanceof ThrottleRequestsException) {
            return $this->limitExceed();
        }

        if ($exception instanceof AuthorizationException) {
            return $this->unauthorised();
        }
        if ($exception instanceof NotFoundHttpException) {
            return $this->notFound();
        }

        if ($exception instanceof ModelNotFoundException) {
            return $this->notFound();
        }

        if($exception instanceof QueryException){
            return $this->serverError();
        }
        return parent::render($request, $exception);
    }

    /**
     * Convert an authentication exception into an not found response.
     * @return \Illuminate\Http\Response
     */
    protected function notFound()
    {
        return response()->json(['error' => HttpStatusCode::getMessageForCode(HttpStatusCode::HTTP_NOT_FOUND)], HttpStatusCode::HTTP_NOT_FOUND);
    }

    /**
     * Convert an authentication exception into an unauthorised response.
     * @return \Illuminate\Http\Response
     */
    private function unauthorised()
    {
        return response()->json(['error' => HttpStatusCode::getMessageForCode(HttpStatusCode::HTTP_UNAUTHORIZED)], HttpStatusCode::HTTP_UNAUTHORIZED);
    }

    /**
     * Convert an server error into an error response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  QueryException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function serverError()
    {
        return response()->json(['error' => 'Internal Server Error.'], 500);
    }

    public function limitExceed()
    {
        return response()->json(['error' => 'Some error occured. Please try again after some time.'], 429);
    }
}
