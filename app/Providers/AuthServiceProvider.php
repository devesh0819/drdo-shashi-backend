<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Laravel\Passport\Passport;
use Illuminate\Auth\Notifications\VerifyEmail;
use Modules\User\Entities\User;
use Illuminate\Notifications\Messages\MailMessage;
use Modules\User\Entities\UserOtp;
use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Support\HtmlString;
use Exception;
use Carbon\Carbon;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        
        ResetPassword::createUrlUsing(function ($user, string $token) {
            return config('auth.reset_password_url').$token."&email=".base64_encode(request()->get('email'));
        });

        if (! $this->app->routesAreCached()){
            Passport::routes();
        }
        
        Passport::tokensExpireIn(now()->addMinutes(60));
        Passport::refreshTokensExpireIn(now()->addDays(1));
        Passport::personalAccessTokensExpireIn(now()->addMinutes(60));
        
        Passport::tokensCan([
            'create-user'=>'create user',
            'update-user'=>'update user',
            'delete-user'=>'delete user',
            'view-user'=>'view user',
            'create-application'=>'create application',
            'update-application'=>'update application',
            'view-application'=>'view application',
            'delete-application'=>'delete application',
            'create-assessment'=>'create assessment',
            'view-assessment'=>'view assessment',
            'update-assessment'=>'update assessment',
            'delete-assessment'=>'delete assessment'
        ]);

        try{
            ResetPassword::toMailUsing(function ($notifiable, string $token) {
                $url = config('auth.reset_password_url').$token."&email=".base64_encode(request()->get('email'));
                $userName = $notifiable->profile->first_name." ".$notifiable->profile->last_name;
                
                return (new MailMessage)
                    ->subject(__('mail.ForgetPasswordSubject'))
                    ->greeting("Hi ".$userName)
                    ->line(new HtmlString(__('mail.ForgotEmailBody')))
                    ->action('Click Here To Reset Your Password', $url);
            });
            VerifyEmail::toMailUsing(function ($notifiable, $url) {
                $otp = $this->generateOTP($notifiable);
                $userName = $notifiable->profile->first_name." ".$notifiable->profile->last_name;
                return (new MailMessage)
                        ->subject(__('mail.UserVerifyEmailSubject'))
                        ->greeting("Hi ".$userName)
                        ->line(new HtmlString(__('mail.VerifyEmailBody', ['otp'=>$otp])))
                        ->action('Click Here To Login', config("api.app_frontend_url").'/login');
                        
            });
        }catch(Exception $ex)
        {
            return $ex;
        }
        
    }

    /**
     * Function to generate USER OTP
     */
    private function generateOTP(User $user)
    {
        // Delete Previous OTP and generate a new one
        UserOtp::where('user_id', $user->id)->delete();

        // Create a new One
        $userOtp = new UserOtp();
        $userOtp->user_id = $user->id;
        $userOtp->otp = rand(100000, 999999);
        $userOtp->attempts_count = 0;
        $userOtp->expired_at = date("Y-m-d H:i:s", strtotime("+30 min"));
        $userOtp->save();
        return $userOtp->otp;
    }
}
