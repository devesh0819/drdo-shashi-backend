<?php

return [
    'msme_type'=>[
        'SM'=>'Small',
        'MIC'=>'Micro',
        'MED'=>'Medium',
        'L'=>'Large'
    ],
    'applicant_applications'=>[
        'app_status'=>[
            'app_basic'=>'In Process',
            'app_financial'=>'In Process',
            'app_ownership'=>'In Process',
            'app_pending_approval'=>'Approval pending',
            'app_approved'=>'Approved',
            'app_scheduled'=>'Scheduled',
            'app_payment_done'=>'Payment completed',
            'in_process'=>'In Process',
            'app_rejected'=>'Rejected',
            'app_assess_complete'=>'Application assessment completed',
            'app_feedback_complete'=>'Feedback Submitted',
            'app_payment_review'=>'Application Payment Reviewed',
            'app_payment_approved'=>'Application Payment Approved'
        ],
        'assess_status'=>[
            'started'=>'Started',
            'assess_assign'=>'Assessor Assigned',
            'param_assign'=>'Parameter Assigneds',
            'asses_init'=>'Assessment Initialized',
            'asses_submit'=>'Assessment Submitted',
            'asses_reopen'=>'Assessment Reopened',
            'asses_complete'=>'Assessment Completed',
            'asses_feed_sub'=>'Assessment Feedback Submitte',
            'assess_started'=>'Assessment Started'
        ],
    ]
];