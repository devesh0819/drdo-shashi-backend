<?php

return [
  'ForgetPasswordSubject'=>'QCI - Reset Password Request',
  'UserVerifyEmailSubject'=>'QCI - Email Verification OTP',
  'ForgotEmailBody'=>'<p>You recently requested to reset the password for your QCI account. Click the button below to proceed.</p> <p>If you did not request a password reset, please ignore this email or reply to let us know. This password reset link is only valid for the next 30 minutes.</p>',
  'VerifyEmailBody'=>'Your email verification otp is :otp',
  'new_user_created'=>'<p>Congratulations! Your account has been created in QCI.</p> 
  <p>Your Credentials to use this account is given below.</p> 
  <p>UNIQUE ID: :userName</p> <p>Password: :password</p>
  <p> Click the button below to login to your account</p>',
  'new_feedback_request_vendor_subject'=>'QCI- Application Feedback Request',
  'new_feedback_request_vendor'=>'<p>You have been recently requested for submitting feedback on the recent assessment process.</p>',
  'new_user_created_subject'=>'Welcome To QCI',
  'new_questionnaire_submitted_subject'=>'QCI- New Questionnaire Request',
  'new_questionnaire_submitted'=>'<p>You have recently requested for approval of a new questionnaire.</p>',
  'new_application_submitted_subject'=>'QCI- New Application Request',
  'new_application_submitted'=>'<p>You have recently requested for approval of a new application from :enterprise_name.</p>',
  'new_application_approved_subject'=>'QCI- Application Approved By Drdo',
  'new_application_approved'=>'<p>Congratulation! Your application has been approved by drdo. Now You can make payment for assessment.</p>',
  'new_cert_renew_subject'=>'QCI- Application Certification Going To Expiry',
  'new_cert_renew'=>'<p>Your certification is going to expire in next 3 month, So Kindly renew your certification</p>'
];