<?php

return [
  'email_not_verified'=>'Your email is not verified yet',
  'internal_server_error'=>'Something Went Wrong!!', 
  'email_verify_success'=>'Email Verified Successfully',
  'email_already_verified'=>'Email Already Verified',
  'verification_link_sent'=>'Email Verification Link Sent Successfully',
  'payment_success'=>'Payment done successfully',
  'payment_error'=>'Something went wrong!',
  'questionnaire_saved_success'=>'Questionnaire Updated Succesffully',
  'questionnaire_saved_error'=>'Error in updating questionnaire',
  'questionnaire_submit_success'=>'Questionnaire Submitted Successfully',
  'questionnaire_submit_error'=>'Error in submitting questionnaire',
  'image_geotagging_error'=>'Image is not properly geotagged',
  'attachment_delete_success'=>'Attachment Deleted Successfully',
  'attachment_delete_error'=>'Error in deleting attachment',
  'feedback_submit_success'=>'Feedback saved successfully',
  'feedback_submit_error'=>'Error in submitting feedback',
  'auditlogs'=>[
    'disciplines'=>[
      'new'=> 'New Discipline Added',
      'removed'=>'Discipline Removed',
      'changed'=>'Discpline Changed'
    ],
    'parameters'=>[
      'new'=> 'New Parameter Added',
      'removed'=>'Parameter Removed',
      'changed'=>"Parameter Changed"
    ],
    'sections'=>[
      'complete_parameter'=> 'All Sections have been changed in parameter :parameter',
      'paramSection'=>"A section of :section_type type changed in parameter ':parameter'"
    ]
    ],
    'bronze-certification'=>'Level 1',
    'silver-certification'=>'Level 2',
    'gold-certification'=>'Level 3',
    'diamond-certification'=>'Level 4',
    'platinum-certification'=>'Level 5',
    'bronze-val'=>'1',
    'silver-val'=>'2',
    'gold-val'=>'3',
    'diamond-val'=>'4',
    'platinum-val'=>'5',
    'assessment_open_success'=>'Assessment started by assessor successfully',
    'assessment_open_error'=>'Unable to start assessment',
    'old_password_mismatch'=>'Old password is incorrect',
    'application_already_exist'=>'You can\'t create a new application before the 3 months from expiry for the existing application'
];