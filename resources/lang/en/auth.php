<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records. You have :attemptsLeft attempts left only',
    'password' => 'The provided password is incorrect.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'inactive' => 'User account has been deactivated temporarily.',
    'unauthorized' => 'You are not authorised to access this page.',
    'assessor_not_active'=>'Your profile is not active yet.',
    'email_verify_already'=>'Email already verified',
    'email_verify_success'=>'Email verified successfully',
    'email_verify_error'=>'Entered Invalid OTP',
    'email_verification_sent'=>'Verification email sent successfully',
    'forgot_password_success'=>'Email link is sent to you for resetting the password'
        
];
