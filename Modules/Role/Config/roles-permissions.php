<?php

return [
    // Note: Roles & permissions added to the system will not be removed once created.
    // All the roles of the QCI
    'roles' => [
        'Admin',
        'Agency',
        'Assessor',
        'Member',
        'Vendor',
        'Drdo'
    ],
    // All the permissions for accessing the QCI
    'permissions'=>[
        'create-user',
        'update-user',
        'delete-user',
        'view-user',
        'create-application',
        'update-application',
        'view-application',
        'delete-application',
        'create-assessment',
        'view-assessment',
        'update-assessment',
        'delete-assessment',
    ],
    // Linking of permissions with each role
    'roles-permissions'=>[
        'Drdo'=>[
            'create-user',
            'update-user',
            'delete-user',
            'view-user',
            'create-application',
            'update-application',
            'view-application',
            'delete-application',
            'create-assessment',
            'view-assessment',
            'update-assessment',
            'delete-assessment',
        ],
        'Admin'=>[
            'create-user',
            'update-user',
            'delete-user',
            'view-user',
            'create-application',
            'update-application',
            'view-application',
            'delete-application',
            'create-assessment',
            'view-assessment',
            'update-assessment',
            'delete-assessment',
        ],
        'Vendor'=>[
            'create-application',
            'update-application',
            'view-application',
            'delete-application'
        ],
        'Agency'=>[
            'view-application',
            'create-assessment',
            'view-assessment',
            'update-assessment',
            'delete-assessment',
        ],
        'Assessor'=>[
            'view-application'
        ]
    ]
];
