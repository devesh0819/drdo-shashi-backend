<?php

namespace Modules\Role\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\User\Entities\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Hash;
use Exception;
use Modules\Core\Helpers\Logger;

class RolesAndPermissionsSeeder extends Seeder
{
    private $roles;
    private $permissions;
    private $rolesPermissions;

    public function run()
    {
        try{
            // Reset cached roles and permissions
            app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

            // Pick all roles and permissions to  be seeded into the system
            $this->roles = config('rolespermissions.roles');
            $this->permissions = config('rolespermissions.permissions');
            $this->rolesPermissions = config('rolespermissions.roles-permissions');

            // Creating Roles
            $this->setupRoles();

            // Creating Permissions
            $this->setupPermissions();
            
            // Assigning permissions to Roles
            $this->assignPermissionsToRole();
            
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

    /**
     * Function to create roles in the Systen
     * @author Daffodil
     */
    private function setupRoles(){
        $roles = $this->roles;

        if(!empty($roles)){
            foreach($roles as $role){
                $checkRole = Role::where(['name' => $role])->first();
                if (empty($checkRole)) {
                    Role::create(['name' => $role]);
                }
            }
        }
        return true;
    }

    /**
     * Function to create permissions in the Systen
     * @author Daffodil
     */
    private function setupPermissions(){
        $permissions = $this->permissions;

        if(!empty($permissions)){
            foreach($permissions as $permission){
                $checkPermission = Permission::where(['name' => $permission])->first();
                if (empty($checkPermission)) {
                    Permission::create(['name' => $permission]);
                }
            }
        }
        return true;
    }

    /**
     * Function to assign permissions to roles in the Systen
     * @author Daffodil
     */
    private function assignPermissionsToRole(){
        $rolesPermissions = $this->rolesPermissions;
        if(!empty($rolesPermissions)){
            foreach($rolesPermissions as $role=>$permissions){
                $role = Role::where(['name' => $role])->first();
                if(!empty($role)){
                    // if Permissions is "*" then assign all permissions otherwise assign all listed permission
                    $permissions = ($permissions == "*") ? $this->permissions : $permissions;
                    $alreadyAssignedPermissions = $role->permissions->pluck('name')->toArray();
                    $permissionToBeRemoved = array_diff($alreadyAssignedPermissions, $permissions); 
                    $permissionsToBeAdded = array_diff($permissions, $alreadyAssignedPermissions);
                    // Delete Already Assigned Permissions
                    if(!empty($permissionToBeRemoved)){
                        foreach($permissionToBeRemoved as $removePermission){
                            $role->revokePermissionTo($removePermission);
                        }
                    }
                    
                    // Assign New Permissions
                    if(!empty($permissionsToBeAdded)){
                        foreach($permissionsToBeAdded as $addPermission){
                            $role->givePermissionTo($addPermission);
                        }
                    }
                }else{
                    throw new Exception('Role not found');
                }
            }
        }
        return true;
    }
}
