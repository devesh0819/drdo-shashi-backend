<?php

namespace Modules\Notification\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\HtmlString;
use Modules\Application\Entities\Application;

class SendAssessmentRenewalEmail extends Notification
{
    use Queueable;
    private $application;
    
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Application $application)
    {
        $this->application = $application;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $userName = !empty($notifiable->profile->first_name) ? $notifiable->profile->first_name." ".$notifiable->profile->last_name : "";
        $sendMail =  (new MailMessage)
                    ->subject(__('mail.new_cert_renew_subject'))
                    ->greeting("Hi ".$userName)
                    ->line(new HtmlString(__('mail.new_cert_renew')))
                    ->action('Renew Certification', config("api.app_frontend_url").'/feedback?application_id='.$this->application->getApplicationUUID());
        return $sendMail;     
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
