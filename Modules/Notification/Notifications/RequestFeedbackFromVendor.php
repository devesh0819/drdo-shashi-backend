<?php

namespace Modules\Notification\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\HtmlString;
use Modules\Application\Entities\Application;

class RequestFeedbackFromVendor extends Notification
{
    use Queueable;
    private $application;
    private $reportPath;
    private $certificatePath;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Application $application)
    {
        $this->application = $application;
        $this->reportPath = storage_path().'/app/public/reports/'.$application->application_number."-report.pdf";
        $this->certificatePath = storage_path().'/app/public/reports/'.$application->application_number."-certificate.pdf";
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $userName = !empty($notifiable->profile->first_name) ? $notifiable->profile->first_name." ".$notifiable->profile->last_name : "";
        $sendMail =  (new MailMessage)
                    ->subject(__('mail.new_feedback_request_vendor_subject'))
                    ->greeting("Hi ".$userName)
                    ->line(new HtmlString(__('mail.new_feedback_request_vendor')))
                    ->attach($this->reportPath)
                    ->attach($this->certificatePath)
                    ->action('Submit Feedback', config("api.app_frontend_url").'/feedback?application_id='.$this->application->getApplicationUUID());
        return $sendMail;           
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
