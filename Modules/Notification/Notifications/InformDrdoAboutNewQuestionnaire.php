<?php

namespace Modules\Notification\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Modules\Questionnaire\Entities\Questionnaire;
use Illuminate\Support\HtmlString;

class InformDrdoAboutNewQuestionnaire extends Notification
{
    use Queueable;
    private $questionnaire;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Questionnaire $questionnaire)
    {
        $this->questionnaire = $questionnaire;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $userName = !empty($notifiable->profile->first_name) ? $notifiable->profile->first_name." ".$notifiable->profile->last_name : "";
        return (new MailMessage)
                    ->subject(__('mail.new_questionnaire_submitted_subject'))
                    ->greeting("Hi ".$userName)
                    ->line(new HtmlString(__('mail.new_questionnaire_submitted')))
                    ->action('Review Questionnaire', config("api.app_frontend_url"));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
