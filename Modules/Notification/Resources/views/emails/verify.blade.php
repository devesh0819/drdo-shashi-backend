<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8"> 
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <style>
        @import  url('https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400');
        a, a:focus, a:hover{
            color: #3f414d !important;
        }
    </style>
    <?php $imageBasePath = config('notification.imageBaseUrl');?>
    <body style="font-family: 'Source Sans Pro', sans-serif;" class="table-responsive">
        <table width="650" cellpadding="0" cellspacing="0" border="0"  align="center" style="background: #F7F7F7; border: 1px solid #ebebeb; box-shadow: 0 1px 8px 1px rgba(0, 0, 0, 0.1);
               background-color: white;">
            @include("notification::emails.partials._head")
            @include("notification::emails.partials._banner")
            
            <tr style="background-color: white;">
                <td>
                    <table width="630" cellpadding="0" cellspacing="0" border="0" align="center">
                        <tr>
                            <td align="center">
                                <p style="font-family: SourceSansPro;
  font-size: 14px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.57;
  letter-spacing: 0.06px;
  text-align: center;
  color: #4a4a4a;">
                                    <strong>
                                        <span style="font-family: SourceSansPro;
  font-size: 24px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: 0.35px;
  text-align: center;
  color: #000000;">Hi
                                    {{!empty($data['recipientName']) ? $data['recipientName'] : ''}}</span>                                        
                                    </strong>
                                </p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="background: #F7F7F7;">
                <td>
                    <table cellpadding="0" cellspacing="0" border="0" align="center" style="background: #fff; border: 1px solid #ebebeb; padding-bottom: 20px;margin-top:16px;width:95%;">
                        <tr>
                            <td align="left" style="padding: 20px 20px">
                                <p style="font-family: SourceSansPro;
  font-size: 14px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.57;
  letter-spacing: 0.06px;
  text-align: center;
  color: #3f414d;">
                                    {!! !empty($data['body']) ? $data['body'] : '' !!}
                                </p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            @include("notification::emails.partials._footer")
        </table>

        <img src="https://mandrillapp.com/track/open.php?u=30770470&id=769dc70881b04915af378c4ada0d360e" height="1" width="1"></body>

</html>
