<?php

namespace Modules\Attachment\Observers;

use Modules\Attachment\Entities\Attachment;
use Modules\Core\Helpers\Logger;
use Exception;
use Storage;

class AttachmentObserver
{
    /**
     * Handle the Attachment "deleted" event.
     *
     * @param  Modules\Attachment\Entities\Attachment $attachment
     * @return void
     */
    public function deleting(Attachment $attachment)
    {
        try{
            Storage::disk($attachment->disk)->delete($attachment->path);
        }catch(Exception $ex){
            Logger::error($ex);
            return $ex;
        }
    }
}
