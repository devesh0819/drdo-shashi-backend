<?php

return [
    'name' => 'Attachment',
    'base_url'=>env('QCI_ATTACH_BASE_URL'),
    'upload_directory' => env('QCI_UPLOAD_DIRECTORY', 'qci/uploads'),
    'evidence_base_url' => env('QCI_DOC_BASE_URL')
];
