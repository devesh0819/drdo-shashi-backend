<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('attachments')) {
            Schema::create('attachments', function (Blueprint $table) {
                $table->increments('id');
                $table->string('attachment_uuid', 255)->unique();
                $table->unsignedInteger('object_id')->nullable();
                $table->string('object_type', 128)->nullable();
                $table->string('file_type', 128)->nullable();
                $table->unsignedMediumInteger('file_size')->nullable();
                $table->string('mime_type', 128)->nullable();
                $table->enum('disk', ["local", "public", "s3"])->default("s3");
                $table->string('path', 255)->nullable();
                $table->unsignedTinyInteger('height')->nullable();
                $table->unsignedTinyInteger('width')->nullable();
                $table->timestamp('created_at')->useCurrent();
                $table->timestamp('updated_at')->nullable();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attachments');
    }
};
