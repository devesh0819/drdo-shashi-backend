<?php

namespace Modules\Attachment\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Modules\Application\Entities\Application;
use Modules\User\Entities\User;
use Modules\Attachment\Entities\Attachment;
use Modules\Core\Enums\UserRole;
use Modules\Core\Enums\AttachmentTypes;
use Modules\Application\Entities\ApplicationOwnerProcess;
use Modules\Assessment\Entities\AssessmentParameterEvidence;
use Modules\Assessment\Entities\AssessmentSiteEvidense;

class AttachmentPolicy
{
    use HandlesAuthorization;
    private $attachment;
    private $parent;
    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        if(!empty(request()->route('id'))){
            if(request()->get('type') == 'evidense'){
                $this->attachment = AssessmentParameterEvidence::where('evidense_uuid', request()->route('id'))->first();
                $this->parent = $this->attachment->parameter;
            }elseif(request()->get('type') == 'site_evidense'){
                $this->attachment = AssessmentSiteEvidense::where('evidense_uuid', request()->route('id'))->first();
                $this->parent = $this->attachment->assessment();
            }else{
                $this->attachment = Attachment::where('attachment_uuid', request()->route('id'))->first();
                if(!empty($this->attachment->object_type) &&  $this->attachment->object_type == AttachmentTypes::APPLICATION){
                    $this->parent = Application::where('id', $this->attachment->object_id)->first();
                }elseif(!empty($this->attachment->object_type) && $this->attachment->object_type == AttachmentTypes::APPLICATION_OWNERSHIP){
                    $object = ApplicationOwnerProcess::where('id', $this->attachment->object_id)->first();
                    if(!empty($object)){
                        $this->parent = Application::where('id', $object->application_id)->first();
                    }
                }
            }
            
            if(empty($this->attachment->id)){
                abort(404);
            }
            
        }
    }

    public function show(User $user){
        if(!empty($this->parent)){
            return $user->can('view', $this->parent);
        }
        return false;
    }

    public function delete(User $user){
        if(!empty($this->parent)){
            return $user->can('view', $this->parent);
        }
        return false;
    }
}
