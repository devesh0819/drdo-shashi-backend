<?php

namespace Modules\Attachment\Entities\Interfaces;

interface AttachmentInterface
{

    const ID = 'id';
    const OBJECT_ID = 'object_id';
    const OBJECT_TYPE = 'object_type';
    const FILE_TYPE = 'file_type';
    const FILE_SIZE = 'file_size';
    const MIME_TYPE = 'mime_type';
    const DISK = 'disk';
    const PATH = 'path';
    const HEIGHT = 'height';
    const WIDTH = 'width';
    const CREATED_DATETIME = 'created_at';
    const UPDATED_DATETIME = 'updated_at';
    const DELETED_DATETIME = 'deleted_at';

    /**
     * Getters
     */
    public function getID();

    public function getObjectId();

    public function getObjectType();

    public function getFileType();

    public function getMimeType();

    public function getDisk();

    public function getPath();

    public function getHeight();

    public function getWidth();

    public function getCreatedAt();

    public function getUpdatedAt();

    public function getDeletedAt();
    
    public function getUrl();
}
