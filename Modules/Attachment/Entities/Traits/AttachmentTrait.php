<?php

namespace Modules\Attachment\Entities\Traits;

trait AttachmentTrait
{

    public function getID()
    {
        return !empty($this->{$this::ID}) ? $this->{$this::ID} : null;
    }

    public function getObjectId()
    {
        return !empty($this->{$this::OBJECT_ID}) ? $this->{$this::OBJECT_ID} : null;
    }

    public function getObjectType()
    {
        return !empty($this->{$this::OBJECT_TYPE}) ? $this->{$this::OBJECT_TYPE} : null;
    }

    public function getFileType()
    {
        return !empty($this->{$this::FILE_TYPE}) ? $this->{$this::FILE_TYPE} : null;
    }

    public function getMimeType()
    {
        return !empty($this->{$this::MIME_TYPE}) ? $this->{$this::MIME_TYPE} : null;
    }

    public function getDisk()
    {
        return !empty($this->{$this::DISK}) ? $this->{$this::DISK} : null;
    }

    public function getPath()
    {
        return !empty($this->{$this::PATH}) ? $this->{$this::PATH} : null;
    }

    public function getHeight()
    {
        return !empty($this->{$this::HEIGHT}) ? $this->{$this::HEIGHT} : null;
    }

    public function getWidth()
    {
        return !empty($this->{$this::WIDTH}) ? $this->{$this::WIDTH} : null;
    }

    public function getCreatedAt()
    {
        return !empty($this->{$this::CREATED_DATETIME}) ? $this->{$this::CREATED_DATETIME} : null;
    }

    public function getUpdatedAt()
    {
        return !empty($this->{$this::UPDATED_DATETIME}) ? $this->{$this::UPDATED_DATETIME} : null;
    }

    public function getDeletedAt()
    {
        return !empty($this->{$this::DELETED_DATETIME}) ? $this->{$this::DELETED_DATETIME} : null;
    }

    public function getUrl()
    {
        return !empty($this->{$this::PATH}) ? config('attachment.image_cdn') . $this->{$this::PATH} : null;
    }

}
