<?php

namespace Modules\Attachment\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Attachment\Entities\Interfaces\AttachmentInterface;
use Modules\Attachment\Entities\Traits\AttachmentTrait;
use Modules\Core\Helpers\BasicHelper;

class Attachment extends Model implements AttachmentInterface
{

    use AttachmentTrait;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'attachments';
    protected $fillable = [];
    protected $appends = ['file'];
    
    protected $hidden = [
        'id', 'object_id', 'object_type', 'file_size', 'mime_type',
        'disk', 'path', 'height', 'width',
        'last_modified_by', 'deleted_at', 'created_at', 'updated_at'
    ];

    public function getFileAttribute()
    {
        return !empty($this->path) ? BasicHelper::getFileURL($this , $this->file_type) : null;
    }
}
