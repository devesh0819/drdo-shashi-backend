<?php

namespace Modules\Attachment\Services;

use Storage;
use Illuminate\Http\UploadedFile;
use Exception;
use Modules\Core\Helpers\Logger;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Enums\ContentType;
use Modules\Attachment\Repositories\Interfaces\AttachmentInterface;
use Image;
use finfo;

class AttachmentService
{

    private $repository;

    public function __construct(AttachmentInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Upload the file
     * @param UploadedFile $uploadedFile
     * @param Model $model
     * @param type $attachmentType
     * @return type
     */
    public function attach(int $attachmentId, int $objectId, string $objectType, string $attachmentType = ContentType::IMAGE)
    {
        try {
            return $this->repository->update($attachmentId, $objectId, $objectType, $attachmentType);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

    /**
     * Remove the attachment
     * @param int $attachmentId
     */
    public function detach(int $attachmentId)
    {
        try {
            return $this->repository->delete($attachmentId);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

    /**
     * Upload the base 64 encoded file into storage
     * 
     * @param UploadedFile $uploadedFile
     * @return string
     */
    public function uploadBase64File($file, $directory = '/' )
    {
        try {
                $uniqueName = $this->repository->uniqueFilename('jpg');
                $uploadPath = $this->repository->uploadDirectory() . $directory . $uniqueName;
                $uploadFile = Storage::disk($this->repository->uploadDestination('public'))
                ->put($uploadPath, $file);
                return ['uploadPath'=>$uploadPath, 'disk'=>$this->repository->uploadDestination('public')];
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

    /**
     * Upload the file to storage and save its entry in table
     * 
     * @param UploadedFile $uploadedFile
     * @return string
     */
    public function upload(UploadedFile $uploadedFile, $directory = '/', $attach=true)
    {
        try {
            $uniqueName = $this->repository->uniqueFilename($uploadedFile->extension());
            $uploadPath = $this->repository->uploadDirectory() . $directory . $uniqueName;
            $isImage = str_contains($uploadedFile->getMimeType(), 'image');
            if (($isImage) && !empty($uploadedFile->extension()) && $uploadedFile->extension() != 'gif') {
                $orientedFile = Image::make($uploadedFile)->orientate();
                // Save File at public directory locally
                $orientedFile->save(public_path('images' . "/" . $uniqueName));
                Storage::disk($this->repository->uploadDestination('public'))
                        ->put($uploadPath, $orientedFile->__toString());

                // Delete File from public directory after uploading to s3 
                Storage::disk('public')->delete('images' . "/" . $uniqueName);
            } else {
                $uploadPath = Storage::disk($this->repository->uploadDestination('public'))
                ->put($uploadPath, $uploadedFile);
            }

            return ($attach) ? $this->repository->insert($uploadedFile, $uploadPath) 
            : ['uploadedFile'=>$uploadedFile, 'uploadPath'=>$uploadPath, 'disk'=>$this->repository->uploadDestination('public')];
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }
    
    /**
     * Funtion to insert image from url
     */
    public function uploadFileFromURL($url)
    {
        try{
            $attachmentId = null;
            if(!empty($url) && filter_var($url, FILTER_VALIDATE_URL)){
                $file = file_get_contents($url);
                $ext = pathinfo(
                    parse_url($url, PHP_URL_PATH), 
                    PATHINFO_EXTENSION
                );
                $uniqueName = $this->repository->uniqueFilename($ext);
                $uploadPath = $this->repository->uploadDirectory() . "/" . $uniqueName;
                $upload = Storage::disk($this->repository->uploadDestination())
                            ->put($uploadPath, $file);
                if($upload){
                    $uploadedImage =  Image::make($file);
                    $attachmentId = $this->repository->insertImage($uploadedImage, $uploadPath);
                }
            }
            return $attachmentId;
        }catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

    /**
     * Destroy the file from storage
     * 
     * @param string $path
     * @return string
     */
    public function destroy(string $path)
    {
        try {
            //1. delete file from storage at $path
            return Storage::disk($this->repository->uploadDestination())->delete($path);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

    /**
     * Get an attachment by id
     * 
     * @param int $attachmentId
     */
    public function get(int $attachmentId)
    {
        try {
            return $this->repository->get($attachmentId);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }
}
