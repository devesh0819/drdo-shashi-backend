<?php

namespace Modules\Attachment\Repositories\Interfaces;

use Illuminate\Http\UploadedFile;
use Intervention\Image\Image;

interface AttachmentInterface
{

    /**
     * Insert an attachment
     * @param type $data
     */
    public function insert(UploadedFile $uploadedFile, string $uploadPath);

    
    public function insertImage(Image $uploadedImage, string $uploadPath);
    /**
     * Update an attachment
     * @param type $data
     * @param type $id
     */
    public function update(int $attachmentId, int $objectId, string $objectType, string $attachmentType);

    /**
     * Delete an attachment
     * @param type $id
     */
    public function delete(int $id);

    /**
     * Get an attachment
     * @param type $id
     */
    public function get(int $id);

    /**
     * Get upload destination file system
     * 
     * @param string $dest
     * @return string
     */
    public function uploadDestination(string $dest);

    /**
     * Get upload directory
     * 
     * @return string
     */
    public function uploadDirectory(string $dir);

    /**
     * Generates a unique file name
     * 
     * @return string
     */
    public function uniqueFilename(string $fileExtension);
}
