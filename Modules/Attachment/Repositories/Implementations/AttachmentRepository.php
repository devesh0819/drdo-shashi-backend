<?php

namespace Modules\Attachment\Repositories\Implementations;

use Modules\Attachment\Repositories\Interfaces\AttachmentInterface;
use Modules\Attachment\Entities\Attachment;
use Illuminate\Http\UploadedFile;
use Intervention\Image\Image;
use Illuminate\Support\Str;

/**
 * Attachment Repository
 */
class AttachmentRepository implements AttachmentInterface
{

    /**
     * Insert an attachment
     * @param type $data
     */
    public function insert(UploadedFile $uploadedFile, string $uploadPath)
    {
        $attachment = new Attachment;
        $attachment->attachment_uuid = (string) Str::orderedUuid();
        $attachment->file_size = $uploadedFile->getMaxFilesize();
        $attachment->mime_type = $uploadedFile->getClientMimeType();
        $attachment->disk = $this->uploadDestination('public');
        $attachment->path = $uploadPath;
        $attachment->save();
        return $attachment;
    }

    public function insertImage(Image $uploadedFile, string $uploadPath)
    {
        $attachment = new Attachment;
        $attachment->file_size = $uploadedFile->filesize();
        $attachment->mime_type = $uploadedFile->mime;
        $attachment->disk = $this->uploadDestination('public');
        $attachment->path = $uploadPath;
        $attachment->save();
        return $attachment;
    }
     /**
     * Update an attachment
     * @param type $data
     * @param type $id
     */
    public function update(int $attachmentId, int $objectId, string $objectType, string $attachmentType)
    {
        $attachment = Attachment::find($attachmentId);
        $attachment->object_id = $objectId;
        $attachment->object_type = $objectType;
        $attachment->file_type = $attachmentType;
        $attachment->save();
        return $attachment;
    }

    /**
     * Delete an attachment
     * @param type $id
     */
    public function delete(int $id)
    {
        return Attachment::destroy($id);
    }

    /**
     * Get an attachment
     * @param type $id
     */
    public function get(int $id)
    {
        return Attachment::find($id);
    }

    /**
     * Get upload destination file system
     * 
     * @param string $dest
     * @return string
     */
    public function uploadDestination(string $dest = NULL)
    {
        $filesystems = array_keys(config('filesystems.disks'));
        return !is_null($dest) && in_array($dest, $filesystems) ? $dest : config('filesystems.local');
    }

    /**
     * Get upload directory
     * 
     * @return string
     */
    public function uploadDirectory(string $dir = "")
    {
        return empty($dir) ? config('attachment.upload_directory') : $dir;
    }

     /**
     * Generates a unique file name
     * 
     * @return string
     */
    public function uniqueFilename(string $fileExtension = NULL)
    {
        $uniqueName = md5(microtime());
        return is_null($fileExtension) ? $uniqueName : $uniqueName . ".$fileExtension";
    }

}
