<?php

namespace Modules\Assessment\Services;

use Modules\Assessment\Repositories\Interfaces\AssessmentParameterInterface;
use Exception;
use Modules\Assessment\Entities\Assessment;
use Modules\Core\Helpers\Logger;
use Modules\Assessment\Entities\AssessmentParameter;
use Modules\User\Entities\User;
use Modules\Assessment\Services\AssessmentService;
use Illuminate\Support\Facades\Validator;
use Modules\Api\Http\Requests\Assessment\AssessmentParemeterCreateRequest;
use Modules\Attachment\Services\AttachmentService;
use Illuminate\Http\UploadedFile;
use Modules\Core\Enums\AssessorAssessStage;
use Modules\Assessment\Entities\AssessmentParameterSection;
use Modules\Assessment\Entities\AssessmentParticipantDetail;
use Modules\Assessment\Entities\AssessmentSiteComment;
use Modules\Assessment\Entities\AssessmentSiteEvidense;

class AssessmentParameterService
{
    private $repository;
    private $assessmentService;
    private $attachmentService;
    private $parameterRequest;
    private $parameterRules = [];
    /**
     * Constructor
     * @param AssessmentInterface $repository
     */
    public function __construct(
        AssessmentParameterInterface $repository,
        AssessmentService $assessmentService,
        AttachmentService $attachmentService,
        AssessmentParemeterCreateRequest $parameterRequest
    )
    {
        $this->repository = $repository;
        $this->assessmentService = $assessmentService;
        $this->attachmentService = $attachmentService;
        $this->parameterRequest = $parameterRequest;
        $this->parameterRules = $this->parameterRequest->rules();
    }

    /**
     * Function to find all assessment list
     * @param Array $filters
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function lists($filters=[], $countOnly=false)
    {
        try {
            return $this->repository->lists($filters, $countOnly);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

    /**
     * Fnctionn to sync all assessor Data
     */
    public function syncAssessmentData($inputs, User $assessor)
    {
        try {
            if(!empty($inputs['data']['assessments'])){
                foreach($inputs['data']['assessments'] as $assessmentData){
                    // please check if assessor can submit assessment
                    $assessment = $this->assessmentService->firstornew($assessmentData['uuid']);
                    if(!empty($assessment) && $assessor->can('accessAssessment', $assessment)){
                        // Sync Opening Meeting
                        if(!empty($assessmentData['opening_meeting']) && $assessor->can('syncOpenMeetData', $assessment)){
                            $this->syncOpeningMeetingData($assessment, $assessmentData['opening_meeting']);
                        }
                        // Sync Closing Meeting
                        if(!empty($assessmentData['closing_meeting']) && $assessor->can('syncCloseMeetData', $assessment)){
                            $this->syncClosingMeetingData($assessment, $assessmentData['closing_meeting']);
                        }
                        if(!empty($assessmentData['disciplines'])){
                            foreach($assessmentData['disciplines'] as $discipline){
                                $parameters = !empty($discipline['parameters']) ? $discipline['parameters'] : [];
                                if(!empty($parameters)){
                                    foreach($parameters as $parameterData){
                                        $parameter = AssessmentParameter::where('_id', $parameterData['parameter_id'])->first();
                                        if(!empty($parameter) && $assessor->can('submitParameter', $parameter)){
                                            $this->syncParameter($parameter, $parameterData);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if(!empty($assessmentData['status']) && $assessmentData['status'] == AssessorAssessStage::ASSESSOR_ASSESS_COMPLETED){
                        // check if user can submit assessment or not
                        if($assessor->can('submitAssessmentToLead', $assessment)){
                            $assessorAssessment = $assessment->assessors()->where('assessor_id', $assessor->id)->first();
                            if(!empty($assessorAssessment)){
                                $assessorAssessment->assessor_assess_status = AssessorAssessStage::ASSESSOR_ASSESS_COMPLETED;
                                $assessorAssessment->save();
                            }
                        }
                    }
                }
            }
            return true;
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

    /**
     * FUnction to sync opening meeting data for assessment
     */
    private function syncOpeningMeetingData(Assessment $assessment, $data)
    {
        if(!empty($data['opening_meeting_participants']) && is_array($data['opening_meeting_participants'])){
            foreach($data['opening_meeting_participants'] as $participant){
                if(!empty($participant['id'])){
                    $object = AssessmentParticipantDetail::where('type', 'open_meet')
                    ->where('_id', $participant['id'])->first();
                    $object = !empty($object) ? $object : new AssessmentParticipantDetail();
                }else{
                    $object = new AssessmentParticipantDetail();
                }
                // Delete Participant 
                if(!empty($object->id) && !empty($participant['deleted'])){
                    $object->delete();
                }elseif(empty($participant['deleted'])){
                    $object->application_id = $assessment->application->id;
                    $object->assessment_id = $assessment->id;
                    $object->type = 'open_meet';
                    $object->name = isset($participant['name']) ? $participant['name'] : null; 
                    $object->designation = isset($participant['designation']) ? $participant['designation'] : null;
                    $object->save();
                }
            }
        }

        if(!empty($data['opening_meeting_comments']) && is_array($data['opening_meeting_comments'])){
            foreach($data['opening_meeting_comments'] as $comment){
                if(!empty($comment['id'])){
                    $commentObject = AssessmentSiteComment::where('type', 'open_meet')
                    ->where('_id', $comment['id'])->first();
                    $commentObject = !empty($commentObject) ? $commentObject : new AssessmentSiteComment();
                }else{
                    $commentObject = new AssessmentSiteComment();
                }
                // Delete Participant 
                if(!empty($commentObject->id) && !empty($comment['deleted'])){
                    $commentObject->delete();
                }elseif(empty($comment['deleted'])){
                    $commentObject->application_id = $assessment->application->id;
                    $commentObject->assessment_id = $assessment->id;
                    $commentObject->type = 'open_meet';
                    
                    $commentObject->comment_author = isset($comment['comment_author']) ? $comment['comment_author'] : null; 
                    $commentObject->comment = isset($comment['comment']) ? $comment['comment'] : null;
                    $commentObject->comment_datetime = isset($comment['last_updated_at']) ? $comment['last_updated_at'] : null;
                    
                    $commentObject->save();
                }
            }
        }
        return true;
    }

    /**
     * FUnction to sync closing meeting data for assessment
     */
    private function syncClosingMeetingData(Assessment $assessment, $data)
    {
        if(!empty($data['closing_meeting_participants']) && is_array($data['closing_meeting_participants'])){
            foreach($data['closing_meeting_participants'] as $participant){
                if(!empty($participant['id'])){
                    $object = AssessmentParticipantDetail::where('type', 'close_meet')
                    ->where('_id', $participant['id'])->first();
                    $object = !empty($object) ? $object : new AssessmentParticipantDetail();
                }else{
                    $object = new AssessmentParticipantDetail();
                }
                // Delete Participant 
                if(!empty($object->id) && !empty($participant['deleted'])){
                    $object->delete();
                }elseif(empty($participant['deleted'])){
                    $object->application_id = $assessment->application->id;
                    $object->assessment_id = $assessment->id;
                    $object->type = 'close_meet';
                    $object->name = !empty($participant['name']) ? $participant['name'] : null; 
                    $object->designation = !empty($participant['designation']) ? $participant['designation'] : null;
                    $object->save();
                }
            }
        }

        if(!empty($data['closing_meeting_comments']) && is_array($data['closing_meeting_comments'])){
            foreach($data['closing_meeting_comments'] as $comment){
                if(!empty($comment['id'])){
                    $commentObject = AssessmentSiteComment::where('type', 'close_meet')
                    ->where('_id', $comment['id'])->first();
                    $commentObject = !empty($commentObject) ? $commentObject : new AssessmentSiteComment();
                }else{
                    $commentObject = new AssessmentSiteComment();
                }
                // Delete Participant 
                if(!empty($commentObject->id) && !empty($comment['deleted'])){
                    $commentObject->delete();
                }elseif(empty($comment['deleted'])){
                    $commentObject->application_id = $assessment->application->id;
                    $commentObject->assessment_id = $assessment->id;
                    $commentObject->type = 'close_meet';
                    
                    $commentObject->comment_author = isset($comment['comment_author']) ? $comment['comment_author'] : null; 
                    $commentObject->comment = isset($comment['comment']) ? $comment['comment'] : null;
                    $commentObject->comment_datetime = isset($comment['last_updated_at']) ? $comment['last_updated_at'] : null;
                    
                    $commentObject->save();
                }
            }
        }
        return true;
    }

    /**
     * FUnction to sync site tour data for assessment
     */
    private function syncSiteTourData(Assessment $assessment, $data)
    {

    }

    /**
     * Function to sync parameter Responses
     */
    private function syncParameter($parameter, $parametersData= []){
        $userId = !empty(request()->user()->id) ? request()->user()->id : $parameter->assessor_id;
        
        // Sync Parameter Response
        if(!empty($parametersData['sections'])){
            foreach($parametersData['sections'] as $sectionData){
                if(!empty($sectionData['section_uuid'])){
                    $section = AssessmentParameterSection::where('section_uuid', $sectionData['section_uuid'])->first();
                    $validatedResponse = $this->validateInputs($sectionData);
                    $this->submitSectionResponse($section, $validatedResponse);
                }
            }
        }
        // Sync Parameter Comments
        if(!empty($parametersData['comments'])){
            foreach($parametersData['comments'] as $comment){
                if(empty($comment['id']) && $comment['comment']){
                    $paramCommentData = [
                        'comment'=>$comment['comment'],
                        'comment_type'=>!empty($comment['comment_type']) 
                        ? $comment['comment_type'] : "",
                        'comment_uuid'=>!empty($comment['comment_uuid']) 
                            ? $comment['comment_uuid'] : ""
                    ];
                    $validatedComment = $this->validateInputs($paramCommentData);
                    if(!empty($validatedComment)){
                        $validatedComment['user_id'] = $userId;
                        $this->submitParametersComment($parameter, $validatedComment);
                    }
                }
            }
            
        }
        return true;
    }

    /**
     *Function to validate assessor inputs 
     */
    private function validateInputs($inputs){
        $validator = Validator::make($inputs, $this->parameterRules);
        $errors = [];
        try {
            $validator->validated();
        } catch (Exception $ex) {
            Logger::error($ex);
            $errors = $validator->messages()->toArray();
        }
        return array_diff_key($inputs, $errors);
    }
    /**
     * Function to get assessment details by id
     * @param type $id
     */
    public function firstornew($id = null)
    {
        try {
            return $this->repository->firstornew($id);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

    /**
     * Function to get paramters details by id
     * @param type $id
     */
    public function findById($ids = [])
    {
        try {
            return $this->repository->findById($ids);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

    /**
     * Function to get section details by id
     * @param type $id
     */
    public function getSection($id = null)
    {
        try {
            return $this->repository->getSection($id);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }
    
    /**
     * Function to assign assessor to paremeter
     * @param AssessmentParameter $parameterID
     * @param ARRAY $data
     */
    public function assignParametersAssessor(AssessmentParameter $parameter, $data=[])
    {
        try {
            return $this->repository->assignParametersAssessor($parameter, $data);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

    /**
     * Function to submit response to section
     * @param AssessmentParameterSection $section
     * @param ARRAY $data
     */
    public function submitSectionResponse(AssessmentParameterSection $section, $data=[])
    {
        try {
            return $this->repository->submitSectionResponse($section, $data);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }
    
    /**
     * Function to submit comment to paremeter
     * @param AssessmentParameter $parameterID
     * @param ARRAY $data
     */
    public function submitParametersComment(AssessmentParameter $parameter, $data=[])
    {
        try {
            return $this->repository->submitParametersComment($parameter, $data);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

    /**
     * Function to get all comment to paremeter
     * @param AssessmentParameter $parameterID
     * @param ARRAY $data
     */
    public function getParameterComments(AssessmentParameter $parameter)
    {
        try {
            return $this->repository->getParameterComments($parameter);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

    /**
     * Function to submit evidence to paremeter
     * @param AssessmentParameter $parameterID
     * @param ARRAY $data
     */
    public function submitParametersEvidence(AssessmentParameter $parameter, $data=[])
    {
        try {
            return $this->repository->submitParametersEvidence($parameter, $data);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

    /**
     * Function to get all evidences to paremeter
     * @param AssessmentParameter $parameterID
     * @param ARRAY $data
     */
    public function getParameterEvidences(AssessmentParameter $parameter)
    {
        try {
            return $this->repository->getParameterEvidences($parameter);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

    /**
     * FUnction to get assessment evidence path
     */
    public function getAssessmentEvidencePath(AssessmentParameter $parameter)
    {
        return "/assessment_".$parameter->assessment_id."/parameter_".$parameter->id."/";
    }
}
