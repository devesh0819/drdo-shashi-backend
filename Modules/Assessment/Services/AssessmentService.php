<?php

namespace Modules\Assessment\Services;

use Modules\Assessment\Repositories\Interfaces\AssessmentInterface;
use Exception;
use Modules\Core\Helpers\Logger;
use Modules\Assessment\Entities\Assessment;
use Modules\User\Entities\User;

class AssessmentService
{
    private $repository;

    /**
     * Constructor
     * @param AssessmentInterface $repository
     */
    public function __construct(AssessmentInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Function to find all assessment list
     * @param Array $filters
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function lists($filters=[], $countOnly=false)
    {
        try {
            return $this->repository->lists($filters, $countOnly);
        } catch (Exception $ex) {
            dd($ex);
            Logger::error($ex);
            return $ex;
        }
    }

    public function getAssessment($filters=[], $countOnly=false)
    {
        try {
            return $this->repository->getAssessment($filters, $countOnly);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

    public function getSiteDetails($assessmentId, $filters =[])
    {
        try {
            return $this->repository->getSiteDetails($assessmentId, $filters);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }
    /**
     * Function to get assessment details by id
     * @param type $id
     */
    public function firstornew($id = null)
    {
        try {
            return $this->repository->firstornew($id);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

    /**
     * Function to save assessment
     * @param Assessment $assessment
     * @param array $data
     */
    public function save(Assessment $assessment, $data=[])
    {
        try {
            return $this->repository->save($assessment, $data);
        } catch (Exception $ex) {
           Logger::error($ex);
            return $ex;
        }
    }

    /**
     * FUnction to show assessments details based on assessmentUID
     * @param STRING $assessmentId
     * @param Array $filters
     */
    public function show($assessmentId, array $filters)
    {
        try {
            return $this->repository->show($assessmentId, $filters);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

    /**
     * Function to clone master questionnaire with applicant assessment in Mongo DB
     * @param Assessment $assessment
     */
    public function cloneAssessment(Assessment $assessment){
        try {
            return $this->repository->cloneAssessment($assessment);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }    
    
    /* Function to delete assessment
     * @param Assessment $assessment
     * @param array $data
     */
    public function delete(Assessment $assessment)
    {
        try {
            return $this->repository->delete($assessment);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

    /* 
     * Function to start a assessment
     * @param Assessment $assessment
     * @param array $data
     */
    public function startAssessment(Assessment $assessment)
    {
        try {
            return $this->repository->startAssessment($assessment);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

    /* 
     * Function to open a assessment
     * @param Assessment $assessment
     * @param array $data
     */
    public function openAssessment(Assessment $assessment, User $user)
    {
        try {
            return $this->repository->openAssessment($assessment, $user);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

    /* 
     * Function to submit a assessment
     * @param Assessment $assessment
     * @param array $data
     */
    public function submitAssessment(Assessment $assessment)
    {
        try {
            return $this->repository->submitAssessment($assessment);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

    /* 
     * Function to complete a assessment
     * @param Assessment $assessment
     * @param array $data
     */
    public function completeAssessment(Assessment $assessment)
    {
        try {
            return $this->repository->completeAssessment($assessment);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

    /**
     * Function to get all evidences to assessment
     * @param Assessment $assessment
     * @param ARRAY $data
     */
    public function getSiteEvidences(Assessment $assessment)
    {
        try {
            return $this->repository->getSiteEvidences($assessment);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

    /**
     * Function to submit evidence to assessment
     * @param Assessment $assessment
     * @param ARRAY $data
     */
    public function submitSiteEvidence(Assessment $assessment, $data=[])
    {
        try {
            return $this->repository->submitSiteEvidence($assessment, $data);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

    /**
     * FUnction to get assessment evidence path
     */
    public function getAssessmentEvidencePath($assessmentId)
    {
        return "/assessment_".$assessmentId."/site_details/";
    }
}
