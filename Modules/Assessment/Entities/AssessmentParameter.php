<?php

namespace Modules\Assessment\Entities;

use Jenssegers\Mongodb\Eloquent\Model;
use Modules\Assessment\Entities\Interfaces\AssessmentParameterInterface;
use Modules\Assessment\Entities\Traits\AssessmentParameterTrait;
use Modules\Assessment\Entities\AssessmentParameterComment;
use Modules\Assessment\Entities\AssessmentParameterEvidence;
use Modules\Assessment\Entities\AssessmentParameterSection;
use Modules\Assessment\Entities\Assessment;
use Modules\Core\Enums\ParameterResponses;

class AssessmentParameter extends Model implements AssessmentParameterInterface
{
    use AssessmentParameterTrait;
    
    protected $connection = 'mongodb';
    
    protected $collection = 'assessment_parameters';
    
    protected $appends = ['section_p_count', 'section_d_count', 'section_m_count', 'sections_completed'];

    protected $fillable = [
        'application_id', 'assessment_id', 'discipline',
        'parameter', 'parameter_slug','assessor_id',
        'parameter_status', 'parameter_score'];

    private $parameterResponses = [
        ParameterResponses::BRONZE,
        ParameterResponses::SILVER,
        ParameterResponses::GOLD,
        ParameterResponses::DIAMOND,
        ParameterResponses::PLATINUM
    ];

    public function sections()
    {
        return $this->hasMany(AssessmentParameterSection::class, 'parameter_id')->whereNotNull('section_uuid');
    }

    public function comments()
    {
        return $this->hasMany(AssessmentParameterComment::class, 'parameter_id')->whereNotNull('comment_uuid')->latest();
    } 
     
    public function evidenses()
    {
        return $this->hasMany(AssessmentParameterEvidence::class, 'parameter_id')->whereNotNull('evidense_uuid')->latest();
    }

    public function assessment()
    {
        return Assessment::where('id', $this->assessment_id)->first();
    }

    public function getSectionPCountAttribute()
    {
        return $this->sections()->where('type', 'P')->count();
    }

    public function getSectionDCountAttribute()
    {
        return $this->sections()->where('type', 'D')->count();
    }

    public function getSectionMCountAttribute()
    {
        return $this->sections()->where('type', 'M')->count();
    }

    public function getSectionsCompletedAttribute()
    {
        $status =  $this->sections()->whereNotIN('response', $this->parameterResponses)->count();
        return !empty($status) ? false : true;
    }
}