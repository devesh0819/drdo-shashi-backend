<?php

namespace Modules\Assessment\Entities;

use Jenssegers\Mongodb\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;
use Modules\Assessment\Entities\Interfaces\AssessmentParameterEvidenceInterface;
use Modules\Assessment\Entities\Traits\AssessmentParameterEvidenceTrait;
use Modules\User\Entities\UserProfile;
use Modules\Core\Helpers\BasicHelper;
use Modules\Assessment\Entities\AssessmentParameter;

class AssessmentParameterEvidence extends Model implements AssessmentParameterEvidenceInterface
{
    use SoftDeletes;
    use AssessmentParameterEvidenceTrait;
    
    protected $connection = 'mongodb';
    
    protected $collection = 'assessment_parameter_evidenses';
    
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'parameter_id', 'user_id', 'file_type', 'file_size', 'mime_type', 'disk',
        'path', 'height', 'width', 'evidense_uuid', 'file_date_time','latitude','longitude'
    ];
    
    protected $appends = ['evidense_author', 'file'];

    protected $hidden = ['parameter_id', 'file_size', 'mime_type', 'disk',
        'path', 'height', 'width'];
   
    public function user()
    {
        return UserProfile::where('user_id', $this->user_id)->first();
    }

    public function getEvidenseAuthorAttribute()
    {
        return !empty($this->user()) ? $this->user()->first_name." ".$this->user()->last_name : null;
    }

    public function parameter()
    {
        return $this->belongsTo(AssessmentParameter::class, 'parameter_id');
    }

    public function getFileAttribute()
    {
        $assetBaseURL = config('core.asset_base_url');   
        return $assetBaseURL.$this->evidense_uuid."?type=evidense";

//        return route('api.v1.core.show.file', $this->evidense_uuid)."?type=evidense";
    }
}