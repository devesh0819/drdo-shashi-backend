<?php

namespace Modules\Assessment\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Agency\Entities\AgencyMeta;
use Modules\Assessment\Entities\Interfaces\AssessmentInterface;
use Modules\Assessment\Entities\Traits\AssessmentTrait;
use Modules\Application\Entities\Application;
use Modules\Assessment\Entities\AssessmentAssessor;
use Modules\Questionnaire\Entities\Questionnaire;
use Modules\User\Entities\UserProfile;
use Modules\Assessment\Entities\AssessmentParameter;
use Modules\Assessment\Entities\AssessmentParticipantDetail;
use Modules\Assessment\Entities\AssessmentSiteComment;
use Modules\Assessment\Entities\AssessmentSiteEvidense;
use Modules\Core\Enums\AssessmentStage;
use Modules\Core\Helpers\BasicHelper;
use Modules\Core\Enums\AssessorAssessStage;

class Assessment extends Model implements AssessmentInterface
{

    use AssessmentTrait;

    protected $table = "assessments";
    
    protected $fillable = ["agency_id", "questionnaire_id", "application_id"];
    
    protected $appends = ['is_ongoing_assessment'];
    
    protected $hidden = [];

    public function application()
    {
        return $this->belongsTo(Application::class, 'application_id');
    }

    public function getIsOngoingAssessmentAttribute(){
        if($this->assessment_stage == AssessmentStage::ASSESS_INIT){
            $inProgressAssessor = $this->assessors()->whereIn('assessor_assess_status', 
            [
                AssessorAssessStage::ASSESSOR_ASSESS_INPROGRESS,
                AssessorAssessStage::ASSESSOR_ASSESS_COMPLETED
            ])->first();
            return !empty($inProgressAssessor) ? true : false;
        }else{
            return false;
        }
    }

    public function assessors()
    {
        return $this->hasMany(AssessmentAssessor::class, 'assessment_id');
    }

    public function questionnaire()
    {
        return $this->belongsTo(Questionnaire::class, 'questionnaire_id');
    }

    public function agency()
    {
        return $this->belongsTo(AgencyMeta::class, 'agency_id', 'user_id');
    }

    public function leadAssessor()
    {
        return $this->belongsTo(UserProfile::class, 'lead_assessor_id', 'user_id');
    }

    public function parameters()
    {
        return AssessmentParameter::where('assessment_id', $this->id)->get();
    }

    /**
     * Function to get assessmentSiteDetails
     */
    public function siteDetails(){
        $data = [
            'opening_meeting'=>[
                'opening_meeting_participants'=>[

                ],
                'opening_meeting_comments'=>[

                ],
                'opening_meeting_evidense'=>[

                ]
            ],
            'closing_meeting'=>[
                'closing_meeting_participants'=>[

                ],
                'closing_meeting_comments'=>[

                ],
                'closing_meeting_evidense'=>[

                ]
            ],
            'site_tour'=>[
                'site_tour_evidenses'=>[

                ]
            ]
                ];
        if(!empty($this->id)){
            $assessmentParticipants = AssessmentParticipantDetail::where('assessment_id', $this->id)->get();
            $assessmentComments = AssessmentSiteComment::where('assessment_id', $this->id)->get();
            $assessmentEvidenses = AssessmentSiteEvidense::where('assessment_id', $this->id)->get();
            // Assessment Particpant 
            if(!empty($assessmentParticipants)){
                foreach($assessmentParticipants as $participant){
                    if(!empty($participant->type) && $participant->type  =='open_meet'){
                        $data['opening_meeting']['opening_meeting_participants'][] = [
                            'id'=>!empty($participant->_id) ? $participant->_id : null,
                            'name'=>!empty($participant->name) ? $participant->name : null,
                            'designation'=>!empty($participant->designation) ? $participant->designation : null
                        ];
                    }elseif(!empty($participant->type) && $participant->type  =='close_meet'){
                        $data['closing_meeting']['closing_meeting_participants'][] = [
                            'id'=>!empty($participant->_id) ? $participant->_id : null,
                            'name'=>!empty($participant->name) ? $participant->name : null,
                            'designation'=>!empty($participant->designation) ? $participant->designation : null
                        ];
                    }
                }
            }
            // Assessment  comment
            if(!empty($assessmentComments)){
                foreach($assessmentComments as $comment){
                    if(!empty($comment->type) && $comment->type  =='open_meet'){
                        $data['opening_meeting']['opening_meeting_comments'][] = [
                            'comment_author'=>!empty($comment->comment_author) ? $comment->comment_author : null,
                            'last_updated_at'=>!empty($comment->comment_datetime) ? $comment->comment_datetime : null,
                            'comment'=>!empty($comment->comment) ? $comment->comment : null,
                            'id'=>!empty($comment->_id) ? $comment->_id : null,
                        ];
                    }elseif(!empty($comment->type) && $comment->type  =='close_meet'){
                        $data['closing_meeting']['closing_meeting_comments'][] = [
                            'comment_author'=>!empty($comment->comment_author) ? $comment->comment_author : null,
                            'last_updated_at'=>!empty($comment->comment_datetime) ? $comment->comment_datetime : null,
                            'comment'=>!empty($comment->comment) ? $comment->comment : null,
                            'id'=>!empty($comment->_id) ? $comment->_id : null,
                        ];
                    }
                }
            }
            // Assessment  Site Evidenses
            if(!empty($assessmentEvidenses)){
                foreach($assessmentEvidenses as $evidense){
                    $evidenseAuthor = !empty($evidense->user()) ? $evidense->user() : null;
                    $evidenseData = [
                        'evidenseAuthor'=>$evidenseAuthor->first_name." ".$evidenseAuthor->last_name,
                        'evidense_uuid'=>$evidense->evidense_uuid,
                        'latittude'=>!empty($evidense->latitude) ? $evidense->latitude : "",
                        'longitude'=>!empty($evidense->longitude) ? $evidense->longitude : "",
                        'caption'=>!empty($evidense->file_caption) ? $evidense->file_caption : "",
                        'file_date_time'=>!empty($evidense->file_date_time) ? BasicHelper::showDate($evidense->file_date_time, 'j M Y H:i') : "",
                        'file'=>!empty($evidense->file) ? $evidense->file : "",
                        'last_updated_at'=>!empty($evidense->updated_at) ? BasicHelper::showDate($evidense->updated_at, 'j M Y H:i')
                        : BasicHelper::showDate($evidense->created_at, 'j M Y H:i')
                    ]; 
                    if(!empty($evidense->type) && $evidense->type  =='open_meet'){
                        $data['opening_meeting']['opening_meeting_evidense'][] = $evidenseData;
                    }elseif(!empty($evidense->type) && $evidense->type  =='close_meet'){
                        $data['closing_meeting']['closing_meeting_evidense'][] = $evidenseData;
                    }elseif(!empty($evidense->type) && $evidense->type  =='site_tour'){
                        $data['site_tour']['site_tour_evidenses'][] = $evidenseData;
                    }
                }
            }
        }
        return $data;
    }
}
