<?php

namespace Modules\Assessment\Entities\Interfaces;

interface AssessmentInterface
{

    const ID = 'id';
    const APPLICATION_UUID = 'application_uuid';
    const QUESTIONNAIRE_ID = 'questionnaire_id';
    const AGENCY_ID = 'agency_id';
    const UUID = 'assessment_uuid';
    const ASSESSMENT_DATE = 'assessment_date';
    const STATUS = 'status';
    const LAST_MODIFIED_BY = 'last_modified_by';
    const DELETED_AT = 'deleted_at';
    const CREATED_DATETIME = 'created_at';
    const UPDATED_DATETIME = 'updated_at';

    /**
     * Getters
     */
    public function getID();

    public function getApplicationUUID();

    public function getQuestionnaireID();

    public function getAgencyID();

    public function getUUID();

    public function getAssessmentDate();

    public function getStatus();

    public function getLastModifiedBy();
    
    public function getCreatedAt();

    public function getDeletedAt();

    public function getUpdatedAt();
}
