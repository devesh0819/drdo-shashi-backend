<?php

namespace Modules\Assessment\Entities\Interfaces;

interface AssessmentParameterEvidenceInterface
{

    const ID = 'id';
    const PARAMETER_ID = 'parameter_id';
    const USER_ID = 'user_id';
    const FILE_TYPE = 'file_type';
    const FILE_SIZE = 'file_size';
    const MIME_TYPE = 'mime_type';
    const DISK = 'disk';
    const PATH = 'path';
    const HEIGHT = 'height';
    const WIDTH = 'width';
    const CREATED_DATETIME = 'created_at';
    const UPDATED_DATETIME = 'updated_at';

    /**
     * Getters
     */
    public function getID();

    public function getParameterID();

    public function getUserID();

    public function getFileType();

    public function getFileSize();
    
    public function getMimeType();
    
    public function getDisk();
    
    public function getPath();
    
    public function getHeight();

    public function getWidth();

    public function getCreatedAt();

    public function getUpdatedAt();

    public function getUrl();
}
