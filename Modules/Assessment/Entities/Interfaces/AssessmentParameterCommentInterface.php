<?php

namespace Modules\Assessment\Entities\Interfaces;

interface AssessmentParameterCommentInterface
{

    const ID = 'id';
    const PARAMETER_ID = 'parameter_id';
    const COMMENT = 'comment';
    const USER_ID = 'user_id';
    const CREATED_DATETIME = 'created_at';
    const UPDATED_DATETIME = 'updated_at';

    /**
     * Getters
     */
    public function getID();

    public function getParameterID();

    public function getComment();

    public function getUserID();

    public function getCreatedAt();

    public function getUpdatedAt();
}
