<?php

namespace Modules\Assessment\Entities\Interfaces;

interface AssessmentParameterInterface
{

    const ID = 'id';
    const APPLICATION_ID = 'application_id';
    const ASSESSMENT_ID = 'assessment_id';
    const ASSESSOR_ID = 'assessor_id';
    const DISCIPLINE = 'discipline';
    const PARAMETER = 'parameter';
    const PARAMETER_SLUG = 'parameter_slug';
    const PARAMETER_P_VAL = 'parameter_p_val';
    const PARAMETER_D_VAL = 'parameter_d_val';
    const PARAMETER_M_VAL = 'parameter_m_val';
    const PARAMETER_P_RESPONSE = 'parameter_p_response';
    const PARAMETER_D_RESPONSE = 'parameter_d_response';
    const PARAMETER_M_RESPONSE = 'parameter_m_response';
    const ASSESSOR_COMMENTS = 'assessor_comments';
    const PARAMETER_STATUS = 'parameter_status';
    const PARAMETER_DOCUMENTS = 'parameter_documents';
    const CREATED_DATETIME = 'created_at';
    const UPDATED_DATETIME = 'updated_at';

    /**
     * Getters
     */
    public function getID();

    public function getApplicationID();

    public function getAssessmentID();

    public function getAssessorID();

    public function getDiscipline();

    public function getParameter();

    public function getParameterSlug();

    public function getParameterPVal();

    public function getParameterDVal();

    public function getParameterMVal();

    public function getParameterPResponse();

    public function getParameterDResponse();

    public function getParameterMResponse();

    public function getAssessorComments();
    
    public function getParameterStatus();

    public function getParameterDocuments();

    public function getCreatedAt();

    public function getUpdatedAt();
}
