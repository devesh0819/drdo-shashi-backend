<?php

namespace Modules\Assessment\Entities;

use Jenssegers\Mongodb\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;
use Modules\Assessment\Entities\AssessmentParameter;
use Modules\Assessor\Entities\AssessorMeta;

class AssessmentParameterSection extends Model
{
    use SoftDeletes;
    protected $connection = 'mongodb';
    
    protected $collection = 'assessment_parameter_sections';
    
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'parameter_id', 'type', 'options', 'optional', 'response', 'section_uuid', 'section_slug'
    ];

    public function parameter()
    {
        return $this->belongsTo(AssessmentParameter::class, 'parameter_id');
    }
}