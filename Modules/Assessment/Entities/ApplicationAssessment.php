<?php

namespace Modules\Assessment\Entities;

use Jenssegers\Mongodb\Eloquent\Model;

class ApplicationAssessment extends Model
{
    protected $connection = 'mongodb';
    
    protected $collection = 'application_assessments';

    protected $fillable = [
        'application_id', 'assessment_id', 'discipline',
        'parameter', 'parameter_slug', 'parameter_p_val', 'parameter_d_val', 'parameter_m_val',
        'assessor_id', 'assessor_comments',
        'parameter_status', 'parameter_documents'];
}