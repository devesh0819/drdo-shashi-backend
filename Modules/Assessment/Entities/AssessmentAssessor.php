<?php

namespace Modules\Assessment\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\User\Entities\UserProfile;

class AssessmentAssessor extends Model 
{

    protected $table = "assessment_assessors";
    
    protected $fillable = [];
    
    protected $appends = [];
    
    protected $hidden = ['last_modified_by','created_at', 'updated_at'];
    
    protected $with = ['profile'];

    public function profile()
    {
        return $this->belongsTo(UserProfile::class,'assessor_id', 'user_id');
    }
   
}
