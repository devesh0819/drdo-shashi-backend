<?php

namespace Modules\Assessment\Entities\Traits;

trait AssessmentTrait
{

    /**
     * Getters
     */
    public function getID()
    {
        return !empty($this->{$this::ID}) ? $this->{$this::ID} : null;
    }

    public function getApplicationUUID()
    {
        return !empty($this->{$this::APPLICATION_UUID}) ? $this->{$this::APPLICATION_UUID} : null;
    }

    public function getQuestionnaireID()
    {
        return !empty($this->{$this::QUESTIONNAIRE_ID}) ? $this->{$this::QUESTIONNAIRE_ID} : null;
    }
    
    public function getAgencyID()
    {
        return !empty($this->{$this::AGENCY_ID}) ? $this->{$this::AGENCY_ID} : null;
    }

    public function getUUID()
    {
        return !empty($this->{$this::UUID}) ? $this->{$this::UUID} : null;
    }

    public function getAssessmentDate()
    {
        return !empty($this->{$this::ASSESSMENT_DATE}) ? $this->{$this::ASSESSMENT_DATE} : null;
    }

    public function getStatus()
    {
        return !empty($this->{$this::STATUS}) ? $this->{$this::STATUS} : null;
    }

    public function getLastModifiedBy()
    {
        return !empty($this->{$this::LAST_MODIFIED_BY}) ? $this->{$this::LAST_MODIFIED_BY} : null;
    }   

    public function getCreatedAt()
    {
        return !empty($this->{$this::CREATED_DATETIME}) ? date("Y-m-d H:i:s", strtotime($this->{$this::CREATED_DATETIME})) : null;
    }

    public function getUpdatedAt()
    {
        return !empty($this->{$this::UPDATED_DATETIME}) ? date("Y-m-d H:i:s", strtotime($this->{$this::UPDATED_DATETIME})) : null;
    }

    public function getDeletedAt()
    {
        return !empty($this->{$this::DELETED_AT}) ? date("Y-m-d H:i:s", strtotime($this->{$this::DELETED_AT})) : null;
    }

}
