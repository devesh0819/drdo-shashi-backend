<?php

namespace Modules\Assessment\Entities\Traits;

trait AssessmentParameterEvidenceTrait
{

     /**
     * Getters
     */
    public function getID()
    {
        return !empty($this->{$this::ID}) ? $this->{$this::ID} : null;
    }

    public function getParameterID()
    {
        return !empty($this->{$this::PARAMETER_ID}) ? $this->{$this::PARAMETER_ID} : null;
    }

    public function getUserID()
    {
        return !empty($this->{$this::USER_ID}) ? $this->{$this::USER_ID} : null;
    }
    
    public function getFileType()
    {
        return !empty($this->{$this::FILE_TYPE}) ? $this->{$this::FILE_TYPE} : null;
    }

    public function getFileSize()
    {
        return !empty($this->{$this::FILE_SIZE}) ? $this->{$this::FILE_SIZE} : null;
    }

    public function getMimeType()
    {
        return !empty($this->{$this::MIME_TYPE}) ? $this->{$this::MIME_TYPE} : null;
    }

    public function getDisk()
    {
        return !empty($this->{$this::DISK}) ? $this->{$this::DISK} : null;
    }

    public function getPath()
    {
        return !empty($this->{$this::PATH}) ? $this->{$this::PATH} : null;
    }

    public function getHeight()
    {
        return !empty($this->{$this::HEIGHT}) ? $this->{$this::HEIGHT} : null;
    }

    public function getWidth()
    {
        return !empty($this->{$this::WIDTH}) ? $this->{$this::WIDTH} : null;
    }

    public function getCreatedAt()
    {
        return !empty($this->{$this::CREATED_DATETIME}) ? date("Y-m-d H:i:s", strtotime($this->{$this::CREATED_DATETIME})) : null;
    }

    public function getUpdatedAt()
    {
        return !empty($this->{$this::UPDATED_DATETIME}) ? date("Y-m-d H:i:s", strtotime($this->{$this::UPDATED_DATETIME})) : null;
    }

    public function getUrl()
    {
        return !empty($this->{$this::PATH}) ? config('attachment.evidence_base_url').'storage/' . $this->{$this::PATH} : null;
    }
}
