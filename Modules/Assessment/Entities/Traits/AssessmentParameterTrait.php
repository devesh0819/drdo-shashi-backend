<?php

namespace Modules\Assessment\Entities\Traits;

trait AssessmentParameterTrait
{

    /**
     * Getters
     */
    public function getID()
    {
        return !empty($this->{$this::ID}) ? $this->{$this::ID} : null;
    }

    public function getApplicationID()
    {
        return !empty($this->{$this::APPLICATION_ID}) ? $this->{$this::APPLICATION_ID} : null;
    }

    public function getAssessmentID()
    {
        return !empty($this->{$this::ASSESSMENT_ID}) ? $this->{$this::ASSESSMENT_ID} : null;
    }
    
    public function getAssessorID()
    {
        return !empty($this->{$this::ASSESSOR_ID}) ? $this->{$this::ASSESSOR_ID} : null;
    }

    public function getDiscipline()
    {
        return !empty($this->{$this::DISCIPLINE}) ? $this->{$this::DISCIPLINE} : null;
    }

    public function getParameter()
    {
        return !empty($this->{$this::PARAMETER}) ? $this->{$this::PARAMETER} : null;
    }

    public function getParameterSlug()
    {
        return !empty($this->{$this::PARAMETER_SLUG}) ? $this->{$this::PARAMETER_SLUG} : null;
    }

    public function getParameterPVal()
    {
        return !empty($this->{$this::PARAMETER_P_VAL}) ? json_decode($this->{$this::PARAMETER_P_VAL}, true) : null;
    }   

    public function getParameterDVal()
    {
        return !empty($this->{$this::PARAMETER_D_VAL}) ? json_decode($this->{$this::PARAMETER_D_VAL}, true) : null;
    }

    public function getParameterMVal()
    {
        return !empty($this->{$this::PARAMETER_M_VAL}) ? json_decode($this->{$this::PARAMETER_M_VAL}, true) : null;
    }

    public function getParameterPResponse()
    {
        return !empty($this->{$this::PARAMETER_P_RESPONSE}) ? $this->{$this::PARAMETER_P_RESPONSE} : null;
    }

    public function getParameterDResponse()
    {
        return !empty($this->{$this::PARAMETER_D_RESPONSE}) ? $this->{$this::PARAMETER_D_RESPONSE} : null;
    }

    public function getParameterMResponse()
    {
        return !empty($this->{$this::PARAMETER_M_RESPONSE}) ? $this->{$this::PARAMETER_M_RESPONSE} : null;
    }

    public function getAssessorComments()
    {
        return !empty($this->{$this::ASSESSOR_COMMENTS}) ? $this->{$this::ASSESSOR_COMMENTS} : null;
    }

    public function getParameterStatus()
    {
        return !empty($this->{$this::PARAMETER_STATUS}) ? $this->{$this::PARAMETER_STATUS} : null;
    }


    public function getParameterDocuments()
    {
        return !empty($this->{$this::PARAMETER_DOCUMENTS}) ? $this->{$this::PARAMETER_DOCUMENTS} : null;
    }

    public function getCreatedAt()
    {
        return !empty($this->{$this::CREATED_DATETIME}) ? date("Y-m-d H:i:s", strtotime($this->{$this::CREATED_DATETIME})) : null;
    }

    public function getUpdatedAt()
    {
        return !empty($this->{$this::UPDATED_DATETIME}) ? date("Y-m-d H:i:s", strtotime($this->{$this::UPDATED_DATETIME})) : null;
    }
}
