<?php

namespace Modules\Application\Entities\Traits;

trait ApplicationFinancialTrait
{

    public function getId()
    {
        return !empty($this->{$this::ID}) ? $this->{$this::ID} : null;
    }
    
    public function getApplicationId()
    {
        return !empty($this->{$this::APPLICATION_ID}) ? $this->{$this::APPLICATION_ID} : null;
    }

    public function getEntrepreneurAadharNumber()
    {
        return !empty($this->{$this::ENTREPRENEUR_AADHAR_NUMBER}) ? $this->{$this::ENTREPRENEUR_AADHAR_NUMBER} : null;
    }

    public function getEntrepreneurGstNumber()
    {
        return !empty($this->{$this::ENTREPRENEUR_GST_NUMBER}) ? $this->{$this::ENTREPRENEUR_GST_NUMBER} : null;
    }

    public function getTanNumber()
    {
        return !empty($this->{$this::TAN_NUMBER}) ? $this->{$this::TAN_NUMBER} : null;
    }

    public function getFinancialDetailsConfirmed()
    {
        return !empty($this->{$this::FINANCIAL_DETAILS_CONFIRMED}) ? $this->{$this::FINANCIAL_DETAILS_CONFIRMED} : null;
    }

    public function getCreatedAt()
    {
        return !empty($this->{$this::CREATED_AT}) ? $this->{$this::CREATED_AT} : null;
    }

    public function getUpdatedAt()
    {
        return !empty($this->{$this::UPDATED_AT}) ? $this->{$this::UPDATED_AT} : null;
    }

    public function getLastModifiedBy()
    {
        return !empty($this->{$this::LAST_MODIFIED_BY}) ? $this->{$this::LAST_MODIFIED_BY} : null;
    }

    public function getDeletedAt()
    {
        return !empty($this->{$this::DELETED_AT}) ? $this->{$this::DELETED_AT} : null;
    }
}
