<?php

namespace Modules\Assessment\Entities\Traits;

trait AssessmentParameterCommentTrait
{

    /**
     * Getters
     */
    public function getID()
    {
        return !empty($this->{$this::ID}) ? $this->{$this::ID} : null;
    }

    public function getParameterID()
    {
        return !empty($this->{$this::PARAMETER_ID}) ? $this->{$this::PARAMETER_ID} : null;
    }

    public function getUserID()
    {
        return !empty($this->{$this::USER_ID}) ? $this->{$this::USER_ID} : null;
    }
    
    public function getComment()
    {
        return !empty($this->{$this::COMMENT}) ? $this->{$this::COMMENT} : null;
    }

    public function getCreatedAt()
    {
        return !empty($this->{$this::CREATED_DATETIME}) ? date("Y-m-d H:i:s", strtotime($this->{$this::CREATED_DATETIME})) : null;
    }

    public function getUpdatedAt()
    {
        return !empty($this->{$this::UPDATED_DATETIME}) ? date("Y-m-d H:i:s", strtotime($this->{$this::UPDATED_DATETIME})) : null;
    }
}
