<?php

namespace Modules\Assessment\Entities;

use Jenssegers\Mongodb\Eloquent\Model;
use Modules\Assessment\Entities\Assessment;
use Modules\User\Entities\UserProfile;

class AssessmentSiteEvidense extends Model
{
    protected $connection = 'mongodb';
    
    protected $collection = 'assessment_site_evidenses';
    
    protected $appends = ['file'];
    
    protected $fillable = [
        'application_id', 'assessment_id', 'type', 'user_id', 'file_type', 'file_size', 'mime_type', 'disk',
        'path', 'height', 'width', 'evidense_uuid', 'file_date_time','latitude','longitude', 'file_caption'
    ];

    public function assessment()
    {
        return Assessment::where('id', $this->assessment_id)->first();
    }

    public function user()
    {
        return UserProfile::where('user_id', $this->user_id)->first();
    }

    public function getFileAttribute()
    {
        $assetBaseURL = config('core.asset_base_url');   
        return $assetBaseURL.$this->evidense_uuid."?type=site_evidense";
       // return route('api.v1.core.show.file', $this->evidense_uuid)."?type=site_evidense";
    }
}