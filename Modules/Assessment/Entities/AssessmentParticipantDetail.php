<?php

namespace Modules\Assessment\Entities;

use Jenssegers\Mongodb\Eloquent\Model;
use Modules\Assessment\Entities\Assessment;

class AssessmentParticipantDetail extends Model
{
    protected $connection = 'mongodb';
    
    protected $collection = 'assessment_participants_details';
    
    protected $appends = [];

    protected $fillable = [
        'application_id', 'assessment_id', 'type', 'name', 'designation'
    ];

    public function assessment()
    {
        return Assessment::where('id', $this->assessment_id)->first();
    }
}