<?php

namespace Modules\Assessment\Entities;

use Jenssegers\Mongodb\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;
use Modules\Assessment\Entities\Interfaces\AssessmentParameterCommentInterface;
use Modules\Assessment\Entities\Traits\AssessmentParameterCommentTrait;
use Modules\User\Entities\UserProfile;

class AssessmentParameterComment extends Model implements AssessmentParameterCommentInterface
{
    use SoftDeletes;
    use AssessmentParameterCommentTrait;
    
    protected $connection = 'mongodb';
    
    protected $collection = 'assessment_parameter_comments';

    protected $fillable = [
        'parameter_id', 'comment', 'user_id', 'comment_uuid', 'comment_type'
    ];
    
    protected $hidden = [];
    
    protected $appends = ['comment_author'];

    public function user()
    {
        return UserProfile::where('user_id', $this->user_id)->first();
    }

    public function getCommentAuthorAttribute()
    {
        return !empty($this->user()) ? $this->user()->first_name." ".$this->user()->last_name : null;
    }
}