<?php

namespace Modules\Assessment\Entities;

use Jenssegers\Mongodb\Eloquent\Model;
use Modules\Assessment\Entities\Assessment;

class AssessmentSiteComment extends Model
{
    protected $connection = 'mongodb';
    
    protected $collection = 'assessment_site_comments';
    
    protected $appends = [];

    protected $fillable = [
        'application_id', 'assessment_id', 'type', 'comment_author', 'comment','comment_datetime'
    ];

    public function assessment()
    {
        return Assessment::where('id', $this->assessment_id)->first();
    }
}