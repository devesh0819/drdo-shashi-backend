<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('assessments')) {
            Schema::create('assessments', function (Blueprint $table) {
                $table->id();
                $table->unsignedInteger('application_id')->unique();
                $table->string('assessment_uuid', 255)->nullable();
                $table->string('assessment_number',255)->unique();
                $table->float('assessment_score')->nullable();
                $table->enum('certification_type', ['bronze', 'silver', 'gold', 'diamond', 'platinum'])->nullable();
                $table->dateTime('certification_expiry')->nullable();
                $table->enum('assessment_stage', 
                [
                    'assess_started',
                    'asses_init',
                    'asses_submit',
                    'asses_reopen',
                    'asses_complete',
                    'asses_feed_sub'
                ])->default('assess_started');
                $table->unsignedInteger('questionnaire_id')->nullable();
                $table->unsignedInteger('agency_id')->nullable();
                $table->unsignedInteger('lead_assessor_id')->nullable();
                $table->timestamp('assessment_date')->nullable();
                $table->timestamp('assessment_review_date')->nullable();
                $table->timestamp('assessment_start_date')->nullable();
                $table->timestamp('assessment_submit_date')->nullable();
                $table->tinyInteger('status')->default(1);
                $table->unsignedInteger('last_modified_by')->nullable();
                $table->softDeletes();
                $table->timestamp('created_at')->useCurrent();
                $table->timestamp('updated_at')->nullable();
            });
    }
}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assessments');
    }
};
