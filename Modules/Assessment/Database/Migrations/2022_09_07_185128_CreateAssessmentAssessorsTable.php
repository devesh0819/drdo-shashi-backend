<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('assessment_assessors')) {
            Schema::create('assessment_assessors', function (Blueprint $table) {
                $table->id();
                $table->unsignedInteger('assessment_id');
                $table->unsignedInteger('assessor_id');
                $table->enum('assessor_assess_status', ['todo', 'inprogress', 'completed','reopened'])->default('ToDo');
                $table->unsignedInteger('last_modified_by')->nullable();
                $table->timestamp('created_at')->useCurrent();
                $table->timestamp('updated_at')->nullable();
            });
    }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assessment_assessors');
    }
};
