<?php

namespace Modules\Assessment\Repositories\Implementations;

use Modules\Assessment\Repositories\Interfaces\AssessmentParameterInterface;
use Modules\Assessment\Entities\Assessment;
use Illuminate\Support\Str;
use Modules\Assessment\Entities\AssessmentParameter;
use Modules\Assessment\Entities\AssessmentAssessor;
use Modules\Questionnaire\Entities\Questionnaire;
use Exception;
use GuzzleHttp\Psr7\UploadedFile;
use Modules\Assessment\Entities\AssessmentParameterComment;
use Modules\Assessment\Entities\AssessmentParameterEvidence;
use Modules\Core\Helpers\BasicHelper;
use Modules\Assessment\Entities\AssessmentParameterSection;

class AssessmentParameterRepository implements AssessmentParameterInterface
{
    private $limit = 10;

    /**
     * Getter
     * @param type $property
     * @return type
     */
    public function __get($property)
    {
        return $this->$property;
    }

    /**
     * Setter
     * @param type $property
     * @param type $value
     * @return $this
     */
    public function __set($property, $value)
    {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
        return $this;
    }

    /**
     * Function to get assessments listing
     * @param ARRAY $filters
     * @param BOOL  $countOnly
     */
    public function lists(array $filters, $countOnly=false)
    {
        $parameters = AssessmentParameter::with('comments', 'evidenses')->where(function($que) use($filters){
            if(!empty($filters['assessment_id'])){
                $que->where('assessment_id', (int)$filters['assessment_id']);
            }
            if(!empty($filters['assessor_id'])){
                $que->where('assessor_id', (int)$filters['assessor_id']);
            }
        });
        if($countOnly){
            return $parameters->count();
        } else {
            return $parameters->orderBy("created_at", "asc")
            ->get();
        }
    }

    /**
     * Fnctionn to sync all assessor Data
     */
    public function syncAssessmentData($inputs)
    {
        return;
    }

    /**
     * Function to get user details by id
     * @param type $id
     * @return User $user
     */
    public function firstornew($id = null)
    {
        if (!empty($id)) {
            $parameter = AssessmentParameter::where('_id', $id)->first();
            $parameter->load('sections');
            $parameter->load('comments');
            $parameter->load('evidenses');
            return $parameter;
        }
        return new AssessmentParameter();
    }

    /**
     * Function to get paramters details by id
     * @param type $id
     */
    public function findById($ids = [])
    {
        if (!empty($ids)) {
            $parameters = AssessmentParameter::whereIn('_id', $ids)->get();
            return $parameters;
        }
        return [];
    }

     /**
     * Function to get section details by id
     * @param type $id
     */
    public function getSection($id = null)
    {
        if (!empty($id)) {
            return AssessmentParameterSection::where('section_uuid', $id)->first();
        }
        return new AssessmentParameterSection();
    }

    /**
     * Function to assign assessor to paremeter
     * @param AssessmentParameter $parameterID
     * @param ARRAY $data
     */
    public function assignParametersAssessor(AssessmentParameter $parameter, $data=[])
    {
        $parameter->assessor_id = $data['assessor_id'];
        $parameter->save();
        return $parameter;
    }

    /**
     * Function to submit response to paremeter
     * @param AssessmentParameter $parameterID
     * @param ARRAY $data
     */
    public function submitSectionResponse(AssessmentParameterSection $section, $data=[])
    {
        $section->response = !empty($data['response']) ? $data['response'] : "";
        // Update Parameter Score
        if(!empty($section->save())){
            $score = BasicHelper::calculateParameterScore($section->parameter);
            $section->parameter->parameter_score = $score;
            $section->parameter->save();
        }
        return $section;
    }
    

    /**
     * Function to submit comment to paremeter
     * @param AssessmentParameter $parameterID
     * @param ARRAY $data
     */
    public function submitParametersComment(AssessmentParameter $parameter, $data=[])
    {
        if(!empty($data['comment_uuid'])){
            $comment = AssessmentParameterComment::where([
                'user_id'=>$data['user_id'],
                'comment_uuid'=>$data['comment_uuid']
            ])->firstornew();
            $comment->update([
                'comment'=>$data['comment'],
                'comment_type'=>!empty($data['comment_type']) ? $data['comment_type'] : 'default'
            ], ['upsert' => true]);
        }else{
            $comment = new AssessmentParameterComment(
                [
                    'comment_uuid'=>!empty($data['comment_uuid']) ? $data['comment_uuid'] :  (string) Str::orderedUuid(),
                    'user_id' => $data['user_id'],
                    'comment' => $data['comment'],
                    'comment_type'=> !empty($data['comment_type']) ? $data['comment_type'] : 'default'
                ]
            );
        }
        return $parameter->comments()->save($comment);
    }

    /**
     * Function to get all comment to paremeter
     * @param AssessmentParameter $parameterID
     * @param ARRAY $data
     */
    public function getParameterComments(AssessmentParameter $parameter)
    {
        return $parameter->comments;
    }

    /**
     * Function to submit evidence to paremeter
     * @param AssessmentParameter $parameterID
     * @param ARRAY $data
     */
    public function submitParametersEvidence(AssessmentParameter $parameter, $data=[])
    {
        if(!empty($data['evidense_uuid'])){
            $attachment = AssessmentParameterEvidence::where([
                'user_id'=>$data['user_id'],
                'evidense_uuid'=>$data['evidense_uuid']
            ])->firstornew();
            $attachment->update([
                'file_size'=>!empty($data['uploadedFile']) && !empty($data['uploadedFile']->getMaxFilesize()) 
                ? $data['uploadedFile']->getMaxFilesize() : "",
                'mime_type'=>!empty($data['uploadedFile']) && !empty($data['uploadedFile']->getClientMimeType()) 
                ? $data['uploadedFile']->getClientMimeType() : $data['mime_type'],
                'disk'=>$data['disk'],
                'path'=>$data['uploadPath']
            ], ['upsert' => true]);
        }else{
            $fileDateTime = !empty($data['file_date_time']) 
            && ($data['file_date_time'] >= strtotime($parameter->assessment()->assessment_start_date))
            && ($data['file_date_time'] <= time()) ? $data['file_date_time'] : time();

            $attachment = new AssessmentParameterEvidence(
                [
                    'user_id' => $data['user_id'],
                    'evidense_uuid'=>!empty($data['evidense_uuid']) ? $data['evidense_uuid'] :  (string) Str::orderedUuid(),
                    'file_size' => !empty($data['uploadedFile']) && !empty($data['uploadedFile']->getMaxFilesize()) ? $data['uploadedFile']->getMaxFilesize() : "",
                    'mime_type' => !empty($data['uploadedFile']) && !empty($data['uploadedFile']->getClientMimeType()) ? $data['uploadedFile']->getClientMimeType() : $data['mime_type'],
                    'latitude'=>!empty($data['latitude']) ? $data['latitude'] : "",
                    'longitude'=>!empty($data['longitude']) ? $data['longitude'] : "",
                    'file_date_time'=>$fileDateTime,
                    'disk' => $data['disk'],
                    'path' => $data['uploadPath']
                ]
            );
        }
        return $parameter->evidenses()->save($attachment);
    }

     /**
     * Function to get all evidences to paremeter
     * @param AssessmentParameter $parameterID
     * @param ARRAY $data
     */
    public function getParameterEvidences(AssessmentParameter $parameter)
    {
        return $parameter->evidenses;
    }
}
