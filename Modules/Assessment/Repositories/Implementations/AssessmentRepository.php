<?php

namespace Modules\Assessment\Repositories\Implementations;

use Modules\Assessment\Repositories\Interfaces\AssessmentInterface;
use Modules\Assessment\Entities\Assessment;
use Illuminate\Support\Str;
use Modules\Assessment\Entities\AssessmentAssessor;
use Modules\Questionnaire\Entities\Questionnaire;
use Exception;
use Modules\Agency\Entities\AgencyMeta;
use Modules\Application\Entities\Application;
use Modules\Assessment\Entities\AssessmentParameter;
use Modules\Assessment\Entities\AssessmentParameterSection;
use Modules\Core\Entities\AssessmentPlan;
use Modules\Core\Enums\AssessmentStage;
use Modules\Core\Helpers\BasicHelper;
use Modules\Core\Enums\Flag;
use Modules\Assessment\Events\AssessmentCompleted;
use Modules\Core\Enums\ApplicationStage;
use Modules\Core\Enums\UserRole;
use Modules\User\Entities\UserProfile;
use Modules\Assessment\Entities\AssessmentParticipantDetail;
use Modules\Assessment\Entities\AssessmentSiteComment;
use Modules\Assessment\Entities\AssessmentSiteEvidense;
use Modules\User\Entities\User;
use Modules\Core\Enums\AssessorAssessStage;

class AssessmentRepository implements AssessmentInterface
{
    private $limit = 10;

    /**
     * Getter
     * @param type $property
     * @return type
     */
    public function __get($property)
    {
        return $this->$property;
    }

    /**
     * Setter
     * @param type $property
     * @param type $value
     * @return $this
     */
    public function __set($property, $value)
    {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
        return $this;
    }

    /**
     * Function to get assessments listing
     * @param ARRAY $filters
     * @param BOOL  $countOnly
     */
    public function lists(array $filters, $countOnly=false)
    {
        $assessorIdArray = [];
        if(!empty($filters['assessor_name'])){
            $assessorIdArray = UserProfile::join('model_has_roles', function ($roleJoin) use ($filters) {
                $roleJoin->on('user_profiles.user_id', '=', 'model_has_roles.model_id')->where("model_has_roles.role_id", Flag::ASSESSOR);
            })->where('first_name', 'like', '%' . $filters['assessor_name'] . '%')
            ->orWhere('last_name', 'like', '%' . $filters['assessor_name'] . '%')->limit(10)->pluck('user_id')->toArray();
        }
        $assessments = Assessment::with(['application', 'assessors'])
            ->where(function($que) use($filters, $assessorIdArray){
                if(!empty($filters['agency_id'])){
                    $que->where('agency_id', $filters['agency_id']);
                }
                if(!empty($filters['assessment_status'])){
                    $que->where('assessment_stage', $filters['assessment_status']);
                }
                if(!empty($filters['certified'])){
                    $que->whereIn('assessment_stage', [AssessmentStage::ASSESS_COMPLETE, AssessmentStage::ASSESS_FEEDBACK_SUBMIT]);
                }
                if(!empty($filters['type_of_enterprise']) || isset($filters['feedback_received']) || !empty($filters['application_start_date']) || !empty($filters['application_end_date']) || !empty($filters['assessment_start_date']) || !empty($filters['assessment_end_date'])
                    ){
                    $que->whereHas('application', function($q) use ($filters) {
                        if(!empty($filters['type_of_enterprise'])){
                            $q->where('enterprice_type', $filters['type_of_enterprise']);
                        }
                        
                        if(!empty($filters['application_start_date'])){
                            $q->whereDate('created_at', '>=',  date("Y-m-d", strtotime($filters['application_start_date'])));
                        }
                
                        if(!empty($filters['application_end_date'])){
                            $q->whereDate('created_at', '<=',  date("Y-m-d", strtotime($filters['application_end_date'])));
                        }
                
                        if(!empty($filters['assessment_start_date'])){
                            $q->whereDate('assessment_date', '>=',  date("Y-m-d", strtotime($filters['assessment_start_date'])));
                        }
                
                        if(!empty($filters['assessment_end_date'])){
                            $q->whereDate('assessment_date', '<=',  date("Y-m-d", strtotime($filters['assessment_end_date'])));
                        }
                        if(!empty($filters['feedback_received'])){
                            $q->where('application_stage', ApplicationStage::APP_FEEDBACK_COMPLETE);
                        }elseif(isset($filters['feedback_received']) && empty($filters['feedback_received'])){
                            $q->where('application_stage', '!=', ApplicationStage::APP_FEEDBACK_COMPLETE);
                        }
                    });
                }

                if(!empty($assessorIdArray)){
                    $que->whereHas('assessors', function($q) use ($assessorIdArray) {
                        $q->whereIn('assessor_id', $assessorIdArray);
                    });
                }

                if(!empty($filters['assessor_id'])){
                    if(!empty($filters['assessor_sync'])){
                        $que->whereHas('assessors', function($q) use ($filters) {
                            $q->where('assessor_id', $filters['assessor_id']);
                            $q->where('assessment_stage', AssessmentStage::ASSESS_INIT);
                        });
                    }else{
                        if(!empty($filters['lead_assess_portal'])){
                            $que->where('lead_assessor_id', $filters['assessor_id']);
                        }else{
                            $que->where('lead_assessor_id', $filters['assessor_id']);
                            $que->orWhereHas('assessors', function($q) use ($filters) {
                                $q->where('assessor_id', $filters['assessor_id']);
                                $q->where('assessment_stage', AssessmentStage::ASSESS_INIT);
                            });
                        }
                    }
                }                
            });
        if($countOnly){
            return $assessments->count();
        } elseif(!empty(request()->get('is_excel'))){
            return $assessments->get();
        }else {
            $offset = !empty($filters['start']) ? ($filters['start']-1)*$this->limit : 0;
            return $assessments->orderBy("created_at", "desc")
            ->limit(!empty($filters['length']) ? $filters['length'] : $this->limit)
            ->offset($offset)
            ->get();
        }
    }

    /**
     * Function to find all assessment Stats
     * @param Array $filters
     * @return Array $data
     */
    public function getAssessment($filters=[], $countOnly = false)
    {
        $offset = !empty($filters['start']) ? ($filters['start']-1)*$this->limit : 0;
        $assessment= Assessment::groupBy('assessment_stage')
        ->selectRaw('assessment_stage, count(id) as total')
        ->limit(!empty($filters['length']) ? $filters['length'] : $this->limit)
        ->offset($offset)
        ->get();
        return $assessment;
    }

    /**
     * FUnction to show assessments details based on assessmentUID
     * @param STRING $assessmentId
     * @param Array $filters
     */
    public function show($assessmentId, array $filters)
    {
        
    }

    /**
     * Function to save assessment
     * @param Assessment $assessment
     * @param array $data
     * @return Assessment $assessment
     */
    public function save(Assessment $assessment, array $data)
    {
        $newRecord = false;
        if(empty($assessment->id)){
            $newRecord = true;
            $assessment->application_id = $data['application_id'];
            $assessment->assessment_uuid = (string) Str::orderedUuid();
            $assessment->assessment_number = BasicHelper::getUniqueAssessmentNumber();
            $assessment->assessment_stage = AssessmentStage::ASSESS_STARTED;

            if(!empty($data['application']) && $data['application'] instanceof Application){
                $questionnaire = Questionnaire::where(function($que) use($data){
                    if($data['application']->enterprice_type == 'SM' || $data['application']->enterprice_type == 'MIC' || $data['application']->enterprice_type == 'MED'){
                        $que->where('type', 'S&M');
                    }else{
                        $que->where('type', 'L');
                    }
                })->where('is_approved', Flag::IS_ACTIVE)
                ->orderBy('version', 'desc')
                ->latest()
                ->first();
                if(!empty($questionnaire)){
                    $assessment->questionnaire_id = $questionnaire->id;
                }
                $assessment->assessment_date = $data['application']->assessment_date;
                $agency = AgencyMeta::latest()->first();
                if(!empty($agency)){
                    $assessment->agency_id = $agency->user_id;
                }
            }
            
        }
        
        if(!empty($data['questionnaire_id'])){
            $assessment->questionnaire_id = $data['questionnaire_id']; 
        }

        $assessment->assessment_date = $assessment->application->assessment_date; 
        
        $assessment->fill($data);
        $assessment->save();
        
        if(!empty($data['assessor_ids'])){
            $syncAssessors = $this->syncAssessmentAssessors($assessment, $data['assessor_ids']);
            if(!empty($syncAssessors) && is_array($syncAssessors) && in_array($data['lead_assessor_id'], $syncAssessors)){
                $assessment->lead_assessor_id = $data['lead_assessor_id'];
                $assessment->save();
            }
        }
        if($newRecord){
            $this->cloneAssessment($assessment);
        }
        return $assessment;
    }

    /**
     * Function to sync assessors of a assessments
     */
    private function syncAssessmentAssessors(Assessment $assessment, array $newAssessorIds)
    {
        try{
            $getExistingAssessments = AssessmentAssessor::where('assessment_id', $assessment->id)->get()->pluck('assessor_id')->toArray();
            $assessorsToAdd =  array_diff($newAssessorIds, $getExistingAssessments);
            $assessorsToRemove = array_diff($getExistingAssessments, $newAssessorIds);
            // Remove Old Assessors
            AssessmentAssessor::whereIn('assessor_id', $assessorsToRemove)
            ->where('assessment_id', $assessment->id)
            ->delete();
            // Add New Assessors
            if(!empty($assessorsToAdd)){
                foreach($assessorsToAdd as $assessorId){
                    $assessmentAssessor = new AssessmentAssessor();
                    $assessmentAssessor->assessment_id = $assessment->id;
                    $assessmentAssessor->assessor_id = $assessorId;
                    $assessmentAssessor->save();
                }
            }
            return $newAssessorIds;
        }catch(Exception $ex){
            return false;
        }
    }
    /**
     * Function to get user details by id
     * @param type $id
     * @return User $user
     */
    public function firstornew($id = null)
    {
        if (!empty($id)) {
            return Assessment::where('assessment_uuid', $id)->first();
        }
        return new Assessment();
    }

    /**
    * Function to clone assessment for Assessor
    */
    public function cloneAssessment(Assessment $assessment)
    {
        if(!empty($assessment->getQuestionnaireID())){
            $questionnaireId = $assessment->getQuestionnaireID();
            $questionnaire = Questionnaire::with(['disciplines', 'disciplines.parameters'])
                ->where('id', $questionnaireId)->first();
               
            if(!empty($questionnaire)){
                if(!empty($questionnaire->disciplines)){
                    foreach($questionnaire->disciplines as $discipline){
                        if(!empty($discipline->parameters)){
                            foreach($discipline->parameters as $parameter){
                                $paramSlug = Str::slug($parameter->title);
                                $applicantAssessment = new AssessmentParameter();
                                $checkApplicationAssessment = AssessmentParameter::where(
                                    [
                                        'parameter_slug' => $paramSlug,
                                        'application_id'=>$assessment->application_id
                                    ])->first();
                                if(!empty($checkApplicationAssessment)){
                                    $applicantAssessment = $checkApplicationAssessment;
                                }else{
                                    $applicantAssessment = new AssessmentParameter();
                                    $applicantAssessment->parameter_slug = $paramSlug;
                                }
                                
                                $applicantAssessment->application_id = $assessment->application_id;
                                $applicantAssessment->assessment_id = $assessment->id;
                                $applicantAssessment->discipline = $discipline->title;
                                $applicantAssessment->parameter = $parameter->title;
                                $applicantAssessment->parameter_status = 'NA';
                                $applicantAssessment->assessor_id = '';
                                $applicantAssessment->assessor_comments = '';
                                $applicantAssessment->parameter_documents = '';
                                $applicantAssessment->save();
                                $parameter->load('sections');
                                if(!empty($applicantAssessment) && !empty($parameter->sections)){
                                    $sectionsData = [];
                                    foreach($parameter->sections as $key=>$section){
                                        $sectionSlug = Str::slug('param-'.$applicantAssessment->id.'-section-'.$key);
                                        $sections = AssessmentParameterSection::where(
                                            [
                                                'parameter_id'=>$applicantAssessment->id,
                                                'section_slug'=>$sectionSlug
                                            ]
                                        )->firstornew();
                                        $sections->section_uuid = (string) Str::orderedUuid();
                                        $sections->parameter_id = $applicantAssessment->id;
                                        $sections->type = $section->type;
                                        $sections->options = $section->options;
                                        $sections->optional = Flag::IS_INACTIVE;
                                        $sections->response = '';
                                        
                                        $applicantAssessment->sections()->save($sections);
                                    }
                                }
                            }
                        }        
                    }
                }
            }    
        }
    }

   
     /* Function to delete assessment
     * @param Assessment $assessment
     * @param array $data
     */
    public function delete(Assessment $assessment)
    {
        return $assessment->delete();
    }

    /* 
     * Function to start a assessment
     * @param Assessment $assessment
     * @param array $data
     */
    public function startAssessment(Assessment $assessment)
    {
        $assessment->assessment_stage = AssessmentStage::ASSESS_INIT;
        $assessment->assessment_start_date = date("Y-m-d H:i:s");
        return $assessment->save();
    }

    /* 
     * Function to open a assessment
     * @param Assessment $assessment
     * @param array $data
     */
    public function openAssessment(Assessment $assessment, User $user)
    {
        $assessmentAssessor = AssessmentAssessor::where([
            'assessor_id'=> $user->id,
            'assessment_id'=> $assessment->id,
            'assessor_assess_status'=>AssessorAssessStage::ASSESSOR_ASSESS_TODO
        ])->first();
        if(!empty($assessmentAssessor)){
            $assessmentAssessor->assessor_assess_status = AssessorAssessStage::ASSESSOR_ASSESS_INPROGRESS;
            return $assessmentAssessor->save();
        }
        return false;
    }

    /* 
     * Function to submit a assessment
     * @param Assessment $assessment
     * @param array $data
     */
    public function submitAssessment(Assessment $assessment)
    {
        $assessment->assessment_stage = AssessmentStage::ASSESS_SUBMIT;
        $parameters = AssessmentParameter::where('assessment_id', $assessment->id)->pluck('parameter_score')->toArray();
        $assessmentScore = !empty($parameters) ? array_sum($parameters)/count($parameters) : 0;
        $assessment->assessment_score = $assessmentScore;    
        $assessment->assessment_submit_date = date("Y-m-d H:i:s");
        
        return $assessment->save();
    }

    /* 
     * Function to complete a assessment
     * @param Assessment $assessment
     * @param array $data
     */
    public function completeAssessment(Assessment $assessment)
    {
        $assessment->assessment_stage = AssessmentStage::ASSESS_COMPLETE;
        if($assessment->save()){
            AssessmentCompleted::dispatch($assessment);
        }
        return true;
    }

    /**
     * Function to get assessmentSiteDetails
     */
    public function getSiteDetails($assessmentId, $filters=[]){
        $data = [
            'opening_meeting'=>[
                'opening_meeting_participants'=>[

                ],
                'opening_meeting_comments'=>[

                ],
                'opening_meeting_evidense'=>[

                ]
            ],
            'closing_meeting'=>[
                'closing_meeting_participants'=>[

                ],
                'closing_meeting_comments'=>[

                ],
                'closing_meeting_evidense'=>[

                ]
            ],
            'site_tour'=>[
                'site_tour_evidenses'=>[

                ]
            ]
                ];
        if(!empty($assessmentId)){
            $assessmentParticipants = AssessmentParticipantDetail::where('assessment_id', $assessmentId)->get();
            $assessmentComments = AssessmentSiteComment::where('assessment_id', $assessmentId)->get();
            if(!empty($filters['assessor_sync']) && !empty($filters['assessor_id'])){
                $assessmentEvidenses = AssessmentSiteEvidense::where('assessment_id', $assessmentId)
                ->where('user_id', $filters['assessor_id'])
                ->get();
            }else{
                $assessmentEvidenses = AssessmentSiteEvidense::where('assessment_id', $assessmentId)->get();
            }
            
            $assessmentEvidensesCount = $assessmentEvidenses->toArray();
            // Assessment Particpant 
            if(!empty($assessmentParticipants)){
                foreach($assessmentParticipants as $participant){
                    if(!empty($participant->type) && $participant->type  =='open_meet'){
                        $data['opening_meeting']['opening_meeting_participants'][] = [
                            'id'=>!empty($participant->_id) ? $participant->_id : null,
                            'name'=>!empty($participant->name) ? $participant->name : null,
                            'designation'=>!empty($participant->designation) ? $participant->designation : null
                        ];
                    }elseif(!empty($participant->type) && $participant->type  =='close_meet'){
                        $data['closing_meeting']['closing_meeting_participants'][] = [
                            'id'=>!empty($participant->_id) ? $participant->_id : null,
                            'name'=>!empty($participant->name) ? $participant->name : null,
                            'designation'=>!empty($participant->designation) ? $participant->designation : null
                        ];
                    }
                }
            }
            // Assessment  comment
            if(!empty($assessmentComments)){
                foreach($assessmentComments as $comment){
                    if(!empty($comment->type) && $comment->type  =='open_meet'){
                        $data['opening_meeting']['opening_meeting_comments'][] = [
                            'comment_author'=>!empty($comment->comment_author) ? $comment->comment_author : null,
                            'last_updated_at'=>!empty($comment->comment_datetime) ? $comment->comment_datetime : null,
                            'comment'=>!empty($comment->comment) ? $comment->comment : null,
                            'id'=>!empty($comment->_id) ? $comment->_id : null,
                        ];
                    }elseif(!empty($comment->type) && $comment->type  =='close_meet'){
                        $data['closing_meeting']['closing_meeting_comments'][] = [
                            'comment_author'=>!empty($comment->comment_author) ? $comment->comment_author : null,
                            'last_updated_at'=>!empty($comment->comment_datetime) ? $comment->comment_datetime : null,
                            'comment'=>!empty($comment->comment) ? $comment->comment : null,
                            'id'=>!empty($comment->_id) ? $comment->_id : null,
                        ];
                    }
                }
            }
            // Assessment  Site Evidenses
            if(!empty($assessmentEvidensesCount)){
                foreach($assessmentEvidenses as $evidense){
                    $evidenseAuthor = !empty($evidense->user()) ? $evidense->user() : null;
                    $evidenseData = [
                        'evidenseAuthor'=>$evidenseAuthor->first_name." ".$evidenseAuthor->last_name,
                        'evidense_uuid'=>$evidense->evidense_uuid,
                        'latittude'=>!empty($evidense->latitude) ? $evidense->latitude : "",
                        'longitude'=>!empty($evidense->longitude) ? $evidense->longitude : "",
                        'caption'=>!empty($evidense->file_caption) ? $evidense->file_caption : "",
                        'file_date_time'=>!empty($evidense->file_date_time) ? BasicHelper::showDate($evidense->file_date_time, 'j M Y H:i') : "",
                        'file'=>!empty($evidense->path) ? $evidense->path : "",
                        'last_updated_at'=>!empty($evidense->updated_at) ? BasicHelper::showDate($evidense->updated_at, 'j M Y H:i')
                        : BasicHelper::showDate($evidense->created_at, 'j M Y H:i')
                    ]; 
                    if(!empty($evidense->type) && $evidense->type  =='open_meet'){
                        $data['opening_meeting']['opening_meeting_evidense'][] = $evidenseData;
                    }elseif(!empty($evidense->type) && $evidense->type  =='close_meet'){
                        $data['closing_meeting']['closing_meeting_evidense'][] = $evidenseData;
                    }elseif(!empty($evidense->type) && $evidense->type  =='site_tour'){
                        $data['site_tour']['site_tour_evidenses'][] = $evidenseData;
                    }
                }
            }
        }
        return $data;
    }

    /**
     * Function to get all evidences to assessment
     * @param Assessment $assessment
     * @param ARRAY $data
     */
    public function getSiteEvidences(Assessment $assessment)
    {
        return AssessmentSiteEvidense::where('assessment_id', $assessment->id)->get();
    }

    /**
     * Function to submit evidence to assessment
     * @param Assessment $assessment
     * @param ARRAY $data
     */
    public function submitSiteEvidence(Assessment $assessment, $data=[])
    {
        if(!empty($data['evidense_uuid'])){
            $attachment = AssessmentSiteEvidense::where([
                'user_id'=>$data['user_id'],
                'evidense_uuid'=>$data['evidense_uuid']
            ])->firstornew();
            $attachment->update([
                'file_size'=>!empty($data['uploadedFile']) && !empty($data['uploadedFile']->getMaxFilesize()) 
                ? $data['uploadedFile']->getMaxFilesize() : "",
                'mime_type'=>!empty($data['uploadedFile']) && !empty($data['uploadedFile']->getClientMimeType()) 
                ? $data['uploadedFile']->getClientMimeType() : $data['mime_type'],
                'disk'=>$data['disk'],
                'path'=>$data['uploadPath']
            ], ['upsert' => true]);
        }else{
            $fileDateTime = !empty($data['file_date_time']) 
            && ($data['file_date_time'] >= strtotime($assessment->assessment_start_date))
            && ($data['file_date_time'] <= time()) ? $data['file_date_time'] : time();

            $attachment = new AssessmentSiteEvidense(
                [
                    'application_id'=>$assessment->application->id,
                    'assessment_id'=>$assessment->id,
                    'user_id' => $data['user_id'],
                    'evidense_uuid'=>!empty($data['evidense_uuid']) ? $data['evidense_uuid'] :  (string) Str::orderedUuid(),
                    'file_size' => !empty($data['uploadedFile']) && !empty($data['uploadedFile']->getMaxFilesize()) ? $data['uploadedFile']->getMaxFilesize() : "",
                    'mime_type' => !empty($data['uploadedFile']) && !empty($data['uploadedFile']->getClientMimeType()) ? $data['uploadedFile']->getClientMimeType() : $data['mime_type'],
                    'file_caption'=>!empty($data['file_caption']) ? $data['file_caption'] : "",
                    'type'=>!empty($data['evidense_type']) ? $data['evidense_type'] : "",
                    'latitude'=>!empty($data['latitude']) ? $data['latitude'] : "",
                    'longitude'=>!empty($data['longitude']) ? $data['longitude'] : "",
                    'file_date_time'=>$fileDateTime,
                    'disk' => $data['disk'],
                    'path' => $data['uploadPath']
                ]
            );
        }
        return $attachment->save();   
    }
}
