<?php

namespace Modules\Assessment\Repositories\Interfaces;

use Modules\Assessment\Entities\AssessmentParameter;
use Modules\Assessment\Entities\AssessmentParameterSection;

interface AssessmentParameterInterface
{

    /**
     * Function to get assessments listing
     * @param ARRAY $filters
     * @param BOOL  $countOnly
     */
    public function lists(array $filters, $countOnly=false);

    /**
     * Function to get user details by id
     * @param type $id
     */
    public function firstornew($id = null);

    /**
     * Function to get paramters details by id
     * @param type $id
     */
    public function findById($ids = []);
    
     /**
     * Function to get section details by id
     * @param type $id
     */
    public function getSection($id = null);

    /**
     * Function to assign assessor to paremeter
     * @param AssessmentParameter $parameterID
     * @param ARRAY $data
     */
    public function assignParametersAssessor(AssessmentParameter $parameterID, $data=[]);

    /**
     * Function to submit response to section
     * @param AssessmentParameter $parameterID
     * @param ARRAY $data
     */
    public function submitSectionResponse(AssessmentParameterSection $parameter, $data=[]);

    /**
     * Function to submit evidence to paremeter
     * @param AssessmentParameter $parameterID
     * @param ARRAY $data
     */
    public function submitParametersEvidence(AssessmentParameter $parameter, $data=[]);

     /**
     * Function to get all evidences to paremeter
     * @param AssessmentParameter $parameterID
     * @param ARRAY $data
     */
    public function getParameterEvidences(AssessmentParameter $parameter);
    
    /**
     * Function to submit comment to paremeter
     * @param AssessmentParameter $parameterID
     * @param ARRAY $data
     */
    public function submitParametersComment(AssessmentParameter $parameter, $data=[]);

    /**
     * Function to get all comment to paremeter
     * @param AssessmentParameter $parameterID
     * @param ARRAY $data
     */
    public function getParameterComments(AssessmentParameter $parameter);

    /**
     * Fnctionn to sync all assessor Data
     */
    public function syncAssessmentData($inputs);
}
