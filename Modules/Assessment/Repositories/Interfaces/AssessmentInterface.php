<?php

namespace Modules\Assessment\Repositories\Interfaces;

use Modules\Assessment\Entities\Assessment;
use Modules\User\Entities\User;

interface AssessmentInterface
{

    /**
     * Function to get assessments listing
     * @param ARRAY $filters
     * @param BOOL  $countOnly
     */
    public function lists(array $filters, $countOnly=false);

     /**
     * Function to get assessments listing
     * @param ARRAY $filters
     * @param BOOL  $countOnly
     */
    public function getAssessment(array $filters, $countOnly=false);

    /**
     * FUnction to show assessments details based on assessmentUID
     * @param STRING $assessmentId
     * @param Array $filters
     */
    public function show($assessmentId, array $filters);

    /**
     * Function to save Assessment
     * @param Assessment $assessment
     * @param array $data
     * @return Assessment $assessment
     */
    public function save(Assessment $assessment, array $data);

    /**
     * Function to get user details by id
     * @param type $id
     */
    public function firstornew($id = null);

    public function cloneAssessment(Assessment $assessment);
     
    /* Function to delete assessment
     * @param Assessment $assessment
     * @param array $data
     */
    public function delete(Assessment $assessment);

    /* 
     * Function to start a assessment
     * @param Assessment $assessment
     * @param array $data
     */
    public function startAssessment(Assessment $assessment);

    /* 
     * Function to open a assessment
     * @param Assessment $assessment
     * @param array $data
     */
    public function openAssessment(Assessment $assessment, User $user);

    /* 
     * Function to submit a assessment
     * @param Assessment $assessment
     * @param array $data
     */
    public function submitAssessment(Assessment $assessment);

    /* 
     * Function to complete a assessment
     * @param Assessment $assessment
     * @param array $data
     */
    public function completeAssessment(Assessment $assessment);

    /**
     * Function to get all evidences to assessment
     * @param Assessment $assessment
     * @param ARRAY $data
     */
    public function getSiteEvidences(Assessment $assessment);

    /**
     * Function to submit evidence to assessment
     * @param Assessment $assessment
     * @param ARRAY $data
     */
    public function submitSiteEvidence(Assessment $assessment, $data=[]);

    public function getSiteDetails($assessmentId, $filters=[]);
}
