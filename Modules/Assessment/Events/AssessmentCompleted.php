<?php

namespace Modules\Assessment\Events;

use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Modules\Assessment\Entities\Assessment;

class AssessmentCompleted
{
    use SerializesModels;
    use Dispatchable;

    public $assessment;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Assessment $assessment)
    {
        $this->assessment = $assessment;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
