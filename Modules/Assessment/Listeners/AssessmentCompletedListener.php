<?php

namespace Modules\Assessment\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Assessment\Events\AssessmentCompleted;
use Modules\Notification\Notifications\AssessmentCompleted as AssessmentCompletedNotification;
use Modules\Core\Enums\ApplicationStage;
use Modules\Notification\Notifications\RequestFeedbackFromVendor;
use Modules\Assessment\Entities\AssessmentParameter;
use Modules\Notification\Notifications\SendAssessmentReportToVendor;
use Barryvdh\Snappy\Facades\SnappyPdf;
use Modules\Application\Entities\AssessFeedback;
use Modules\Core\Entities\FeedbackQuestionSetting;

class AssessmentCompletedListener
{
    private $reportPath;
    private $certificatePath;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(AssessmentCompleted $event)
    {
        if(!empty($event->assessment)){
            // Update Assessment Score And Assign Certificate
            $parameters = AssessmentParameter::where('assessment_id', $event->assessment->id)->pluck('parameter_score')->toArray();
            $assessmentScore = !empty($parameters) ? array_sum($parameters)/count($parameters) : 0;
            $certification_type = $this->getCertificationType($assessmentScore);
            $certification_expiry_time = date('Y-m-d', strtotime("+2 year"));
            
            // Update Assessment
            $event->assessment->assessment_score = $assessmentScore;
            $event->assessment->certification_type = $certification_type;
            $event->assessment->certification_expiry = $certification_expiry_time;
            $event->assessment->save();
            $event->assessment->application->application_stage = ApplicationStage::APP_ASSESS_COMPLETE;
            $event->assessment->application->save();
            
          //  $this->generateCertificate($event->assessment);
           // $this->generateReport($event->assessment);
            $this->notifyToVendor($event->assessment->application);
        }
    }


    private function getCertificationType($assessmentScore)
    {
        $certification_type = 'bronze';
        if($assessmentScore < 2.5){
            $certification_type = 'bronze';
        }elseif($assessmentScore >= 2.5 && $assessmentScore < 3){
            $certification_type = 'silver';
        }elseif($assessmentScore >= 3 && $assessmentScore < 3.5){
            $certification_type = 'gold';
        }elseif($assessmentScore >= 3.5 && $assessmentScore < 4){
            $certification_type = 'diamond';
        }elseif($assessmentScore >= 4){
            $certification_type = 'platinum';
        }
        return $certification_type;
    }

    private function generateCertificate($application)
    {
        $parameters = $application->assessment->parameters();
        $pdf = SnappyPdf::loadView('api::application.certificate', ['application'=>$application,'parameters'=>$parameters]);
        $pdf->setOption('enable-javascript', true);
        $pdf->setOption('javascript-delay', 5000);
        $pdf->setOption('enable-smart-shrinking', true);
        $pdf->setOption('no-stop-slow-scripts', true);
        return $pdf->save(storage_path().'/app/public/reports/'.$application->application_number."-certificate.pdf"); 
    }

    private function generateReport($application)
    {
        $pages = config('api.applicationReportFormat.pages');
        $parameters = $application->assessment->parameters();
        $performanceData = [];
        $latLongData = [];
        if(!empty($parameters)){
            foreach($parameters as $parameter){
                if(!empty($parameter['discipline'])){
                    if(!empty($performanceData[$parameter['discipline']]['parameters'])){
                        array_push($performanceData[$parameter['discipline']]['parameters'],[
                            'title'=>$parameter['parameter'],
                            'score'=>$parameter['parameter_score']
                        ]);
                    }else{
                        $performanceData[$parameter['discipline']]['strength'] = [];
                        $performanceData[$parameter['discipline']]['ofi'] = [];
                        $performanceData[$parameter['discipline']]['parameters'] = [
                            [
                                'title'=>$parameter['parameter'],
                                'score'=>$parameter['parameter_score']
                            ]
                        ];
                    }
                    if(!empty($parameter->comments)){
                        foreach($parameter->comments as $comment){
                            if(!empty($comment->comment_type) && $comment->comment_type == 'strength'){
                                array_push($performanceData[$parameter['discipline']]['strength'], $comment);
                            }elseif(!empty($comment->comment_type) && $comment->comment_type == 'ofi'){
                                array_push($performanceData[$parameter['discipline']]['ofi'], $comment);
                            }
                        }
                    }
                    if(!empty($parameter->evidenses)){
                        foreach($parameter->evidenses as $evidense){
                            if(!empty($evidense->latitude) && !empty($evidense->longitude)){
                                $latLongData = [
                                    'latitude'=>$evidense->latitude,
                                    'longitude'=>$evidense->longitude
                                ];
                                break;
                            }
                        }
                    }
                }
            }
        }
        $pdf = SnappyPdf::loadView('api::application.report', ['latLongData'=>$latLongData, 'application'=>$application, 'pages'=>$pages, 'performanceData'=>$performanceData]);
        $pdf->setOption('enable-javascript', true);
        $pdf->setOption('javascript-delay', 5000);
        $pdf->setOption('enable-smart-shrinking', true);
        $pdf->setOption('no-stop-slow-scripts', true);
        return $pdf->save(storage_path().'/app/public/reports/'.$application->application_number."-report.pdf");    
    }

    private function notifyToVendor($application)
    {
        $this->generateApplicationFeedback($application);
        $this->generateReport($application);
        $this->generateCertificate($application);
        $application->user->user->notify(new RequestFeedbackFromVendor($application));
    }

    public function generateApplicationFeedback($application){
        $feedbackQuestions = FeedbackQuestionSetting::get()->pluck('title','question_uuid')->toArray();
        $applicationFeedback = AssessFeedback::where('application_id', $application->id)->firstornew();
        $applicationFeedback->application_id = $application->id;
        $applicationFeedback->user_id = $application->user_id;
        $applicationFeedback->feedback_payload = !empty($feedbackQuestions) ? json_encode($feedbackQuestions) : json_encode([]);
        $applicationFeedback->save();
        return true;
    }
}
