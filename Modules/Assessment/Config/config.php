<?php

return [
    'name' => 'Assessment',
    'Parameter_P_Ratio'=>20,
    'Parameter_D_Ratio'=>50,
    'Parameter_M_Ratio'=>30,
    'param_response_score'=>[
        'bronze'=>1,
        'silver'=>2,
        'gold'=>3,
        'diamond'=>4,
        'platinum'=>5
    ]
];
