<?php

namespace Modules\Assessment\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Modules\Assessment\Events\AssessmentCompleted;
use Modules\Assessment\Listeners\AssessmentCompletedListener;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        AssessmentCompleted::class=>[
            AssessmentCompletedListener::class
        ]
    ];
}
