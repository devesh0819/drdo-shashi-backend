<?php

namespace Modules\Assessment\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Modules\User\Entities\User;
use Modules\Assessment\Entities\AssessmentParameter;
use Modules\Assessment\Services\AssessmentParameterService;
use Modules\Core\Enums\UserRole;
use Illuminate\Validation\ValidationException;
use Modules\Core\Enums\AssessmentStage;
use Modules\Core\Enums\AssessorAssessStage;

class ParameterPolicy
{
    use HandlesAuthorization;
    private $assessmentParameter;
    private $parameter;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct(
        AssessmentParameterService $assessmentParameterService
    )
    {
        $this->assessmentParameter = $assessmentParameterService;
        if(!empty(request()->route('parameterID'))){
            if(request()->route('parameterID') == 'bulk'){
                $paramIDs = request()->get('parameterIDs');
                $this->parameter = AssessmentParameter::whereIn('_id', $paramIDs)->get();
            }else{
                $this->parameter = $this->assessmentParameter->firstornew(request()->route('parameterID'));
                if(empty($this->parameter->id)){
                    abort(404);
                }
            }
        }
    }

    public function assignParameterAssessor(User $user, AssessmentParameter $assessmentParameter=null)
    {
        if(request()->route('parameterID') == 'bulk'){
            $pass = true;
            foreach($this->parameter as $parameter){
                if(!empty($parameter->assessment()->lead_assessor_id) && $parameter->assessment()->lead_assessor_id == $user->id){
                    $pass = true;
                }else{
                    $pass = false;
                    break;
                }
            }
            return $pass;
        }else{
            $parameter = !empty($this->parameter->id) ? $this->parameter : $assessmentParameter;
            return ($parameter->assessment()->lead_assessor_id == $user->id);
        }
    }

    public function submitParameter(User $user, AssessmentParameter $assessmentParameter=null)
    {
        $parameter = !empty($this->parameter) ? $this->parameter : $assessmentParameter;
        if($parameter->assessor_id == $user->id || $user->hasRole(UserRole::ADMIN)){
            return true;
        }else{
            throw ValidationException::withMessages(['parameters' => __('validation.assessor_not_authorised')]);
        }
    }

    public function submitParameterComment(User $user, AssessmentParameter $assessmentParameter=null)
    {
        $parameter = !empty($this->parameter->id) ? $this->parameter : $assessmentParameter;
        if($parameter->assessor_id == $user->id || $user->hasRole(UserRole::ADMIN) || $user->id == $this->parameter->assessment()->lead_assessor_id){
            return true;
        }else{
            throw ValidationException::withMessages(['parameters' => __('validation.assessor_not_authorised')]);
        }
    }

    public function submitParameterEvidense(User $user, AssessmentParameter $assessmentParameter=null)
    {
        $parameter = !empty($this->parameter->id) ? $this->parameter : $assessmentParameter;
        
        if($parameter->assessor_id == $user->id){
            return true;
        }else{
            throw ValidationException::withMessages(['parameters' => __('validation.assessor_not_authorised')]);
        }
    }

    public function view(User $user, AssessmentParameter $assessmentParameter=null)
    {
        $parameter = !empty($this->parameter->id) ? $this->parameter : $assessmentParameter;
        if($user->hasRole(UserRole::ADMIN) || $user->hasRole(UserRole::DRDO)){
            return in_array($parameter->assessment()->assessment_stage , [AssessmentStage::ASSESS_INIT, AssessmentStage::ASSESS_SUBMIT]) ? true : false;
        }
        if($parameter->assessment()->lead_assessor_id == request()->user()->id){
            return true;
        }
        return $parameter->assessor_id == request()->user()->id ? true : false;
    }
}
