<?php

namespace Modules\Assessment\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\AuthorizationException;
use Mockery\Generator\Parameter;
use Modules\User\Entities\User;
use Modules\Assessment\Services\AssessmentService;
use Modules\Application\Services\ApplicationService;
use Modules\Assessment\Entities\Assessment;
use Route;
use Modules\Core\Enums\AssessmentStage;
use Modules\Core\Enums\ApplicationStage;
use Modules\Core\Enums\UserRole;
use Modules\Assessment\Entities\AssessmentParameter;
use Modules\Core\Enums\ParameterResponses;
use Illuminate\Validation\ValidationException;
use Modules\Assessment\Entities\AssessmentAssessor;
use Modules\Core\Enums\AssessorAssessStage;

class AssessmentPolicy
{
    use HandlesAuthorization;
    private $assessmentService;
    private $applicationService;
    private $assessment;
    private $application;
    private $parameterResponses = [
        ParameterResponses::BRONZE,
        ParameterResponses::SILVER,
        ParameterResponses::GOLD,
        ParameterResponses::DIAMOND,
        ParameterResponses::PLATINUM
    ];
    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct(
        AssessmentService $assessmentService,
        ApplicationService $applicationService
    )
    {
        $this->assessmentService = $assessmentService;
        $this->applicationService = $applicationService;

        if(!empty(request()->route('applicationID'))){
            $this->application = $this->applicationService->firstornew(request()->route('applicationID'));
            if(empty($this->application->id)){
                abort(404);
            }
            if(! in_array($this->application->application_stage, [
                ApplicationStage::APP_SCHEDULED
            ])){
                throw new AuthorizationException();
            }
        }

        if(!empty(request()->route('assessmentID'))){
            $this->assessment = $this->assessmentService->firstornew(request()->route('assessmentID'));
            if(empty($this->assessment->id)){
                abort(404);
            }
        }
    }

    /**
     * Function to check if user is allowed to create application or not
     */
    public function create(User $user)
    {
        return request()->user()->can('create-assessment');
    }

    /**
     * Function to check if user is allowed to update application or not
     */
    public function update(User $user)
    {
        if($user->hasAnyRole([UserRole::DRDO, UserRole::ADMIN, UserRole::AGENCY])){
            if($user->hasAnyRole([UserRole::AGENCY])){
                return ($user->id == $this->assessment->agency_id) ? true : false;
            }else{
                return true;
            }
        }
        return (request()->user()->can('update-assessment')) && ($user->id === $this->application->user_id);
    }

    /**
     * Function to check if user is allowed to view application or not
     */
    public function view(User $user)
    {
        if($user->hasAnyRole([UserRole::DRDO, UserRole::ADMIN])){
            return true;
        }
        return (request()->user()->can('view-application')) && (empty($this->application) || ($user->id === $this->application->user_id));
    }

    /**
     * Function to check if user is allowed to make payment for a application or not
     */
    public function payment(User $user)
    {
        return (request()->user()->can('update-application')) && ($user->id === $this->application->user_id);
    }

    /**
     * Function to check if user is allowed to delete application or not
     */
    public function delete(User $user)
    {
        return (request()->user()->can('delete-application')) && ($user->id === $this->application->user_id);
    }

    /**
     * Function to check if admin can view assessments parameters
     */
    public function viewParameter(User $user)
    {
        return !empty($this->assessment->assessment_stage) && in_array($this->assessment->assessment_stage, [AssessmentStage::ASSESS_INIT, AssessmentStage::ASSESS_SUBMIT]) ? true : false;
    }

    /**
     * Function to check if assessor has access to assessment or not
     */
    public function accessAssessment(User $user, Assessment $assessment)
    {
        // If Assessor is Assigned in Assessment
        $allAssessors = $assessment->assessors()->whereIn('assessor_assess_status',
        [
            AssessorAssessStage::ASSESSOR_ASSESS_TODO,
            AssessorAssessStage::ASSESSOR_ASSESS_INPROGRESS
        ]
        )->pluck('assessor_id')->toArray();
        return in_array($user->id, $allAssessors);
    }

    /**
     * Function to check if assessor has access to sync opening meeting details 
     */
    public function syncOpenMeetData(User $user, Assessment $assessment)
    {
        if($assessment->lead_assessor_id != $user->id){
            return false;
        }
        return true;
    }

    /**
     * Function to check if assessor has access to sync closing meeting details 
     */
    public function syncCloseMeetData(User $user, Assessment $assessment)
    {
        if($assessment->lead_assessor_id != $user->id){
            return false;
        }
        return true;
    }

    /**
     * Function to check if assessor has access to sync site tour evidenses 
     */
    public function submitSiteEvidense(User $user, Assessment $assessment = null)
    {
        $assessment = !empty($this->assessment->id) ? $this->assessment : $assessment;
        
        // If Assessor is Assigned in Assessment
        $allAssessors = $assessment->assessors()->whereIn('assessor_assess_status', 
        [
            AssessorAssessStage::ASSESSOR_ASSESS_TODO,
            AssessorAssessStage::ASSESSOR_ASSESS_INPROGRESS
        ])->pluck('assessor_id')->toArray();
        return in_array($user->id, $allAssessors);
    }
    
    /**
     * Function to check if assessor can submit the assessment or not
     */
    public function startAssessment(User $user, Assessment $assessment=null)
    {
        $assessment = !empty($assessment) ? $assessment : $this->assessment;
        if($assessment->lead_assessor_id != $user->id){
            return false;
        }
        $assessorIDs = $assessment->assessors->pluck('assessor_id')->toArray();
        /*
        * If there is still any parameter on which response is not added assessment
        * can't be submitted by the assessor  
        */ 
        $parameters = AssessmentParameter::where('assessment_id', $assessment->id)
        ->whereNotIn('assessor_id', $assessorIDs)
        ->get()->toArray();
        if(!empty($parameters)){
            throw ValidationException::withMessages(['parameters' => __('validation.assessment_start_error')]);
        }
        return true;
    }

    /**
     * Function to check if assessor can submit the assessment or not
     */
    public function submitAssessmentToLead(User $user, Assessment $assessment)
    {
        /*
        * If there is still any parameter on which response is not added assessment
        * can't be submitted by the assessor  
        */ 
        $parameters = AssessmentParameter::where([
            'assessment_id'=> $assessment->id,
            'assessor_id' =>$user->id
        ])->whereHas('sections', function($q){
            $q->whereNotIN('response', $this->parameterResponses);
        })->get()->toArray();
        if(!empty($parameters)){
            throw ValidationException::withMessages(['parameters' => __('validation.assessment_submit_error')]);
        }
        return true;
    }

    /**
     * Function to check if assessor can submit the assessment or not
     */
    public function submitAssessmentToAdmin(User $user, Assessment $assessment=null)
    {
        $assessment = !empty($assessment) ? $assessment : $this->assessment;
        if($assessment->lead_assessor_id != $user->id){
            return false;
        }
        /*
        * If there is still any parameter on which response is not added assessment
        * can't be submitted by the assessor  
        */ 
        $assessorAssessPending = AssessmentAssessor::where('assessment_id', $assessment->id)
        ->whereNot('assessor_assess_status', AssessorAssessStage::ASSESSOR_ASSESS_COMPLETED)->pluck('assessor_id')->toArray();
        
        if(!empty($assessorAssessPending)){
            $pendingParameters = AssessmentParameter::whereIn('assessor_id', $assessorAssessPending)
            ->where('assessment_id', $assessment->id)->get()->toArray();
            
            if(!empty($pendingParameters)){
                throw ValidationException::withMessages(['parameters' => __('validation.assessment_submit_error')]);
            }else{
                return true;
            }

        }
        
        return !empty($assessorAssessPending) ? false : true;
    }
}
