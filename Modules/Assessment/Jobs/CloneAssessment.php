<?php

namespace Modules\Assessment\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Modules\Assessment\Entities\Assessment;
use Exception;
use Modules\Core\Helpers\Logger;

class CloneAssessment implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $assessent;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Assessment $assessent)
    {
        $this->assessent = $assessent;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try{
            dd("in", $this->assessent);
        }catch(Exception $ex){
            Logger::error($ex);
            return $ex;
        }
    }
}
