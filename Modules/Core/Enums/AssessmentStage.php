<?php

namespace Modules\Core\Enums;

class AssessmentStage
{
    const ASSESS_STARTED = 'assess_started';
    const ASSESS_INIT = 'asses_init';
    const ASSESS_SUBMIT = 'asses_submit';
    const ASSESS_REOPENED = 'asses_reopen';
    const ASSESS_COMPLETE = 'asses_complete';
    const ASSESS_FEEDBACK_SUBMIT = 'asses_feed_sub';
}