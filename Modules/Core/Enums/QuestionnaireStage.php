<?php

namespace Modules\Core\Enums;

class QuestionnaireStage
{
    const DRAFT = 'draft';
    const SUBMITTED = 'sumitted';
    const REJECTED = 'rejected';
    const PENDING_APPROVAL = 'approval_pending';
    const APPROVED = 'approved';
}