<?php

namespace Modules\Core\Enums;

class UserRole
{
    const SUPER_ADMIN = 'Super Admin';
    const ADMIN = 'Admin';
    const VENDOR = 'Vendor';
    const AGENCY = 'Agency';
    const ASSESSOR = 'Assessor';
    const MEMBER = 'Member';
    const DRDO = 'Drdo';
}
