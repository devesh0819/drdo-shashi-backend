<?php

namespace Modules\Core\Enums;

class ApplicationStage
{
    const APP_BASIC = 'app_basic';
    const APP_FINANCIAL = 'app_financial';
    const APP_OWNERSHIP = 'app_ownership';
    const APP_PENDING_APPROVAL = 'app_pending_approval';
    const APP_REJECTED = 'app_rejected';
    const APP_APPROVED = 'app_approved';
    const APP_PAYMENT_APPROVED = 'app_payment_approved';
    const APP_PAYMENT_DONE = 'app_payment_done';
    const APP_SCHEDULED = 'app_scheduled';
    const APP_ASSESS_PROGRESS = 'app_assess_in_progress';
    const APP_ASSESS_COMPLETE = 'app_assess_complete';
    const APP_FEEDBACK_COMPLETE = 'app_feedback_complete';
}