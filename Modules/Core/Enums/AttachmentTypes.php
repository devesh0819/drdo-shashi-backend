<?php

namespace Modules\Core\Enums;

class AttachmentTypes
{
    const APPLICATION = 'application';
    const APPLICATION_OWNERSHIP = 'application_ownership';
    const ASSESSMENT = 'assessment';
    const PARAMETER = 'parameter';
    const IMAGE = 'image';
    const FILE = 'file';
}