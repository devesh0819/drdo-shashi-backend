<?php

namespace Modules\Core\Enums;

class AssessorAssessStage
{
    const ASSESSOR_ASSESS_TODO = 'todo';
    const ASSESSOR_ASSESS_INPROGRESS = 'inprogress';
    const ASSESSOR_ASSESS_COMPLETED = 'completed';
    const ASSESSOR_ASSESS_REOPEN = 'reopened';
}