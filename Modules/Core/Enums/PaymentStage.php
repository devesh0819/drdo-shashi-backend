<?php

namespace Modules\Core\Enums;

class PaymentStage
{
    const PENDING = 'Pending';
    const COMPLETE = 'Complete';
}