<?php

namespace Modules\Core\Enums;

class Flag
{
    const ADMIN = 1;
    const AGENCY = 2;
    const ASSESSOR = 3;
    const MEMBER = 4;
    const VENDOR = 5;
    const DRDO = 6;
    const IS_ACTIVE = 1;
    const IS_INACTIVE = 0;
    const QUESTIONNAIRE_SM = 2;
    const QUESTIONNAIRE_MED = 2;
    const QUESTIONNAIRE_MIC = 2;
    const QUESTIONNAIRE_L = 1;

}