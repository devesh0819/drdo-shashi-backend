<?php

namespace Modules\Core\Enums;

class ParameterResponses
{
    const BRONZE = 'bronze';
    const SILVER = 'silver';
    const DIAMOND = 'diamond';
    const GOLD = 'gold';
    const PLATINUM = 'platinum';
}