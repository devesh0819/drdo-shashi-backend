<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('assessment_plans')) {
            Schema::create('assessment_plans', function (Blueprint $table) {
                $table->id();
                $table->enum('enterprice_type',['MIC','SM','MED',"L"]);
                $table->string('certification_fee', 55)->nullable();
                $table->string('assessment_cost', 55)->nullable();
                $table->tinyInteger('is_subsidized')->nullable();
                $table->tinyInteger('subsidy_percent')->nullable();
                $table->integer('gst')->nullable();
                $table->text('tds')->nullable();
                $table->jsonb('payment_breakup')->nullable();
                $table->tinyInteger('status')->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assessment_plans');
    }
};
