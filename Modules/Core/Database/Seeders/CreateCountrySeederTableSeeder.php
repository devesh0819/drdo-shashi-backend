<?php

namespace Modules\Core\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Entities\Country;
use Exception;
use Modules\Core\Helpers\Logger;
use Illuminate\Support\Facades\DB;

class CreateCountrySeederTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try{
            // Get All States
            $countries = $this->prepareCountryData(config('country'));
            if(!empty($countries) && is_array($countries)){
                Country::truncate();
                Country::insert($countries);
            }
            return true;
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

    private function prepareCountryData($records)
    {
        $countryData = [];
        if(!empty($records)){
            foreach($records as $record){
                array_push($countryData,[
                    'id' => $record[0],
                    'title' =>$record[2],
                    'country_code'=>$record[1],
                    'created_at'=> date("Y-m-d H:i:s")
                ]);
            }
        }
        return $countryData;
    }
}
