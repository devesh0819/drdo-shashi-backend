<?php

namespace Modules\Core\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Exception;
use Modules\Core\Helpers\Logger;
use Modules\Core\Entities\AssessmentPlan;
use Modules\Core\Enums\Flag;

class AssessmentPlanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try{
            $assessmentPlans = config('assessment_plans');
            if(!empty($assessmentPlans)){
                $assessmentPlanData = [];
                if(!empty($assessmentPlans)){
                    foreach($assessmentPlans as $key=>$assessmentPlan){
                        array_push($assessmentPlanData, [
                            'enterprice_type'=>$key,
                            'assessment_cost'=>$assessmentPlan['assessment_cost'],
                            'is_subsidized'=>$assessmentPlan['is_subsidized'],
                            'subsidy_percent'=>$assessmentPlan['subsidy_percent'],
                            'status'=>Flag::IS_ACTIVE,
                            'created_at'=>date("Y-m-d H:i:s")
                        ]);
                    }
                    AssessmentPlan::insert($assessmentPlanData);
                }
            }
            return true;
        }catch(Exception $ex){
            Logger::error($ex);
            return $ex;
        }
    }
}
