<?php

namespace Modules\Core\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Exception;
use Modules\Core\Helpers\Logger;
use Modules\Core\Entities\State;
use Modules\Core\Entities\District;
use Illuminate\Support\Str;
use Modules\Core\Imports\StateImport;
use Modules\Core\Imports\DistrictImport;

class DistrictStateTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try{
            // Get All States
            $states = $this->prepareStateData(config('state'));
            State::insert($states);
            // Get All Districts
            $districts = $this->prepareDistrictData(config('district'));
            District::insert($districts);
            return true;
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

    private function prepareStateData($records)
    {
        echo $stateFilePath = public_path()."statedistrict/state.xlsx";
        die;
        $states = [];
        if(!empty($records)){
            foreach($records as $record){
                array_push($states,[
                    'id' => $record[0],
                    'title' =>$record[1],
                    'title_slug'=>Str::slug($record[1]),
                    'description' => $record[2],
                    'status' => ($record[3] =='Active') ? 1 : 0,
                    'created_at'=> date("Y-m-d H:i:s")
                ]);
            }
        }
        return $states;
    }
    
    private function prepareDistrictData($records)
    {
        $districts = [];
        if(!empty($records)){
            $states = config('state');
            $stateByID = array_combine(array_column($states, 0), $states);
            foreach($records as $record){
                array_push($districts,[
                    'id' => $record[0],
                    'title' =>$record[1],
                    'title_slug'=>Str::slug($record[1]),
                    'state_id'=>$stateByID[$record[2]][0],
                    'description' => $record[3],
                    'status' => ($record[4] =='Active') ? 1 : 0,
                    'created_at'=> date("Y-m-d H:i:s")
                ]);
            }
        }
        return $districts;
    }
}
