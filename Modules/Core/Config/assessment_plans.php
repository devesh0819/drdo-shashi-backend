<?php
return [
    'MIC'=>[
        'assessment_cost'=>env('MIC_ASS_COST', 90000),
        'assessor_fee'=>env('MIC_ASSESSOR_FEE', 37840),
        'is_subsidized'=>env('MIC_SUBSIDIZED', true),
        'subsidy_percent'=>env('MIC_SUBSID_PERCENT', 80)
    ],
    'SM'=>[
        'assessment_cost'=>env('SM_ASS_COST', 90000),
        'assessor_fee'=>env('SM_ASSESSOR_FEE', 37840),
        'is_subsidized'=>env('SM_SUBSIDIZED', true),
        'subsidy_percent'=>env('SM_SUBSID_PERCENT', 50)
    ],
    'MED'=>[
        'assessment_cost'=>env('MIC_ASS_COST', 112000),
        'assessor_fee'=>env('MED_ASSESSOR_FEE', 44220),
        'is_subsidized'=>env('MED_SUBSIDIZED', true),
        'subsidy_percent'=>env('MED_SUBSID_PERCENT', 0)
    ],
    'L'=>[
        'assessment_cost'=>env('MIC_ASS_COST', 135000),
        'assessor_fee'=>env('L_ASSESSOR_FEE', 49640),
        'is_subsidized'=>env('L_SUBSIDIZED', false),
        'subsidy_percent'=>env('L_SUBSID_PERCENT', 0)
    ]
];