<?php
return [
    'Prop'=>'Proprietary',
    'PL'=>'PrivateLimited',
    'Pub'=>'Public Limited',
    'Part'=>'Partnership',
    'LimPart'=>'Limited Liability Partnership',
    'Join'=>'Joint Venture'
];