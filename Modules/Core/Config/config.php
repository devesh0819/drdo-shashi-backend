<?php

return [
    'name' => 'Core',
    'asset_base_url'=>env('ASSET_BASE_URL', ''),
    'drdo_admin_email'=>env('DRDO_ADMIN_EMAIL', 'drdo@gmail.com'),
    'social_categories'=>[
        'GN'=>'General',
        'SC'=>'SC',
        'ST'=>'ST',
        'OBC'=>'OBC'
    ],
    'special_categories'=>[
        'EXS'=>'Ex-Servicemen',
        'PWD'=>'Person With Disability',
        'WOM'=>'Women',
        'MIN'=>'Minority',
        'NONE'=> 'None'
    ],
    'business_nature'=>[
        'manufacturer'=>'Manufacturer',
        'fabricator'=>'Fabricator',
        'assembler'=>'Assembler'
    ],
    'enterprice_types'=>[
        'MIC'=>"Micro",
        "SM"=>"Small",
        "MED"=>"Medium",
        "L"=>"Large"
    ],
    'cpcb_code'=>[
        "R"=>"Red",
        "G"=>"Green",
        "O"=>"Orange",
        "W"=>"White"
    ]
];
