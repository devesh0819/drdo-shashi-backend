<?php

namespace Modules\Core\Helpers;

use Modules\Core\Helpers\Logger;
use Exception;
use Modules\Application\Entities\Application;
use Modules\Assessment\Entities\Assessment;
use Modules\Core\Enums\HttpStatusCode;
use Modules\Attachment\Entities\Attachment;
use Storage;
use Modules\Assessment\Entities\AssessmentParameter;
use Modules\Core\Entities\District;
use Modules\Core\Enums\ApplicationStage;
use Modules\Core\Enums\ParameterResponses;

class BasicHelper
{

    /**
     * Display date in d m y format
     * @param datetime $date
     * @return string
     */
    public static function showDate($date = '0', $format = 'j M Y')
    {
        //Get Data from object
        if (is_object($date)) {
            $date = (string)$date;
        }

        //changing date format and return 0 incase og wrong date
        if (strtotime($date) && $date != 0) {
            $date = date($format, strtotime($date));
        } else {
            $date = 'N/A';
        }

        return $date;
    }

    /**
     * Function to get admin default date filter
     */
    public static function adminDefaultDateFiler()
    {
        return date('d/m/Y', strtotime("-7 day")) . '-' . date('d/m/Y');
    }

    /**
     * Check if passed value is an instance of Exception
     * @param Exception $value
     * @return type
     */
    public static function isException($value)
    {
        return ($value instanceof Exception) ? true : false;
    }

    /**
     * Make a cURL HTTP Request
     * @param string $endpoint
     * @param string $method
     * @param array $data
     * @param boolean $jsonDecode
     * @return boolean
     */
    public static function httpCurlRequest(
        string $endpoint,
        string $method = "GET",
        array $data = array(),
        bool $jsonDecode = false,
        $headers = []
    ) {
        try {
            if (!empty($headers)) {
                $client = new \GuzzleHttp\Client(['headers' => $headers]);
            } else {
                $client = new \GuzzleHttp\Client();
            }
            $response = $client->request($method, $endpoint, ['query' => $data]);
            return ($jsonDecode) ? json_decode($response->getBody()->getContents()) : $response->getBody()->getContents();
        } catch (Exception $ex) {
            Logger::error($ex);
            return false;
        }
    }

    /**
     * Function to get unique application number
     */
    public static function getUniqueApplicationNumber($data=[])
    {
        $applicationNumber = "SAMAR-{{CERT_NUM}}-{{DIST}}-{{MY}}";
        $certNum = Application::count();

        $certNum =  !empty($certNum) ? $certNum+1 : 1;
        
        $district = District::where('id', $data['registered_district'])->pluck('district_acr')->first();
        $my = date("my");
        return "SAMAR-$certNum-$district-$my";
    }

    /**
     * Function to get unique assessment number
     */
    public static function getUniqueAssessmentNumber()
    {
        $assessmentNumber = "ASS-";
        
        // Append Current Year
        $assessmentNumber = $assessmentNumber.date("ymd");
        //Append Random String
        $assessmentNumber = $assessmentNumber.mt_rand(111,999);
        return $assessmentNumber;
    }

    /**
     * Function to get file url
     */
    public static function getFileURL(Attachment $attachment, $type="image")
    {
        $assetBaseURL = config('core.asset_base_url');
        if(!empty($assetBaseURL)){
            return $assetBaseURL.$attachment->attachment_uuid;
        }else{
            return route('api.v1.core.show.file', $attachment->attachment_uuid);
        }
        
        if($type == 'image'){
            $image = Storage::disk($attachment->disk)->get($attachment->path);
            return base64_encode($image);
        }else{
            return route('api.v1.core.show.file', $attachment->attachment_uuid);
        }
    }

    /**
     * Function to get file for assessor 
     */
    public static function getFileForAssessor($disk, $path, $type="image")
    {
            $image = Storage::disk($disk)->get($path);
            return $image;
    }

    /**
     * get_image_location
     * Returns an array of latitude and longitude from the Image file
     * @param $image file path
     * @return multitype:array|boolean
     */
    public static function getImageData($image = ''){
        try{
            $imageData = [];
            $exif = exif_read_data($image, 0, true);

            if(!empty($exif['FILE']['FileDateTime'])){
                $imageData['dateTime'] = $exif['FILE']['FileDateTime'];
            }
            if($exif && isset($exif['GPS'])){
                $GPSLatitudeRef = $exif['GPS']['GPSLatitudeRef'];
                $GPSLatitude    = $exif['GPS']['GPSLatitude'];
                $GPSLongitudeRef= $exif['GPS']['GPSLongitudeRef'];
                $GPSLongitude   = $exif['GPS']['GPSLongitude'];
                
                $lat_degrees = count($GPSLatitude) > 0 ? self::gps2Num($GPSLatitude[0]) : 0;
                $lat_minutes = count($GPSLatitude) > 1 ? self::gps2Num($GPSLatitude[1]) : 0;
                $lat_seconds = count($GPSLatitude) > 2 ? self::gps2Num($GPSLatitude[2]) : 0;
                
                $lon_degrees = count($GPSLongitude) > 0 ? self::gps2Num($GPSLongitude[0]) : 0;
                $lon_minutes = count($GPSLongitude) > 1 ? self::gps2Num($GPSLongitude[1]) : 0;
                $lon_seconds = count($GPSLongitude) > 2 ? self::gps2Num($GPSLongitude[2]) : 0;
                
                $lat_direction = ($GPSLatitudeRef == 'W' or $GPSLatitudeRef == 'S') ? -1 : 1;
                $lon_direction = ($GPSLongitudeRef == 'W' or $GPSLongitudeRef == 'S') ? -1 : 1;
                
                $latitude = $lat_direction * ($lat_degrees + ($lat_minutes / 60) + ($lat_seconds / (60*60)));
                $longitude = $lon_direction * ($lon_degrees + ($lon_minutes / 60) + ($lon_seconds / (60*60)));
                return array_merge($imageData, ['latitude'=>$latitude, 'longitude'=>$longitude]);
            }else{
                return false;
            }
        }catch(Exception $ex){
            return false;
        }
    }

    public static function gps2Num($coordPart){
        $parts = explode('/', $coordPart);
        if(count($parts) <= 0)
        return 0;
        if(count($parts) == 1)
        return $parts[0];
        return floatval($parts[0]) / floatval($parts[1]);
    }

    /**
     * Function to calculate score for a paramater
     */
    public static function calculateParameterScore(AssessmentParameter $assessmentParameter)
    {
        $score = 0;
        $sections = $assessmentParameter->sections;
        $paramPScore = [];
        $paramDScore = [];
        $paramMScore = [];
        
        if(!empty($sections)){
            foreach($sections as $section){
                if($section->type == "P"){
                    $paramPScore[] = config('assessment.param_response_score.'.$section->response);
                }elseif($section->type == "D"){
                    $paramDScore[] = config('assessment.param_response_score.'.$section->response);
                }elseif($section->type == "M"){
                    $paramMScore[] = config('assessment.param_response_score.'.$section->response);
                }
            }
        }
        $paramPScore = !empty(count($paramPScore)) ? array_sum($paramPScore) / count($paramPScore) : 0;
        $paramDScore = !empty(count($paramDScore)) ? array_sum($paramDScore) / count($paramDScore) : 0;
        $paramMScore = !empty(count($paramMScore)) ? array_sum($paramMScore) / count($paramMScore) : 0;
        
        $score =  (($paramPScore*config('assessment.Parameter_P_Ratio'))/100)+(($paramDScore*config('assessment.Parameter_D_Ratio'))/100)+(($paramMScore*config('assessment.Parameter_M_Ratio'))/100);
        return round($score, 2);
    }

    public static function getLatLongFromPincode($zipcode)
    {
        $url = "http://maps.googleapis.com/maps/api/geocode/json?address=".$zipcode."&sensor=false";
        $details=file_get_contents($url);
        $result = json_decode($details,true);
        dd($result);
        $lat=$result['results'][0]['geometry']['location']['lat'];
    
        $lng=$result['results'][0]['geometry']['location']['lng'];
        return ['lat'=>$lat, 'lng'=>$lng];
    }
}
