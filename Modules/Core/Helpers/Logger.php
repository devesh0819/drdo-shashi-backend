<?php

namespace Modules\Core\Helpers;

use Log;

class Logger
{
    public static function getChannel($channel)
    {
        if (!empty($channel) && $channel != null) {
            return $channel;
        }
        return config('logging.default');
    }

    /**
     * Log an error
     * @param Exception/string $exception
     * @param string $channel
     */
    public static function error($exception, $channel = null)
    {
        Log::channel(self::getChannel($channel))->error($exception);
    }

    /**
     * Log a warning
     * @param Exception/string $exception
     * @param string $channel
     */
    public static function warning($exception, $channel = null)
    {
        Log::channel(self::getChannel($channel))->warning($exception);
    }

    /**
     * Log an information
     * @param Exception/string $exception
     * @param string $channel
     */
    public static function info($exception, $channel = null)
    {
        Log::channel(self::getChannel($channel))->info($exception);
    }
}
