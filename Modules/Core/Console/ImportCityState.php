<?php

namespace Modules\Core\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Exception;
use Modules\Core\Helpers\Logger;
use Maatwebsite\Excel\Facades\Excel;
use Modules\Core\Entities\State;
use Modules\Core\Entities\District;
use Modules\Core\Imports\StateImport;
use Modules\Core\Imports\DistrictImport;

class ImportCityState extends Command
{
    private $questionnaireService;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'sync:state-district';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to sync district state into the database.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $stateFilePath = public_path().'/statedistrict/state.xlsx';
            $districtFilePath = public_path().'/statedistrict/district.xlsx';
            
            if (!file_exists($stateFilePath) || !is_readable($stateFilePath)){
                throw new Exception('Unable to access state file');
            }
            if (!file_exists($districtFilePath) || !is_readable($districtFilePath)){
                throw new Exception('Unable to access district file');
            }
            $state = Excel::import(new StateImport(), $stateFilePath);
            $district = Excel::import(new DistrictImport(), $districtFilePath);    
            return true;
        } catch (Exception $ex) {
            dd($ex);
            Logger::error($ex);
            return $ex;
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
        ];
    }
}
