<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Core\Entities\District;

class AssessmentPlan extends Model
{
    protected $table = "assessment_plans";

    protected $fillable = ['id', 'enterprice_type', 'assessment_cost', 'is_subsidized','subsidy_percent', 'status', 'created_at', 'updated_at'];
}
