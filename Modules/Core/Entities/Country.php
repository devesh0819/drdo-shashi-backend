<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Core\Entities\District;

class Country extends Model
{
    protected $table = "countries";
}
