<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Core\Entities\District;

class FeedbackQuestionSetting extends Model
{
    protected $table = "feedback_questions";

    protected $fillable = ['id', 'question_uuid', 'title', 'created_at', 'updated_at'];
}
