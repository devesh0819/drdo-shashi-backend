<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    protected $table = "districts";

    protected $fillable = ['id', 'state_id', 'title', 'title_slug', 'description', 'status', 'created_at', 'updated_at'];
    protected $hidden = ['state_id', 'title_slug', 'description', 'status', 'created_at', 'updated_at'];
    
}
