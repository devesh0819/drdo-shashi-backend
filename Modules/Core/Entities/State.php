<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Core\Entities\District;

class State extends Model
{
    protected $table = "states";

    protected $fillable = ['id', 'title', 'title_slug', 'description', 'status', 'created_at', 'updated_at'];
    protected $hidden = ['title_slug', 'description', 'status', 'created_at', 'updated_at'];
    public function districts()
    {
        return $this->hasMany(District::class, 'state_id');
    }
}
