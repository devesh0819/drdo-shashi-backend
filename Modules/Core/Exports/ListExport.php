<?php

namespace Modules\Core\Exports;

use Modules\Agency\Entities\AgencyMeta;
use Maatwebsite\Excel\Concerns\FromArray;

class ListExport implements FromArray
{
    private $data = [];
    
    public function __construct($collections, $type)
    {
        if(!empty($type)){
            switch ($type) {
                case "applicant_applications":
                  $this->data = $this->vendorApplicationListTransformer($collections);
                  break;
                case "admin_applications":
                  $this->data = $this->adminApplicationListTransformer($collections);
                  break;
                case "drdo_application":
                $this->data = $this->drdoApplicationListTransformer($collections);
                break;
                case "admin_assessments":
                $this->data = $this->assessmentListTransformer($collections);
                break;
                case "assessor_assessments":
                $this->data = $this->assessorAssessmentListTransformer($collections);
                break;
                case "admin_assessor":
                $this->data = $this->assessorListTransformer($collections);
                break;
                case "payment_history":
                    $this->data = $this->paymentHistoryTransformer($collections);
                    break;
                default:
                  $this->data = $collections;
              }
        }
        
        $this->collections = $collections;
    }

    public function array(): array
    {
        return $this->data;
    }

    public function vendorApplicationListTransformer($collections)
    {
        $dataArray = [];
        $dataArray[] = [
            'Application number', 'Unit/Enterprise Name', 'Application Date', 'Payment Status', 'Application Status', 'Assessment Date', 'Rating'
        ];
        if(!empty($collections)){
            foreach($collections as $collection){
                $dataArray[] = [
                    !empty($collection->application_number) ? $collection->application_number : null,
                    !empty($collection->enterprise_name) ? $collection->enterprise_name : null,
                    !empty($collection->created_at) ? date("j F Y", strtotime($collection->created_at)) : null,
                    !empty($collection->paymentOrder->status) && ($collection->paymentOrder->status=='Complete') ? 'Complete' : 'Yet to Pay',
                    !empty($collection->application_stage) ? __('exports.applicant_applications.app_status.'.$collection->application_stage) : null,
                    !empty($collection->assessment_date) ? date("j F Y", strtotime($collection->assessment_date)) : 'Yet to be scheduled',
                    !empty($collection->assessment->certification_type) ? __('api.'.$collection->assessment->certification_type.'-certification') : 'Yet to be assessed'
                ];
            }
        }
        return $dataArray;
    }

    public function adminApplicationListTransformer($collections)
    {
        
        $dataArray = [];
        $dataArray[] = [
            'Application number', 'Name of Enterprise', 'Type of Enterprise', 'Payment Status', 'Application Status', 'Application Date', 'Assessment Date', 'Subsidy', 'Feedback'
        ];
        if(!empty($collections)){
            foreach($collections as $collection){
                $dataArray[] = [
                    !empty($collection->application_number) ? $collection->application_number : null,
                    !empty($collection->enterprise_name) ? $collection->enterprise_name : null,
                    !empty($collection->enterprice_type) ? __('exports.msme_type.'.$collection->enterprice_type) : null,
                    !empty($collection->paymentOrder->status) && ($collection->paymentOrder->status=='Complete') ? 'Complete' : 'Yet to Pay',
                    !empty($collection->application_stage) ? __('exports.applicant_applications.app_status.'.$collection->application_stage) : null,
                    !empty($collection->created_at) ? date("j F Y", strtotime($collection->created_at)) : null,
                    !empty($collection->assessment_date) ? date("j F Y", strtotime($collection->assessment_date)) : 'Yet to be scheduled',
                    !empty($collection->subsidy_approved == 1) ? 'Yes' : 'No',
                    !empty($collection->feedback) ? 'Received' : 'Not Received'
                ];
            }
        }
        return $dataArray;

    }

    public function drdoApplicationListTransformer($collections)
    {
        $dataArray = [];
        $dataArray[] = [
            'Application number', 'Name of Enterprise', 'Type of Enterprise','Application Date', 'DRDO Vendor Reg Form', 'Status'
        ];
        if(!empty($collections)){
            foreach($collections as $collection){
                $dataArray[] = [
                    !empty($collection->application_number) ? $collection->application_number : null,
                    !empty($collection->enterprise_name) ? $collection->enterprise_name : null,
                    !empty($collection->enterprice_type) ? __('exports.msme_type.'.$collection->enterprice_type) : null,
                    !empty($collection->created_at) ? date("j F Y", strtotime($collection->created_at)) : null,
                    !empty($collection->vendor_certificate_url) ? 'Yes' : 'No',
                    !empty($collection->application_stage) ? __('exports.applicant_applications.app_status.'.$collection->application_stage) : null,
                ];
            }
        }
        return $dataArray;

    }

    public function assessmentListTransformer($collections)
    {
        $agency = AgencyMeta::latest()->first();
        $dataArray = [];
        $dataArray[] = [
            'Application number', 'Assessment number','Name of Enterprise', 'Type of Enterprise', 'Application Date','Assessment Date', 'No of Days', 'Assigned Agency', 'Assessors', 'Status', 'Feedback'
        ];
        if(!empty($collections)){
            foreach($collections as $collection){
                $assessStartDate = !empty($collection->assessment_start_date) ? date("Y-m-d", strtotime($collection->assessment_start_date)) :  date("Y-m-d");
                $assessSubmitDate = !empty($collection->assessment_submit_date) ? date("Y-m-d", strtotime($collection->assessment_submit_date)) :  date("Y-m-d");
                $duration = 1;
                $duration = $duration+ date_diff(date_create($assessStartDate), date_create($assessSubmitDate))->format("%R%a");
                $assessors = $collection->assessors;
                $assessorNames = [];
                if(!empty($assessors)){
                    foreach($assessors as $assessor){
                        if(!empty($assessor->profile->full_name)){
                            $assessorNames[] = $assessor->profile->full_name;
                        }
                    }
                }
                $dataArray[] = [
                    !empty($collection->application->application_number) ? $collection->application->application_number : null,
                    !empty($collection->assessment_number) ? $collection->assessment_number : null,
                    !empty($collection->application->enterprise_name) ? $collection->application->enterprise_name : null,
                    !empty($collection->application->enterprice_type) ? __('exports.msme_type.'.$collection->application->enterprice_type) : null,
                    !empty($collection->application->created_at) ? date("j F Y", strtotime($collection->application->created_at)) : null,
                    !empty($collection->assessment_date) ? date("j F Y", strtotime($collection->assessment_date))  : 'Yet to be scheduled',
                    !empty($duration) ? $duration : null,
                    !empty($agency->title) ? $agency->title : null,
                    !empty($assessorNames) ? implode(",", $assessorNames) : 'NA',
                    !empty($collection->assessment_stage) ? __('exports.applicant_applications.assess_status.'.$collection->assessment_stage) : null,
                    !empty($collection->application->feedback) ? 'Received' : 'Not Received'
                ];
            
            }                
        }
        
        return $dataArray;

    }

    public function assessorAssessmentListTransformer($collections)
    {
        $agency = AgencyMeta::latest()->first();
        $dataArray = [];
        $dataArray[] = [
            'Application number', 'Assessment number', 'Assessment Date', 'Application Date', 'Name of Enterprise', 'Assessment Type', 'Assigned Agency', 'Status'
        ];
        if(!empty($collections)){
            foreach($collections as $collection){
                 $dataArray[] = [
                    !empty($collection->application->application_number) ? $collection->application->application_number : null,
                    !empty($collection->assessment_number) ? $collection->assessment_number : null,
                    !empty($collection->assessment_date) ? date("j F Y", strtotime($collection->assessment_date)) : 'Yet to be scheduled',
                    !empty($collection->application->created_at) ? date("j F Y", strtotime($collection->application->created_at)) : null,
                    !empty($collection->application->enterprise_name) ? $collection->application->enterprise_name : null,
                    !empty($collection->questionnaire->type) ? $collection->questionnaire->type : null,
                    !empty($agency->title) ? $agency->title : null,
                    !empty($collection->assessment_stage) ? __('exports.applicant_applications.assess_status.'.$collection->assessment_stage) : null
                ];               
            }
        }
        
        return $dataArray;

    }
    
    public function assessorListTransformer($collections)
    {
        $dataArray = [];
        $dataArray[] = [
            'Assessor name', 'Base location','Highest qualification with specialization', 'Domain Experience', 'Years of experience', 'Status'
        ];
        if(!empty($collections)){
            foreach($collections as $collection){
                $dataArray[] = [
                    !empty($collection->first_name. ' ' .$collection->last_name) ? $collection->first_name. ' ' .$collection->last_name : null,
                    !empty($collection->assessorMeta->assessorState->title) && !empty($collection->assessorMeta->assessorDistrict->title) ? $collection->assessorMeta->assessorDistrict->title.','.$collection->assessorMeta->assessorState->title : null,
                    !empty($collection->assessorMeta->expertise) ? $collection->assessorMeta->expertise : null,
                    !empty($collection->assessorMeta->work_domain) ? $collection->assessorMeta->work_domain : null,
                    !empty($collection->assessorMeta->years_of_experience) ? $collection->assessorMeta->years_of_experience : null,
                    !empty($collection->assessorMeta->assessor_status) ? $collection->assessorMeta->assessor_status : 'NA'
                ];
            }
        }
        return $dataArray;

    }

    public function paymentHistoryTransformer($collections)
    {
        $dataArray = [];
        $dataArray[] = [
            'Order No', 'Enterprise Name','Payment Date', 'Amount(INR)', 'Status'
        ];
        if(!empty($collections)){
            foreach($collections as $collection){
                $dataArray[] = [
                    !empty($collection->order_number) ? $collection->order_number : null,
                    !empty($collection->application->enterprise_name) ? $collection->application->enterprise_name : null,
                    !empty($collection->updated_at) ? date("j F Y", strtotime($collection->updated_at)) : null,
                    !empty($collection->final_amount) ? (int)$collection->final_amount : 'NA',
                    !empty($collection->status=='Complete') ? 'Complete' : 'Pending'
                ];
            }
        }
        return $dataArray;

    }
}
