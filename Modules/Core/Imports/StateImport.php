<?php

namespace Modules\Core\Imports;

use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Modules\Core\Entities\State;
use Excel;
use Illuminate\Support\Str;
use Modules\Core\Enums\Flag;

class StateImport implements ToModel, WithHeadingRow, WithChunkReading
{
    /**
     * Import Meta Tags From a CSV
     * @param array $row
     * @return ObjectMetaTag
     */
    public function model(array $row)
    {
        if(!empty($row['s_no'])){
            $state = State::where('state_code', $row['state_code'])->firstornew();
            
            $state->title = $row['state_name_in_english'];
            $state->title_slug = Str::slug($row['state_name_in_english']);
            $state->state_code = $row['state_code'];
            $state->state_or_uat = $row['state_or_ut'];
            $state->status = Flag::IS_ACTIVE;

            return $state;
        }
    }

    /**
     * Size of chunk to be imported once from excel sheet
     */
    public function chunkSize(): int
    {
        return 20;
    }
}
