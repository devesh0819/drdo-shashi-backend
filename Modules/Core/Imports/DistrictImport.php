<?php

namespace Modules\Core\Imports;

use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Excel;
use Modules\Core\Entities\District;
use Illuminate\Support\Str;
use Modules\Core\Entities\State;
use Modules\Core\Enums\Flag;

class DistrictImport implements ToModel, WithHeadingRow, WithChunkReading
{
    /**
     * Import Meta Tags From a CSV
     * @param array $row
     * @return ObjectMetaTag
     */
    public function model(array $row)
    {
        if(!empty($row['s_no'])){
            $district = District::where('district_code', $row['district_code'])->firstornew();
            $state = State::where('title', str_replace("(State)", "", $row['hierarchy']))->first();
            if(!empty($state)){
                $district->state_id = $state->id;
            }
            $district->title = $row['district_name_in_english'];
            $district->title_slug = Str::slug($row['short_name_of_district']."-".$row['district_name_in_english']);
            $district->district_code = $row['district_code'];
            $district->district_acr = $row['short_name_of_district'];
            $district->status = Flag::IS_ACTIVE;

            return $district;
        }
    }

    /**
     * Size of chunk to be imported once from excel sheet
     */
    public function chunkSize(): int
    {
        return 200;
    }
}
