<?php

namespace Modules\Committee\Services;

use Modules\Committee\Repositories\Interfaces\MeetingInterface;
use Exception;
use Modules\Core\Helpers\Logger;
use Modules\Committee\Entities\Meeting;

class MeetingService
{
    private $repository;

    /**
     * Constructor
     * @param MeetingInterface $repository
     */
    public function __construct(MeetingInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Function to find all users list
     * @param Array $filters
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function lists($filters=[], $countOnly=false)
    {
        try {
            return $this->repository->lists($filters, $countOnly);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }



    /**
     * Function to get meeting details by id
     * @param type $id
     */
    public function firstornew($id = null)
    {
        try {
            return $this->repository->firstornew($id);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

    /**
     * Function to save user
     * @param Meeting $committee
     * @param array $data
     */
    public function save(Meeting $meeting, $data=[])
    {
        try {
            return $this->repository->save($meeting, $data);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

    /**
     * Function to save meeting Note
     * @param Meeting $committee
     * @param array $data
     */
    public function saveNote(Meeting $meeting, $data=[])
    {
        try {
            return $this->repository->saveNote($meeting, $data);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

    /**
     * Function to delete committee
     * @param Meeting $committee
     * @param array $data
     */
    // public function delete(Committee $committee)
    // {
    //     try {
    //         return $this->repository->delete($committee);
    //     } catch (Exception $ex) {
    //         Logger::error($ex);
    //         return $ex;
    //     }
    // }






}
