<?php

namespace Modules\Committee\Services;

use Exception;
use Modules\Core\Helpers\Logger;
use Modules\Committee\Entities\Committee;
use Modules\Committee\Repositories\Interfaces\CommitteeInterface;

class CommitteeService
{
    private $repository;

    /**
     * Constructor
     * @param CommitteeInterface $repository
     */
    public function __construct(CommitteeInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Function to find all users list
     * @param Array $filters
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function lists($filters=[], $countOnly=false)
    {
        try {
            return $this->repository->lists($filters, $countOnly);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }



    /**
     * Function to get meeting details by id
     * @param type $id
     */
    public function firstornew($id = null)
    {
        try {
            return $this->repository->firstornew($id);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

    /**
     * Function to get meeting details by id
     * @param type $id
     */
    public function getCommittee($id = null)
    {
        try {
            return $this->repository->getCommittee($id);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }
    
    /**
     * Function to save user
     * @param Committee $committee
     * @param array $data
     */
    public function save(Committee $committee, $data=[])
    {
        try {
            return $this->repository->save($committee, $data);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

    /**
     * Function to delete committee
     * @param Committee $committee
     * @param array $data
     */
    public function delete(Committee $committee)
    {
        try {
            return $this->repository->delete($committee);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }
}
