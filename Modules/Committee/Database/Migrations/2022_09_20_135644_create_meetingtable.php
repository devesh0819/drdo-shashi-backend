<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('committee_meetings')) {
            Schema::create('committee_meetings', function (Blueprint $table) {
                $table->id();
                $table->unsignedInteger('committee_id');
                $table->string('meeting_uuid', 255)->unique();
                $table->string('meeting_title');
                $table->string('description')->nullable();
                $table->enum('meeting_type',['online', 'offline'])->default('online');
                $table->string('meeting_link')->nullable();
                $table->string('meeting_address')->nullable();
                $table->jsonb('meeting_assessments')->nullable();
                $table->jsonb('notes')->nullable();
                $table->timestamp('meeting_date_time')->nullable();
                $table->unsignedInteger('last_modified_by')->nullable();
                $table->softDeletes();
                $table->timestamp('created_at')->useCurrent();
                $table->timestamp('updated_at')->nullable();
            });
        }
        


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('committee_meetings');
    }
};
