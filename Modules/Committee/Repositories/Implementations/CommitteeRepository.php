<?php

namespace Modules\Committee\Repositories\Implementations;

use Illuminate\Support\Str;
use Modules\Committee\Entities\Committee;
use Modules\Committee\Repositories\Interfaces\CommitteeInterface;
use Modules\Committee\Entities\CommitteeMember;
class CommitteeRepository implements CommitteeInterface
{
    private $limit = 10;

    /**
     * Getter
     * @param type $property
     * @return type
     */
    public function __get($property)
    {
        return $this->$property;
    }

    /**
     * Setter
     * @param type $property
     * @param type $value
     * @return $this
     */
    public function __set($property, $value)
    {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
        return $this;
    }

    /**
     * Function to find all committee data
     * @param Array $filters
     * @return Array $data
     */
    public function lists($filters=[], $countOnly = false)
    {
        $committees = new Committee();
        if($countOnly){
            return $committees->count();
        } else {
            $offset = !empty($filters['start']) ? ($filters['start']-1)*$this->limit : 0;
            return $committees->orderBy("created_at", "desc")
            ->limit(!empty($filters['length']) ? $filters['length'] : $this->limit)
            ->offset($offset)
            ->get();
        }
    }

    /**
     * Function to save user
     * @param User $user
     * @param array $data
     * @return User $user
     */
    public function save(Committee $committee, array $data)
    {
        if(empty($committee->id)){
            $committee->committee_uuid = (string) Str::orderedUuid();
        }

        $committee->title = $data['title'];

        $committee->save();
        if(!empty($data['members']) && is_array($data['members'])){
            if(!empty($data['addNew'])){
                $committee->members()->attach($data['members']);
            }else{
                $committee->members()->sync($data['members']);
            }
        }
        return $committee;
    }

    /**
     * Function to get user details by id
     * @param int $id
     * @return User $user
     */
    public function firstornew($id = null)
    {
        if (!empty($id)) {
            return  Committee::where('committee_uuid', $id)->first();
        }
        return new Committee();
    }

    /**
     * Function to get meeting details by id
     * @param type $id
     */
    public function getCommittee($id = null)
    {   
        $data = [];
        if (!empty($id)) {
            $data['committee'] =  Committee::where('committee_uuid', $id)->first();
            $data['membersData']  = [];
            if(!empty($data['committee'])){
                $data['membersData']  = CommitteeMember::where('committee_id', $data['committee']->id)->get();
            }
        }
        return $data;

    }
    /**
     * Function to delete user
     * @param Meeting $user
     * @param array $data
     */
    public function delete(Committee $committee)
    {
        return $committee->delete();
    }
}
