<?php

namespace Modules\Committee\Repositories\Implementations;

use Modules\Committee\Repositories\Interfaces\MeetingInterface;

use Illuminate\Support\Str;
use Modules\Committee\Entities\Meeting;
use Illuminate\Support\Facades\DB;
use Modules\Committee\Entities\MeetingNotes;
use Modules\Assessment\Entities\Assessment;

class MeetingRepository implements MeetingInterface
{
    private $limit = 10;

    /**
     * Getter
     * @param type $property
     * @return type
     */
    public function __get($property)
    {
        return $this->$property;
    }

    /**
     * Setter
     * @param type $property
     * @param type $value
     * @return $this
     */
    public function __set($property, $value)
    {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
        return $this;
    }

    /**
     * Function to find all users list
     * @param Array $filters
     * @return Array $data
     */
    public function lists($filters=[], $countOnly = false)
    {
        $meeting =Meeting::all();

        return $meeting;

    }

    /**
     * Function to save user
     * @param User $user
     * @param array $data
     * @return User $user
     */
    public function save(Meeting $meeting, array $data)
    {
        if(empty($meeting->id)){
            $meeting->meeting_uuid = (string) Str::orderedUuid();
            $meeting->committee_id = $data['committee_id'];
        }
        $meeting->meeting_title = !empty($data['meeting_title']) ? $data['meeting_title'] : null;
        $meeting->description = $data['description'];
        $meeting->meeting_date_time = date("Y-m-d H:i:s", strtotime($data['meeting_date_time']));
        $meeting->meeting_type = !empty($data['meeting_type']) ? $data['meeting_type'] : null;
        $meeting->meeting_link = !empty($data['meeting_link']) ? $data['meeting_link'] : null;
        $meeting->meeting_address = !empty($data['meeting_address']) ? $data['meeting_address'] : null;
        $meeting->meeting_assessments = json_encode($data['meeting_assessments']);

        if($meeting->save()){
            $assessments = Assessment::whereIn('id', $data['meeting_assessments'])->get();
            if(!empty($assessments)){
                foreach($assessments as $assessment){
                    $assessment->assessment_review_date = $meeting->meeting_date_time;
                    $assessment->save();
                }
            }
        }
        return $meeting;
    }

    /**
     * Function to save meeting Note
     * @param Meeting $committee
     * @param array $data
     */
    public function saveNote(Meeting $meeting, $data=[])
    {
        if(!empty($meeting->id) && !empty($data['meeting_notes'])){
            $meetingNote = new MeetingNotes();
            $meetingNote->meeting_notes = !empty($data['meeting_notes']) ? $data['meeting_notes'] : null;
            $meetingNote->user_id = $data['user_id'];
            $meeting->notes()->save($meetingNote);
            return $meetingNote;
        }
        return false;
    }

    /**
     * Function to get user details by id
     * @param int $id
     * @return User $user
     */
    public function firstornew($id = null)
    {
        if (!empty($id)) {
            return Meeting::where('meeting_uuid', $id)->first();
        }
        return new Meeting();
    }

    /**
     * Function to delete user
     * @param Meeting $user
     * @param array $data
     */
    public function delete(Meeting $meeting)
    {
        return $meeting->delete();
    }

    /**
     * Function to verify email verification otp
     * @param User $user
     * @param array $data
     */

}
