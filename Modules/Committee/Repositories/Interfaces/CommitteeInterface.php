<?php

namespace Modules\Committee\Repositories\Interfaces;

use Modules\Committee\Entities\Committee;

interface CommitteeInterface
{
     /**
     * Function to find all users list
     * @param Array $filters
     * @param Boolean $countOnly
     *
     * @return Array $data
     */
    public function lists($filters=[], $countOnly=false);

    /**
     * Function to save committee
     * @param Meeting $meeting
     * @param array $data
     */
    public function save(Committee $committee, array $data);

    /**
     * Function to delete committee
     * @param Meeting $meeting
     * @param array $data
     */
    public function delete(Committee $committee);



    /**
     * Function to get user details by id
     * @param type $id
     */
    public function firstornew($id = null);

    /**
     * Function to get meeting details by id
     * @param type $id
     */
    public function getCommittee($id = null);
    
}
