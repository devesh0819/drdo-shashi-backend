<?php

namespace Modules\Committee\Repositories\Interfaces;

use Modules\Committee\Entities\Meeting;

interface MeetingInterface
{

    /**
     * Function to find all users list
     * @param Array $filters
     * @param Boolean $countOnly
     *
     * @return Array $data
     */
    public function lists($filters=[], $countOnly=false);



    /**
     * Function to save committee
     * @param Meeting $meeting
     * @param array $data
     */
    public function save(Meeting $meeting, array $data);

    /**
     * Function to save meeting Note
     * @param Meeting $committee
     * @param array $data
     */
    public function saveNote(Meeting $meeting, $data=[]);

    /**
     * Function to delete committee
     * @param Meeting $meeting
     * @param array $data
     */
    public function delete(Meeting $meeting);



    /**
     * Function to get user details by id
     * @param type $id
     */
    public function firstornew($id = null);



}
