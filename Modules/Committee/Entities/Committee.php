<?php

namespace Modules\Committee\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Committee\Entities\Traits\CommitteeTrait;
use Illuminate\Notifications\Notifiable;
use Modules\Committee\Entities\Interfaces\CommitteeInterface;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Committee\Entities\CommitteeMember;
use Modules\Committee\Entities\Meeting;

class Committee extends Model implements CommitteeInterface
{
    use CommitteeTrait;
    use SoftDeletes;
    protected $table = 'committees';

    protected $fillable = ['title', 'meeting_members'];
    protected $guard_name = 'web';

    public function members()
    {
        return $this->belongsToMany(CommitteeMember::class, 'committee_members','committee_id','member_id');
    }

    public function meetings()
    {
        return $this->hasMany(Meeting::class, );
    }

    public function members_count()
    {
        return CommitteeMember::where('committee_id', $this->id)->count();
    }
}
