<?php

namespace Modules\Committee\Entities\Traits;

trait MeetingTrait
{

    public function getID()
    {
        return !empty($this->{$this::ID}) ? $this->{$this::ID} : null;
    }

    public function getMeetingTitle()
    {
        return !empty($this->{$this::MEETING_TITLE}) ? $this->{$this::MEETING_TITLE} : null;
    }

    public function getDescription()
    {
        return !empty($this->{$this::DESCRIPTION}) ? $this->{$this::DESCRIPTION} : null;
    }

    public function getMeetingDate()
    {
        return !empty($this->{$this::MEETING_DATE}) ? $this->{$this::MEETING_DATE} : null;
    }

    public function getMeetingTime()
    {
        return !empty($this->{$this::MEETING_TIME}) ? $this->{$this::MEETING_TIME} : null;
    }

    public function getMeetingType()
    {
        return !empty($this->{$this::MEETING_TYPE}) ? $this->{$this::MEETING_TYPE} : null;
    }

    public function getMeetingLink()
    {
        return !empty($this->{$this::MEETING_LINK}) ? $this->{$this::MEETING_LINK} : null;
    }

    public function getMeetingAddress()
    {
        return !empty($this->{$this::MEETING_ADDRESS}) ? $this->{$this::MEETING_ADDRESS} : null;
    }

    public function getMeetingAssessments()
    {
        return !empty($this->{$this::MEETING_ASSESSMENTS}) ? json_decode($this->{$this::MEETING_ASSESSMENTS}) : null;
    }

    public function getMeetingNotes()
    {
        return !empty($this->{$this::MEETING_NOTES}) ? $this->{$this::MEETING_NOTES} : null;
    }

    public function getCommitteeID()
    {
        return !empty($this->{$this::COMMITTEE_ID}) ? $this->{$this::COMMITTEE_ID} : null;
    }


    public function getCreatedAt()
    {
        return !empty($this->{$this::CREATED_DATETIME}) ? date("Y-m-d H:i:s", strtotime($this->{$this::CREATED_DATETIME})) : null;
    }

    public function getUpdatedAt()
    {
        return !empty($this->{$this::UPDATED_DATETIME}) ? date("Y-m-d H:i:s", strtotime($this->{$this::UPDATED_DATETIME})) : null;
    }
}
