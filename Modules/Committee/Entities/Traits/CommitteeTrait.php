<?php

namespace Modules\Committee\Entities\Traits;

trait CommitteeTrait
{

    public function getID()
    {
        return !empty($this->{$this::ID}) ? $this->{$this::ID} : null;
    }

    public function getTitle()
    {
        return !empty($this->{$this::TITLE}) ? $this->{$this::TITLE} : null;
    }

    public function getDescription()
    {
        return !empty($this->{$this::DESCRIPTION}) ? $this->{$this::DESCRIPTION} : null;
    }


    public function getMembers()
    {
        return !empty($this->{$this::MEMBERS}) ? $this->{$this::MEMBERS} : null;
    }




    public function getCreatedAt()
    {
        return !empty($this->{$this::CREATED_DATETIME}) ? date("Y-m-d H:i:s", strtotime($this->{$this::CREATED_DATETIME})) : null;
    }

    public function getUpdatedAt()
    {
        return !empty($this->{$this::UPDATED_DATETIME}) ? date("Y-m-d H:i:s", strtotime($this->{$this::UPDATED_DATETIME})) : null;
    }
}
