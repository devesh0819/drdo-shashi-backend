<?php

namespace Modules\Committee\Entities;

use Illuminate\Database\Eloquent\Model;

use Modules\User\Entities\UserProfile;

class MeetingNotes extends Model
{
    protected $table = 'meeting_notes';
    protected $with = ['profile'];
    protected $hidden = ['id', 'last_modified_by', 'deleted_at'];
    
    public function profile()
    {   
        return $this->hasOne(UserProfile::class, 'user_id');
    }
}
