<?php

namespace Modules\Committee\Entities;

use Illuminate\Database\Eloquent\Model;

use Modules\User\Entities\UserProfile;

class CommitteeMember extends Model
{
    protected $table = 'committee_members';

    public function profile()
    {   
        return $this->hasOne(UserProfile::class, 'user_id', 'member_id');
    }
}
