<?php

namespace Modules\Committee\Entities\Interfaces;

interface MeetingInterface
{

    const ID = 'id';
    const MEETING_TITLE = 'meeting_title';
    const DESCRIPTION = 'description';
    const MEETING_DATE = 'meeting_date';
    const MEETING_TIME = 'meeting_time';
    const MEETING_TYPE = 'meeting_type';
    const MEETING_LINK = 'meeting_link';
    const MEETING_ADDRESS = 'meeting_address';
    const MEETING_ASSESSMENTS = 'meeting_assessments';
    const MEETING_NOTES = 'meeting_notes';
    const COMMITTEE_ID = 'committee_id';
    const CREATED_DATETIME = 'created_at';
    const UPDATED_DATETIME = 'updated_at';

    /**
     * Getters
     */
    public function getID();

    public function getCreatedAt();
    public function getMeetingTitle();
    public function getDescription();
    public function getMeetingType();
    public function getMeetingLink();
    public function getMeetingAddress();
    public function getMeetingAssessments();
    public function getMeetingNotes();
    public function getCommitteeID();
    public function getMeetingDate();
    public function getMeetingTime();
    public function getUpdatedAt();
}
