<?php

namespace Modules\Committee\Entities\Interfaces;

interface CommitteeInterface
{

    const ID = 'id';
    const TITLE = 'title';
    const MEMBERS = 'members';
    const CREATED_DATETIME = 'created_at';
    const UPDATED_DATETIME = 'updated_at';

    /**
     * Getters
     */
    public function getID();
    public function getCreatedAt();
    public function getTitle();
    public function getMembers();
    public function getUpdatedAt();
}
