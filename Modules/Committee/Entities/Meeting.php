<?php

namespace Modules\Committee\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Committee\Entities\Traits\MeetingTrait;
use Modules\Committee\Entities\Interfaces\MeetingInterface;

class Meeting extends Model implements MeetingInterface
{
    use MeetingTrait;
    protected $table = 'committee_meetings';
    protected $fillable = ['meeting_title', 'description', 'meeting_date_time', 'meeting_type', 'meeting_link', 'meeting_address', 'meeting_assessments', 'committee_id'];
    protected $guard_name = 'web';
    protected $with = ['notes'];
    
    public function notes()
    {
        return $this->hasMany(MeetingNotes::class,'meeting_id');
    }

}
