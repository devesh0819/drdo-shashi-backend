<?php

namespace Modules\Agency\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Agency\Entities\Interfaces\AgencyMetaInterface;
use Modules\Agency\Entities\Traits\AgencyMetaTrait;
use Modules\Assessor\Entities\AssessorMeta;

class AgencyMeta extends Model implements AgencyMetaInterface
{

    use AgencyMetaTrait;

    protected $table = "agency_meta";
    
    protected $fillable = ["title", "address"];
    
    protected $appends = [];
    
    protected $hidden = [];
    
        
    /**
     * Get the user that owns the profile.
     */
    public function user()
    {
        return $this->belongsTo('Modules\User\Entities\User');
    }

    public function assessors()
    {
        return $this->hasMany(AssessorMeta::class, 'agency_id', 'user_id');
    }
}
