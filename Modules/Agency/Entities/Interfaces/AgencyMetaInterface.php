<?php

namespace Modules\Agency\Entities\Interfaces;

interface AgencyMetaInterface
{

    const ID = 'id';
    const USER_ID = 'user_id';
    const TITLE = 'title';
    const ADDRESS = 'address';
    const CREATED_DATETIME = 'created_at';
    const UPDATED_DATETIME = 'updated_at';

    /**
     * Getters
     */
    public function getID();

    public function getUserId();

    public function getTitle();

    public function getAddress();

    public function getCreatedAt();

    public function getUpdatedAt();
}
