<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Migration to create Agency Profile.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('agency_meta')) {
            Schema::create('agency_meta', function (Blueprint $table) {
                $table->id();
                $table->foreignId('user_id')
                    ->constrained()
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
                $table->string('title', 255)->nullable();
                $table->text('address')->nullable();
                $table->unsignedInteger('last_modified_by')->nullable();
                $table->softDeletes();
                $table->timestamp('created_at')->useCurrent();
                $table->timestamp('updated_at')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agency_meta');
    }
};
