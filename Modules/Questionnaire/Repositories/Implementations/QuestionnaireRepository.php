<?php

namespace Modules\Questionnaire\Repositories\Implementations;

use Exception;
use Modules\Questionnaire\Repositories\Interfaces\QuestionnaireInterface;
use Modules\Questionnaire\Entities\Questionnaire;
use Modules\Questionnaire\Entities\QuestionnaireDiscipline;
use Modules\Questionnaire\Entities\QuestionnaireDisciplineParam;
use Modules\Questionnaire\Entities\QuestionDiscipParamSection;
use Modules\Core\Enums\Flag;
use Modules\Questionnaire\Jobs\ImportQuestionnaireData;
use Modules\Questionnaire\Entities\QuestionnaireAuditLog;
use Modules\Core\Enums\QuestionnaireStage;
use Modules\Notification\Notifications\InformDrdoAboutNewQuestionnaire;
use Modules\User\Entities\User;

class QuestionnaireRepository implements QuestionnaireInterface
{
    private $limit = 10;
    /**
     * Getter
     * @param type $property
     * @return type
     */
    public function __get($property)
    {
        return $this->$property;
    }

    /**
     * Setter
     * @param type $property
     * @param type $value
     * @return $this
     */
    public function __set($property, $value)
    {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
        return $this;
    }

    /**
     * Function to find all users list
     * @param Array $filters
     * @return Array $data
     */
    public function lists($filters=[], $countOnly = false)
    {
        $questionnaire= Questionnaire::where(function($que) use($filters){
            if(!empty($filters['role']) && $filters['role'] == 'drdo'){
                $que->whereNotIn('state', [QuestionnaireStage::REJECTED, QuestionnaireStage::DRAFT]);
            }
        });
        if($countOnly){
            return $questionnaire->count();
        }else{
        $offset = !empty($filters['start']) ? ($filters['start']-1)*$this->limit : 0;
        return $questionnaire->orderBy("created_at", "desc")
        ->limit(!empty($filters['length']) ? $filters['length'] : $this->limit)
        ->offset($offset)
        ->get();
        }
    }

     /**
     * Function to get questionnaire details by its ID
     * @param Array $filters
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function firstornew($questionnaireId=null)
    {
        if(empty($questionnaireId)){
            return new Questionnaire();
        }else{
            return Questionnaire::where('id', $questionnaireId)->first();
        }
    }

    /**
     * Function to fillAuditLogs for a questionnaire
     * @param Array $filters
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function fillAuditLogs($newQuestionnaire, $approvedQuestionnaire)
    {
        $auditData = [];
        if(!empty($newQuestionnaire) && !empty($approvedQuestionnaire)){
            // Checking For Disciplines Audit
            $newDiscip = $newQuestionnaire->disciplines->pluck('title_hash', 'disp_number')->toArray();
            $oldDiscip = $approvedQuestionnaire->disciplines->pluck('title_hash', 'disp_number')->toArray();
            $newDiscipByOldQues = [];
            if(!empty($newDiscip)){
                foreach($newDiscip as $key=>$newD){
                    $newKey = str_replace("QUES-$newQuestionnaire->id", "QUES-$approvedQuestionnaire->id", $key);
                    $newDiscipByOldQues[$newKey] = $newD;
                }
            }
            $auditData['disciplines']['new'] = array_diff_key($newDiscipByOldQues, $oldDiscip);
            $auditData['disciplines']['removed'] = array_diff_key($oldDiscip, $newDiscipByOldQues);
            $auditData['disciplines']['changed'] = [];
            if(!empty($newDiscipByOldQues)){
                foreach($newDiscipByOldQues as $key=>$new){
                    if(!empty($oldDiscip[$key]) && !hash_equals($new, $oldDiscip[$key])){
                        $auditData['disciplines']['changed'][$key]=true;
                    }
                }
            }
            $this->fillLogs('new', 'disciplines', $auditData['disciplines']['new'], $newQuestionnaire->id, $approvedQuestionnaire->id);
            $this->fillLogs('removed', 'disciplines', $auditData['disciplines']['removed'], $newQuestionnaire->id, $approvedQuestionnaire->id);
            $this->fillLogs('changed', 'disciplines', $auditData['disciplines']['changed'], $newQuestionnaire->id, $approvedQuestionnaire->id);
            
            // Checking For Param Audit
            $oldDisciplineIds = $approvedQuestionnaire->disciplines->pluck('id')->toArray();
            $newDisciplineIds = $newQuestionnaire->disciplines->pluck('id')->toArray();

            $newParams = QuestionnaireDisciplineParam::whereIn('questionnaire_discipline_id', $newDisciplineIds)->pluck('title_hash', 'param_number')->toArray();
            $oldParams = QuestionnaireDisciplineParam::whereIn('questionnaire_discipline_id', $oldDisciplineIds)->pluck('title_hash', 'param_number')->toArray();
            
            $newParamsByOldQues = [];
            if(!empty($newParams)){
                foreach($newParams as $key=>$newP){
                    $newKey = str_replace("QUES-$newQuestionnaire->id", "QUES-$approvedQuestionnaire->id", $key);
                    $newParamsByOldQues[$newKey] = $newP;
                }
            }
            $auditData['parameters']['new'] = array_diff_key($newParamsByOldQues, $oldParams);
            $auditData['parameters']['removed'] = array_diff_key($oldParams, $newParamsByOldQues);
            $auditData['parameters']['changed'] = [];
            if(!empty($newParamsByOldQues)){
                foreach($newParamsByOldQues as $key=>$new){
                    if(!empty($oldParams[$key]) && !hash_equals($new, $oldParams[$key])){
                        $auditData['parameters']['changed'][$key]=true;
                    }
                }
            }
            $this->fillLogs('new', 'parameters', $auditData['parameters']['new'], $newQuestionnaire->id, $approvedQuestionnaire->id);
            $this->fillLogs('removed', 'parameters', $auditData['parameters']['removed'], $newQuestionnaire->id, $approvedQuestionnaire->id);
            $this->fillLogs('changed', 'parameters', $auditData['parameters']['changed'], $newQuestionnaire->id, $approvedQuestionnaire->id);
            
            // Checking for Sections Audit
            $altersSectionsParams = array_keys($auditData['parameters']['changed']);
            $altersSectionsNewParams = [];
            if(!empty($altersSectionsParams)){
                foreach($altersSectionsParams as $sec){
                    array_push($altersSectionsNewParams, str_replace("QUES-$approvedQuestionnaire->id", "QUES-$newQuestionnaire->id", $sec));
                }
            }
            $oldSecSequence = QuestionnaireDisciplineParam::whereIn('param_number', $altersSectionsParams)->pluck('section_sequence', 'id')->toArray();
            $newSecSequence = QuestionnaireDisciplineParam::whereIn('param_number', $altersSectionsNewParams)->pluck('section_sequence','id')->toArray();
            if(!empty($newSecSequence) && !empty($oldSecSequence)){
                $i = 0;
                /***** Parameters to check for some text changes ****/
                $changedSectionParamIds = [];
                /****** Parameters where complete section changes ****/
                $completeParamIds = [];
                foreach($newSecSequence as $nKey=>$newSec){
                    $i++;
                    $j=0;
                    foreach($oldSecSequence as $oKey=>$oldSec){
                        $j++;
                        if($i == $j){
                            if($oldSec == $newSec){
                                // Check for Changeif()
                                array_push($changedSectionParamIds, ['oldParamID'=>$oKey, 'newParamID'=>$nKey]);     
                            }else{
                                array_push($completeParamIds, $nKey);
                                // Log Section has been changed for this parameter.
                            }
                            break;
                        }
                    }
                }
                $checkOldSection = QuestionDiscipParamSection::with('parameter')->whereIn('question_discip_param_id', array_column($changedSectionParamIds, 'oldParamID'))->get()->toArray();
                $checkOldSection = array_combine(array_column($checkOldSection, 'options_hash'), $checkOldSection);
                $checkNewSection = QuestionDiscipParamSection::with('parameter')->whereIn('question_discip_param_id', array_column($changedSectionParamIds, 'newParamID'))->get()->toArray();
                $checkNewSection = array_combine(array_column($checkNewSection, 'options_hash'), $checkNewSection);
                
                $completeParamSections = QuestionDiscipParamSection::with('parameter')->whereIn('question_discip_param_id', $completeParamIds)->get()->toArray();
                
                $sectionChanges  = array_diff_key($checkNewSection, $checkOldSection);
                $changeData = [];
                    
                if(!empty($sectionChanges)){
                    foreach($sectionChanges as $section){
                        if(!empty($checkOldSection[$section['options_hash']]['options'])){
                            $oldValue = $checkOldSection[$section['options_hash']]['options'];
                            $changeData[] = [
                                'paramTitle'=>$section['parameter']['title'],
                                'type'=>$section['type'],
                                'new_value'=>$section['options'],
                                'old_value'=>$oldValue
                            ];
                        }
                    }
                }
                // Insert Section Changes
                $this->fillLogs('completeParam', 'sections', array_column($completeParamSections, 'parameter'), $newQuestionnaire->id, $approvedQuestionnaire->id);
                $this->fillLogs('paramSection', 'sections', $changeData, $newQuestionnaire->id, $approvedQuestionnaire->id);
            }
        }
        return true;
    }
    
    private function fillLogs($action='new', $object_type='disciplines', $data , $newQuestionID, $oldQuestionID)
    {
        if(!empty($data)){
            $logsData = [];

            if($object_type == 'disciplines'){
                $dispNumbers = array_keys($data);
                $oldDispData = QuestionnaireDiscipline::whereIn('disp_number', $dispNumbers)->pluck('title', 'id')->toArray();
                $dispNumbers = str_replace("QUES-$oldQuestionID", "QUES-$newQuestionID", $dispNumbers);
                $newDispData = QuestionnaireDiscipline::whereIn('disp_number', $dispNumbers)->pluck('title', 'id')->toArray();
                $logsData = array_merge_recursive($oldDispData, $newDispData);
            }elseif($object_type == 'parameters'){
                $paramNumbers = array_keys($data);
                $oldParamData = QuestionnaireDisciplineParam::whereIn('questionnaire_discipline_id', $paramNumbers)->pluck('title', 'id')->toArray();
                $paramNumbers = str_replace("QUES-$oldQuestionID", "QUES-$newQuestionID", $paramNumbers);
                $newParamData = QuestionnaireDisciplineParam::whereIn('questionnaire_discipline_id', $paramNumbers)->pluck('title', 'id')->toArray();
                $logsData = array_merge_recursive($oldParamData, $newParamData);
            }elseif($object_type == 'sections'){
                $prepareData = [];
                    
                if($action == 'completeParam'){
                    foreach($data as $d){
                        $newData = [
                            'questionnaireId'=>$newQuestionID,
                            'object_type'=> $object_type,
                            'description'=>__('api.auditlogs.'.$object_type.'.complete_parameter', ['parameter'=> $d['title']]),
                            'created_at'=>date("Y-m-d H:i:s")
                        ];
                        $prepareData[$d['id']] = $newData;
                    }
                }else{
                    foreach($data as $d){
                        $newData = [
                            'questionnaireId'=>$newQuestionID,
                            'object_type'=> $object_type,
                            'old_value'=>$d['old_value'],
                            'new_value'=>$d['new_value'],
                            'description'=>__('api.auditlogs.'.$object_type.'.paramSection', ['parameter'=> $d['paramTitle'], 'section_type'=>$d['type']]),
                            'created_at'=>date("Y-m-d H:i:s")
                        ];
                        $prepareData[] = $newData;
                    }
                }
                
            }
            if($object_type == 'disciplines' || $object_type == 'parameters'){
                $prepareData = [];
                foreach($logsData as $d){
                    if(!empty($d)){
                        $newData = [
                            'questionnaireId'=>$newQuestionID,
                            'object_type'=> $object_type,
                            'description'=>__('api.auditlogs.'.$object_type.'.'.$action),
                            'created_at'=>date("Y-m-d H:i:s")
                        ];
                        if($action == 'new'){
                            $newData['new_value'] = $d; 
                        }elseif($action == 'changed'){
                            if(is_array($d)){
                                $newData['old_value'] = $d[0]; 
                                $newData['new_value'] = $d[1]; 
                            }  
                        }else{
                            $newData['old_value'] = $d;
                        }
                        array_push($prepareData, $newData);
                    }
                }
            }
        }
        if(!empty($prepareData)){
            QuestionnaireAuditLog::insert($prepareData);
        }
        return false;
    }

    /**
     * Function to get parameter
     * @param Array $filters
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function getParameter($parameterID)
    {
        $parameter = QuestionnaireDisciplineParam::where('id', $parameterID)->first();
        return $parameter;
    }

    /**
     * Function to get audit logs
     * @param Array $filters
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function getAuditLogs($questionnaireID)
    {
        return QuestionnaireAuditLog::where('questionnaireId', $questionnaireID)->get();
    }

    /**
     * Function to clone questionnaire
     * @param Array $filters
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function clone(Questionnaire $questionnaire)
    {
        // Clone Questionnaire
        $clonedQuestionnaire = new Questionnaire();
        $checkClones = Questionnaire::where('type', $questionnaire->type)
        ->orderBy('version', 'desc')
        ->first();
        $clonedQuestionnaire->version =$checkClones->version+0.1;
        $clonedQuestionnaire->title = $questionnaire->title;
        $clonedQuestionnaire->type = $questionnaire->type;
        $clonedQuestionnaire->status = Flag::IS_INACTIVE;
        $clonedQuestionnaire->is_approved = Flag::IS_INACTIVE;
        if($clonedQuestionnaire->save()){
            ImportQuestionnaireData::dispatch($questionnaire, $clonedQuestionnaire);
        }
        return $clonedQuestionnaire;
    }

    /**
     * Function to get discipline details by its ID
     * @param Array $filters
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function firstornewDiscipline($disciplineId=null)
    {
        return QuestionnaireDiscipline::where('id', $disciplineId)->firstornew();
    }

    /**
     * Function to get parameter details by its ID
     * @param Array $filters
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function firstornewParameter($parameterId=null)
    {
        return QuestionnaireDisciplineParam::where('id', $parameterId)->firstornew();
    }

    /**
     * Function to get section details by its ID
     * @param Array $filters
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function firstornewSection($sectionId=null)
    {
        return QuestionDiscipParamSection::where('id', $sectionId)->firstornew();
    }

     /**
     * Function to save discipline details 
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function saveDiscipline(QuestionnaireDiscipline $discipline, $data = [])
    {
        $newDiscipline = empty($discipline->id) ? true : false; 
        $discipline->questionnaire_id = !empty($data['questionnaire_id']) ? $data['questionnaire_id'] : null;
        $discipline->title = !empty($data['title']) ? $data['title'] : null;
        if($discipline->save()){
            if($newDiscipline){
                $discipline->disp_number = "QUES-".$data['questionnaire_id']."-DISP-".$discipline->id;
                $discipline->save();
            }
        }
        return $discipline;
    }

    /**
     * Function to save parameter details
     * @param Array $filters
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function saveParameter(QuestionnaireDisciplineParam $parameter, $data = [])
    {
        $newParameter = empty($parameter->id) ? true : false; 
        $parameter->questionnaire_discipline_id = !empty($data['discipline_id']) ? $data['discipline_id'] : null;
        $parameter->title = !empty($data['title']) ? $data['title'] : null;
        if($parameter->save()){
            if($newParameter){
                $parameter->param_number = "QUES-".$data['questionnaire_id']."-PARAM-".$parameter->id;
                $parameter->save();
            }
        }
        return $parameter;
    }

    /**
     * Function to save section details by its ID
     * @param Array $filters
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function saveSection(QuestionDiscipParamSection $section , $data= [])
    {
        $section->question_discip_param_id = !empty($data['parameter_id']) ? $data['parameter_id'] : null;
        $section->type = !empty($data['type']) ? $data['type'] : null;
        $section->options = !empty($data['options']) ? $data['options'] : null;
        $section->isOptional = !empty($data['isOptional']) ? $data['isOptional'] : null;

        return $section;
    }

    /**
     * Function to delete discipline details by its ID
     * @param Array $filters
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function deleteDiscipline(QuestionnaireDiscipline $discipline)
    {
        return $discipline->delete();
    }

    /**
     * Function to delete parameter details by its ID
     * @param Array $filters
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function deleteParameter(QuestionnaireDisciplineParam $parameter)
    {
        return $parameter->delete();
    }

    /**
     * Function to delete section details by its ID
     * @param Array $filters
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function deleteSection(QuestionDiscipParamSection $section)
    {   
        return $section->delete();
    }

    /**
     * Function to submit questionnaire parameter
     * @param Array $filters
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function submitQuestionnaire(Questionnaire $questionnaire)
    {
        $questionnaire->state = QuestionnaireStage::PENDING_APPROVAL;
        $questionnaire->save();
        $drdoAdminEmail = config('core.drdo_admin_email');
        if(!empty($drdoAdminEmail)){
            $drdoAdmin = User::where('email', $drdoAdminEmail)->first();
            if(!empty($drdoAdmin->email)){
                $drdoAdmin->notify(new InformDrdoAboutNewQuestionnaire($questionnaire));
            }
        }
        return $questionnaire;
    }

    /**
     * Function to approve questionnaire 
     * @param Array $filters
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function approveQuestionnaire(Questionnaire $questionnaire, $data){
        if($data['is_approved']){
            $questionnaire->state = QuestionnaireStage::APPROVED;
            $questionnaire->is_approved =1;
            $questionnaire->status = 1;
            $questionnaire->save();
        }else{
            $questionnaire->state = QuestionnaireStage::REJECTED;
            $questionnaire->save();
        }
        
        return $questionnaire;
    }
}
