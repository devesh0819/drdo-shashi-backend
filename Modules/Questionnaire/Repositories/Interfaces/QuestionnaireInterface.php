<?php

namespace Modules\Questionnaire\Repositories\Interfaces;

use Modules\Questionnaire\Entities\Questionnaire;
use Modules\Questionnaire\Entities\QuestionnaireDiscipline;
use Modules\Questionnaire\Entities\QuestionnaireDisciplineParam;
use Modules\Questionnaire\Entities\QuestionDiscipParamSection;

interface QuestionnaireInterface
{

    /**
     * Function to find all questionnaire list
     * @param Array $filters
     * @param Boolean $countOnly
     * 
     * @return Array $data
     */
    public function lists($filters=[], $countOnly=false);

     /**
     * Function to get questionnaire details by its ID
     * @param Array $filters
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function firstornew($questionnaireId=null);

    /**
     * Function to fillAuditLogs for a questionnaire
     * @param Array $filters
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function fillAuditLogs($newQuestionnaire, $approvedQuestionnaire);
    
    /**
     * Function to get parameter
     * @param Array $filters
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function getParameter($parameterID);

    /**
     * Function to get audit logs
     * @param Array $filters
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function getAuditLogs($questionnaireID);

    /**
     * Function to clone questionnaire
     * @param Array $filters
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function clone(Questionnaire $questionnaire);
    
    /**
     * Function to get discipline details by its ID
     * @param Array $filters
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function firstornewDiscipline($disciplineId=null);

    /**
     * Function to get parameter details by its ID
     * @param Array $filters
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function firstornewParameter($parameterId=null);

    /**
     * Function to get section details by its ID
     * @param Array $filters
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function firstornewSection($sectionId=null);

     /**
     * Function to save discipline details 
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function saveDiscipline(QuestionnaireDiscipline $discipline, $data = []);

    /**
     * Function to save parameter details
     * @param Array $filters
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function saveParameter(QuestionnaireDisciplineParam $parameter, $data = []);

    /**
     * Function to save section details by its ID
     * @param Array $filters
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function saveSection(QuestionDiscipParamSection $section , $data= []);

    /**
     * Function to delete discipline details by its ID
     * @param Array $filters
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function deleteDiscipline(QuestionnaireDiscipline $discipline);

    /**
     * Function to delete parameter details by its ID
     * @param Array $filters
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function deleteParameter(QuestionnaireDisciplineParam $parameter);

    /**
     * Function to delete section details by its ID
     * @param Array $filters
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function deleteSection(QuestionDiscipParamSection $section);

     /**
     * Function to submit questionnaire parameter
     * @param Array $filters
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function submitQuestionnaire(Questionnaire $questionnaire);

    /**
     * Function to approve questionnaire 
     * @param Array $filters
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function approveQuestionnaire(Questionnaire $questionnaire, $data);

}
