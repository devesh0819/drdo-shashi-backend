<?php

namespace Modules\Questionnaire\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Questionnaire\Entities\QuestionnaireDisciplineParam;

class QuestionDiscipParamSection extends Model 
{
    protected $table = "question_discip_param_sections";

    protected $fillable = ['question_discip_param_id', 'type', 'options', 'isOptional', 'options_hash'];
    protected $hidden = ['status', 'deleted_at', 'created_at', 'updated_at', 'last_modified_by'];

    public function parameter()
    {
        return $this->belongsTo(QuestionnaireDisciplineParam::class, 'question_discip_param_id', 'id');
    }
}
