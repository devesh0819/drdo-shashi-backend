<?php

namespace Modules\Questionnaire\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Questionnaire\Entities\Interfaces\QuestionnaireInterface;
use Modules\Questionnaire\Entities\Traits\QuestionnaireTrait;
use Modules\Questionnaire\Entities\QuestionnaireDiscipline;

class Questionnaire extends Model implements QuestionnaireInterface
{
    use QuestionnaireTrait;

    protected $table = "questionnaires";
    
    protected $fillable = ['title', 'type', 'status'];
    
    protected $appends = [];
    
    protected $hidden = ['status', 'deleted_at', 'created_at', 'updated_at', 'last_modified_by'];
    
    protected $with = ['disciplines'];

    public function disciplines()
    {
        return $this->hasMany(QuestionnaireDiscipline::class, 'questionnaire_id');
    }
}
