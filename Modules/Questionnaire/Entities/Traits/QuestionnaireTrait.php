<?php

namespace Modules\Questionnaire\Entities\Traits;

trait QuestionnaireTrait
{

    /**
     * Getters
     */
    public function getID()
    {
        return !empty($this->{$this::ID}) ? $this->{$this::ID} : null;
    }
 
    public function getTitle()
    {
        return !empty($this->{$this::TITLE}) ? $this->{$this::TITLE} : null;
    }

    public function getType()
    {
        return !empty($this->{$this::TYPE}) ? $this->{$this::TYPE} : null;
    }

    public function getVersion()
    {
        return !empty($this->{$this::VERSION}) ? $this->{$this::VERSION} : null;
    }

    public function getStatus()
    {
        return !empty($this->{$this::STATUS}) ? $this->{$this::STATUS} : null;
    }

    public function getLastModifiedBy()
    {
        return !empty($this->{$this::LAST_MODIFIED_BY}) ? $this->{$this::LAST_MODIFIED_BY} : null;
    }   

    public function getCreatedAt()
    {
        return !empty($this->{$this::CREATED_DATETIME}) ? date("Y-m-d H:i:s", strtotime($this->{$this::CREATED_DATETIME})) : null;
    }

    public function getUpdatedAt()
    {
        return !empty($this->{$this::UPDATED_DATETIME}) ? date("Y-m-d H:i:s", strtotime($this->{$this::UPDATED_DATETIME})) : null;
    }

    public function getDeletedAt()
    {
        return !empty($this->{$this::DELETED_AT}) ? date("Y-m-d H:i:s", strtotime($this->{$this::DELETED_AT})) : null;
    }

}
