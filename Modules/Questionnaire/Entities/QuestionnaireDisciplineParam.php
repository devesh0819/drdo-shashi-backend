<?php

namespace Modules\Questionnaire\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Questionnaire\Entities\QuestionDiscipParamSection;

class QuestionnaireDisciplineParam extends Model 
{
    protected $table = "questionnaire_discipline_params";
    
    protected $fillable = ['title', 'questionnaire_discipline_id', 'status', 'section_sequence'];
    
    protected $appends = ['section_p_count', 'section_d_count', 'section_m_count'];
    
    protected $hidden = ['status', 'deleted_at', 'created_at', 'updated_at', 'last_modified_by', 'title_hash'];
    protected $with = ['sections'];

    public function sections()
    {
        return $this->hasMany(QuestionDiscipParamSection::class, 'question_discip_param_id');
    }

    public function getSectionPCountAttribute()
    {
        return $this->sections()->where('type', 'P')->count();
    }

    public function getSectionDCountAttribute()
    {
        return $this->sections()->where('type', 'D')->count();
    }

    public function getSectionMCountAttribute()
    {
        return $this->sections()->where('type', 'M')->count();
    }
}
