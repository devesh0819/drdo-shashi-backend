<?php

namespace Modules\Questionnaire\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Questionnaire\Entities\QuestionnaireDisciplineParam;

class QuestionnaireDiscipline extends Model 
{
    protected $table = "questionnaire_disciplines";
    
    protected $fillable = ['title', 'questionnaire_id', 'status'];
    
    protected $appends = [];
    
    protected $hidden = ['status', 'deleted_at', 'created_at', 'updated_at', 'last_modified_by', 'title_hash'];
    
    protected $with = ['parameters'];

    public function parameters()
    {
        return $this->hasMany(QuestionnaireDisciplineParam::class, 'questionnaire_discipline_id');
    }
}
