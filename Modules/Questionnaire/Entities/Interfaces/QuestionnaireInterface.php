<?php

namespace Modules\Questionnaire\Entities\Interfaces;

interface QuestionnaireInterface
{

    const ID = 'id';
    const TITLE = 'title';
    const TYPE = 'type';
    const STATUS = 'status';
    const VERSION = 'version';
    const DELETED_AT = 'deleted_at';
    const CREATED_DATETIME = 'created_at';
    const UPDATED_DATETIME = 'updated_at';
    const LAST_MODIFIED_BY = 'last_modified_by';
    

    /**
     * Getters
     */
    public function getID();

    public function getTitle();

    public function getType();

    public function getVersion();

    public function getStatus();

    public function getLastModifiedBy();
    
    public function getCreatedAt();

    public function getDeletedAt();

    public function getUpdatedAt();
}
