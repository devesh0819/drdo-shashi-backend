<?php

namespace Modules\Questionnaire\Entities;

use Illuminate\Database\Eloquent\Model;

class QuestionnaireAuditLog extends Model 
{
    
    protected $table = "questionnaires_audit_logs";
}
