<?php

namespace Modules\Questionnaire\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Modules\User\Entities\User;
use Modules\Questionnaire\Services\QuestionnaireService;

class QuestionnairePolicy
{
    use HandlesAuthorization;
    private $questionnaire;
    private $questionnaireService;
    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct(QuestionnaireService $questionnaireService)
    {
        $this->questionnaireService = $questionnaireService;
        if(request()->route('questionnaireId')){
            $this->questionnaire = $this->questionnaireService->firstornew(request()->route('questionnaireId'));
        }
    }

    public function addDiscipline(User $user)
    {
       // return !$this->questionnaire->
    }

    public function updateDiscipline(User $user)
    {

    }

    public function deleteDiscipline(User $user)
    {

    }

    public function addParameter(User $user)
    {

    }

    public function updateParameter(User $user)
    {

    }

    public function deleteParameter(User $user)
    {

    }

    public function addSection(User $user)
    {

    }

    public function updateSection(User $user)
    {

    }

    public function deleteSection(User $user)
    {

    }

}
