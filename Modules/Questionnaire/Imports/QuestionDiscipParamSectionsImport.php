<?php

namespace Modules\Questionnaire\Imports;

use Maatwebsite\Excel\Concerns\ToModel;
use Modules\Questionnaire\Entities\QuestionnaireDisciplineParam;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Excel;
use Modules\Core\Enums\Flag;
use Modules\Questionnaire\Entities\QuestionDiscipParamSection;


class QuestionDiscipParamSectionsImport implements ToModel, WithHeadingRow, WithChunkReading
{
    private $questionnaireId;
    public $parameterId;

    private $paramNumberPrefix;

    public function __construct($questionnaireId)
    {
        $this->questionnaireId = $questionnaireId;
        $this->paramNumberPrefix = "QUES-$this->questionnaireId-PARAM-";
    }

    /**
     * Import Meta Tags From a CSV
     * @param array $row
     * @return ObjectMetaTag
     */
    public function model(array $row)
    {
        if(!empty($row['p_s_n'])){
            $paramNumber = $this->paramNumberPrefix.$row['p_s_n'];
            
            $parameter =  QuestionnaireDisciplineParam::where('param_number', $paramNumber)->first();
            $options = json_encode([
                'bronze'=>$row['bronze'],
                'silver'=>$row['silver'],
                'gold'=>$row['gold'],
                'diamond'=>$row['diamond'],
                'platinum'=>$row['platinum']
            ]);
            if(!empty($parameter)){
                $parameter->section_sequence = !empty($parameter->section_sequence) ? $parameter->section_sequence."-".$row['pdm_20_503_0'] : $row['pdm_20_503_0'];
                $parameter->save();
            }
            $hash = hash('sha1', $options);
            if(!empty($parameter->id) && !empty($row['pdm_20_503_0'])){
                return new QuestionDiscipParamSection(
                    [
                        'question_discip_param_id'=>$parameter->id,
                        'type'=>$row['pdm_20_503_0'],
                        'options'=>$options,
                        'options_hash'=>$hash,
                        'isOptional'=>Flag::IS_INACTIVE
                    ]
                );
            }          
        }
    }

    /**
     * Size of chunk to be imported once from excel sheet
     */
    public function chunkSize(): int
    {
        return config('questionnaire.chunkSize');
    }
}
