<?php

namespace Modules\Questionnaire\Imports;

use Maatwebsite\Excel\Concerns\ToModel;
use Modules\Questionnaire\Entities\QuestionnaireDiscipline;
use Modules\Questionnaire\Entities\QuestionnaireDisciplineParam;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Excel;
use Modules\Core\Enums\Flag;
use Modules\Questionnaire\Entities\Questionnaire;

class QuestionnaireDisciplineParamsImport implements ToModel, WithHeadingRow, WithChunkReading
{
    private $questionnaireId;
    public $disciplineId;
    private $paramNumberPrefix;
    private $dispNumberPrefix;
   
    public function __construct($questionnaireId)
    {
        $this->questionnaireId = $questionnaireId;
        $this->dispNumberPrefix = "QUES-$this->questionnaireId-DISP-";
        $this->paramNumberPrefix = "QUES-$this->questionnaireId-PARAM-";
    }
    /**
     * Import Meta Tags From a CSV
     * @param array $row
     * @return ObjectMetaTag
     */
    public function model(array $row)
    {
        if(!empty($row['s_n']) && !empty($row['p_s_n']) && !empty($row['parameter'])){
            $dispNumber = $this->dispNumberPrefix.$row['s_n'];
            $discipline =  QuestionnaireDiscipline::where('disp_number', $dispNumber)->first();
            if(!empty($discipline)){
                //Check If Parameter exist
                $parameterNumber = $this->paramNumberPrefix.$row['p_s_n'];
                $parameter = QuestionnaireDisciplineParam::where('param_number', $parameterNumber)->firstornew();
                $parameter->questionnaire_discipline_id = $discipline->id;
                $parameter->param_number = $parameterNumber;
                if(!empty($row['parameter'])){
                    $parameter->title = $row['parameter'];
                    $parameter->title_hash = hash( 'sha1', $row['parameter']);
                }
                $parameter->status = Flag::IS_ACTIVE;
                return $parameter;
            }          
        }
    }

    /**
     * Size of chunk to be imported once from excel sheet
     */
    public function chunkSize(): int
    {
        return config('questionnaire.chunkSize');
    }
}
