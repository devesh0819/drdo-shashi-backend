<?php

namespace Modules\Questionnaire\Imports;

use Maatwebsite\Excel\Concerns\ToModel;
use Modules\Questionnaire\Entities\QuestionnaireDiscipline;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Excel;
use Modules\Questionnaire\Entities\Questionnaire;

class QuestionnaireDisciplinesImport implements ToModel, WithHeadingRow, WithChunkReading
{
    private $questionnaireId;
    private $dispNumberPrefix;
   
    public function __construct($questionnaireId)
    {
        $this->questionnaireId = $questionnaireId; 
        $this->dispNumberPrefix = "QUES-$this->questionnaireId-DISP-";
    }
    /**
     * Import Meta Tags From a CSV
     * @param array $row
     * @return ObjectMetaTag
     */
    public function model(array $row)
    {   
        if(!empty($row['s_n'])){
            $dispNumber = $this->dispNumberPrefix.$row['s_n'];
            $discipline = QuestionnaireDiscipline::where('disp_number', $dispNumber)->firstornew();
            
            if(!empty($row['discipline'])){
                $discipline->title = $row['discipline'];
                $discipline->title_hash = hash( 'sha1', $row['discipline']);
            }
            $discipline->disp_number = $dispNumber;
            $discipline->questionnaire_id = $this->questionnaireId;
            return $discipline;
        }
    }

    /**
     * Size of chunk to be imported once from excel sheet
     */
    public function chunkSize(): int
    {
        return config('questionnaire.chunkSize');
    }
}
