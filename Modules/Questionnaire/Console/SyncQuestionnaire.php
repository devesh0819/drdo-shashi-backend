<?php

namespace Modules\Questionnaire\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Exception;
use Modules\Core\Helpers\Logger;
use Excel;
use Illuminate\Http\UploadedFile;
use Modules\Questionnaire\Entities\Questionnaire;
use Modules\Questionnaire\Imports\QuestionnaireDisciplinesImport;
use Modules\Questionnaire\Imports\QuestionnaireDisciplineParamsImport;
use Modules\Questionnaire\Imports\QuestionDiscipParamSectionsImport;
use Modules\Core\Enums\Flag;
use Modules\Questionnaire\Services\QuestionnaireService;
use Illuminate\Support\Facades\DB;

class SyncQuestionnaire extends Command
{
    private $questionnaireService;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'sync:questionnaire {title} {type} {--filePath=} {--questionnaireID=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to sync questionnaire into the database.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(QuestionnaireService $questionnaireService)
    {
        $this->questionnaireService = $questionnaireService;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $filePath = $this->option('filePath');
            $questionnaireID = $this->option('questionnaireID');
            if($filePath instanceof UploadedFile){
                $filePath = $filePath;
            }else if (!file_exists($filePath) || !is_readable($filePath)){
                throw new Exception('Unable to access file');
            }
            DB::beginTransaction();
            if(!empty($questionnaireID)){
                $questionnaire = Questionnaire::where('id', $questionnaireID)->first();
            }else{
                $type = $this->argument('type') ? $this->argument('type') : 'S&M';
                $title = $this->argument('title') ? $this->argument('title')."-".date("YMDHIS") : 'QCI-Questionnaire'."-".date("YMDHIS"); 
            
                $checkQuestionnaire = Questionnaire::where('type', $type)
                ->orderBy('version', 'desc')
                ->first();
                $version = !empty($checkQuestionnaire->version) ? $checkQuestionnaire->version+0.1 : "1.0";
                $questionnaire =  new Questionnaire();
                $questionnaire->version = $version;
                $questionnaire->title = $title;
                $questionnaire->type = $type;
                $questionnaire->status = Flag::IS_INACTIVE;
                $questionnaire->is_approved = Flag::IS_INACTIVE;
            }
            $questionnaire->save();
            if(!empty($questionnaire->id)){
                $approvedQuestionnaire = Questionnaire::where('type', $type)
                ->where('is_approved', 1)
                ->orderBy('version', 'desc')
                ->first();
                $this->info("Importing Questionnaire Disciplines...");
                $disciplines = Excel::import(new QuestionnaireDisciplinesImport($questionnaire->id), $filePath);
                $this->info("Importing Questionnaire Parameters...");
                $parameters =   Excel::import(new QuestionnaireDisciplineParamsImport($questionnaire->id), $filePath);
                $this->info("Importing Questionnaire Sections...");
                $sections =   Excel::import(new QuestionDiscipParamSectionsImport($questionnaire->id), $filePath);
                $this->info("Questionaire imported successfully");
                if(!empty($questionnaire) && !empty($approvedQuestionnaire)){
                    $this->questionnaireService->fillAuditLogs($questionnaire, $approvedQuestionnaire);
                }
            }
            DB::commit();
            return true;
        } catch (Exception $ex) {
            DB::rollBack();
            Logger::error($ex);
            return $ex;
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['title', InputArgument::REQUIRED, 'Title of Questionaire'],
            ['type', InputArgument::REQUIRED, 'Type of Questionaire'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['filePath', null, InputOption::VALUE_OPTIONAL, 'path of Excel file to be imported.', null],
            ['questionnaireID', null, InputOption::VALUE_OPTIONAL, "ID of Questionnaire to be updated"]
        ];
    }
}
