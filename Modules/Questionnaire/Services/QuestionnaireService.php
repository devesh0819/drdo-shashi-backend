<?php

namespace Modules\Questionnaire\Services;

use Modules\Questionnaire\Repositories\Interfaces\QuestionnaireInterface;
use Exception;
use Modules\Core\Helpers\Logger;
use Modules\Questionnaire\Entities\Questionnaire;
use Modules\Questionnaire\Entities\QuestionnaireDiscipline;
use Modules\Questionnaire\Entities\QuestionnaireDisciplineParam;
use Modules\Questionnaire\Entities\QuestionDiscipParamSection;

class QuestionnaireService
{
    private $repository;

    /**
     * Constructor
     * @param QuestionnaireInterface $repository
     */
    public function __construct(QuestionnaireInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Function to find all users list
     * @param Array $filters
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function lists($filters=[], $countOnly=false)
    {
        
        try {
            return $this->repository->lists($filters, $countOnly);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

    /**
     * Function to get questionnaire details by its ID
     * @param Array $filters
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function firstornew($questionnaireId=null){
        try {
            return $this->repository->firstornew($questionnaireId);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

    /**
     * Function to fillAuditLogs for a questionnaire
     * @param Array $filters
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function fillAuditLogs($newQuestionnaire, $approvedQuestionnaire){
        return $this->repository->fillAuditLogs($newQuestionnaire, $approvedQuestionnaire);
    }
    
    /**
     * Function to get audit logs
     * @param Array $filters
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function getAuditLogs($questionnaireID){
        try {
            return $this->repository->getAuditLogs($questionnaireID);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

    /**
     * Function to get parameter
     * @param Array $filters
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function getParameter($parameterID){
        try {
            return $this->repository->getParameter($parameterID);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

    /**
     * Function to submit questionnaire parameter
     * @param Array $filters
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function submitQuestionnaire(Questionnaire $questionnaire){
        try {
            return $this->repository->submitQuestionnaire($questionnaire);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

    /**
     * Function to approve questionnaire 
     * @param Array $filters
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function approveQuestionnaire(Questionnaire $questionnaire, $data){
        try {
            return $this->repository->approveQuestionnaire($questionnaire, $data);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

    /**
     * Function to clone questionnaire
     * @param Array $filters
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function clone(Questionnaire $questionnaire){
        try {
            return $this->repository->clone($questionnaire);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

    /**
     * Function to get discipline details by its ID
     * @param Array $filters
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function firstornewDiscipline($disciplineId=null){
        try {
            return $this->repository->firstornewDiscipline($disciplineId);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

    /**
     * Function to get parameter details by its ID
     * @param Array $filters
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function firstornewParameter($parameterId=null){
        try {
            return $this->repository->firstornewParameter($parameterId);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

    /**
     * Function to get section details by its ID
     * @param Array $filters
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function firstornewSection($sectionId=null){
        try {
            return $this->repository->firstornewSection($sectionId);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

    /**
     * Function to save discipline details 
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function saveDiscipline(QuestionnaireDiscipline $discipline, $data = []){
        try {
            return $this->repository->saveDiscipline($discipline, $data);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

    /**
     * Function to save parameter details
     * @param Array $filters
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function saveParameter(QuestionnaireDisciplineParam $parameter, $data = []){
        try {
            return $this->repository->firstornewParameter($parameter, $data);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

    /**
     * Function to save section details by its ID
     * @param Array $filters
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function saveSection(QuestionDiscipParamSection $section , $data= []){
        try {
            return $this->repository->firstornewSection($section, $data);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

    /**
     * Function to delete discipline details by its ID
     * @param Array $filters
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function deleteDiscipline(QuestionnaireDiscipline $discipline){
        try {
            return $this->repository->deleteDiscipline($discipline);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

    /**
     * Function to delete parameter details by its ID
     * @param Array $filters
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function deleteParameter(QuestionnaireDisciplineParam $parameter){
        try {
            return $this->repository->deleteParameter($parameter);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

    /**
     * Function to delete section details by its ID
     * @param Array $filters
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function deleteSection(QuestionDiscipParamSection $section){
        try {
            return $this->repository->deleteSection($section);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }
}
