<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         // Questionnaires Table
         if (!Schema::hasTable('questionnaires_audit_logs')) {
            Schema::create('questionnaires_audit_logs', function (Blueprint $table) {
                $table->id();
                $table->unsignedInteger('questionnaireId');
                $table->string('object_type', 255);
                $table->text('old_value', 255)->nullable();
                $table->text('new_value', 255)->nullable();
                $table->string('description', 255)->nullable();
                $table->softDeletes();
                $table->unsignedInteger('last_modified_by')->nullable();
                $table->timestamp('created_at')->useCurrent();
                $table->timestamp('updated_at')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questionnaires_audit_logs');
    }
};
