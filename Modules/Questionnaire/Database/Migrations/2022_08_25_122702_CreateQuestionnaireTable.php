<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Questionnaires Table
        if (!Schema::hasTable('questionnaires')) {
            Schema::create('questionnaires', function (Blueprint $table) {
                $table->id();
                $table->float('version')->default(1.0);
                $table->string('title', 255);
                $table->enum('type', ['S&M', 'L'])->default('S&M');
                $table->tinyInteger('status')->default(1);
                $table->enum('state',['draft', 'submitted', 'approval_pending', 'approved', 'rejected'])->default('draft');
                $table->tinyInteger('is_approved')->default(0);
                $table->softDeletes();
                $table->unsignedInteger('last_modified_by')->nullable();
                $table->timestamp('created_at')->useCurrent();
                $table->timestamp('updated_at')->nullable();
            });
        }

        // Questionnaire Disciplines Table
        if (!Schema::hasTable('questionnaire_disciplines')) {
            Schema::create('questionnaire_disciplines', function (Blueprint $table) {
                $table->id();
                $table->foreignId('questionnaire_id')
                    ->constrained()
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
                $table->string('disp_number', 255)->unique();
                $table->string('title', 255);
                $table->text('title_hash');
                $table->tinyInteger('status')->default(1);
                $table->softDeletes();
                $table->unsignedInteger('last_modified_by')->nullable();
                $table->timestamp('created_at')->useCurrent();
                $table->timestamp('updated_at')->nullable();
            });
        }

        // Questionnaire Parameters Table
        if (!Schema::hasTable('questionnaire_discipline_params')) {
            Schema::create('questionnaire_discipline_params', function (Blueprint $table) {
                $table->id();
                $table->unsignedInteger('questionnaire_discipline_id');
                $table->string('param_number', 255)->unique();
                $table->string('title', 255);
                $table->text('title_hash');
                $table->text('section_sequence')->nullable();
                $table->tinyInteger('status')->default(1);
                $table->softDeletes();
                $table->unsignedInteger('last_modified_by')->nullable();
                $table->timestamp('created_at')->useCurrent();
                $table->timestamp('updated_at')->nullable();
            });
        }

         // Questionnaire Parameters Sections Table
         if (!Schema::hasTable('question_discip_param_sections')) {
            Schema::create('question_discip_param_sections', function (Blueprint $table) {
                $table->id();
                $table->unsignedInteger('question_discip_param_id');
                $table->enum('type', ["P", "D", "M"])->default("P");
                $table->jsonb('options')->nullable();
                $table->text('options_hash')->nullable();
                $table->tinyInteger('isOptional')->default(0);
                $table->softDeletes();
                $table->unsignedInteger('last_modified_by')->nullable();
                $table->timestamp('created_at')->useCurrent();
                $table->timestamp('updated_at')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('question_discip_param_sections');
        Schema::dropIfExists('questionnaire_discipline_params');
        Schema::dropIfExists('questionnaire_disciplines');
        Schema::dropIfExists('questionnaires');
    }
};
