<?php

namespace Modules\Questionnaire\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Modules\Questionnaire\Entities\Questionnaire;
use Exception;
use Modules\Core\Helpers\Logger;
use Modules\Questionnaire\Entities\QuestionnaireDiscipline;
use Modules\Questionnaire\Entities\QuestionnaireDisciplineParam;
use Modules\Questionnaire\Entities\QuestionDiscipParamSection;

class ImportQuestionnaireData implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $questionnaire;
    private $cloneQuestionnaire;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Questionnaire $questionnaire, Questionnaire $cloneQuestionnaire)
    {
        $this->questionnaire = $questionnaire;
        $this->cloneQuestionnaire  = $cloneQuestionnaire;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try{
            if(!empty($this->questionnaire->disciplines)){
                $disciplineCount = 1;
                $parameterCount =1;
                        
                foreach($this->questionnaire->disciplines as $discipline){
                    $cloneDiscipline = new QuestionnaireDiscipline();
                    $cloneDiscipline->questionnaire_id = $this->cloneQuestionnaire->id;
                    $cloneDiscipline->disp_number = "QUES-".$this->cloneQuestionnaire->id."-DISP-".$disciplineCount+1;
                        
                    $cloneDiscipline->title = $discipline->title;
                    $cloneDiscipline->status = $discipline->status;
                    if($cloneDiscipline->save()){
                        $disciplineCount++;
                        // Clone Paramter
                        if(!empty($discipline->parameters)){
                            foreach($discipline->parameters as $parameter){
                                $cloneParameter = new QuestionnaireDisciplineParam();
                                $cloneParameter->questionnaire_discipline_id = $cloneDiscipline->id;
                                $cloneParameter->param_number = "QUES-".$this->cloneQuestionnaire->id."-PARAM-".$parameterCount;
                                $cloneParameter->title = $parameter->title;
                                $cloneParameter->status = $parameter->status;
                                if($cloneParameter->save()){
                                    $parameterCount++;
                                    // Clone Sections
                                    if(!empty($parameter->sections)){
                                        foreach($parameter->sections as $section){
                                            $cloneSection = new QuestionDiscipParamSection();
                                            $cloneSection->question_discip_param_id = $cloneParameter->id;
                                            $cloneSection->type = $section->type;
                                            $cloneSection->options = $section->options;
                                            $cloneSection->isOptional = $section->isOptional;
                                            $cloneSection->save();
                                        }
                                    }
                                }
                            }
                        }    
                    }
                }
            }
            return true;
        }catch(Exception $ex){
            Logger::error($ex);
            return false;
        }
    }
}
