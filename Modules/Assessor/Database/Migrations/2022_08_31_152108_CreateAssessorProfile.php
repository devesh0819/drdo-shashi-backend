<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('assessor_meta')) {
            Schema::create('assessor_meta', function (Blueprint $table) {
                $table->id();
                $table->foreignId('user_id')
                    ->constrained()
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
                $table->unsignedInteger('agency_id');
                $table->string('expertise', 255)->nullable();
                
                $table->unsignedInteger('assessor_state')->nullable();
                $table->unsignedInteger('assessor_district')->nullable();
                $table->text('assessor_certificate')->nullable();
                $table->enum('assessor_status', ['active', 'onhold', 'blocked'])->default('active');
                $table->string('work_domain', 255)->nullable();
                $table->string('years_of_experience', 55)->nullable();
                $table->text("assessor_status_comment")->nullable();
                $table->unsignedInteger('resume_id')->nullable();
                
                $table->text('address')->nullable();   
                $table->unsignedInteger('last_modified_by')->nullable();
                $table->softDeletes();
                $table->timestamp('created_at')->useCurrent();
                $table->timestamp('updated_at')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assessor_meta');
    }
};
