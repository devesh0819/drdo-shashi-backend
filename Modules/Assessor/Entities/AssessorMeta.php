<?php

namespace Modules\Assessor\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Assessor\Entities\Interfaces\AssessorMetaInterface;
use Modules\Assessor\Entities\Traits\AssessorMetaTrait;
use Modules\Core\Entities\State;
use Modules\Core\Entities\District;
use Modules\Core\Helpers\BasicHelper;
use Modules\Attachment\Entities\Attachment;

class AssessorMeta extends Model implements AssessorMetaInterface
{

    use AssessorMetaTrait;

    protected $table = "assessor_meta";
    
    protected $fillable = [
        "address","expertise", "agency_id",
        "assessor_state", "assessor_district","assessor_certificate",
        "assessor_status", "work_domain", "years_of_experience",
         "assessor_status_comment", "resume_id", "expertise"
    ];
    
    protected $appends = ['resume_url'];
    
    protected $with = ['assessorState', 'assessorDistrict'];

    protected $hidden = ['id', 'user_id', 'last_modified_by', 'created_at', 'updated_at', 'deleted_at'];
    
    
    /**
     * FUnction to get Assessor URL
     */
    public function getResumeUrlAttribute()
    {
        return !empty($this->assessorResume->path) ? BasicHelper::getFileURL($this->assessorResume) : null;
    }

    /**
     * Get the user that owns the assessor Meta.
     */
    public function user()
    {
        return $this->belongsTo('Modules\User\Entities\User');
    }

    /**
     * Get the agency that owns the assessor.
     */
    public function agency()
    {
        return $this->belongsTo('Modules\User\Entities\User');
    }

    public function assessorState()
    {
        return $this->belongsTo(State::class, 'assessor_state');
    }

    public function assessorDistrict()
    {
        return $this->belongsTo(District::class, 'assessor_district');
    }

    public function assessorResume()
    {
        return $this->belongsTo(Attachment::class, 'resume_id' , 'id');
    }
}
