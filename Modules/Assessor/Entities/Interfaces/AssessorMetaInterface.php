<?php

namespace Modules\Assessor\Entities\Interfaces;

interface AssessorMetaInterface
{

    const ID = 'id';
    const USER_ID = 'user_id';
    const AGENCY_ID = 'agency_id';
    const EXPERTISE = 'expertise';
    const CREATED_DATETIME = 'created_at';
    const UPDATED_DATETIME = 'updated_at';
    const ADDRESS = 'address';

    /**
     * Getters
     */
    public function getID();

    public function getUserId();

    public function getAgencyId();

    public function getExpertise();

    public function getAddress();

    public function getCreatedAt();

    public function getUpdatedAt();
}
