<?php

namespace Modules\Assessor\Entities\Traits;

trait AssessorMetaTrait
{

    public function getID()
    {
        return !empty($this->{$this::ID}) ? $this->{$this::ID} : null;
    }

    public function getUserId()
    {
        return !empty($this->{$this::USER_ID}) ? $this->{$this::USER_ID} : null;
    }

    public function getAgencyId()
    {
        return !empty($this->{$this::AGENCY_ID}) ? $this->{$this::AGENCY_ID} : null;
    }

    public function getExpertise()
    {
        return !empty($this->{$this::EXPERTISE}) ? $this->{$this::EXPERTISE} : null;
    }

    public function getAddress()
    {
        return !empty($this->{$this::ADDRESS}) ? $this->{$this::ADDRESS} : null;
    }

    public function getCreatedAt()
    {
        return !empty($this->{$this::CREATED_DATETIME}) ? date("Y-m-d H:i:s", strtotime($this->{$this::CREATED_DATETIME})) : null;
    }

    public function getUpdatedAt()
    {
        return !empty($this->{$this::UPDATED_DATETIME}) ? date("Y-m-d H:i:s", strtotime($this->{$this::UPDATED_DATETIME})) : null;
    }
}
