<?php

namespace Modules\Payment\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Modules\Payment\Events\NewOrderCreated;
use Modules\Payment\Listeners\NewOrderListener;
use Modules\Payment\Events\PaymentOrderSuccess as PaymentOrderSuccessEvent;
use Modules\Payment\Listeners\PaymentOrderSuccess;
use Modules\Payment\Events\PaymentOrderFailed as PaymentOrderFailedEvent;
use Modules\Payment\Listeners\PaymentOrderFailed;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        NewOrderCreated::class=>[
            NewOrderListener::class
        ],
        PaymentOrderSuccessEvent::class=>[
            PaymentOrderSuccess::class
        ],
        PaymentOrderFailedEvent::class=>[
            PaymentOrderFailed::class
        ]
    ];
}
