<?php

namespace Modules\Payment\Events;

use Illuminate\Queue\SerializesModels;
use Modules\Payment\Entities\PaymentOrder;
use Illuminate\Foundation\Events\Dispatchable;
use Exception;
use Modules\Core\Helpers\Logger;

class PaymentOrderSuccess
{
    use SerializesModels;
    use Dispatchable;

    public $order;
    public $response;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(PaymentOrder $paymentOrder, $response= [])
    {
        $this->order = $paymentOrder;
        $this->response = $response;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
