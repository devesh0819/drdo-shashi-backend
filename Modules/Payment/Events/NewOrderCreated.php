<?php

namespace Modules\Payment\Events;

use Illuminate\Queue\SerializesModels;
use Modules\Application\Entities\Application;
use Illuminate\Foundation\Events\Dispatchable;

class NewOrderCreated
{
    use SerializesModels;
    use Dispatchable;

    public $application;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Application $application)
    {
        $this->application = $application;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
