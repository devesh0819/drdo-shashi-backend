<?php

return [
    'name' => 'Payment',
    'gateway'=>'CCAvenue',//others
    'gst_percent'=>env("GST_PERCENT", 18),
    'ccavenue'=>[
        'testResponse'=>env('CCENV_TEST_RESPONSE', ''),
        'testMode'=>env('CCENV_TEST', true),
        'workingKey'=>env('CCENV_WK'),
        'accessCode'=>env('CCENV_AK'),
        'merchantId'=>env('CCENV_MID'),
        'currency'=>env('CCENV_CURRENCY', 'INR'),
        'redirect_url'=>env('CCENV_REDIRECT_URL', 'https://qci-api.cloudzmall.com/api/v1/applicant/payment/success'),
        'cancel_url'=>env('CCENV_CANCEL_URL', 'https://qci-api.cloudzmall.com/api/v1/applicant/payment/cancel'),
        'language'=>env('CCENV_LANG', 'EN')
    ]
];
