<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('payment_orders')) {
            Schema::create('payment_orders', function (Blueprint $table) {
                $table->id();
                $table->unsignedInteger('user_id');
                $table->unsignedInteger('application_id');
                $table->tinyInteger('plan_id');
                $table->integer('gst_percent')->nullable();
                $table->integer('tax_percent')->nullable();
                $table->string('assessor_fee', 55)->nullable();
                $table->string('order_number')->unique();
                $table->jsonb('payment_breakup')->nullable();
                $table->string('amount', 55);
                $table->string('final_amount', 55)->nullable();
                $table->float('discount')->nullable();
                $table->enum('status',['Pending', 'Complete']);
                $table->timestamps();
            });
        }
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_orders');
    }
};
