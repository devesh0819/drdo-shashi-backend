<?php

namespace Modules\Payment\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Core\Entities\AssessmentPlan;
use Modules\Application\Entities\Application;
use Modules\User\Entities\UserProfile;

class PaymentOrder extends Model
{

    protected $table = "payment_orders";
    
    protected $fillable = [
    ];
    protected $with=['user'];
    
    protected $appends = [];
    
    protected $hidden = [];
    
    public function assessmentPlan()
    {
        return $this->hasOne(AssessmentPlan::class, 'enterprise_type', 'enterprise_type');
    }        

    public function application()
    {
        return $this->belongsTo(Application::class, 'application_id');
    }

    public function user()
    {
        return $this->hasOne(UserProfile::class, 'user_id');
    }
}
