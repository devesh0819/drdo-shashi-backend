<?php

namespace Modules\Payment\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Payment\Entities\PaymentOrder;

class PaymentTransaction extends Model
{

    protected $table = "payment_transactions";
    
    public function order()
    {
        return $this->belongsTo(PaymentOrder::class, 'order_id');
    }
}
