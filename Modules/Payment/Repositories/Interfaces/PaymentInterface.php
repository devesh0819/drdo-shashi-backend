<?php

namespace Modules\Payment\Repositories\Interfaces;

use Modules\User\Entities\User;
use Modules\Application\Entities\Application;

interface PaymentInterface
{

    /**
     * Function to initiate a new Payment
     * @param Application $application
     * @param Array $data
     * 
     * @return Array $data
     */
    public function inititate(Application $application, $data=[]);

    public function createOrder(Application $application);

     /**
     * Function to complete order after successful Payment
     * @param Array $data
     * @return Array $data
     */
    public function completeOrder(Array $data =[]);

    public function lists($filters=[], $countOnly=false);

    /**
     * Function to save payment configurations
     * @param Array $filters
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function saveConfig($inputs);

    /**
     * Function to get payment configurations
     * @param Array $filters
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function getConfig();

    /**
     * Function to approve payment
     * @param Application $application
     */
    public function approvePayment(Application $application);
    
}
