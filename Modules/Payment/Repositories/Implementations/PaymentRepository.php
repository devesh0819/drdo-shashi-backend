<?php

namespace Modules\Payment\Repositories\Implementations;

use Modules\Payment\Repositories\Interfaces\PaymentInterface;
use Modules\User\Entities\User;
use Modules\Payment\Src\Payment;
use Config;
use Exception;
use Modules\Application\Entities\Application;
use Modules\Core\Entities\AssessmentPlan;
use Modules\Payment\Entities\PaymentOrder;
use Modules\Core\Enums\Flag;
use Modules\Core\Enums\PaymentStage;
use Modules\Core\Enums\ApplicationStage;
use Modules\Payment\Events\PaymentOrderSuccess;
use Modules\Payment\Events\PaymentOrderFailed;

class PaymentRepository implements PaymentInterface
{
    private $limit = 10;
    private $payment;

    /**
     * Constructor
     */
    public function __construct(Payment $payment)
    {
        $this->payment = $payment;
    }
    
    /**
     * Getter
     * @param type $property
     * @return type
     */
    public function __get($property)
    {
        return $this->$property;
    }

    /**
     * Setter
     * @param type $property
     * @param type $value
     * @return $this
     */
    public function __set($property, $value)
    {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
        return $this;
    }

    /**
     * Function to save payment configurations
     * @param Array $filters
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function saveConfig($inputs)
    {
        if(!empty($inputs)){
            $plansKeys = [
                'MIC'=>'micro',
                'SM'=>'small',
                'MED'=>'medium',
                'L'=>'large'
            ];
            $assessmentPlans = AssessmentPlan::where('status', Flag::IS_ACTIVE)->get();
            if(!empty($assessmentPlans)){
                foreach($assessmentPlans as $plan){
                    $planType = $plan->enterprice_type;

                    $certificationValueKey = $plansKeys[$planType]."_certification_fee_value";
                    $gstValueKey = $plansKeys[$planType]."_gst_value";
                    $tdsValueKey = $plansKeys[$planType]."_tds_value";
                    $assessorValueKey = $plansKeys[$planType]."_assessor_fee_value";
                    $subsidizedKeyValue = $plansKeys[$planType]."_subsidy_value";

                    if(isset($inputs[$certificationValueKey])){
                        $plan->certification_fee = $inputs[$certificationValueKey];
                    }
                    if(isset($inputs[$assessorValueKey])){
                        $plan->assessment_cost = $inputs[$assessorValueKey];
                    }
                    if(isset($inputs[$gstValueKey])){
                        $plan->gst = $inputs[$gstValueKey];
                    }
                    if(isset($inputs[$tdsValueKey])){
                        $plan->tds = $inputs[$tdsValueKey];
                    }
                    if(isset($inputs[$subsidizedKeyValue])){
                        $plan->is_subsidized = !empty($inputs[$subsidizedKeyValue]) ? 1 : 0;
                        $plan->subsidy_percent = !empty($inputs[$subsidizedKeyValue]) ? $inputs[$subsidizedKeyValue] : 0;
                    }
                    $paymentBreakup = [];
                    foreach($inputs as $key=>$value){
                        if(str_contains($key, $plansKeys[$planType])){
                            $paymentBreakup[$key]=$value;
                        }
                    }
                    $plan->payment_breakup = json_encode($paymentBreakup);
                    $plan->save();
                }
            }
            $updatedAssessPlans = AssessmentPlan::where('status', Flag::IS_ACTIVE)->get();
            return $updatedAssessPlans;
        }
    }

    /**
     * Function to get payment configurations
     * @param Array $filters
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function getConfig()
    {
        return AssessmentPlan::where('status', Flag::IS_ACTIVE)->get();
    }

    /**
     * Function to initiate a new Payment
     * @param Application $application
     * @param Array $data
     * 
     * @return Array $data
     */
    public function inititate(Application $application, $data=[])
    {
        if(!empty($application->paymentOrder)){
            $application->paymentOrder->tax_percent = !empty($data['tds']) ? $data['tds'] : 0;
            if($application->paymentOrder->save()){
                $parameters = $this->preparePaymentData($application->paymentOrder);
                $order = $this->payment->prepare($parameters);
                return $this->payment->process($order);
            }
            return false;
        }
        throw new Exception('Order not initialised');
    }

    /**
     * Function to prepare payment data
     * @param PaymentOrder $paymentOrder
     * @return Array $data
     */
    private function preparePaymentData(PaymentOrder $paymentOrder)
    {
        $baseamount = $paymentOrder->amount;
        // Add Assessor Fee
        if(!empty($paymentOrder->assessor_fee)){
            $baseamount = ($baseamount + $paymentOrder->assessor_fee);
        }
        
        // Remove Discount
        if(!empty($paymentOrder->discount)){
            $baseamount = (int)($baseamount - (($baseamount*$paymentOrder->discount)/100));
        }
        $amount = $baseamount;
        
        // Add GST Amount
        if(!empty($paymentOrder->gst_percent)){
            $amount = $amount + (($baseamount*$paymentOrder->gst_percent)/100);
        }

        // Add Tax
        if(!empty($paymentOrder->tax_percent)){
            $amount = $amount - (($baseamount*$paymentOrder->tax_percent)/100);
        }

        $paymentOrder->final_amount = $amount;
        $paymentOrder->save();
        return [
            'order_id' => $paymentOrder->order_number,
            'amount' => $amount,
        ];
    }

    
     /**
     * Function to create order for assessment plan
     * @param Application $application
     * @return Array $data
     */
    public function createOrder(Application $application)
    {
        if(!empty($application->assessmentPlan)){
            $amount = $this->getFinalAmount($application, $application->assessmentPlan);
            $availableTDS = explode(",", $application->assessmentPlan->tds);

            $paymentOrder = new PaymentOrder();
            $paymentOrder->user_id = $application->getUserID();
            $paymentOrder->application_id = $application->id;
            $paymentOrder->plan_id = $application->assessmentPlan->id;
            $paymentOrder->order_number = $this->getUniqueOrderNumber($application);
            $paymentOrder->amount = $amount['amount'];
            $paymentOrder->assessor_fee = $application->assessmentPlan->assessment_cost;
            $paymentOrder->gst_percent = $application->assessmentPlan->gst;
            $paymentOrder->tax_percent = null;
            $paymentOrder->payment_breakup = $application->assessmentPlan->payment_breakup;
            $paymentOrder->discount = $application->subsidy_approved==1 ? $amount['discount'] : 0;
            $paymentOrder->status = PaymentStage::PENDING;
            $paymentOrder->save();
        }
        return false;
    }

     /**
     * Function to complete order after successful Payment
     * @param Array $data
     * @return Array $data
     */
    public function completeOrder($data= [])
    {
        if(!empty($data['encResp']) && !empty($data['orderNo'])){
            $response = $this->payment->response((object) $data);
            $order = PaymentOrder::with('application')->where('order_number', $data['orderNo'])->first();
            if(!empty($order->application) && !empty($response['order_status'])){
                if($response['order_status'] == 'Success'){
                    PaymentOrderSuccess::dispatch($order, $response);
                }elseif(in_array($response['order_status'], ['Failure', 'Aborted', 'Invalid'])){
                    PaymentOrderFailed::dispatch($order, $response);
                }
            }
            return $order;
        }
        return true;
    }

    /**
     * Function to get unique Order number
     */
    private function getUniqueOrderNumber(Application $application)
    {
        return "QCI".substr($application->getEnterpriseName(),0,3).time();
    }

    private function getFinalAmount(Application $application, AssessmentPlan $assessmentPlan)
    {
        $amount = $assessmentPlan->certification_fee;
        $isSubsidized = $application->subsidy_approved ? Flag::IS_ACTIVE : $assessmentPlan->is_subsidized;
        
        return [
            'amount'=>$amount,
            'discount' => ($isSubsidized) ? $assessmentPlan->subsidy_percent : 0
        ];
    }

    /**
     * Function to find all Payment detail
     * @param Array $filters
     * @return Array $data
     */
    public function lists($filters=[], $countOnly=false)
    {
        $payments = PaymentOrder::with(['application']);
        if (!empty($filters['status'])) {
            $payments->where('status', $filters['status']);
        }
        if(!empty($filters['user_id'])){
            $payments->where('user_id', $filters['user_id']);
        }
        if ($countOnly) {
            return $payments->count();
        } elseif(!empty(request()->get('is_excel'))){
            return $payments->get();
        } else {
            $offset = !empty($filters['start']) ? ($filters['start']-1)*$this->limit : 0;
            return $payments->orderBy("updated_at", "desc")
            ->limit(!empty($filters['length']) ? $filters['length'] : $this->limit)
            ->offset($offset)
            ->get();
        }
    }

    /**
     * Function to approve payment
     * @param Application $application
     */
    public function approvePayment(Application $application, $data=[])
    {
        if(!empty($application->paymentOrder) && !empty($application->assessmentPlan)){
            $assessmentPlan = json_decode($application->assessmentPlan->payment_breakup);
            if(empty($data['local_assessor'])){
                $application->paymentOrder->assessor_fee = !empty($application->paymentOrder->assessor_fee) ? $application->paymentOrder->assessor_fee : $application->assessmentPlan->assessment_cost;
            }else{
                $application->paymentOrder->assessor_fee = null;
            }
            if($application->paymentOrder->save()){
                $application->application_stage = ApplicationStage::APP_PAYMENT_APPROVED;
                $application->save();
            }
            return true;
        }else{
            return false;
        }
    }
}
