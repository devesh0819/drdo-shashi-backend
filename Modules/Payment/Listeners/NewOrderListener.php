<?php

namespace Modules\Payment\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Application\Entities\Application;
use Modules\Payment\Events\NewOrderCreated;
use Modules\Payment\Services\PaymentService;
use Modules\Notification\Notifications\ApplicationApprovedByDRDO;

class NewOrderListener
{
    protected $paymentService;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(PaymentService $paymentService)
    {
        $this->paymentService = $paymentService;
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(NewOrderCreated $event)
    {
        if(!empty($event->application)){
            $createOrder = $this->paymentService->createOrder($event->application);
            if(!empty($event->application->user)){
                $event->application->user->user->notify(new ApplicationApprovedByDRDO($event->application));
            }
        }
        return true;        
    }
}
