<?php

namespace Modules\Payment\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Payment\Entities\PaymentOrder;
use Modules\Payment\Services\PaymentService;
use Modules\Payment\Events\PaymentOrderFailed as PaymentOrderFailedEvent;
use Exception;
use Modules\Core\Helpers\Logger;
use Modules\Payment\Entities\PaymentTransaction;

class PaymentOrderFailed
{
    protected $paymentService;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(PaymentService $paymentService)
    {
        $this->paymentService = $paymentService;
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(PaymentOrderFailedEvent $event)
    {
        try{
            if(!empty($event->order)){
                // Log Transaction
                $this->transaction = PaymentTransaction::where('order_id', $event->order->id)->firstOrNew();
                $this->transaction->order_id = $event->order->id;
                $this->transaction->status = 'Failed';
                $this->transaction->payload = json_encode($event->response);
                $this->transaction->save();
            }
            return true;
        }catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }
}
