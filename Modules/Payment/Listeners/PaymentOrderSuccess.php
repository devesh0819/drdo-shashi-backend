<?php

namespace Modules\Payment\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Payment\Entities\PaymentOrder;
use Modules\Payment\Services\PaymentService;
use Modules\Payment\Events\PaymentOrderSuccess as PaymentOrderSuccessEvent;
use Exception;
use Modules\Core\Helpers\Logger;
use Modules\Payment\Entities\PaymentTransaction;
use Modules\Core\Enums\PaymentStage;
use Modules\Core\Enums\ApplicationStage;

class PaymentOrderSuccess
{
    protected $paymentService;
    private $transaction;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(
        PaymentService $paymentService,
        PaymentTransaction $transaction
    )
    {
        $this->paymentService = $paymentService;
        $this->transaction = $transaction;
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(PaymentOrderSuccessEvent $event)
    {
        try{
            if(!empty($event->order)){
                // Log Transaction
                $this->transaction = PaymentTransaction::where('order_id', $event->order->id)->firstOrNew();
                $this->transaction->order_id = $event->order->id;
                $this->transaction->status = 'Success';
                $this->transaction->payload = json_encode($event->response);
                $this->transaction->save();
                // Update Order Status
                $event->order->status = PaymentStage::COMPLETE;
                $event->order->save();
                // Update Application Status
                $event->order->application->application_stage = ApplicationStage::APP_PAYMENT_DONE;
                $event->order->application->save();
            }
            return true;
        }catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }
}
