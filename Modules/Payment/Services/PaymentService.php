<?php

namespace Modules\Payment\Services;

use Modules\Payment\Repositories\Interfaces\PaymentInterface;
use Exception;
use Modules\Core\Helpers\Logger;
use Modules\User\Entities\User;
use Modules\Application\Entities\Application;

class PaymentService
{
    private $repository;

    /**
     * Constructor
     * @param PaymentInterface $repository
     * @param ApplicationInterface $repository
     */
    public function __construct(PaymentInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Function to save payment configurations
     * @param Array $filters
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function saveConfig($inputs)
    {
        try {
            return $this->repository->saveConfig($inputs);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

    /**
     * Function to get payment configurations
     * @param Array $filters
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function getConfig()
    {
        try {
            return $this->repository->getConfig();
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

    /**
     * Function to find all users list
     * @param Array $filters
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function lists($filters=[], $countOnly=false)
    {
        try {
            return $this->repository->lists($filters, $countOnly);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }
    
    
    /**
     * Function to initiate a new Payment
     * @param Application $application
     * @param Array $data
     * 
     * @return Array $data
     */
    public function inititate(Application $application, $data=[])
    {
        try {
            return $this->repository->inititate($application, $data);
        }catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

    /* Function to create order for assessment plan
     * @param Application $application
     * @return Array $data
     */
    public function createOrder(Application $application)
    {
        try{
            return $this->repository->createOrder($application);
        }catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

    /**
     * Function to complete order after successful Payment
     * @param Array $data
     * @return Array $data
     */
    public function completeOrder($data=[])
    {
        try{
            return $this->repository->completeOrder($data);
        }catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

    /**
     * Function to approve payment
     * @param Application $application
     */
    public function approvePayment(Application $application, $data=[])
    {
        try{
            return $this->repository->approvePayment($application, $data);
        }catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }
}
