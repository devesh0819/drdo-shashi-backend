
<html>

<head>
    <style>
        body {
            width: 216mm;
            height: 303mm;
            background-image: url("{{public_path('pdf/certificate_front.jpg')}}");
            background-repeat: no-repeat;
            background-size: 216mm 303mm;
        }

        .score {
            position: absolute;
            top: 101mm;
            left: 115mm;
            font-size: 19px;
            font-weight: bold;
            color: white;
        }

        .unit-name {
            position: absolute;
            top: 170mm;
            left: 30mm;
            font-weight: bold;
        }

        .unit-address {
            position: absolute;
            top: 195mm;
            left: 30mm;
            font-weight: bold;
        }
        .date-issue {
            position: absolute;
            top: 271mm;
            left: 53mm;
        }

        .validity {
            position: absolute;
            top: 281.5mm;
            left: 42mm;
        }
    </style>
</head>
<body>
    <div class="score">
    {{!empty($application->assessment->assessment_score) ? __("api.".$application->assessment->certification_type."-val") : null}}
    </div>
    <div class="unit-name">
    {{!empty($application->enterprise_name) ? $application->enterprise_name : null}}
    </div>
    <div class="unit-address">
    {{!empty($application->unit_address) ? $application->unit_address : null}}
    </div>
    <div class="date-issue">
    {{!empty($application->assessment->certification_expiry) ? date("Y-m-d", strtotime("-2 year", strtotime($application->assessment->certification_expiry))) : null}}
    </div>
    <div class="validity">
    </div>
</body>

</html>