<div class="heading">
SITE ASSESSMENT SUMMARY
</div>
<br />
<div class="small-content">
<table class="summaryTable" cellspacing=0 cellpadding=0 width="100%"
 style='width:100.0%;border:1px solid black;'>
    <tr style="height:16.5pt;background:#D5DCE4;border:solid #9E9E9E 1.0pt; text-align:left;">
        <th style="font-size:12.0pt;font-family:Times New Roman,serif;
  color:#232527; padding:0in 5.4pt 0in 5.4pt;">
            SAMAR ID    
        </th>
        <th style="font-size:12.0pt;font-family:Times New Roman,serif;
  color:#232527; padding:0in 5.4pt 0in 5.4pt;">
            Enterprise Name    
        </th>
        <th style="font-size:12.0pt;font-family:Times New Roman,serif;
  color:#232527; padding:0in 5.4pt 0in 5.4pt;">
            Address of the unit assessed 
        </th>
        <th style="font-size:12.0pt;font-family:Times New Roman,serif;
  color:#232527; padding:0in 5.4pt 0in 5.4pt;">
            Assessment Dates 
        </th>
        <th style="font-size:12.0pt;font-family:Times New Roman,serif;
  color:#232527; padding:0in 5.4pt 0in 5.4pt;">
            Key Products Manufactured
        </th>
    </tr>
    <tr style="border:solid #9E9E9E 1.0pt;">
        <td>{{$application->user->user->username}}</td>
        <td>{{$application->enterprise_name}}</td>
        <td>{{$application->unit_address}} – {{$application->enterprise_name}}</td>
        <td>{{\Modules\Core\Helpers\BasicHelper::showDate($application->assessment_date)}} </td>
        <td>  <?php 
  $products = !empty($application->ownership->product_manufactured) ? json_decode($application->ownership->product_manufactured) : [];
  $allProductNames = array_column($products, 'productName');
  $allProductNames = implode(",", $allProductNames);
  ?>
    {{$allProductNames}}</td>
    </tr>
</table>
</div>
<br /><br />
@if(!empty($latLongData))
<div class="heading">
{{!empty($latLongData['latitude']) ? $latLongData['latitude'] : ""}},
{{!empty($latLongData['longitude']) ? $latLongData['longitude'] : ""}}
</div>
<br /><br />
@endif
<div class="heading">
SAMAR Rating
</div>
<div class=MsoNormal align=center style='text-align:center;'>
<hr style="width:400px; text-align:center;border-color:blue; border-width:2px;">
{{__("api.".$application->assessment->certification_type."-certification")}}
<hr style="width:400px; text-align:center;border-color:blue;border-width:2px;">

<br clear=ALL>
<span lang=EN-IN style='line-height:107%;font-family:"Cambria",serif; font-weight:bold;font-size;20px;'>
Score: {{$application->assessment->assessment_score}}
</span>
</div>