<div class="coverPage">
    <div class="score">
        {{!empty($application->assessment->assessment_score) ? __("api.".$application->assessment->certification_type."-val") : null}}
    </div>
    <div class="unit-name">
        {{!empty($application->enterprise_name) ? $application->enterprise_name : null}}
    </div>
    <div class="unit-address">
        {{!empty($application->unit_address) ? $application->unit_address : null}}
    </div>
    <div class="cover-lat">
        {{!empty($application->user->user->username) ? $application->user->user->username : null}}<br /><br />
        {{!empty($latLongData['latitude']) ? round($latLongData['latitude'], 2) : ""}}
        <br /><br />
        {{!empty($latLongData['longitude']) ? round($latLongData['longitude'],2 ) : ""}}  
    </div>
    <div class="cover-long">
        <br />
    </div>
</div>