<?php
// HorizontalBarGraph
require_once public_path().'/SVGGraph/autoloader.php';
?>
<div class="heading">
DETAILED FINDINGS
</div>
<br />
@if(!empty($performanceData))
<?php $i =0;?>
@foreach($performanceData as $key=>$data)
@php 
$i++;
$pGraphData = [];
if(!empty($data['parameters'])){
  foreach($data['parameters'] as $param){
    $gKey = substr($param['title'], 0, 20);
     $pGraphData[$gKey]  = $param['score'];
  }
} 
@endphp
<p><b>Discipline {{$i}} : {{$key}} </b></p>
<div style="border: 2px solid green;height:300px; vertical-align:center; text-align:center; font-size:20px; font-weight:bold;">

<?php
$settings = array(
  'auto_fit' => true,
  'back_colour' => 'white',
  'back_stroke_width' => 0,
  'back_stroke_colour' => 'white',
  'stroke_colour' => '#000',
  'axis_colour' => '#333',
  //'axis_overlap' => 2,
  'grid_colour' => '#666',
  'label_colour' => '#000',
  'axis_font' => 'Arial',
  'axis_font_size' => 10,
  'pad_right' => 20,
  'pad_left' => 20,
  'link_base' => '/',
  'link_target' => '_top',
  'minimum_grid_spacing' => 20,
  'show_subdivisions' => false,
  'show_grid_subdivisions' => false,
  'grid_subdivision_colour' => '#ccc',
);

$width = 300;
$height = 200;
$type = 'HorizontalBarGraph';
$values = [$pGraphData];
$colours = [ ['#42f5c5']];

$graph = new Goat1000\SVGGraph\SVGGraph($width, $height, $settings);
$graph->colours($colours);
$graph->values($values);
$graph->render($type);
?>
</div>
<br />
<p> <b>Strengths</b></p>
@if(!empty($data['strength']))
<ol type="i">
@foreach($data['strength'] as $d)
<li>
{{!empty($d->comment) ? $d->comment : ''}}
</li>
@endforeach
</ol>
@endif

<br />
<p> <b>Opportunities for Improvement (OFI)</b></p>
@if(!empty($data['ofi']))
<ol type="i">
@foreach($data['ofi'] as $d)
<li>
{{!empty($d->comment) ? $d->comment : ''}}
</li>
@endforeach
</ol>
@endif
<br /><br />
@endforeach
@endif