<div class="heading">
About SAMAR ASSESSMENT
</div>
<div class="small-content">
<p>
System for Advance Manufacturing Assessment and Rating (SAMAR) Certification’ is a benchmark to measure the
maturity of defence manufacturing enterprises. The certification framework has been developed by the Quality Council of
India and is based on a maturity assessment framework and is applicable to all defence manufacturing enterprises i.e., micro, small, medium and large enterprises.
</p>
<p>
SAMAR is an outcome of the collaboration between DRDO and QCI to strengthen the ‘defence manufacturing’ ecosystem
in the country with an objective to further the vision of making India self-reliant in defence manufacturing.
</p>
<p>
SAMAR Certification framework recognizes 5 levels of enterprise maturity in terms of the defined organisational
attributes:
</p>

<table class="aboutUsTable" cellspacing=0 cellpadding=0 width="100%"
 style='width:100.0%;border:1px solid black;'>
    <tr style="height:16.5pt;background:#A9D08E;border:solid #9E9E9E 1.0pt; text-align:left;">
        <th style="font-size:12.0pt;font-family:Times New Roman,serif;
  color:#232527; padding:0in 5.4pt 0in 5.4pt;">
            Score    
        </th>
        <th style="font-size:12.0pt;font-family:Times New Roman,serif;
  color:#232527; padding:0in 5.4pt 0in 5.4pt;">
            Maturity Level    
        </th>
        <th style="font-size:12.0pt;font-family:Times New Roman,serif;
  color:#232527; padding:0in 5.4pt 0in 5.4pt;">
            Organisational Attribute    
        </th>
    </tr>
    <tr style="border:solid #9E9E9E 1.0pt;">
        <td>Upto 2.5 </td>
        <td>Level 1 </td>
        <td>Largely ad hoc processes </td>
    </tr>
    <tr style="border:solid #9E9E9E 1.0pt;">
        <td>> 2.5-3.0 </td>
        <td>Level 2 </td>
        <td>Process of standardisation has just begun </td>
    </tr>
    <tr style="border:solid #9E9E9E 1.0pt;">
        <td>>3.0-3.5 </td>
        <td>Level 3 </td>
        <td>Processes are standardised </td>
    </tr>
    <tr style="border:solid #9E9E9E 1.0pt;">
        <td>>3.5-4.0 </td>
        <td>Level 4 </td>
        <td>Processes are standardised, measured and controlled </td>
    </tr>
    <tr style="border:solid #9E9E9E 1.0pt;">
        <td>>4.0 </td>
        <td>Level 5 </td>
        <td>Processes are standardised, measured and continually improved for
optimization </td>
    </tr>
</table>
<br /><br />  
</div> 
<div class="heading">
SCOPE OF ASESSMENT
</div>
<div class="small-content">
<p>
The assessment is conducted at the manufacturing facility for
which the application is made and is based on the requirements as laid down in
the relevant SAMAR Maturity Assessment Model.
</p>
<p>
This report is an outcome of an extensive site-assessment of the unit based on the SAMAR Maturity Assessment Model.
</p>
<p style="background:yellow;">In this report, findings are presented on an exception basis only.</p>
</div> 
<br /><br />  
<div class="heading">
DISCLAIMER
</div>
<div class="small-content">
<p>
This disclaimer governs the use
of this report.&nbsp;By using this report, the organisation accepts this
disclaimer in full.
</p>
<ol>
<li><p>This is an electronically generated report/transcript and is authenticated by the Quality Council of India (QCI). It does not
require any signature.</p></li>
<li> <p>The rating and certification provided, pertains only to the processes and systems of the unit for which assessment is
conducted.</p></li>
<li> <p>The applicant enterprise has understood fully that it needs to ensure compliance to all legal, statutory and regulatory
requirements. Any deviation or non-fulfilment of compliances may result in withdrawal of the rating and certification.</p></li>
<li> <p>The rating & certification provided is valid only for a period of 2 years from the date of issue as mentioned on the
Certificate.</p></li>
<li> <p>This report contains a summary of critically evaluated assessment findings as observed during the assessment and
represents the final rating and certification, validated by the Rating Committee, based on the relevant SAMAR Maturity
Assessment model.</p></li>
<li> <p>This report is meant for the use and benefit of the applicant enterprise and DRDO/QCI shall not accept any liability that may
arise if this report is used for any alternative purpose.</p></li>
<li><p>Information not disclosed by the applicant enterprise may alter the findings outlined in this report. In such situations, QCI
reserves the right to withdraw or amend its rating and conclusions.</p></li>
</ol>
</div> 
