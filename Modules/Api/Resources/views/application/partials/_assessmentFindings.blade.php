<div class="heading">
ASSESSMENT FINDINGS
</div>
<br />
<div class="subheading">
Methodology
</div>
<br />
<div class="small-content">
    <p>
    To ensure transparency in the site assessment process, all site-assessment components are monitored electronically and the
entire process (documents, photographs, comments etc.) is captured on a secure mobile application by the assessors. The data
captured during site-assessment is not shared externally with anyone except the relevant stakeholders involved for award of
score/rating.
    </p>
</div>
<br />
<div class="site-evidense">
    <div style="width:45%;">
        @if(!empty($application->siteExteriorPicture->path))
            <img width="430px" style="max-height:350px;" src="{{storage_path('app/public/'.$application->siteExteriorPicture->path)}}" align="left">
            Site Exterior Picture
        @endif
    </div>
    <div style="width:45%;">
        @if(!empty($assessmentEvidenses))
            @foreach($assessmentEvidenses as $evidense)
                @if(!empty($evidense->type) && $evidense->type == 'open_meet')
                    <img width="430px" style="max-height:350px;" src="{{storage_path('app/public/'.$evidense->path)}}">
                    Opening Meeting
                @endif
            @endforeach
        @endif
    </div>
</div>
<br />
<div class="site-evidense">
    <div style="width:100%;">
        @if(!empty($assessmentEvidenses))
            @foreach($assessmentEvidenses as $evidense)
                @if(!empty($evidense->type) && $evidense->type == 'close_meet')
                    <img width="430px" style="max-height:350px;" src="{{storage_path('app/public/'.$evidense->path)}}">
                    Closing Meeting
                @endif
            @endforeach
        @endif
    </div>
</div>
<br /><br />    
<div class="subheading">
Performance on Individual Parameters
</div>
<br /><br />
<?php
$parameters = $application->assessment->parameters();
?>
<div class="small-content">
<table class="aboutUsTable" cellspacing=0 cellpadding=0 width="100%"
 style='width:100.0%;border:1px solid black;'>
    <tr style="height:16.5pt;background:#A9D08E;border:solid #9E9E9E 1.0pt; text-align:left;">
        <th style="font-size:12.0pt;font-family:Times New Roman,serif;
  color:#232527; padding:0in 5.4pt 0in 5.4pt;">
            S. No  
        </th>
        <th style="font-size:12.0pt;font-family:Times New Roman,serif;
  color:#232527; padding:0in 5.4pt 0in 5.4pt;">
            Name of Parameter  
        </th>
        <th style="font-size:12.0pt;font-family:Times New Roman,serif;
  color:#232527; padding:0in 5.4pt 0in 5.4pt;">
            Score 
        </th>
    </tr>
    @if(!empty($parameters))
    @foreach($parameters as $key=>$parameter)
    <tr style="border:solid #9E9E9E 1.0pt;">
        <td>{{$key+1}}</td>
        <td>{{$parameter->parameter}} </td>
        <td>{{$parameter->parameter_score}} </td>
    </tr>
    @endforeach
    @endif
</table>
</div>
<br />
<center><span style="color:red;"><b>All parameters (as applicable, to be listed)</b></span></center>
<br /><br />
<div class="small-content">
    <p>
    The scores obtained on the above parameters are a weighted average of the unit’s performance as assessed on the relevant
SAMAR Maturity Assessment Model. The key purpose of this assessment is to objectively measure the performance of the unit’s
processes and systems on a maturity scale of 1 to 5. This is intended for the applicant enterprise to identify and address areas of
concern and subsequently improve the current performance to higher maturity levels thereby enhancing its capabilities and
competitiveness.
    </p>
</div>
<br /><br />
<div class="subheading">
Score Obtained and Rating
</div>
<br /><br />
<div class="small-content">
    <p>
    Your performance on SAMAR parameters are tabulated below:
    </p>
</div>
<br />
<?php 
 $totalParameter = !empty($parameters) ? $parameters->toArray() : [];
 $totalParameterScore= array_sum(array_column($totalParameter, 'parameter_score'));
 ?> 
<table class="aboutUsTable" cellspacing=0 cellpadding=0 width="100%"
 style='width:100.0%;border:1px solid black;'>
    <tr  style="height:16.5pt;background:#A9D08E;border:solid #9E9E9E 1.0pt; text-align:left;">
        <th colspan="2" style="font-size:12.0pt;font-family:Times New Roman,serif;
  color:#232527; padding:0in 5.4pt 0in 5.4pt;">
            Parameters
        </th>
        <th style="font-size:12.0pt;font-family:Times New Roman,serif;
  color:#232527; padding:0in 5.4pt 0in 5.4pt;">
            Score 
        </th>
        <th style="font-size:12.0pt;font-family:Times New Roman,serif;
  color:#232527; padding:0in 5.4pt 0in 5.4pt;">
            SAMAR Rating
        </th>
    </tr>
    <tr style="border:solid #9E9E9E 1.0pt;">
        <td>Number</td>
        <td>{{!empty($parameters) ? count($parameters) : 0}}</td>
        <td style="border-bottom: none;">{{$application->assessment->assessment_score}}</td>
        <td style="border-bottom: none;">{{__("api.".$application->assessment->certification_type."-certification")}}</td>
    </tr>
    <tr style="border:solid #9E9E9E 1.0pt;">
        <td>Score</td>
        <td>{{$totalParameterScore}}</td>
        <td></td>
        <td></td>
    </tr>
    </table>   