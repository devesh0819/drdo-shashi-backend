<html>

<head>
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<meta name=Generator content="Microsoft Word 15 (filtered)">
<style>
    body {
            width: 260mm;
            height: 350mm;
            background-image: url("{{public_path('pdf/report_front.jpg')}}");
            background-repeat: no-repeat;
            background-size: 260mm 350mm;
        }

 /* Font Definitions */
 @font-face
	{font-family:Wingdings;
	panose-1:5 0 0 0 0 0 0 0 0 0;}
@font-face
	{font-family:Mangal;
	panose-1:2 4 5 3 5 2 3 3 2 2;}
@font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;}
@font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;}
@font-face
	{font-family:Cambria;
	panose-1:2 4 5 3 5 4 6 3 2 4;}
@font-face
	{font-family:"Segoe UI";
	panose-1:2 11 5 2 4 2 4 2 2 3;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin-top:0in;
	margin-right:0in;
	margin-bottom:8.0pt;
	margin-left:0in;
	line-height:107%;
	font-size:11.0pt;
	font-family:"Calibri",sans-serif;}
p
	{margin-top:0in;
	margin-right:0in;
	margin-bottom:8.0pt;
	margin-left:0in;
	line-height:107%;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;}
p.MsoListParagraph, li.MsoListParagraph, div.MsoListParagraph
	{margin-top:0in;
	margin-right:0in;
	margin-bottom:8.0pt;
	margin-left:.5in;
	line-height:107%;
	font-size:11.0pt;
	font-family:"Calibri",sans-serif;}
p.MsoListParagraphCxSpFirst, li.MsoListParagraphCxSpFirst, div.MsoListParagraphCxSpFirst
	{margin-top:0in;
	margin-right:0in;
	margin-bottom:0in;
	margin-left:.5in;
	line-height:107%;
	font-size:11.0pt;
	font-family:"Calibri",sans-serif;}
p.MsoListParagraphCxSpMiddle, li.MsoListParagraphCxSpMiddle, div.MsoListParagraphCxSpMiddle
	{margin-top:0in;
	margin-right:0in;
	margin-bottom:0in;
	margin-left:.5in;
	line-height:107%;
	font-size:11.0pt;
	font-family:"Calibri",sans-serif;}
p.MsoListParagraphCxSpLast, li.MsoListParagraphCxSpLast, div.MsoListParagraphCxSpLast
	{margin-top:0in;
	margin-right:0in;
	margin-bottom:8.0pt;
	margin-left:.5in;
	line-height:107%;
	font-size:11.0pt;
	font-family:"Calibri",sans-serif;}
.MsoChpDefault
	{font-family:"Calibri",sans-serif;}
.MsoPapDefault
	{margin-bottom:8.0pt;
	line-height:107%;}
 /* Page Definitions */
 @page WordSection1
	{size:595.3pt 841.9pt;
	margin:1.0in 56.7pt 56.7pt 56.7pt;}
div.WordSection1
	{page:WordSection1;}
 /* List Definitions */
 ol
	{margin-bottom:0in;}
ul
	{margin-bottom:0in;}
    @page coverPage {
                size: A4 portrait;
                margin: 2cm;
            }
            @page aboutUsPage {
                size: A4 portrait;
                margin: 2cm;
                background-image: url("{{public_path('pdf/normal_page.jpg')}}");
                background-repeat: no-repeat;
                background-size: 216mm 303mm;
            }
            @page assessmentSummaryPage {
                size: A4 portrait;
                margin: 2cm;
                background-image: url("{{public_path('pdf/normal_page.jpg')}}");
                background-repeat: no-repeat;
                background-size: 216mm 303mm;
            }
            @page assessmentFindingsPage {
                size: A4 portrait;
                margin: 2cm;
                background-image: url("{{public_path('pdf/normal_page.jpg')}}");
                background-repeat: no-repeat;
                background-size: 216mm 303mm;
            }
            @page performanceAnalysisPage {
                size: A4 portrait;
                margin: 2cm;
                background-image: url("{{public_path('pdf/normal_page.jpg')}}");
                background-repeat: no-repeat;
                background-size: 216mm 303mm;
            }
            @page wayForwardPage {
                size: A4 portrait;
                margin: 2cm;
                background-image: url("{{public_path('pdf/normal_page.jpg')}}");
                background-repeat: no-repeat;
                background-size: 216mm 303mm;
            }
            @page thankYouPage {
                size: A4 portrait;
                margin: 2cm;
                background-image: url("{{public_path('pdf/normal_page.jpg')}}");
                background-repeat: no-repeat;
                background-size: 216mm 303mm;
            }
            
            .coverPage {
                page: coverPage;
                page-break-after: always;
            }
            .aboutUs {
                page: aboutUsPage;
                page-break-after: always;
            }
            .assessmentSummary {
                page: assessmentSummaryPage;
                page-break-after: always;
            }
            .assessmentFindings {
                page: assessmentFindingsPage;
                page-break-after: always;
            }
            .performanceAnalysis {
                page: performanceAnalysisPage;
                margin-bottom: 0; 
                padding-bottom: 0;
            }
            .wayForward {
                page: wayForwardPage;
                page-break-after: always;
            }
            .thankYou {
                page: thankYouPage;
                page-break-after: always;
            }
            .topRuler{
                color:brown;
                margin-top: 50px;
            }
            .footerRuler{
                color:green;
                margin-bottom: 50px;
            }

            .score {
            position: absolute;
            top: 153mm;
            left: 115mm;
            letter-spacing: 3px;
            font-size: 19px;
            font-weight: bold;
            color: white;
        }

        .unit-name {
            position: absolute;
            top: 232mm;
            font-size:20px;
            left: 70mm;
            font-weight: bold;
        }

        .unit-address {
            position: absolute;
            top: 237mm;
            font-size:20px;
            left: 70mm;
            font-weight: bold;
        }
        .cover-lat {
            position: absolute;
            top: 245mm;
            font-size:16px;
            font-weight:bold;   
            left: 85mm;
        }

</style>

</head>

<body lang=EN-US link="#0563C1" vlink="#954F72" style='word-wrap:break-word'>

<div class=WordSection1>
        @if(!empty($pages))
            @foreach($pages as $key=>$page)
            <div class="{{$key}}" style="font-family: 'Cambria'">
                @include("api::application.partials._$key")
            </div>
            @endforeach
        @endif
</div>

</body>

</html>