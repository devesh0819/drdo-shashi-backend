<style>
table, th, td {
  border: 1px solid black;
  border-collapse: collapse;
}
</style>
<div > <img src="{{ asset('zed_logo.png') }}" style="margin-left:5px;height:50px;">
            <hr style="background:#01A08C;height:2px;width:650px;margin-top:-12px;margin-left:90px;"><br>
            <h4 style="margin-top:-55px;float:right;font-style:italic;">Site Assessment Report</h4>
</div>
<div><h3 style="background-color:#009078;color:white;text-align:center;">WAY FORWARD</h3> </div>
<div>
<p style="font-size:10px;">
Quality improvement models vary in approach and methods; however, a basic underlying principle is that
Quality improvement is a continuous activity and not a one-time thing. As you implement changes, there will
always be concerns to address and challenges to manage. The ZED Maturity Assessment Model strives to
assist you in moving incrementally towards your improvement goals. Today's global business environment
calls for organizations to develop, implement and maintain effective quality management systems. While the
objectives of quality improvement are clear, the roadmap to reaching a superior quality operation, though
more difficult and complex, is achievable when practiced.
</p>
<p style="font-size:10px;">
Compared to large companies, MSMEs actually have a significant advantage in implementing environmental
friendly practices. The small scale of operations makes it flexible to adapt with constantly changing regulation
or environmental requirements. Environmental sustainability is a major focus in today’s business world.
Companies across all sectors explicitly state environmental concerns as being part of their management
agenda. Businesses are striving to be more responsible towards the environment, whether by reducing their
operational impact, by working towards a sustainable environment or by taking preventive measures. It is
hence imperative that MSMEs realize the significant advantages of inculcating quality practices in their
processes and adopting environmentally friendly initiatives. We encourage you to adopt measures as
suggested in the maturity model and we are confident that you will understand and prioritize your business
needs by helping identify any deficiencies or shortcomings that need to be overcome, thereby performing
better on the ZED parameters.
</p>
<p style="font-size:10px;">
We urge you to leverage the competence of ZED trained consultants who will share their expertise and
knowledge to help your business attain goals and solve problems. These trained professionals will view the
situation from a fresh perspective and can act as the catalyst for change. A consultant or a Consulting
Organization will be allocated to you after a due process and in case you have achieved a minimum Bronze
rating after site-assessment, you will also be eligible for financial support if you opt for this component.
After the handholding process is complete, you have an option to apply for re-assessment/re-rating if you
are confident that your processes have considerably improved. It may be noted that the financial support on
the re-rating component will only be applicable if your rating improves by at least one level from the previous
site assessment, i.e. from Bronze to Silver or Silver to Gold etc.
</p>
<p style="font-size:10px;">
Finally, we urge you to explore participating in various schemes offered by the Government, for example
schemes related to Technology Upgradation, Lean Manufacturing, Intellectual Property Rights, Design Clinic,
Domestic & Export Market Promotion, Employment Generation etc., which are aimed at encouraging,
enhancing and handholding the MSMEs resulting in enhanced productivity & profitability of the enterprises
and making them adopt environment friendly technologies. To know more about the Ministry of MSME
Schemes, please visit: http://msme.gov.in.
All the best!
</p>
<p style="font-size:10px;">
The quality movement in product manufacturing and delivery of
service has undergone change from its initial emphasis on quality
through inspection, to the present- day emphasis on quality
through the development of robust processes capable of
performing consistently in developing products and services that
meet and exceed user needs and expectations.
</p>
<p style="font-size:10px;text-align:right;">
Drury, Eklund: 1997
</p>