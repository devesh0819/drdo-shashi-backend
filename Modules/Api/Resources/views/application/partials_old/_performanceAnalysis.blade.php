<?php
// HorizontalBarGraph
require_once public_path().'/SVGGraph/autoloader.php';
?>

<br /><br />

<p class=MsoNormal style='margin-bottom:0in;text-align:justify'>
<table cellpadding=0 cellspacing=0 align=left width="99%">
 <tr>
  <td style="width: 100%;
    font-size: 20;
    height: 30px;
    color: white;
    font-weight: bold;
    font-color: white;
    background: #2EA08C;
    padding-left:20px;">
   DETAILED FINDINGS
  </td>
 </tr>
</table>
</p>
<br /><br />

@if(!empty($performanceData))
<?php $i =0;?>
@foreach($performanceData as $key=>$data)
@php 
$i++;
$pGraphData = [];
if(!empty($data['parameters'])){
  foreach($data['parameters'] as $param){
    $gKey = substr($param['title'], 0, 20);
     $pGraphData[$gKey]  = $param['score'];
  }
} 
@endphp
<p class=MsoNormal style='text-align:justify;line-height:115%;'><b><span
lang=EN-IN style='line-height:115%;font-family:"Cambria",serif;color:black'>Discipline
{{$i}} : {{$key}} 
</span></b></p>
<p class=MsoNormal align=center style='margin-top:12.0pt;margin-right:0in;
margin-bottom:0in;margin-left:0in;text-align:center;line-height:115%'>
<div style="border: 2px solid green;height:300px; vertical-align:center; text-align:center; font-size:20px; font-weight:bold;">
<?php
$settings = array(
  'auto_fit' => true,
  'back_colour' => 'white',
  'back_stroke_width' => 0,
  'back_stroke_colour' => 'white',
  'stroke_colour' => '#000',
  'axis_colour' => '#333',
  //'axis_overlap' => 2,
  'grid_colour' => '#666',
  'label_colour' => '#000',
  'axis_font' => 'Arial',
  'axis_font_size' => 10,
  'pad_right' => 20,
  'pad_left' => 20,
  'link_base' => '/',
  'link_target' => '_top',
  'minimum_grid_spacing' => 20,
  'show_subdivisions' => false,
  'show_grid_subdivisions' => false,
  'grid_subdivision_colour' => '#ccc',
);

$width = 300;
$height = 200;
$type = 'HorizontalBarGraph';
$values = [$pGraphData];
$colours = [ ['#42f5c5']];

$graph = new Goat1000\SVGGraph\SVGGraph($width, $height, $settings);
$graph->colours($colours);
$graph->values($values);
$graph->render($type);?>
</div>
</p>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
115%;'><b><span lang=EN-IN style='line-height:115%;
font-family:"Cambria",serif;color:black'>Strengths</span></b><span lang=EN-IN
style='line-height:115%;font-family:"Cambria",serif;color:black'>  </span></p>

@if(!empty($data['strength']))
@foreach($data['strength'] as $d)
<p class=MsoListParagraphCxSpLast style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.25in;text-align:justify;text-indent:-.25in;
line-height:115%;'><span lang=EN-IN style='font-family:Wingdings'>v<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-IN
style='line-height:115%;font-family:"Cambria",serif;color:black'>{{!empty($d->comment) ? $d->comment : ''}}</span>
</p>
@endforeach
@endif

<p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
115%;'><b><span lang=EN-IN style='line-height:115%;
font-family:"Cambria",serif;color:black'>Opportunities for Improvement (OFI)</span></b></p>

@if(!empty($data['ofi']))
@foreach($data['ofi'] as $d)
<p class=MsoListParagraphCxSpLast style='margin-top:0in;margin-right:0in;
margin-bottom:0in;margin-left:.25in;text-align:justify;text-indent:-.25in;
line-height:115%;'><span lang=EN-IN style='font-family:Wingdings'>v<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-IN
style='line-height:115%;font-family:"Cambria",serif;color:black'>{{!empty($d->comment) ? $d->comment : ''}}</span>
</p>
@endforeach
@endif
<br /><br />
@endforeach
@endif

