<p class=MsoNormal style='margin-bottom:0in;text-align:justify'>

<table cellpadding=0 cellspacing=0 align=left width="99%">
 <tr>
  <td></td>
  <td style="width: 100%;
    font-size: 20;
    height: 30px;
    color: white;
    font-weight: bold;
    font-color: white;
    background: #2EA08C;
    padding-left:20px;">
    About SAMAR ASSESSMENT
  </td>
 </tr>
</table>
</p>

<p style='text-align:justify;background:white'><span lang=EN-IN
style='color:#232527'>&nbsp;</span></p>

<br clear=ALL>

<p style='text-align:justify;background:white'><span lang=EN-IN
style='color:#232527'>System for Advance Manufacturing Assessment and Rating (SAMAR) Certification’ is a benchmark to measure the
maturity of defence manufacturing enterprises. The certification framework has
been developed by the Quality Council of India and is based on a maturity
assessment framework and is applicable to all defence manufacturing enterprises
i.e., micro, small, medium and large enterprises.</span></p>

<p style='text-align:justify;background:white'><span lang=EN-IN
style='color:#232527'>SAMAR is an outcome of the collaboration between DRDO
and QCI to strengthen the ‘defence manufacturing’ ecosystem in the country with
an objective to further the vision of making India self-reliant in defence
manufacturing. </span></p>

<p style='text-align:justify;background:white'><span lang=EN-IN
style='color:#232527'>SAMAR Certification framework recognizes 5 levels of enterprise
maturity in terms of the defined organisational attributes:</span></p>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width="100%"
 style='width:100.0%;border-collapse:collapse'>
 <tr style='height:16.5pt'>
  <td width="20%" valign=top style='width:20.18%;border:solid #9E9E9E 1.0pt;
  background:#A9D08E;padding:0in 5.4pt 0in 5.4pt;height:16.5pt'>
  <p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
  normal'><b><span lang=EN-IN style='font-size:12.0pt;font-family:"Times New Roman",serif;
  color:#232527'>Score</span></b></p>
  </td>
  <td width="20%" style='width:20.18%;border:solid #9E9E9E 1.0pt;border-left:
  none;background:#A9D08E;padding:0in 5.4pt 0in 5.4pt;height:16.5pt'>
  <p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
  normal'><b><span lang=EN-IN style='font-size:12.0pt;font-family:"Times New Roman",serif;
  color:#232527'>Maturity Level</span></b></p>
  </td>
  <td width="59%" style='width:59.64%;border:solid #9E9E9E 1.0pt;border-left:
  none;background:#A9D08E;padding:0in 5.4pt 0in 5.4pt;height:16.5pt'>
  <p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
  normal'><b><span lang=EN-IN style='font-size:12.0pt;font-family:"Times New Roman",serif;
  color:#232527'>Organisational Attribute</span></b></p>
  </td>
 </tr>
 <tr style='height:16.5pt'>
  <td width="20%" style='width:20.18%;border:solid #9E9E9E 1.0pt;border-top:
  none;padding:0in 5.4pt 0in 5.4pt;height:16.5pt'>
  <p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
  normal'><span lang=EN-IN style='color:#232527'>Upto 2.5</span></p>
  </td>
  <td width="20%" style='width:20.18%;border-top:none;border-left:none;
  border-bottom:solid #9E9E9E 1.0pt;border-right:solid #9E9E9E 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:16.5pt'>
  <p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
  normal'><span lang=EN-IN style='font-size:12.0pt;font-family:"Times New Roman",serif;
  color:#232527'>Level 1</span></p>
  </td>
  <td width="59%" style='width:59.64%;border-top:none;border-left:none;
  border-bottom:solid #9E9E9E 1.0pt;border-right:solid #9E9E9E 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:16.5pt'>
  <p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
  normal'><span lang=EN-IN style='font-size:12.0pt;font-family:"Times New Roman",serif;
  color:#232527'>Largely ad hoc processes </span></p>
  </td>
 </tr>
 <tr style='height:16.5pt'>
  <td width="20%" style='width:20.18%;border:solid #9E9E9E 1.0pt;border-top:
  none;padding:0in 5.4pt 0in 5.4pt;height:16.5pt'>
  <p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
  normal'><span lang=EN-IN style='color:#232527'>&gt; 2.5-3.0</span></p>
  </td>
  <td width="20%" style='width:20.18%;border-top:none;border-left:none;
  border-bottom:solid #9E9E9E 1.0pt;border-right:solid #9E9E9E 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:16.5pt'>
  <p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
  normal'><span lang=EN-IN style='font-size:12.0pt;font-family:"Times New Roman",serif;
  color:#232527'>Level 2</span></p>
  </td>
  <td width="59%" style='width:59.64%;border-top:none;border-left:none;
  border-bottom:solid #9E9E9E 1.0pt;border-right:solid #9E9E9E 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:16.5pt'>
  <p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
  normal'><span lang=EN-IN style='font-size:12.0pt;font-family:"Times New Roman",serif;
  color:#232527'>Process of standardisation has just begun</span></p>
  </td>
 </tr>
 <tr style='height:16.5pt'>
  <td width="20%" style='width:20.18%;border:solid #9E9E9E 1.0pt;border-top:
  none;padding:0in 5.4pt 0in 5.4pt;height:16.5pt'>
  <p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
  normal'><span lang=EN-IN style='color:#232527'>&gt;3.0-3.5</span></p>
  </td>
  <td width="20%" style='width:20.18%;border-top:none;border-left:none;
  border-bottom:solid #9E9E9E 1.0pt;border-right:solid #9E9E9E 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:16.5pt'>
  <p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
  normal'><span lang=EN-IN style='font-size:12.0pt;font-family:"Times New Roman",serif;
  color:#232527'>Level 3</span></p>
  </td>
  <td width="59%" style='width:59.64%;border-top:none;border-left:none;
  border-bottom:solid #9E9E9E 1.0pt;border-right:solid #9E9E9E 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:16.5pt'>
  <p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
  normal'><span lang=EN-IN style='font-size:12.0pt;font-family:"Times New Roman",serif;
  color:#232527'>Processes are standardised</span></p>
  </td>
 </tr>
 <tr style='height:16.5pt'>
  <td width="20%" style='width:20.18%;border:solid #9E9E9E 1.0pt;border-top:
  none;padding:0in 5.4pt 0in 5.4pt;height:16.5pt'>
  <p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
  normal'><span lang=EN-IN style='color:#232527'>&gt;3.5-4.0</span></p>
  </td>
  <td width="20%" style='width:20.18%;border-top:none;border-left:none;
  border-bottom:solid #9E9E9E 1.0pt;border-right:solid #9E9E9E 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:16.5pt'>
  <p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
  normal'><span lang=EN-IN style='font-size:12.0pt;font-family:"Times New Roman",serif;
  color:#232527'>Level 4</span></p>
  </td>
  <td width="59%" style='width:59.64%;border-top:none;border-left:none;
  border-bottom:solid #9E9E9E 1.0pt;border-right:solid #9E9E9E 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:16.5pt'>
  <p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
  normal'><span lang=EN-IN style='font-size:12.0pt;font-family:"Times New Roman",serif;
  color:#232527'>Processes are standardised, measured and controlled</span></p>
  </td>
 </tr>
 <tr style='height:32.25pt'>
  <td width="20%" style='width:20.18%;border:solid #9E9E9E 1.0pt;border-top:
  none;padding:0in 5.4pt 0in 5.4pt;height:32.25pt'>
  <p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
  normal'><span lang=EN-IN style='color:#232527'>&gt;4.0</span></p>
  </td>
  <td width="20%" style='width:20.18%;border-top:none;border-left:none;
  border-bottom:solid #9E9E9E 1.0pt;border-right:solid #9E9E9E 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:32.25pt'>
  <p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
  normal'><span lang=EN-IN style='font-size:12.0pt;font-family:"Times New Roman",serif;
  color:#232527'>Level 5</span></p>
  </td>
  <td width="59%" style='width:59.64%;border-top:none;border-left:none;
  border-bottom:solid #9E9E9E 1.0pt;border-right:solid #9E9E9E 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:32.25pt'>
  <p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
  normal'><span lang=EN-IN style='font-size:12.0pt;font-family:"Times New Roman",serif;
  color:#232527'>Processes are standardised, measured and continually improved
  for optimization</span></p>
  </td>
 </tr>
</table>

<p class=MsoNormal style='margin-right:-2.3pt;text-align:justify'><span
lang=EN-IN>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify'>

<table cellpadding=0 cellspacing=0 align=left width="99%">
 <tr>
  <td></td>
  <td style="width: 100%;
    font-size: 20;
    height: 30px;
    color: white;
    font-weight: bold;
    font-color: white;
    background: #2EA08C;
    padding-left:20px;">
   SCOPE OF ASESSMENT 
  </td>
 </tr>
</table>
  </p>
<br clear=ALL>

<p class=MsoNormal style='text-align:justify;line-height:115%;'><span
lang=EN-IN style='line-height:115%;font-family:"Cambria",serif;
color:black'>The assessment is conducted at the manufacturing facility for
which the application is made and is based on the requirements as laid down in
the relevant SAMAR Maturity Assessment Model.</span></p>

<p class=MsoNormal style='margin-top:12.0pt;text-align:justify;line-height:
115%;'><span lang=EN-IN style='line-height:
115%;font-family:"Cambria",serif;color:black'>This report is an outcome of an
extensive site-assessment of the unit based on the SAMAR Maturity Assessment Model.
</span></p>
<p class=MsoNormal style='text-align:justify;line-height:115%;'><i><span
style='line-height:115%;font-family:"Cambria",serif;color:black;
background:yellow'>In this report, findings are presented on an exception basis
only.</span></i></p>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify'>

<table cellpadding=0 cellspacing=0 align=left width="99%">
 <tr>
  <td></td>
  <td style="width: 100%;
    font-size: 20;
    height: 30px;
    color: white;
    font-weight: bold;
    font-color: white;
    background: #2EA08C;
    padding-left:20px;">
    DISCLAIMER
  </td>
 </tr>
</table>
</p>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
115%;'><i><span lang=EN-IN style='font-size:10.0pt;
line-height:115%;font-family:"Cambria",serif'>&nbsp;</span></i></p>

<br clear=ALL>

<p class=MsoNormal style='margin-bottom:0in;text-align:left;line-height:
115%;'><i><span lang=EN-IN style='line-height:
115%;font-family:"Cambria",serif;color:black'>This disclaimer governs the use
of this report.&nbsp;By using this report, the organisation accepts this
disclaimer in full.</span></i></p>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
115%;'><i><span lang=EN-IN style='line-height:
115%;font-family:"Cambria",serif'>&nbsp;</span></i></p>

<p class=MsoListParagraphCxSpFirst style='text-align:justify;text-indent:-.25in;
line-height:115%;'><b><span lang=EN-IN style='line-height:115%;font-family:"Cambria",serif'>1.<span style='font:7.0pt "Times New Roman"'>
</span></span></b><span lang=EN-IN style='line-height:115%;
font-family:"Cambria",serif;color:black'>This is an electronically generated
report/transcript and is authenticated by the Quality Council of India (QCI).
It does not require any signature.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-indent:-.25in;
line-height:115%;'><b><span lang=EN-IN style='line-height:115%;font-family:"Cambria",serif'>2.<span style='font:7.0pt "Times New Roman"'>
</span></span></b><span lang=EN-IN style='line-height:115%;
font-family:"Cambria",serif;color:black'>The rating and certification provided,
pertains only to the processes and systems of the unit for which assessment is
conducted.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-indent:-.25in;
line-height:115%;'><b><span lang=EN-IN style='line-height:115%;font-family:"Cambria",serif'>3.<span style='font:7.0pt "Times New Roman"'>
</span></span></b><span lang=EN-IN style='line-height:115%;
font-family:"Cambria",serif;color:black'>The applicant enterprise has
understood fully that it needs to ensure compliance to all legal, statutory and
regulatory requirements. Any deviation or non-fulfilment of compliances may
result in withdrawal of the rating and certification. </span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-indent:-.25in;
line-height:115%;'><b><span lang=EN-IN style='line-height:115%;font-family:"Cambria",serif'>4.<span style='font:7.0pt "Times New Roman"'>
</span></span></b><span lang=EN-IN style='line-height:115%;
font-family:"Cambria",serif;color:black'>The rating &amp; certification
provided is valid only for a period of 2 years from the date of issue as
mentioned on the Certificate. </span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-indent:-.25in;
line-height:115%;'><b><span lang=EN-IN style='line-height:115%;font-family:"Cambria",serif'>5.<span style='font:7.0pt "Times New Roman"'>
</span></span></b><span lang=EN-IN style='line-height:115%;
font-family:"Cambria",serif;color:black'>This report contains a summary of
critically evaluated assessment findings as observed during the assessment and
represents the final rating and certification, validated by the Rating
Committee, based on the relevant SAMAR Maturity Assessment model. </span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-indent:-.25in;
line-height:115%;'><b><span lang=EN-IN style='line-height:115%;font-family:"Cambria",serif'>6.<span style='font:7.0pt "Times New Roman"'>
</span></span></b><span lang=EN-IN style='line-height:115%;
font-family:"Cambria",serif;color:black'>This report is meant for the use and
benefit of the applicant enterprise and DRDO/QCI shall not accept any liability
that may arise if this report is used for any alternative purpose. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;text-align:justify;
text-indent:-.25in;line-height:115%;'><b><span lang=EN-IN
style='line-height:115%;font-family:"Cambria",serif'>7.<span
style='font:7.0pt "Times New Roman"'> </span></span></b><span
lang=EN-IN style='line-height:115%;font-family:"Cambria",serif;
color:black'>Information not disclosed by the applicant enterprise may alter
the findings outlined in this report. In such situations, QCI reserves the
right to withdraw or amend its rating and conclusions.</span></p>

<p class=MsoListParagraphCxSpLast style='margin-bottom:0in;text-align:justify;
line-height:115%;'><span lang=EN-IN style='font-size:9.0pt;
line-height:115%;font-family:"Cambria",serif'>&nbsp;</span></p>