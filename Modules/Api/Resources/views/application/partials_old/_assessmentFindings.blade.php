<p class=MsoNormal style='margin-bottom:0in;text-align:justify'>
<table cellpadding=0 cellspacing=0 align=left width="99%">
 <tr>
  <td style="width: 100%;
    font-size: 20;
    height: 30px;
    color: white;
    font-weight: bold;
    font-color: white;
    background: #2EA08C;
    padding-left:20px;">
    ASSESSMENT FINDINGS
  </td>
 </tr>
</table>
</p>
<br /><br />

<div class=MsoNormal style='margin-bottom:0in;text-align:justify'><span
style='position:absolute;z-index:251715583;left:0px;margin-left:0px;margin-top:
17px;width:642px;height:21px;
width: 100%;
    font-size: 16;
    font-color: white;
    background: #F2F2F2;
    padding-left:20px;
    padding-top:8px;
'>
Methodology
</span><span lang=EN-IN>                </span><b><span
lang=EN-IN style='font-family:"Cambria",serif'>                </span></b>
</div>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify'><i><span
lang=EN-IN style='font-size:14.0pt;line-height:107%;font-family:"Cambria",serif'>&nbsp;</span></i></p>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify'><span
lang=EN-IN style='line-height:107%;font-family:"Cambria",serif'>&nbsp;</span></p>

<br clear=ALL>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify;background:#F2F2F2'><span
lang=EN-IN style='line-height:107%;font-family:"Cambria",serif;color:black'>To
ensure transparency in the site assessment process, all site-assessment components
are monitored electronically and the entire process (documents, photographs,
comments etc.) is captured on a secure mobile application by the assessors. The
data captured during site-assessment is not shared externally with anyone
except the relevant stakeholders involved for award of score/rating.</span></p>
<br />

<div class="site-pictures" style="margin-bottom: 20px;">
    <div style="width:45%; float:left;height:400px;">
        @if(!empty($application->siteExteriorPicture->path))
            <img width="430px" style="max-height:350px;" src="{{storage_path('app/public/'.$application->siteExteriorPicture->path)}}" align="left">
            Site Exterior Picture
        @endif
    </div>
    <div style="width:45%; float:right;height:400px;">
    @if(!empty($assessmentEvidenses))
        @foreach($assessmentEvidenses as $evidense)
            @if(!empty($evidense->type) && $evidense->type == 'open_meet')
                <img width="430px" style="max-height:350px;" src="{{storage_path('app/public/'.$evidense->path)}}">
                Opening Meeting
            @endif
        @endforeach
    @endif
    </div>
</div>
<div class="site-pictures" style="margin-bottom: 20px;">
    <div style="width:45%; float:left;height:400px;">
    @if(!empty($assessmentEvidenses))
        @foreach($assessmentEvidenses as $evidense)
            @if(!empty($evidense->type) && $evidense->type == 'close_meet')
                <img width="430px" style="max-height:350px;" src="{{storage_path('app/public/'.$evidense->path)}}">
                Closing Meeting
            @endif
        @endforeach
    @endif
    </div>
    <div style="width:45%; float:right;height:400px;"></div>
</div>
<br />
<br />
<br />
<div class=MsoNormal style='margin-bottom:0in;text-align:justify'><span
style='position:absolute;z-index:251715583;left:0px;margin-left:0px;margin-top:
700px;width:642px;height:21px;
width: 100%;
    font-size: 16;
    font-color: white;
    background: #F2F2F2;
    padding-left:20px;
    padding-top:8px;
'>
Performance on Individual Parameters
<?php
$parameters = $application->assessment->parameters();
?>
</span><span lang=EN-IN>                </span><b><span
lang=EN-IN style='font-family:"Cambria",serif'>                </span></b>
</div>
<p class=MsoNormal style='margin-bottom:0in'><span lang=EN-IN style='font-family:
"Cambria",serif'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0in'><span lang=EN-IN style='font-family:
"Cambria",serif'>&nbsp;</span></p>

<div align=center>

<table class=MsoTable15Grid1LightAccent3 border=1 cellspacing=0 cellpadding=0
 width="100%" style='width:100.32%;border-collapse:collapse;border:none'>
 <tr style='height:1.0pt'>
  <td width="9%" style='width:9.92%;border:solid #DBDBDB 1.0pt;border-bottom:
  solid #C9C9C9 1.5pt;background:#319F82;padding:0in 5.4pt 0in 5.4pt;
  height:1.0pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:200%'><b><span lang=EN-GB style='line-height:200%;font-family:
  "Cambria",serif;color:white'>S.</span></b><b><span lang=EN-GB
  style='line-height:200%;font-family:"Cambria",serif;color:white'> No</span></b></p>
  </td>
  <td width="80%" style='width:80.16%;border-top:solid #DBDBDB 1.0pt;
  border-left:none;border-bottom:solid #C9C9C9 1.5pt;border-right:solid #DBDBDB 1.0pt;
  background:#319F82;padding:0in 5.4pt 0in 5.4pt;height:1.0pt'>
  <p class=MsoNormal style='margin-bottom:0in;line-height:200%'><b><span
  lang=EN-GB style='line-height:200%;font-family:"Cambria",serif;color:white'>Name
  of Parameter</span></b></p>
  </td>
  <td width="9%" style='width:9.92%;border-top:solid #DBDBDB 1.0pt;border-left:
  none;border-bottom:solid #C9C9C9 1.5pt;border-right:solid #DBDBDB 1.0pt;
  background:#319F82;padding:0in 5.4pt 0in 5.4pt;height:1.0pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:200%'><b><span lang=EN-GB style='line-height:200%;font-family:
  "Cambria",serif;color:white'>Score</span></b></p>
  </td>
 </tr>
 @if(!empty($parameters))
  @foreach($parameters as $key=>$parameter)
  <tr style='height:1.0pt'>
  <td width="9%" style='width:9.92%;border:solid #DBDBDB 1.0pt;border-top:none;
  padding:0in 5.4pt 0in 5.4pt;height:1.0pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:200%'><b><span lang=EN-GB style='line-height:200%;font-family:
  "Cambria",serif'>
  {{$key+1}}</span></b></p></td>
  <td width="9%" style='width:9.92%;border:solid #DBDBDB 1.0pt;border-top:none;
  padding:0in 5.4pt 0in 5.4pt;height:1.0pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:200%'><b><span lang=EN-GB style='line-height:200%;font-family:
  "Cambria",serif'>
  {{$parameter->parameter}}</span></b></p></td>
  <td width="9%" style='width:9.92%;border:solid #DBDBDB 1.0pt;border-top:none;
  padding:0in 5.4pt 0in 5.4pt;height:1.0pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:200%'><b><span lang=EN-GB style='line-height:200%;font-family:
  "Cambria",serif'>
  {{$parameter->parameter_score}}</span></b></p></td>
  </tr>
  @endforeach
  @endif
</table>

</div>

<p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
line-height:200%'><b><span lang=EN-GB style='line-height:200%;font-family:"Cambria",serif;
color:red'>All parameters (as applicable, to be listed)</span></b></p>

<span lang=EN-IN style='font-size:11.0pt;line-height:107%;font-family:"Cambria",serif'><br
clear=all>
</span>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify'><span
lang=EN-IN style='font-family:"Cambria",serif'>The scores obtained on the above
parameters are a weighted average of the unit’s performance as assessed on the
relevant SAMAR Maturity Assessment Model. The key purpose of this assessment
is to objectively measure the performance of the unit’s processes and systems on
a maturity scale of 1 to 5. This is intended for the applicant enterprise to
identify and address areas of concern and subsequently improve the current
performance to higher maturity levels thereby enhancing its capabilities and
competitiveness.</span></p>

<p class=MsoNormal style='margin-top:12.0pt;margin-right:0in;margin-bottom:
0in;margin-left:0in;text-align:justify'>

<div class=MsoNormal style='margin-bottom:0in;text-align:justify'><span
style='position:absolute;z-index:251715583;left:0px;margin-left:0px;margin-top:
17px;width:642px;height:21px;
width: 100%;
    font-size: 16;
    font-color: white;
    background: #F2F2F2;
    padding-left:20px;
    padding-top:8px;
'>
Score Obtained and Rating
</span><span lang=EN-IN>                </span><b><span
lang=EN-IN style='font-family:"Cambria",serif'>                </span></b>
</div>
<br /><br />
<br clear=ALL>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify;background:#F2F2F2'><span
lang=EN-IN style='font-family:"Cambria",serif;color:black'>Your performance on SAMAR
parameters are tabulated below:</span></p>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify'><span
lang=EN-IN>&nbsp;</span></p>

<div align=center>

<table class=MsoTableGridLight border=1 cellspacing=0 cellpadding=0 width="100%"
 style='width:100.0%;border-collapse:collapse;border:none'>
 <tr style='height:36.65pt'>
  <td width="49%" colspan=2 style='width:49.94%;border:solid #BFBFBF 1.0pt;
  background:#319F82;padding:0in 5.4pt 0in 5.4pt;height:36.65pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span lang=EN-GB style='font-size:12.0pt;font-family:
  "Cambria",serif;color:white'>Parameters</span></p>
  </td>
  <td width="17%" style='width:17.44%;border:solid #BFBFBF 1.0pt;border-left:
  none;background:#319F82;padding:0in 5.4pt 0in 5.4pt;height:36.65pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><b><span lang=EN-GB style='font-size:12.0pt;font-family:
  "Cambria",serif;color:white'> Score</span></b></p>
  </td>
  <td width="32%" style='width:32.62%;border:solid #BFBFBF 1.0pt;border-left:
  none;background:#319F82;padding:0in 5.4pt 0in 5.4pt;height:36.65pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><b><span lang=EN-GB style='font-size:12.0pt;font-family:
  "Cambria",serif;color:white'>SAMAR Rating</span></b></p>
  </td>
 </tr>
 <tr style='height:34.65pt'>
  <td width="24%" style='width:24.86%;border:solid #BFBFBF 1.0pt;border-top:
  none;padding:0in 5.4pt 0in 5.4pt;height:34.65pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><b><span lang=EN-IN style='font-size:10.0pt;font-family:
  "Cambria",serif'>Number </span></b></p>
  </td>
  <td width="25%" style='width:25.08%;border-top:none;border-left:none;
  border-bottom:solid #BFBFBF 1.0pt;border-right:solid #BFBFBF 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:34.65pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span lang=EN-IN style='font-family:"Cambria",serif'>{{count($parameters)}}</span></p>
  </td>
  <td width="17%" rowspan=2 style='width:17.44%;border-top:none;border-left:
  none;border-bottom:solid #BFBFBF 1.0pt;border-right:solid #BFBFBF 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:34.65pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><b><span lang=EN-IN style='font-family:"Cambria",serif'>{{$application->assessment->assessment_score}}</span></b></p>
  </td>
  <td width="32%" rowspan=2 style='width:32.62%;border-top:none;border-left:
  none;border-bottom:solid #BFBFBF 1.0pt;border-right:solid #BFBFBF 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:34.65pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><b>{{__("api.".$application->assessment->certification_type."-certification")}}</b></p>
  </td>
 </tr>
 <tr style='height:36.65pt'>
  <td width="24%" style='width:24.86%;border:solid #BFBFBF 1.0pt;border-top:
  none;padding:0in 5.4pt 0in 5.4pt;height:36.65pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><b><span lang=EN-IN style='font-family:"Cambria",serif'>Score</span></b></p>
  </td>
 <?php 
 $totalParameter = !empty($parameters) ? $parameters->toArray() : [];
 $totalParameterScore= array_sum(array_column($totalParameter, 'parameter_score'));
 ?> 
  <td width="25%" style='width:25.08%;border-top:none;border-left:none;
  border-bottom:solid #BFBFBF 1.0pt;border-right:solid #BFBFBF 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:36.65pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span lang=EN-IN style='font-family:"Cambria",serif'>{{$totalParameterScore}}</span></p>
  </td>
 </tr>
</table>

</div>
