<p class=MsoNormal style='margin-bottom:0in;text-align:justify'>

<table cellpadding=0 cellspacing=0 align=left width="99%">
 <tr>
  <td></td>
  <td style="width: 100%;
    font-size: 20;
    height: 30px;
    color: white;
    font-weight: bold;
    font-color: white;
    background: #2EA08C;
    padding-left:20px;">
    SITE ASSESSMENT SUMMARY 
  </td>
 </tr>
</table>
</p>
<p class=MsoNormal><span lang=EN-IN>&nbsp;</span></p>

<br clear=ALL>

<table class=MsoTableGridLight border=1 cellspacing=0 cellpadding=0 width="99%"
 style='border-collapse:collapse;border:none'>
 <tr style='height:42.75pt'>
  <td width="14%" style='width:14.68%;border:solid #BFBFBF 1.0pt;background:
  #D5DCE4;padding:0in 5.4pt 0in 5.4pt;height:42.75pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:115%'><b><span lang=EN-IN style='font-size:9.0pt;line-height:
  115%;font-family:"Cambria",serif'>SAMAR <span style='color:black'>ID</span></span></b></p>
  </td>
  <td width="11%" style='width:11.78%;border:solid #BFBFBF 1.0pt;border-left:
  none;background:#D5DCE4;padding:0in 5.4pt 0in 5.4pt;height:42.75pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:115%'><b><span lang=EN-IN style='font-size:9.0pt;line-height:
  115%;font-family:"Cambria",serif;color:black'>Enterprise Name</span></b></p>
  </td>
  <td width="23%" style='width:23.6%;border:solid #BFBFBF 1.0pt;border-left:
  none;background:#D5DCE4;padding:0in 5.4pt 0in 5.4pt;height:42.75pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:115%'><b><span lang=EN-IN style='font-size:9.0pt;line-height:
  115%;font-family:"Cambria",serif;color:black'>Address of the unit assessed</span></b></p>
  </td>
  <td width="13%" style='width:13.18%;border:solid #BFBFBF 1.0pt;border-left:
  none;background:#D5DCE4;padding:0in 5.4pt 0in 5.4pt;height:42.75pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:115%'><b><span lang=EN-IN style='font-size:9.0pt;line-height:
  115%;font-family:"Cambria",serif;color:black'>Assessment Dates</span></b></p>
  </td>
  <td width="36%" valign=top style='width:36.76%;border:solid #BFBFBF 1.0pt;
  border-left:none;background:#D5DCE4;padding:0in 5.4pt 0in 5.4pt;height:42.75pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:115%'><b><span lang=EN-IN style='font-size:9.0pt;line-height:
  115%;font-family:"Cambria",serif'>&nbsp;</span></b></p>
  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:115%'><b><span lang=EN-IN style='font-size:9.0pt;line-height:
  115%;font-family:"Cambria",serif;color:black'>Key Products Manufactured</span></b></p>
  </td>
 </tr>
 <tr style='height:28.5pt'>
  <td width="14%" style='width:14.68%;border:solid #BFBFBF 1.0pt;border-top:
  none;padding:0in 5.4pt 0in 5.4pt;height:28.5pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'>
  <span lang=EN-IN style='font-size:9.0pt;font-family:"Cambria",serif;
  color:black'>&nbsp;</span>
  {{$application->user->user->username}}
  </p>
  </td>
  <td width="11%" style='width:11.78%;border-top:none;border-left:none;
  border-bottom:solid #BFBFBF 1.0pt;border-right:solid #BFBFBF 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:28.5pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span lang=EN-IN style='font-size:9.0pt;font-family:"Cambria",serif;
  color:black'>&nbsp;</span>
    {{$application->enterprise_name}}
  </p>
  </td>
  <td width="23%" style='width:23.6%;border-top:none;border-left:none;
  border-bottom:solid #BFBFBF 1.0pt;border-right:solid #BFBFBF 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:28.5pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span lang=EN-IN style='font-size:9.0pt;font-family:"Cambria",serif;
  color:black'>&nbsp;</span>
    {{$application->unit_address}} – {{$application->enterprise_name}}
  </p>
  </td>
  <td width="13%" style='width:13.18%;border-top:none;border-left:none;
  border-bottom:solid #BFBFBF 1.0pt;border-right:solid #BFBFBF 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:28.5pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span lang=EN-IN style='font-size:9.0pt;font-family:"Cambria",serif;
  color:black'>&nbsp;</span>
    {{\Modules\Core\Helpers\BasicHelper::showDate($application->assessment_date)}}
  </p>
  </td>
  <td width="36%" valign=top style='width:36.76%;border-top:none;border-left:
  none;border-bottom:solid #BFBFBF 1.0pt;border-right:solid #BFBFBF 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:28.5pt'>
  <p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
  line-height:normal'><span lang=EN-IN style='font-size:9.0pt;font-family:"Cambria",serif;
  color:black'>&nbsp;</span>
  <?php 
  $products = !empty($application->ownership->product_manufactured) ? json_decode($application->ownership->product_manufactured) : [];
  $allProductNames = array_column($products, 'productName');
  $allProductNames = implode(",", $allProductNames);
  ?>
    {{$allProductNames}}
  </p>
  </td>
 </tr>
</table>
@if(!empty($latLongData))
<p class=MsoNormal style='margin-bottom:0in;text-align:justify'><b><span
lang=EN-IN style='font-size:12.0pt;line-height:107%;font-family:"Cambria",serif'>&nbsp;</span></b></p>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify'>

<table cellpadding=0 cellspacing=0 align=left width="99%">
 <tr>
  <td></td>
  <td style="width: 100%;
    font-size: 20;
    height: 30px;
    color: white;
    font-weight: bold;
    font-color: white;
    background: #2EA08C;
    padding-left:20px;">
    {{!empty($latLongData['latitude']) ? $latLongData['latitude'] : ""}},
    {{!empty($latLongData['longitude']) ? $latLongData['longitude'] : ""}}
  </td>
 </tr>
</table>
<br /><br /><br />

<!--
<img width=98% height=211
src="{{asset('reports/img/')}}/image015.png"
align='left' hspace=12></p>
-->
@endif

<p class=MsoNormal align=center style='margin-bottom:0in;text-align:center'><b><span
lang=EN-IN style='font-size:12.0pt;line-height:107%;font-family:"Cambria",serif'>&nbsp;</span></b></p>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify'>

<table cellpadding=0 cellspacing=0 align=left width="99%">
 <tr>
  <td style="width: 100%;
    font-size: 20;
    height: 30px;
    color: white;
    font-weight: bold;
    font-color: white;
    background: #2EA08C;
    padding-left:20px;">
    SAMAR Rating
  </td>
 </tr>
</table>

<b><span lang=EN-IN style='font-size:12.0pt;line-height:107%;font-family:"Cambria",serif'>&nbsp;</span></b></p>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify'><b><span
lang=EN-IN style='font-size:12.0pt;line-height:107%;font-family:"Cambria",serif'>&nbsp;</span></b></p>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify'><b><span
lang=EN-IN style='font-size:12.0pt;line-height:107%;font-family:"Cambria",serif'>&nbsp;</span></b></p>

<div class=MsoNormal align=center style='text-align:center;'>
<hr style="width:400px; text-align:center;border-color:blue; border-width:2px;">
{{__("api.".$application->assessment->certification_type."-certification")}}
<hr style="width:400px; text-align:center;border-color:blue;border-width:2px;">

<br clear=ALL>
<span lang=EN-IN style='line-height:107%;font-family:"Cambria",serif; font-weight:bold;font-size;20px;'>
Score: {{$application->assessment->assessment_score}}
</span>
</div>

<p class=MsoNormal style='text-align:justify'><span lang=EN-IN
style='line-height:107%;font-family:"Cambria",serif'>&nbsp;</span></p>

<p class=MsoNormal style='text-align:justify'><span lang=EN-IN>&nbsp;</span></p>

<br clear=ALL>
