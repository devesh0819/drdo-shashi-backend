<!DOCTYPE html>
<html>

<head>
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<meta name=Generator content="Microsoft Word 15 (filtered)">
<style>
  .level1
  {
    border-color: #4A361A;
    border: 5px solid;
    height: 720px;
    width: 630px;
  }
  .level2
  {
    border-color: #C0C0C0;
    border: 2px solid;
    height: 600px;
    width: 500px;
  }
  .level3
  {
    border-color: #FFD700;
    border: 2px solid;
    height: 600px;
    width: 500px;
  }
  .level4
  {
    border-color: #B9F2FF;
    border: 2px solid;
    height: 600px;
    width: 500px;
  }
  .level5
  {
    border-color: #E5E4E2;
    border: 2px solid;
    height: 600px;
    width: 500px;
  }
</style>
</head>

<body lang=EN-US link="#0563C1" vlink="#954F72" style='word-wrap:break-word'>

<div class=WordSection1 style="background-image:url({{public_path('pdf/img/certificate.jpg')}});
   background-repeat:no-repeat;
   background-size:contain;
   height:720px;width:630px;font-size:13px;">
      
      <div class="details">
          <div class="score" style="margin-left: -29px;
    padding-top: 239px;
    text-align: center;
    font-size: 13px;
    color: white;
    font-weight: bold;">
              {{!empty($application->assessment->assessment_score) ? __("api.".$application->assessment->certification_type."-val") : null}}
          </div>
          <div class="unit_name" style="margin-left: 252px;
    margin-top: 140px;
    font-size: 18px;">
              {{!empty($application->enterprise_name) ? $application->enterprise_name : null}}
          </div>
          <div class="unit_address" style="margin-left: 237px;
    margin-top: 35px;
    font-size: 18px;">
          {{!empty($application->unit_address) ? $application->unit_address : null}}
          </div>
          <div class="issuedate" style="margin-left: 144px;
    margin-top: 169px;">
            {{!empty($application->assessment->certification_expiry) ? date("Y-m-d", strtotime("-2 year", strtotime($application->assessment->certification_expiry))) : null}}
          </div>
          <div class="expirydate" style="margin-left: 114px;
    margin-top: -2px;">
          {{!empty($application->assessment->certification_expiry) ? date("Y-m-d", strtotime($application->assessment->certification_expiry)) : null}}</div>
        </div>
      </div>
</div>

</body>

</html>