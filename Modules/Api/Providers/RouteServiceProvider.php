<?php

namespace Modules\Api\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    private $frontendAPIPrefix = '/applicant';
    private $backendApiPrefix = '/admin';
    
    /**
     * The module namespace to assume when generating URLs to actions.
     *
     * @var string
     */
    protected $moduleNamespace = 'Modules\Api\Http\Controllers';

    /**
     * Called before routes are registered.
     *
     * Register any model bindings or pattern based filters.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
            ->namespace($this->moduleNamespace)
            ->group(module_path('Api', '/Routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {   
        $middlewares = ['api', 'purify'];
        
        /**
         * Auth apis
         */
        Route::prefix('api/v1')
            ->middleware($middlewares)
            ->namespace('Modules\Api\Http\Controllers\Auth')
            ->name('api.v1.')
            ->group(__DIR__ . '/../Routes/api/auth.php');
        
        /**
         * Applicant apis
         */
        Route::prefix('api/v1'.$this->frontendAPIPrefix)
            ->middleware(array_merge($middlewares, ['auth:api', 'isApplicant', 'IsEmailVerified']))
            ->namespace('Modules\Api\Http\Controllers\Applicant')
            ->name('api.v1.')
            ->group(__DIR__ . '/../Routes/api/applicant.php');
        /**
         * Admin apis
         */
        Route::prefix('api/v1'. $this->backendApiPrefix)
        ->middleware(array_merge($middlewares, ['auth:api', 'isAdmin']))
        ->namespace('Modules\Api\Http\Controllers\Admin')
        ->name('api.v1.')
        ->group(__DIR__ . '/../Routes/api/admin.php');
        
        /**
         * Agency apis
         */
        Route::prefix('api/v1/agency')
        ->middleware(array_merge($middlewares, ['auth:api', 'isAgency']))
        ->namespace('Modules\Api\Http\Controllers\Agency')
        ->name('api.v1.')
        ->group(__DIR__ . '/../Routes/api/agency.php');

        /**
         * Assessor apis
         */
        Route::prefix('api/v1/assessor')
        ->middleware(array_merge($middlewares, ['auth:api', 'isAssessor']))
        ->namespace('Modules\Api\Http\Controllers\Assessor')
        ->name('api.v1.')
        ->group(__DIR__ . '/../Routes/api/assessor.php');

        /**
         * Assessor apis
         */
        Route::prefix('api/v1/assessor')
        ->middleware(array_merge($middlewares, ['auth:api', 'isAssessor']))
        ->namespace('Modules\Api\Http\Controllers\Assessor')
        ->name('api.v1.')
        ->group(__DIR__ . '/../Routes/api/assessor_sync.php');
        
        /**
         * DRDO apis
         */
        Route::prefix('api/v1/drdo')
        ->middleware(array_merge($middlewares, ['auth:api', 'isDrdo']))
        ->namespace('Modules\Api\Http\Controllers\Drdo')
        ->name('api.v1.drdo.')
        ->group(__DIR__ . '/../Routes/api/drdo.php');

        /**
         * Core Apis
         */
        Route::prefix('api/v1')
        ->middleware(array_merge($middlewares, []))
        ->namespace('Modules\Api\Http\Controllers\Core')
        ->name('api.v1.core.')
        ->group(__DIR__ . '/../Routes/api/core.php');
    }
}
