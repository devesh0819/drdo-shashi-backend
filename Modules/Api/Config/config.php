<?php

return [
    'name' => 'Api',
    'app_backend_url'=>env('APP_BACKEND_URL', 'https://qci-api.cloudzmall.com'),
    'app_frontend_url'=>env('APP_FRONTEND_URL', 'http://qci-web.cloudzmall.com'),
    'payment_success_url'=>env('PAYMENT_SUCCESS_URL', 'http://qci-web.cloudzmall.com'),
    'payment_failed_url'=>env('PAYMENT_FAILED_URL', 'http://qci-web.cloudzmall.com/login'),
    'application'=>[
        'maxTestingMethodCount'=>5,
        'maxProductManufactureCount'=>5,
        'maxKeyDomesticCustomerCount'=>5,
        'maxKeyRawMaterialsCount'=>5,
        'maxBoughtoutpartsCount'=>5,
        'maxKeyVendorsCount'=>5,
        'maxKeyProcessesCount'=>10,
        'maxProcessesOutsourcedCount'=>10,
        'maxEnergyResourcesUsedCount'=>10,
        'maxNaturalResourcesUsedCount'=>10,
        'maxEnvironmentConsentsNameCount'=>10
    ],
    'applicationReportFormat'=>[
        'pages'=>[
            'coverPage'=>[

            ],
            'aboutUs'=>[

            ],
            'assessmentSummary'=>[

            ],
            'assessmentFindings'=>[

            ],
            'performanceAnalysis'=>[

            ]
        ]
    ]
];
