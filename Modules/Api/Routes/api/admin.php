<?php
use Illuminate\Support\Facades\Route;

/** Admin Crud **/
Route::resource('admin', AdminController::class)->only([
    'index', 'store', 'update', 'destroy'
]);

/** Member Crud **/
Route::resource('member', MemberController::class)->only([
    'index', 'store', 'update', 'destroy', 'show'
]);

/** Agency Crud **/
Route::resource('agency', AgencyController::class)->only([
    'index', 'store', 'update', 'destroy', 'show'
]);

/** Assessor Crud */
Route::resource('assessor', AssessorController::class)->only([
    'index', 'store', 'destroy', 'show'
]);
Route::post('assessor/{assessor}', 'AssessorController@update')
->name('assessor.update');

/** Committee Crud */
Route::resource('committee', CommitteeController::class)->only([
    'index', 'store', 'update', 'destroy', 'show'
]);
/**Meetings Routes */
Route::post('committee/{committeeID}/meeting', 'MeetingController@store')
->name('meeting.store');
Route::post('meeting/{meetingID}', 'MeetingController@update')
->name('meeting.update');
Route::get('meeting/{meetingID}', 'MeetingController@show')
->name('meeting.show');

/**Meetings Notes Routes */
Route::post('meeting/{meetingID}/note', 'MeetingController@storeNote')
->name('meeting.store');

/**Application ROutes */
Route::get('applications', 'ApplicationController@index')
->name('application.index')->middleware(['can:view,Modules\Application\Entities\Application']);
Route::get('application/{applicationID}', 'ApplicationController@show')
->name('application.show')->middleware(['can:view,Modules\Application\Entities\Application']);
Route::get('application/payment-detail/{applicationID}', 'ApplicationController@paymentDetail')
->name('application.paymentDetail')->middleware(['can:view,Modules\Application\Entities\Application']);
Route::post('application/{applicationID}/approvePayment', 'ApplicationController@approvePayment')
->name('application.approvePayment')->middleware(['can:view,Modules\Application\Entities\Application']);

/** Save Payment Configgurations */
Route::get('payment/get-config', 'AdminController@getPaymentConfig')
->name('payment.get-config')->withoutMiddleware(['auth:api', 'isAdmin']);

Route::post('payment/save-config', 'AdminController@savePaymentConfig')
->name('payment.store-config');

/** Feedback Question Configurations */
Route::get('feedback/get-config', 'AdminController@getFeedbackConfig')
->name('feedback.get-config')->withoutMiddleware(['auth:api', 'isAdmin']);

Route::post('feedback/save-config', 'AdminController@saveFeedbackConfig')
->name('feedback.store-config');

Route::delete('feedback/{Id}', 'AdminController@deleteFeedback')
->name('feedbackConfig.delete');

Route::post('feedback/{Id}/update', 'AdminController@updateFeedback')
->name('feedback.update');

/** Admin Assessments Crud */
Route::get('assessments', 'AssessmentController@index')
->name('assessment.index')->middleware(['can:view,Modules\Assessment\Entities\Assessment']);
Route::post('application/{applicationID}/assessment', 'AssessmentController@store')
->name('assessment.create')->middleware(['can:create,Modules\Assessment\Entities\Assessment']);
Route::post('assessment/{assessmentID}/update', 'AssessmentController@update')
->name('assessment.update')->middleware(['can:update,Modules\Assessment\Entities\Assessment']);
Route::get('assessment/{assessmentID}/show', 'AssessmentController@show')
->name('assessment.show')->middleware(['can:view,Modules\Assessment\Entities\Assessment']);
Route::get('assessment/{assessmentID}/complete', 'AssessmentController@completeAssessment')
->name('admin_assessment.complete')->middleware(['can:view,Modules\Assessment\Entities\Assessment']);


/** Admin Stats API */
Route::get('getStats', 'DashboardStatsController@getStats')
->name('DashboardStats.getStats');

/** Questionnaire Management */
Route::get('questionnaires', 'QuestionnaireController@index')
->name('questionnaire.index');

Route::post('questionnaire/import', 'QuestionnaireController@import')
->name('questionnaire.import');

Route::get('questionnaire/{questionnaireId}', 'QuestionnaireController@show')
->name('questionnaire.index');
Route::get('questionnaire/{questionnaireId}/auditlogs', 'QuestionnaireController@showAudit')
->name('questionnaire.audit');
Route::get('questionnaire/parameter/{parameterId}', 'QuestionnaireController@showParameter')
->name('questionnaire.parameter.show');
Route::get('questionnaire/{questionnaireId}/sendApproval', 'QuestionnaireController@sendApproval')
->name('questionnaire.send-approval');


/** Parameters APIS */
Route::get('assessment/{assessmentID}/parameters', 'AssessmentController@getAssessmentParameters')
->name('assessor-assessment.parameters')->middleware(['can:viewParameter,Modules\Assessment\Entities\Assessment']);

Route::get('parameter/{parameterID}', 'AssessmentController@showParameter')
->name('admin.showParameter')->middleware(['can:view,Modules\Assessment\Entities\AssessmentParameter']);

Route::post('parameter/{parameterID}/submit-parameters-response', 'AssessmentController@submitParametersResponse')
->name('admin.submit-parameter-response')->middleware(['can:submitParameter,Modules\Assessment\Entities\AssessmentParameter']);

Route::post('parameter/{parameterID}/store-parameters-comment', 'AssessmentController@submitParameterComment')
->name('admin.submit-parameter-comment')->middleware(['can:submitParameter,Modules\Assessment\Entities\AssessmentParameter']);


