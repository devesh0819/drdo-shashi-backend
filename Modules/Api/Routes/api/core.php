<?php

use Illuminate\Support\Facades\Route;

Route::get('qci_master_values', 'CoreController@getMasterValues')->name('get-qci_master_values');

Route::get('file/{id}', 'FileController@showFile')->name('show.file')
->middleware(['auth:api', 'can:show,Modules\Attachment\Entities\Attachment']);

Route::get('file/{id}/delete', 'FileController@deleteFile')->name('delete.file')
->middleware(['auth:api', 'can:delete,Modules\Attachment\Entities\Attachment']);

Route::get('application/{applicationID}/report', 'ReportController@downloadReport')->name('report.download')
->middleware(['auth:api', 'can:downloadReport,Modules\Application\Entities\Application']);
Route::get('application/{applicationID}/certificate', 'ReportController@downloadCertificate')->name('certificate.download')
->middleware(['auth:api', 'can:downloadReport,Modules\Application\Entities\Application']);

