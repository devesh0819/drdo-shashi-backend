<?php

use Illuminate\Support\Facades\Route;

/** Admin Stats API */
Route::get('getStats', 'DashboardStatsController@getStats')
->name('DashboardStats.getStats');

Route::get('application', 'ApplicationController@index')
->name('application.index')->middleware(['can:view,Modules\Application\Entities\Application']);

Route::get('application/{applicationID}', 'ApplicationController@show')
->name('application.show')->middleware(['can:view,Modules\Application\Entities\Application']);

Route::post('application/{applicationID}/drdo-approval', 'ApplicationController@approveApplication')
->name('application.drdo-approval')
->middleware(['can:update,Modules\Application\Entities\Application']);


Route::get('questionnaires', 'QuestionnaireController@index')
->name('questionnaire.index');

Route::get('questionnaire/{questionnaireId}', 'QuestionnaireController@show')
->name('questionnaire.index');
Route::get('questionnaire/{questionnaireId}/auditlogs', 'QuestionnaireController@showAudit')
->name('questionnaire.audit');
Route::get('questionnaire/parameter/{parameterId}', 'QuestionnaireController@showParameter')
->name('questionnaire.parameter.show');

Route::post('questionnaire/{questionnaireId}/approval', 'QuestionnaireController@checkApproval')
->name('questionnaire.approve');