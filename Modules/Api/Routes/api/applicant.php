<?php
use Illuminate\Support\Facades\Route;

Route::post('signup', 'ApplicantController@signup')->name('signup')
->middleware('throttle:auth_api')
->withoutMiddleware(['auth:api', 'isApplicant' ,'IsEmailVerified']);

Route::post('update-profile', 'ApplicantController@updateProfile')->name('update-profile');

/***Application Specific Routes *****/
Route::get('applications', 'ApplicationController@index')
->name('application.index')->middleware(['can:view,Modules\Application\Entities\Application']);

Route::post('application', 'ApplicationController@store')->name('application.store')
->middleware(['can:create,Modules\Application\Entities\Application']);

Route::post('application/{applicationID}', 'ApplicationController@update')
->name('application.update-basic')
->middleware(['can:update,Modules\Application\Entities\Application']);

Route::post('application/{applicationID}/ownership', 'ApplicationController@syncOwnership')
->name('application.update-ownership')
->middleware(['can:update,Modules\Application\Entities\Application']);

Route::get('application/{applicationID}/submit-application', 'ApplicationController@submitApplication')
->name('application.submit-application')
->middleware(['can:update,Modules\Application\Entities\Application']);


Route::get('application/{applicationID}', 'ApplicationController@show')
->name('application.show')->middleware(['can:view,Modules\Application\Entities\Application']);

Route::delete('application/{applicationID}', 'ApplicationController@delete')
->name('application.delete')->middleware(['can:delete,Modules\Application\Entities\Application']);
/***Application Specific Routes *****/

Route::post('application/{applicationID}/schedule-assessment', 'ApplicationController@scheduleAssessment')
->name('application.schedule-assessment')->middleware(['can:update,Modules\Application\Entities\Application']);

Route::post('feedback', 'FeedbackController@create')->name('feedback.create');

/****Payment Specific Routes */
Route::get('application/{applicationID}/order', 'PaymentController@startOrder')
->name('order-create')->middleware(['can:payment,Modules\Application\Entities\Application']);

Route::get('payment-history', 'PaymentController@paymentHistory')
->name('payment-history');

Route::get('application/{applicationID}/payment-initiate', 'PaymentController@initiate')->name('payment.initiate')
->name('payment-initiate')->middleware(['can:update,Modules\Application\Entities\Application']);

//Route::post('payment', 'PaymentController@create')->name('payment.create');
Route::any('payment/success', 'PaymentController@paymentResponse')->name('payment.success')->withoutMiddleware(['auth:api', 'isApplicant', 'IsEmailVerified']);
Route::post('payment/cancel', 'PaymentController@paymentResponse')->name('payment.cancel')->withoutMiddleware(['auth:api', 'isApplicant', 'IsEmailVerified']);

/**Feedback Routes */
Route::post('application/{applicationID}/feedback', 'FeedbackController@submitFeedback')
->name('feedback.save')->middleware(['can:submitFeedback,Modules\Application\Entities\Application']);
Route::get('application/{applicationID}/feedback', 'FeedbackController@show')
->name('feedback.show')->middleware(['can:viewFeedback,Modules\Application\Entities\Application']);
