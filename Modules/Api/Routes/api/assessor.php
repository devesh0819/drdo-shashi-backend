<?php
Route::post('parameter/{parameterID}/assign-parameters-assessor', 'AssessorAssessmentController@assignParametersAssessor')
->name('assessor.assign-parameter')->middleware(['can:assignParameterAssessor,Modules\Assessment\Entities\AssessmentParameter']);

/** Assessor Assessment APIS */
Route::get('assessments', 'AssessorAssessmentController@getAssessments')
->name('assessor-assessment.index')->middleware(['can:view,Modules\Assessment\Entities\Assessment']);

Route::get('assessment/{assessmentID}/show', 'AssessorAssessmentController@showAssessment')
->name('assessor-assessment.show')->middleware(['can:view,Modules\Assessment\Entities\Assessment']);

Route::get('assessment/{assessmentID}/parameters', 'AssessorAssessmentController@getAssessmentParameters')
->name('assessor-assessment.parameters')->middleware(['can:view,Modules\Assessment\Entities\Assessment']);

Route::get('assessment/{assessmentID}/start', 'AssessorAssessmentController@startAssessment')
->name('assessor-assessment.start')->middleware(['can:startAssessment,Modules\Assessment\Entities\Assessment']);

Route::get('assessment/{assessmentID}/open', 'AssessorAssessmentController@openAssessment')
->name('assessor-assessment.open');

Route::get('assessment/{assessmentID}/submit', 'AssessorAssessmentController@submitAssessment')
->name('assessor-assessment.submit')->middleware(['can:submitAssessmentToAdmin,Modules\Assessment\Entities\Assessment']);

Route::get('assessment/{assessmentID}/get-site-evidence', 'AssessorAssessmentController@getSiteEvidence')
->name('assessor.get-assessment-evidence')->middleware(['can:submitSiteEvidense,Modules\Assessment\Entities\Assessment']);

Route::post('assessment/{assessmentID}/store-site-evidence', 'AssessorAssessmentController@submitSiteEvidence')
->name('assessor.submit-assessment-evidence')->middleware(['can:submitSiteEvidense,Modules\Assessment\Entities\Assessment']);

/** Parameters APIS */

Route::get('parameter/{parameterID}', 'AssessorAssessmentController@showParameter')
->name('assessor.showParameter')->middleware(['can:view,Modules\Assessment\Entities\AssessmentParameter']);

Route::post('parameter/{parameterID}/submit-parameters-response', 'AssessorAssessmentController@submitParametersResponse')
->name('assessor.submit-parameter-response')->middleware(['can:submitParameter,Modules\Assessment\Entities\AssessmentParameter']);

Route::get('parameter/{parameterID}/get-parameters-comment', 'AssessorAssessmentController@getParameterComment')
->name('assessor.get-parameter-comment')->middleware(['can:submitParameter,Modules\Assessment\Entities\AssessmentParameter']);

Route::post('parameter/{parameterID}/store-parameters-comment', 'AssessorAssessmentController@submitParameterComment')
->name('assessor.submit-parameter-comment')->middleware(['can:submitParameterComment,Modules\Assessment\Entities\AssessmentParameter']);

Route::get('parameter/{parameterID}/get-parameters-evidence', 'AssessorAssessmentController@getParameterEvidence')
->name('assessor.get-parameter-evidence')->middleware(['can:submitParameter,Modules\Assessment\Entities\AssessmentParameter']);

Route::post('parameter/{parameterID}/store-parameters-evidence', 'AssessorAssessmentController@submitParameterEvidence')
->name('assessor.submit-parameter-evidence')->middleware(['can:submitParameterEvidense,Modules\Assessment\Entities\AssessmentParameter']);
