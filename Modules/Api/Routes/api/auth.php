<?php
use Illuminate\Support\Facades\Route;

/** Auth APIS */

Route::post('login', 'AuthController@login')->name('login');
Route::post('forgot-password', 'AuthController@sendForgotEmail')->name('forgot-password')->middleware('throttle:auth_api');
Route::post('reset-password', 'AuthController@resetPassword')->name('reset-password')->middleware('throttle:auth_api');
Route::post('change-password', 'AuthController@changePassword')->name('change-password')->middleware('throttle:auth_api', 'auth:api');
Route::post('verify-email', 'AuthController@verifyOTP')->name('verify-otp')->middleware('auth:api')->middleware('throttle:auth_api');
Route::get('resend-verify-email', 'AuthController@resend')->name('verify-email')->middleware('throttle:auth_api', 'auth:api');

Route::get('logout', 'AuthController@logout')->name('logout')->middleware('auth:api');

Route::get('captcha', 'CaptchaController@getCaptcha')->name('get-captcha');
             