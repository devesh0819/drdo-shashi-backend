<?php
 
/** Assessor Assessment APIS */
Route::get('/sync-data', 'SyncController@syncData')
->name('sync-data')->middleware(['can:view,Modules\Assessment\Entities\Assessment']);
Route::post('/sync-data', 'SyncController@storeData')
->name('store-data')->middleware(['can:view,Modules\Assessment\Entities\Assessment']);
