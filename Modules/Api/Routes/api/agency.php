<?php

/** Agency Assessments Api */
Route::get('assessments', 'AgencyAssessmentController@getAssessments')
->name('agency-assessment.index')->middleware(['can:view,Modules\Assessment\Entities\Assessment']);

Route::get('assessment/{assessmentID}/show', 'AgencyAssessmentController@showAssessment')
->name('agency-assessment.show')->middleware(['can:view,Modules\Assessment\Entities\Assessment']);

Route::post('assessment/{assessmentID}/assign-assessors', 'AgencyAssessmentController@assignAssessors')
->name('agency-assessment.update')->middleware(['can:update,Modules\Assessment\Entities\Assessment']);
