<?php

namespace Modules\Api\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Core\Enums\HttpStatusCode;
use Illuminate\Http\JsonResponse;
use Modules\Core\Helpers\CryptoHelper;

class LoginCreateRequest extends FormRequest
{

    /**
     * Function to get rules for Login
    */
    public function rules()
    {
        $rules = [
            'email' => 'string|required',
            'password' => 'string|required',
        ];
        if(empty(request()->header('Source')) && request()->header('Source') !=  'App'){
            $rules['captcha'] = ['required', 'captcha_api:'. request('key')];
        }
         return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {
        $response = new JsonResponse([
            'errors' => $validator->errors()
                ], HttpStatusCode::HTTP_UNPROCESSABLE_ENTITY);

        throw new \Illuminate\Validation\ValidationException($validator, $response);
    }

    /**
     * Modify the request before validation
     */
    protected function prepareForValidation()
    {
        $appKey = !empty(config('app.key')) ? substr(config('app.key'), 7) : null; 
        $mergeArray = [];
        if(!empty(request()->get('password'))){
            $decrypted = CryptoHelper::decrypt(request()->get('password'), $appKey);
            $mergeArray['password'] = $decrypted;
            request()->merge(['password' =>$decrypted]);
        }
        $this->merge($mergeArray);
    }
}
