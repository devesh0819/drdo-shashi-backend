<?php

namespace Modules\Api\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Core\Enums\HttpStatusCode;
use Illuminate\Http\JsonResponse;
use Modules\Auth\Rules\PasswordPolicy;
use Modules\Core\Helpers\CryptoHelper;

class ChangePasswordRequest extends FormRequest
{

    /**
     * Function to get rules for Changing Password
    */
    public function rules()
    {
        $rules =  [
            'old_password'=>'required',
            'password' => ['required','required_with:password_confirmation','same:password_confirmation', new PasswordPolicy],
            'password_confirmation' => ['required'],
        ];
        if(empty(request()->header('Source')) && request()->header('Source') !=  'App'){
            $rules['captcha'] = ['required', 'captcha_api:'. request('key')];
        }
        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {
        $response = new JsonResponse([
            'errors' => $validator->errors()
                ], HttpStatusCode::HTTP_UNPROCESSABLE_ENTITY);

        throw new \Illuminate\Validation\ValidationException($validator, $response);
    }
    
    /**
     * Modify the request before validation
     */
    protected function prepareForValidation()
    {
        $appKey = !empty(config('app.key')) ? substr(config('app.key'), 7) : null; 
        $mergeArray = [];
        if(!empty(request()->get('old_password'))){
            $decrypted = CryptoHelper::decrypt(request()->get('old_password'), $appKey);
            $mergeArray['old_password'] = $decrypted;
            request()->merge(['old_password' =>$decrypted]);
        }
        if(!empty(request()->get('password'))){
            $decrypted = CryptoHelper::decrypt(request()->get('password'), $appKey);
            $mergeArray['password'] = $decrypted;
            request()->merge(['password' =>$decrypted]);
        }
        if(!empty(request()->get('password_confirmation'))){
            $decrypted = CryptoHelper::decrypt(request()->get('password_confirmation'), $appKey);
            $mergeArray['password_confirmation'] = $decrypted;
            request()->merge(['password_confirmation' =>$decrypted]);
        }
        $this->merge($mergeArray);
    }
}
