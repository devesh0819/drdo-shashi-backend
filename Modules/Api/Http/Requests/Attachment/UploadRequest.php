<?php

namespace Modules\Api\Http\Requests\Attachment;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Core\Helpers\BasicHelper;
use Illuminate\Http\UploadedFile;
use Illuminate\Validation\Rule;

class UploadRequest extends FormRequest
{

    /**
     * Function to get rules for Uploadig attachment
    */
    public function rules()
    {
        return [
            'file' => 'required|mimes:jpeg,png,jpg,gif|max:5120',
            'latitude' => 'required|between:-90,90',
            'file_caption'=>'nullable|string|min:3|max:255',
            'evidense_type'=>'nullable|'.Rule::in(['open_meet', 'close_meet', 'site_tour']),
            'longitude' => 'required|between:-180,180'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * merge lat long and datetime for image 
     */
    protected function prepareForValidation()
    {
        $file = request()->file('file');
        if($file instanceof UploadedFile){
            // Assign Vendor Role to User
            $getImageData =BasicHelper::getImageData($file);
            if(!empty($getImageData['dateTime'])){
                $this->merge([
                    'file_date_time'=>$getImageData['dateTime']
                ]);
            }
            if(!empty($getImageData['latitude']) && !empty($getImageData['longitude'])){
                $this->merge([
                    'latitude'=>$getImageData['latitude'],
                    'longitude'=>$getImageData['longitude']
                ]);
            }
        }
    }

    public function messages()
    {
        return [
            'latitude.required'=>__('api.image_geotagging_error'),
            'latitude.between'=>__('api.image_geotagging_error'),
            'longitude.required'=>__('api.image_geotagging_error'),
            'longitude.between'=>__('api.image_geotagging_error'),
        ];
    }



}
