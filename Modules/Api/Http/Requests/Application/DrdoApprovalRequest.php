<?php

namespace Modules\Api\Http\Requests\Application;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Core\Enums\HttpStatusCode;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\Rule;
use Modules\Core\Enums\ApplicationStage;

class DrdoApprovalRequest extends FormRequest
{
   /**
     * Function to get rules for DRDO approval
     */
    public function rules()
    {
        return [
            'application_stage'=>'required|'.Rule::in([ApplicationStage::APP_APPROVED, ApplicationStage::APP_REJECTED]),
            'subsidy_approved'=>'sometimes|bool'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {
        $response = new JsonResponse([
            'errors' => $validator->errors()
                ], HttpStatusCode::HTTP_UNPROCESSABLE_ENTITY);

        throw new \Illuminate\Validation\ValidationException($validator, $response);
    }
}
