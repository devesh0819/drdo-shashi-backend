<?php

namespace Modules\Api\Http\Requests\Application;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Core\Enums\HttpStatusCode;
use Illuminate\Http\JsonResponse;
use Modules\Core\Enums\UserRole;
use Illuminate\Support\Facades\Route;
use Modules\Core\Enums\Flag;
use Illuminate\Validation\Rule;
use Modules\Api\Rules\ApplicationOwnershipJson;
use Modules\Api\Rules\ApplicationOwnershipTags;

class ApplicationUpdateRequest extends FormRequest
{

    private $enterpriceTypes = [];
    
    public function __construct()
    {
        $this->enterpriceTypes = array_keys(config('core.enterprice_types'));
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "admin_manpower_count"=>"nullable|string|max:55",
            "is_competent_banned"=>"nullable|bool",
            "is_enquiry_against_firm"=>"nullable|bool",
            "app_data_confirmation"=>"nullable|bool",
            "app_acceptance_confirmation"=>"nullable|bool",
            "key_processes"=>["nullable", app(ApplicationOwnershipTags::class)],
            "processes_outsourced"=>["nullable", app(ApplicationOwnershipTags::class)],
            "product_manufactured"=>["nullable", app(ApplicationOwnershipJson::class)],
            "key_domestic_customers"=>["nullable", app(ApplicationOwnershipJson::class)],
            "key_raw_materials"=>["nullable", app(ApplicationOwnershipJson::class)]
        ];
    }

     /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {
        $response = new JsonResponse([
            'errors' => $validator->errors()
                ], HttpStatusCode::HTTP_UNPROCESSABLE_ENTITY);

        throw new \Illuminate\Validation\ValidationException($validator, $response);
    }

}
