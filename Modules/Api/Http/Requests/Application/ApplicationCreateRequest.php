<?php

namespace Modules\Api\Http\Requests\Application;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Core\Enums\HttpStatusCode;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\Rule;

class ApplicationCreateRequest extends FormRequest
{
    private $enterpriseNature = [];
    private $enterpriceTypes = [];
    
    public function __construct()
    {
        $this->enterpriseNature = array_keys(config('enterprise_nature')); 
        $this->enterpriceTypes = array_keys(config('core.enterprice_types'));
    }
    /**
     * Function to get rules for Creating application
     */
    public function rules()
    {
        return [
            'is_existing_vendor'=>'nullable|bool',
            'vendor_certificate'=>[
                'nullable',
                'mimes:jpeg,png,jpg,gif',
                'max:5120'
            ],
            'enterprise_name'=>'required|alpha_spaces|min:2|max:100',
            'enterprice_nature'=>Rule::in($this->enterpriseNature),
            'enterprice_type'=>Rule::in($this->enterpriceTypes),
            'registered_address'=>'required|string|min:5|max:55',
            'registered_state'=>'required|integer',
            'registered_district'=>'required|integer',
            'registered_pin'=>'required|regex:/(^[1-9][0-9]{5}$)/u',
            'registered_mobile_number'=>'required|regex:/(^(\+\d{1,3}[-\s]{1}?)?\d{10}$)/',
            'registered_landline_number'=>'nullable|regex:/(^\d{5}([- ]*)\d{6}$)/',
            'registered_email'=>'required|email',
            'registered_alternate_email'=>'nullable|email',
            'unit_address'=>'required|string|min:5|max:55',
            'unit_state'=>'required|integer',
            'unit_district'=>'required|integer',
            'unit_pin'=>'required|regex:/(^[1-9][0-9]{5}$)/u',
            'unit_mobile_number'=>'required|regex:/(^(\+\d{1,3}[-\s]{1}?)?\d{10}$)/',
            'unit_landline_number'=>'nullable|regex:/(^\d{5}([- ]*)\d{6}$)/',
            'unit_email'=>'required|email',
            'unit_alternate_email'=>'nullable|email',
            'enterprise_commencement_date'=>'required|date',
            "unit_commencement_date"=>'required|date',
            "drdo_registration_number"=>'nullable|string|min:5|max:55',
            'poc_name'=>'nullable|alpha_spaces|min:2|max:100',
            'poc_designation'=>'nullable|alpha_spaces|min:2|max:100',
            'poc_mobile_number'=>'nullable|regex:/(^(\+\d{1,3}[-\s]{1}?)?\d{10}$)/',
            'entrepreneur_gst_number'=>'required|regex:/(^\d{2}[A-Z]{5}\d{4}[A-Z]{1}[A-Z\d]{1}[Z]{1}[A-Z\d]{1}$)/',
            'entrepreneur_gst_certificate'=>'mimes:jpeg,png,jpg,gif|max:5120',
            'uam_number'=>[
                'nullable',
                'string',
                'min:12',
                'max:12'
            ],
            'udyam_certificate'=>[
                'nullable',
                'mimes:jpeg,png,jpg,gif',
                'max:5120'
            ],
            'incorporation_certificate'=>[
                'nullable',
                'mimes:jpeg,png,jpg,gif',
                'max:5120'
            ],
            'site_exterior_picture'=>'mimes:jpeg,png,jpg,gif|max:5120'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {
        $response = new JsonResponse([
            'errors' => $validator->errors()
                ], HttpStatusCode::HTTP_UNPROCESSABLE_ENTITY);

        throw new \Illuminate\Validation\ValidationException($validator, $response);
    }
}
