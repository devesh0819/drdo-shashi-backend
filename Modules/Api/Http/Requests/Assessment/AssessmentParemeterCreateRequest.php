<?php

namespace Modules\Api\Http\Requests\Assessment;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Core\Enums\HttpStatusCode;
use Illuminate\Http\JsonResponse;
use Modules\Core\Enums\UserRole;
use Modules\Application\Entities\Application;
use Route;
use Illuminate\Validation\Rule;

class AssessmentParemeterCreateRequest extends FormRequest
{
    private $parameterResponses = [
        'bronze',
        'silver',
        'gold',
        'diamond',
        'platinum'
    ];
    /**
     * Function to get rules for Creating Assessment Parameter
    */
    public function rules()
    { 
        $route= Route::current(); 
        $routeName = !empty($route)? $route->getName() : null;
        
        $rules = [];
        if(str_contains($routeName, 'store-data')){
            $rules = [
                'response' => 'sometimes|'.Rule::in($this->parameterResponses),
                'comment'=>'sometimes|min:3|max:255'
            ];
        }elseif(str_contains($routeName, 'assign-parameter')) { 
            $rules = [
                'assessor_id' => 'required'
            ];
        }elseif(str_contains($routeName, 'submit-parameter-comment')){
            $rules = [
                'comment'=>'required|string|min:3|max:255',
                'comment_type'=>'nullable|'.Rule::in(['strength', 'ofi','observations','default'])
            ];
        }elseif(str_contains($routeName, 'submit-parameter-evidence')){
            $rules = [
                'file' => 'required|file'
            ];
        }elseif(str_contains($routeName, 'submit-parameter-response')){
            $rules = [
                'section_uuid'=>'required',
                'response' => 'sometimes|'.Rule::in($this->parameterResponses)
            ];
        }else{
            
        }

        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {
        $response = new JsonResponse([
            'errors' => $validator->errors()
                ], HttpStatusCode::HTTP_UNPROCESSABLE_ENTITY);

        throw new \Illuminate\Validation\ValidationException($validator, $response);
    }

        /**
     * Modify the request before validation
     */
    protected function prepareForValidation()
    {
        // Assign Vendor Role to User
        $this->merge([
            'application_id'=>request()->route('id'),
            'questionnaire_id'=>1,
            'assessment_date'=>date("Y-m-d H:i:s")
        ]);
    }
}
