<?php

namespace Modules\Api\Http\Requests\Assessment;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Core\Enums\HttpStatusCode;
use Illuminate\Http\JsonResponse;
use Modules\Core\Enums\UserRole;

class AssessmentUpdateRequest extends FormRequest
{

    /**
     * Function to get rules for Updating Assessment
    */
    public function rules()
    {
        return [
            'assessor_ids'=>'sometimes|array'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {
        $response = new JsonResponse([
            'errors' => $validator->errors()
                ], HttpStatusCode::HTTP_UNPROCESSABLE_ENTITY);

        throw new \Illuminate\Validation\ValidationException($validator, $response);
    }

        /**
     * Modify the request before validation
     */
    protected function prepareForValidation()
    {
    }
}
