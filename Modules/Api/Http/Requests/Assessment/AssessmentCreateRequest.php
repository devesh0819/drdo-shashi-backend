<?php

namespace Modules\Api\Http\Requests\Assessment;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Core\Enums\HttpStatusCode;
use Illuminate\Http\JsonResponse;
use Modules\Core\Enums\UserRole;
use Modules\Application\Entities\Application;
use Modules\Core\Enums\Flag;
use Modules\Questionnaire\Entities\Questionnaire;

class AssessmentCreateRequest extends FormRequest
{
    private $application;
    /**
     * Function to get rules for Creating Assessment
    */
    public function rules()
    {
        $rules =  [
            'agency_id'=>'required',
            'questionnaire_id'=>'required'
        ];
        if(empty(request()->route('assessmentID'))){
            $rules['application_id'] = 'required|unique:assessments,application_id';
        }
        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {
        $response = new JsonResponse([
            'errors' => $validator->errors()
                ], HttpStatusCode::HTTP_UNPROCESSABLE_ENTITY);

        throw new \Illuminate\Validation\ValidationException($validator, $response);
    }

        /**
     * Modify the request before validation
     */
    protected function prepareForValidation()
    {
        $application = Application::where('application_uuid', request()->route('applicationID'))->first();
        $questionnaire = Questionnaire::where(function($que) use($application){
            if($application->enterprice_type == 'SM' || $application->enterprice_type == 'MIC' || $application->enterprice_type == 'MED'){
                $que->where('type', 'S&M');
            }else{
                $que->where('type', 'L');
            }
        })->where('is_approved', Flag::IS_ACTIVE)
        ->orderBy('version', 'desc')
        ->latest()
        ->first();
        if(!empty($questionnaire)){
            $this->merge([
                'questionnaire_id'=> $questionnaire->id
            ]);
        }
       // Assign Vendor Role to User
        $this->merge([
            'application_id'=>$application->id,
            'assessment_date'=>$application->assessment_date,
        ]);
    }
}
