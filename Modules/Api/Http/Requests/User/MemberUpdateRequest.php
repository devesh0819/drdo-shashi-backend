<?php

namespace Modules\Api\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Core\Enums\HttpStatusCode;
use Illuminate\Http\JsonResponse;
use Modules\Core\Enums\UserRole;
use Modules\User\Entities\User;

class MemberUpdateRequest extends FormRequest
{

     /**
     * Function to get rules for Updating Member
    */
    public function rules()
    {
        $rules = [
            'phone_number'=>'string|nullable|regex:/(^(\+\d{1,3}[-\s]{1}?)?\d{10}$)/',
            'first_name'=>'string|alpha_spaces|required|min:3|max:55',
            'last_name'=>'string|alpha_spaces|required|min:3|max:55',
            'member_role'=>'string|alpha_spaces|required|min:3|max:55',
            "organisation"=>"nullable|min:3|max:255",
            "designation"=>"nullable|min:3|max:255",
            "alternate_email"=>"nullable|email",
            "alternate_phone_number"=>'string|nullable|regex:/(^(\+\d{1,3}[-\s]{1}?)?\d{10}$)/',
        ];
        if(!empty(request()->get('email'))){
            $user = User::where('uuid', request()->route('member'))->first();
            if(!empty($user->id)){
                $rules['email'] = 'string|required|email|unique:users,email,'.$user->id;
            }else{
                $rules['email'] = 'string|required|email|unique:users,email';
            }
        }
        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {
        $response = new JsonResponse([
            'errors' => $validator->errors()
                ], HttpStatusCode::HTTP_UNPROCESSABLE_ENTITY);

        throw new \Illuminate\Validation\ValidationException($validator, $response);
    }
}
