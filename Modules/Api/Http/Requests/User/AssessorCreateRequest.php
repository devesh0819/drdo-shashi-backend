<?php

namespace Modules\Api\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Core\Enums\HttpStatusCode;
use Illuminate\Http\JsonResponse;
use Modules\Core\Enums\UserRole;
use Modules\Agency\Entities\AgencyMeta;
use Illuminate\Validation\Rule;

class AssessorCreateRequest extends FormRequest
{
    /**
     * Function to get rules for Creating Assessor
    */
    public function rules()
    {
        return [
            'email' => 'required|string|email|unique:users,email',
            'phone_number'=>'nullable|string|regex:/(^(\+\d{1,3}[-\s]{1}?)?\d{10}$)/',
            'first_name'=>'required|alpha_spaces|string|min:3|max:55',
            'last_name'=>'required|alpha_spaces|string|min:3|max:55',
            'agency_id'=>'required|integer',
            'expertise'=>'required|string|min:3|max:55',
            "assessor_state"=>"nullable|integer",
            "assessor_district"=>"nullable|integer",
            "assessor_certificate"=>"nullable|min:3|max:255",
            "assessor_status"=>"nullable|".Rule::in(['active', 'onhold', 'blocked']),
            "work_domain"=>"nullable|min:3|max:55",
            "years_of_experience"=>"nullable|integer",
            'email' => 'required|string|email|unique:users,email',
            "assessor_status_comment"=>"nullable|min:3|max:55",
            "alternate_email"=>"nullable|email",
            "alternate_phone_number"=>'string|nullable|regex:/(^(\+\d{1,3}[-\s]{1}?)?\d{10}$)/',
            "resume"=>"nullable|mimes:jpeg,png,jpg,gif|max:5120"
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {
        $response = new JsonResponse([
            'errors' => $validator->errors()
                ], HttpStatusCode::HTTP_UNPROCESSABLE_ENTITY);

        throw new \Illuminate\Validation\ValidationException($validator, $response);
    }

    /**
     * Modify the request before validation
     */
    protected function prepareForValidation()
    {
        // Assign Vendor Role to User
        $agency = AgencyMeta::latest()->first();
        $this->merge(['roles' => UserRole::ASSESSOR,
         'agency_id'=>$agency->user_id
    ]);
    }

}
