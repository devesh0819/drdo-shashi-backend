<?php

namespace Modules\Api\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Core\Enums\HttpStatusCode;
use Illuminate\Http\JsonResponse;
use Modules\Core\Enums\UserRole;
use Modules\Auth\Rules\PasswordPolicy;
use Modules\Core\Helpers\CryptoHelper;

class UserCreateRequest extends FormRequest
{

     /**
     * Function to get rules for Creating User
    */
    public function rules()
    {
        return [
            'first_name'=>'alpha_spaces||required|max:55',
            'last_name'=>'alpha_spaces||required|min:3|max:55',
            'email' => 'string|required|email|unique:users,email',
            'password' => 'required|confirmed|min:6',
            'password_confirmation' => ['required_with:password','same:password', 'min:6',new PasswordPolicy()],
            'phone_number'=>'string|required|regex:/(^(\+\d{1,3}[-\s]{1}?)?\d{10}$)/'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {
        $response = new JsonResponse([
            'errors' => $validator->errors()
                ], HttpStatusCode::HTTP_UNPROCESSABLE_ENTITY);

        throw new \Illuminate\Validation\ValidationException($validator, $response);
    }

    /**
     * Modify the request before validation
     */
    protected function prepareForValidation()
    {
        $appKey = !empty(config('app.key')) ? substr(config('app.key'), 7) : null; 
        $mergeArray = [];
        $mergeArray['roles']  =  UserRole::VENDOR;
        
        // Assign Vendor Role to User
        if(!empty(request()->get('password'))){
            $decrypted = CryptoHelper::decrypt(request()->get('password'), $appKey);
            $mergeArray['password'] = $decrypted;
            request()->merge(['password' =>$decrypted]);
        }
        if(!empty(request()->get('password_confirmation'))){
            $decrypted = CryptoHelper::decrypt(request()->get('password_confirmation'), $appKey);
            $mergeArray['password_confirmation'] = $decrypted;
            request()->merge(['password_confirmation' =>$decrypted]);
        }
        $this->merge($mergeArray);
    }

}
