<?php

namespace Modules\Api\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Core\Enums\HttpStatusCode;
use Illuminate\Http\JsonResponse;
use Modules\Core\Enums\UserRole;

class AdminUpdateRequest extends FormRequest
{

    /**
     * Function to get rules for updating admin
    */
    public function rules()
    {
        return [
            'phone_number'=>'string|required|regex:/(^(\+\d{1,3}[-\s]{1}?)?\d{10}$)/',
            'first_name'=>'string|alpha_spaces|required|min:3|max:55',
            'last_name'=>'string|alpha_spaces|required|min:3|max:55',
            'title'=>'string|required|min:3|max:55',
            'address'=>'string|required|min:3|max:55'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {
        $response = new JsonResponse([
            'errors' => $validator->errors()
                ], HttpStatusCode::HTTP_UNPROCESSABLE_ENTITY);

        throw new \Illuminate\Validation\ValidationException($validator, $response);
    }

}
