<?php

namespace Modules\Api\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Core\Enums\HttpStatusCode;
use Illuminate\Http\JsonResponse;
use Modules\Core\Enums\UserRole;
use Modules\Auth\Rules\PasswordPolicy;
use Modules\Core\Helpers\CryptoHelper;
use Modules\User\Entities\User;

class UserUpdateRequest extends FormRequest
{

     /**
     * Function to get rules for Creating User
    */
    public function rules()
    {
        $rules = [
            'first_name'=>'string|alpha_spaces|required|min:3|max:55',
            'last_name'=>'string|alpha_spaces|required|min:3|max:55',
            'phone_number'=>'string|required|regex:/(^(\+\d{1,3}[-\s]{1}?)?\d{10}$)/',
            "alternate_email"=>"nullable|email",
            "alternate_phone_number"=>'string|nullable|regex:/(^(\+\d{1,3}[-\s]{1}?)?\d{10}$)/',
        ];
        
        if(!empty(request()->get('email'))){
            $user = User::where('id', request()->user()->id)->first();
            if(!empty($user->id)){
                $rules['email'] = 'string|required|email|unique:users,email,'.$user->id;
            }else{
                $rules['email'] = 'string|required|email|unique:users,email';
            }
        }
        
        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {
        $response = new JsonResponse([
            'errors' => $validator->errors()
                ], HttpStatusCode::HTTP_UNPROCESSABLE_ENTITY);

        throw new \Illuminate\Validation\ValidationException($validator, $response);
    }

    /**
     * Modify the request before validation
     */
    protected function prepareForValidation()
    {
        $appKey = !empty(config('app.key')) ? substr(config('app.key'), 7) : null; 
        $mergeArray = [];
        // Assign Vendor Role to User
        if(!empty(request()->get('password'))){
            $decrypted = CryptoHelper::decrypt(request()->get('password'), $appKey);
            $mergeArray['password'] = $decrypted;
            request()->merge(['password' =>$decrypted]);
        }
        if(!empty(request()->get('password_confirmation'))){
            $decrypted = CryptoHelper::decrypt(request()->get('password_confirmation'), $appKey);
            $mergeArray['password_confirmation'] = $decrypted;
            request()->merge(['password_confirmation' =>$decrypted]);
        }
        $this->merge($mergeArray);
    }

}
