<?php

namespace Modules\Api\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Core\Enums\HttpStatusCode;
use Illuminate\Http\JsonResponse;
use Modules\Core\Enums\UserRole;
use Illuminate\Validation\Rule;
use Modules\User\Entities\User;

class AssessorUpdateRequest extends FormRequest
{

     /**
     * Function to get rules for Updating Assessor
    */
    public function rules()
    {
        $rules =  [
            'phone_number'=>'nullable|string|regex:/(^(\+\d{1,3}[-\s]{1}?)?\d{10}$)/',
            'first_name'=>'required|alpha_spaces|string|min:3|max:55',
            'last_name'=>'required|alpha_spaces|string|min:3|max:55',
            'agency_id'=>'required|integer',
            'expertise'=>'required|string|min:3|max:55',
            "assessor_state"=>"nullable|integer",
            "assessor_district"=>"nullable|integer",
            "assessor_certificate"=>"nullable|min:3|max:255",
            "assessor_status"=>"nullable|".Rule::in(['active', 'onhold', 'blocked']),
            "work_domain"=>"nullable|min:3|max:55",
            "years_of_experience"=>"nullable|integer",
            "assessor_status_comment"=>"nullable|min:3|max:55",
            "alternate_email"=>"nullable|email",
            "alternate_phone_number"=>'string|nullable|regex:/(^(\+\d{1,3}[-\s]{1}?)?\d{10}$)/',
            "resume"=>"nullable|mimes:jpeg,png,jpg,gif|max:5120"
        ];
        if(!empty(request()->get('email'))){
            $user = User::where('uuid', request()->route('assessor'))->first();
            if(!empty($user->id)){
                $rules['email'] = 'string|required|email|unique:users,email,'.$user->id;
            }else{
                $rules['email'] = 'string|required|email|unique:users,email';
            }
        }
        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {
        $response = new JsonResponse([
            'errors' => $validator->errors()
                ], HttpStatusCode::HTTP_UNPROCESSABLE_ENTITY);

        throw new \Illuminate\Validation\ValidationException($validator, $response);
    }
}
