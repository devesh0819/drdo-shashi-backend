<?php

namespace Modules\Api\Http\Requests\Agency;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Core\Enums\HttpStatusCode;
use Illuminate\Http\JsonResponse;
use Modules\Core\Enums\UserRole;

class AgencyUpdateRequest extends FormRequest
{

    /**
     * Function to get rules for Updating
    */
    public function rules()
    {
        return [
            'email' => 'string|required|email',
            'first_name' => 'string|alpha_spaces|required',
            'last_name' => 'string|alpha_spaces|required',
            'phone_number'=>'string|required',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {
        $response = new JsonResponse([
            'errors' => $validator->errors()
                ], HttpStatusCode::HTTP_UNPROCESSABLE_ENTITY);

        throw new \Illuminate\Validation\ValidationException($validator, $response);
    }

    /**
     * Modify the request before validation
     */
    protected function prepareForValidation()
    {
        // Assign Vendor Role to User
        $this->merge(['roles' => [UserRole::AGENCY]]);
    }

}
