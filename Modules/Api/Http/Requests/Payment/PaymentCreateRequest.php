<?php

namespace Modules\Api\Http\Requests\Payment;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Core\Enums\HttpStatusCode;
use Illuminate\Http\JsonResponse;

class PaymentCreateRequest extends FormRequest
{

   /**
     * Function to get rules for Initiating Payment
    */
    public function rules()
    {
        return [
            'order_number'=>'required'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {
        $response = new JsonResponse([
            'errors' => $validator->errors()
                ], HttpStatusCode::HTTP_UNPROCESSABLE_ENTITY);

        throw new \Illuminate\Validation\ValidationException($validator, $response);
    }

        /**
     * Modify the request before validation
     */
    protected function prepareForValidation()
    {
        
    }
}
