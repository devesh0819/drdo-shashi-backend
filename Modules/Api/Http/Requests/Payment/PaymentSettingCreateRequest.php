<?php

namespace Modules\Api\Http\Requests\Payment;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Core\Enums\HttpStatusCode;
use Illuminate\Http\JsonResponse;

class PaymentSettingCreateRequest extends FormRequest
{

   /**
     * Function to get rules for Initiating Payment
    */
    public function rules()
    {
        return [
            'large_certification_fee_text'=>'required|string|min:3|max:55',
            'large_assessor_fee_text'=>'required|string|min:3|max:55',
            'large_gst_text'=>'required|string|min:3|max:55',
            'large_tds_text'=>'required|string|min:3|max:55',
            'large_miscell_fee_text'=>'nullable|string|min:3|max:55',
            'large_miscell_discount_text'=>'nullable|string|min:3|max:55',
            'large_certification_fee_value'=>'required|integer|max:9999999999',
            'large_assessor_fee_value'=>'nullable|integer|max:999999',
            'large_subsidy_value'=>'nullable|integer|max:100',
            'large_gst_value'=>'nullable|integer|min:1|max:100',
            'large_tds_value'=>'nullable|string|max:55',
            'large_miscell_fee_value'=>'nullable|integer',
            'large_miscell_discount_value'=>'nullable|integer',
            'medium_certification_fee_text'=>'required|string|min:3|max:55',
            'medium_assessor_fee_text'=>'required|string|min:3|max:55',
            'medium_gst_text'=>'required|string|min:3|max:55',
            'medium_tds_text'=>'required|string|min:3|max:55',
            'medium_miscell_fee_text'=>'nullable|string|min:3|max:55',
            'medium_miscell_discount_text'=>'nullable|string|min:3|max:55',
            'medium_certification_fee_value'=>'required|integer|max:9999999999',
            'medium_assessor_fee_value'=>'nullable|integer|max:999999',
            'medium_subsidy_value'=>'nullable|integer|max:100',
            'medium_gst_value'=>'nullable|integer|min:1|max:100',
            'medium_tds_value'=>'nullable|string|max:55',
            'medium_miscell_fee_value'=>'nullable|integer',
            'medium_miscell_discount_value'=>'nullable|integer',
            'small_certification_fee_text'=>'required|string|min:3|max:55',
            'small_assessor_fee_text'=>'required|string|min:3|max:55',
            'small_gst_text'=>'required|string|min:3|max:55',
            'small_tds_text'=>'required|string|min:3|max:55',
            'small_miscell_fee_text'=>'nullable|string|min:3|max:55',
            'small_miscell_discount_text'=>'nullable|string|min:3|max:55',
            'small_certification_fee_value'=>'required|integer|max:9999999999',
            'small_assessor_fee_value'=>'nullable|integer|max:999999',
            'small_subsidy_value'=>'nullable|integer|max:100',
            'small_gst_value'=>'nullable|integer|max:100',
            'small_tds_value'=>'nullable|string|max:55',
            'small_miscell_fee_value'=>'nullable|integer',
            'small_miscell_discount_value'=>'nullable|integer',
            'micro_certification_fee_text'=>'required|string|min:3|max:55',
            'micro_assessor_fee_text'=>'required|string|min:3|max:55',
            'micro_gst_text'=>'required|string|min:3|max:55',
            'micro_tds_text'=>'required|string|min:3|max:55',
            'micro_miscell_fee_text'=>'nullable|string|min:3|max:55',
            'micro_miscell_discount_text'=>'nullable|string|min:3|max:55',
            'micro_certification_fee_value'=>'required|integer|max:9999999999',
            'micro_assessor_fee_value'=>'nullable|integer|max:999999',
            'micro_subsidy_value'=>'nullable|integer|max:100',
            'micro_gst_value'=>'nullable|integer|min:1|max:100',
            'micro_tds_value'=>'nullable|string|max:55',
            'micro_miscell_fee_value'=>'nullable|integer',
            'micro_miscell_discount_value'=>'nullable|integer',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {
        $response = new JsonResponse([
            'errors' => $validator->errors()
                ], HttpStatusCode::HTTP_UNPROCESSABLE_ENTITY);

        throw new \Illuminate\Validation\ValidationException($validator, $response);
    }

        /**
     * Modify the request before validation
     */
    protected function prepareForValidation()
    {
        
    }
}
