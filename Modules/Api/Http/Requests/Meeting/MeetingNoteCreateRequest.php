<?php

namespace Modules\Api\Http\Requests\Meeting;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Core\Enums\HttpStatusCode;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\Rule;

class MeetingNoteCreateRequest extends FormRequest
{

   /**
     * Function to get rules for Creating Meeting Note
    */
    public function rules()
    {
        return [
             "meeting_notes"=>"required|string|min:3|max:255",
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {
        $response = new JsonResponse([
            'errors' => $validator->errors()
                ], HttpStatusCode::HTTP_UNPROCESSABLE_ENTITY);

        throw new \Illuminate\Validation\ValidationException($validator, $response);
    }
}
