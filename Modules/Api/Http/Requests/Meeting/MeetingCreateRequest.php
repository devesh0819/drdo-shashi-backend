<?php

namespace Modules\Api\Http\Requests\Meeting;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Core\Enums\HttpStatusCode;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\Rule;

class MeetingCreateRequest extends FormRequest
{

    /**
     * Function to get rules for Creating Meeting
    */
    public function rules()
    {
        return [
            "meeting_title"=>"required|string|min:3|max:55",
            "description"=>"required|string|min:3|max:55",
            "meeting_date_time"=>"required|date",
            "meeting_type"=>Rule::in(['online', 'offline']),
            "meeting_address"=>"nullable|string|min:3|max:55",
            "meeting_link"=>"nullable|string|min:3|max:55",
            "meeting_assessments"=> "required|array"
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {
        $response = new JsonResponse([
            'errors' => $validator->errors()
                ], HttpStatusCode::HTTP_UNPROCESSABLE_ENTITY);

        throw new \Illuminate\Validation\ValidationException($validator, $response);
    }
}
