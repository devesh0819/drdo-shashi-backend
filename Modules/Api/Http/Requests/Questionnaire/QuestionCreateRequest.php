<?php

namespace Modules\Api\Http\Requests\Questionnaire;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Core\Enums\HttpStatusCode;
use Illuminate\Http\JsonResponse;


class QuestionCreateRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {
        $response = new JsonResponse([
            'errors' => $validator->errors()
                ], HttpStatusCode::HTTP_UNPROCESSABLE_ENTITY);

        throw new \Illuminate\Validation\ValidationException($validator, $response);
    }

    /**
     * Modify the request before validation
     */
    protected function prepareForValidation()
    {
        
    }

}
