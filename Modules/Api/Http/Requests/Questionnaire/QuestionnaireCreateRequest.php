<?php

namespace Modules\Api\Http\Requests\Questionnaire;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Core\Enums\HttpStatusCode;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\Rule;

class QuestionnaireCreateRequest extends FormRequest
{

    /**
     * Function to get rules for creating questionnaire
    */
    public function rules()
    {
        return [
            'file'=>'required|file',
            'type'=>Rule::in(['S&M', 'L']),
            'title'=>'required|string|min:3|max:55'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {
        $response = new JsonResponse([
            'errors' => $validator->errors()
                ], HttpStatusCode::HTTP_UNPROCESSABLE_ENTITY);

        throw new \Illuminate\Validation\ValidationException($validator, $response);
    }

    /**
     * Modify the request before validation
     */
    protected function prepareForValidation()
    {
        
    }

}
