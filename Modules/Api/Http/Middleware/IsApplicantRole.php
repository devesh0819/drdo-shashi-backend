<?php

namespace Modules\Api\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Auth\Access\AuthorizationException;
use Modules\Core\Enums\UserRole;

class IsApplicantRole
{
    /**
     * Handle an incoming request to check whether the role is Vendor.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if($request->user()->hasRole(UserRole::VENDOR)){
            request()->merge([
                'user_id'=>request()->user()->id
            ]);
            return $next($request);
        }else{
            throw new AuthorizationException();
        }
    }
}
