<?php

namespace Modules\Api\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Modules\Core\Enums\UserRole;
use Illuminate\Auth\Access\AuthorizationException;

class IsAdminRole
{
    /**
     * Handle an incoming request to check whether the role is  ADMIN, SUPER_ADMIN or DRDO.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if($request->user()->hasRole(UserRole::ADMIN) || $request->user()->hasRole(UserRole::SUPER_ADMIN) || ($request->user()->hasRole(UserRole::DRDO) && $request->method() == 'GET')){
            return $next($request);
        }else{
            throw new AuthorizationException();
        }
    }
}
