<?php

namespace Modules\Api\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Modules\Core\Enums\HttpStatusCode;

class IsEmailVerified
{
    /**
     * Handle an incoming request to check whether the Email is Verified or not.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(!empty($request->user()->email_verified_at)){
            return $next($request);
        }else{
            return response()->json(['error'=>__('api.email_not_verified')], HttpStatusCode::HTTP_BAD_REQUEST);
        }
    }
}
