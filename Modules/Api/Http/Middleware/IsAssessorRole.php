<?php

namespace Modules\Api\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Auth\Access\AuthorizationException;
use Modules\Core\Enums\UserRole;

class IsAssessorRole
{
    /**
     * Handle an incoming request to check whether the role is ASSESSOR.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if($request->user()->hasRole(UserRole::ASSESSOR)){
            return $next($request);
        }else{
            throw new AuthorizationException();
        }
    }
}
