<?php

namespace Modules\Api\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Auth\Access\AuthorizationException;
use Modules\Core\Enums\UserRole;

class IsAgencyRole
{
    /**
     * Handle an incoming request to check whether the role is AGENCY.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if($request->user()->hasRole(UserRole::AGENCY) || $request->user()->hasRole(UserRole::ADMIN) || $request->user()->hasRole(UserRole::SUPER_ADMIN) || $request->user()->hasRole(UserRole::DRDO)){
            return $next($request);
        }else{
            throw new AuthorizationException();
        }
    }
}
