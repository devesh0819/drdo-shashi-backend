<?php

namespace Modules\Api\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Stevebauman\Purify\Facades\Purify;
use Illuminate\Http\UploadedFile;
use Modules\Core\Enums\UserRole;

class PurifyRequest
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $this->moderation($request);
        return $next($request);
    }

    /**
     * Function to perform moderation on request coming  via admin panel
     *
     * @return void
     */
    public function moderation($request){
        if(!empty($request->user()) && ($request->user()->hasRole(UserRole::ADMIN) || $request->user()->hasRole(UserRole::DRDO))){
            return true;
        }
        $inputs = $request->all();
        // Skip Long Description when request contains the post.
        if(!empty($inputs)){
            foreach($inputs as $key=>$value){
                if(!empty($value)){
                    $cleanText = Purify::clean($value);
                    $type =gettype($value);
                    if(in_array($type, ['string', 'integer'])){
                        settype($cleanText, $type);
                    }
                    $request->merge([$key=>$cleanText]);
                }    
            }
        }
        return $request;
    }
}
