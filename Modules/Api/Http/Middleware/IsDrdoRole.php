<?php

namespace Modules\Api\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Modules\Core\Enums\UserRole;
use Illuminate\Auth\Access\AuthorizationException;

class IsDrdoRole
{
    /**
     * Handle an incoming request to check whether the role is DRDO
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if($request->user()->hasRole(UserRole::DRDO)){
            return $next($request);
        }else{
            throw new AuthorizationException();
        }
    }
}
