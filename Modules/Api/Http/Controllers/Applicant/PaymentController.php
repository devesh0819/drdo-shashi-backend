<?php

namespace Modules\Api\Http\Controllers\Applicant;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Modules\Api\Http\Controllers\ApiController;
use Modules\Core\Enums\HttpStatusCode;
use Modules\Application\Services\ApplicationService;
use Modules\Api\Transformers\ApplicationTransformer;
use Modules\Api\Http\Requests\Payment\PaymentCreateRequest;
use Modules\Application\Entities\Application;
use Modules\Payment\Services\PaymentService;
use Modules\Core\Exports\ListExport;
Use Maatwebsite\Excel\Facades\Excel;

class PaymentController extends ApiController
{
    private $applicationService;
    private $transformer;
    private $applicationOwnership;
    private $applicationFinancial;
    private $attachmentService;
    private $paymentService;

    public function __construct(
        ApplicationService $applicationService,
        ApplicationTransformer $transformer,
        PaymentService $paymentService
    )
    {
        $this->applicationService = $applicationService;
        $this->transformer = $transformer;
        $this->paymentService = $paymentService;
    }

    /**
     * Function to get all payment history for an applicant
     * @param Request $request
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function paymentHistory(Request $request)
    {   $filters = $request->only('start');
        $filters['user_id'] = $request->user()->id;
        
        if(!empty(request()->get('is_excel'))){
            unset($filters['start']);
            $transactions = $this->paymentService->lists($filters);
            $excelType = !empty(request()->get('excel_type')) ? request()->get('excel_type') : null;
            return Excel::download(new ListExport($transactions, $excelType), 'payment_history.xlsx');
        }else{
            $transactions = $this->paymentService->lists($filters);
        }
        $data = [
            'total'=>0,
            'transactions'=>[]
        ];
        
        if(!empty($transactions)){
            $data['total'] = $this->paymentService->lists($filters, true);
            foreach($transactions as $transaction){
                $data['transactions'][] = $transaction; 
            }
        }

        $this->prepareApiResponse($data);
        return $this->sendApiResponse();
    }

    /**
     * Function to initialise a new order for Application
     * @param Request $request
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function startOrder(Request $request, $applicationId)
    {
        $application = $this->applicationService->firstornew($applicationId);
        if(!empty($application)){
            $payment = $this->paymentService->createOrder($application);
            $data = $this->transformer->transform($application, ['paymentOrder'=>true]);
            $this->prepareApiResponse($data);
        } else {
            $this->prepareApiResponse(['error'=>HttpStatusCode::HTTP_NOT_FOUND]);
            $this->httpCode = HttpStatusCode::HTTP_NOT_FOUND;
        }

        return $this->sendApiResponse();  
    }

    /**
     * Function to start a payment process for a application
     * @param Request $request
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function initiate(Request $request, $applicationId)
    {
        $application = $this->applicationService->firstornew($applicationId);
        if(!empty($application)){
            $data = [
                'user_id'=>request()->user()->id,
                'tds'=>(int) $request->get('tds')
            ];
            $payment = $this->paymentService->inititate($application, $data);
            $this->prepareApiResponse($payment);
        } else {
            $this->prepareApiResponse(['error'=>HttpStatusCode::HTTP_NOT_FOUND]);
            $this->httpCode = HttpStatusCode::HTTP_NOT_FOUND;
        }

        return $this->sendApiResponse();   
    }

    /**
     * Function to create a application's payment
     * @param Request $request
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function create(PaymentCreateRequest $request, $applicationId)
    {
        $application = $this->applicationService->firstornew($applicationId);
        
        if(!empty($application)){
            $submitPayment = $this->paymentService->completeOrder($application);
            $data = $this->transformer->transform($application);
            $this->prepareApiResponse($data);
        } else {
            $this->prepareApiResponse(['error'=>HttpStatusCode::HTTP_NOT_FOUND]);
            $this->httpCode = HttpStatusCode::HTTP_NOT_FOUND;
        }

        return $this->sendApiResponse();   
    }

     /**
     * Function to show payment response
     * @param Request $request
     * @return Array $data
     * @author Daffodil Softwere
     */

    public function paymentResponse(Request $request)
    {
        $inputs = $request->only('encResp', 'orderNo');
        if(config('payment.ccavenue.testMode')){
            $inputs['encResp'] = config('payment.ccavenue.testResponse');
        }
        if(!empty($inputs)){
            $order = $this->paymentService->completeOrder($inputs);
            if(!empty($order) && !empty($order->application)){
                return redirect(config('api.payment_success_url')."/payment-successful/".$order->application->application_uuid);
            }else{
                return redirect(config('api.payment_failed_url'));
            }
        } else {
            return redirect(config('api.payment_failed_url'));
        }
    }
}
