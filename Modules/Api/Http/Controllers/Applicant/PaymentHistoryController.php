<?php

namespace Modules\Api\Http\Controllers\Applicant;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Modules\Api\Http\Controllers\ApiController;
use Modules\Core\Enums\HttpStatusCode;
use Modules\Application\Services\ApplicationService;
use Modules\Api\Transformers\ApplicationTransformer;
use Modules\Api\Http\Requests\Payment\PaymentCreateRequest;
use Modules\Application\Entities\Application;
use Modules\Payment\Services\PaymentService;
use Modules\Core\Exports\ListExport;
use Maatwebsite\Excel\Facades\Excel;

class PaymentHistoryController extends ApiController
{
    private $applicationService;
    private $transformer;
    private $applicationOwnership;
    private $applicationFinancial;
    private $attachmentService;
    private $paymentService;

    public function __construct(
        ApplicationService $applicationService,
        ApplicationTransformer $transformer,
        PaymentService $paymentService
    )
    {
        $this->applicationService = $applicationService;
        $this->transformer = $transformer;
        $this->paymentService = $paymentService;
    }

     /**
     * Function to get all payment history for an applicant
     * @param Request $request
     * @return Array $data
     * @author Daffodil Softwere
     */

    public function paymentHistory(Request $request, $user_id)
    {   
        $filters = $request->only('start');
        $filters['user_id'] = $user_id;
        if(!empty(request()->get('is_excel'))){
            unset($filters['start']);
            $history = $this->paymentService->lists($user_id);
            $excelType = !empty(request()->get('excel_type')) ? request()->get('excel_type') : null;
            return Excel::download(new ListExport($history, $excelType), 'applicant_payments.xlsx');
        }else{
            $history = $this->paymentService->lists($filters['user_id']);
        }
       
        $this->prepareApiResponse($history);
        return $this->sendApiResponse();
    }
}