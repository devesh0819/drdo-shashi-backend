<?php

namespace Modules\Api\Http\Controllers\Applicant;

use Modules\Api\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use Modules\Api\Http\Requests\Application\ApplicationCreateRequest;
use Modules\Api\Http\Requests\Application\ApplicationUpdateRequest;
use Modules\Core\Enums\HttpStatusCode;
use Modules\Application\Services\ApplicationService;
use Modules\Api\Transformers\ApplicationTransformer;
use Modules\Application\Entities\ApplicationOwnerProcess;
use Illuminate\Http\UploadedFile;
use Modules\Application\Entities\Application;
use Modules\Attachment\Services\AttachmentService;
use Modules\Api\Http\Requests\Application\AssessmentScheduleRequest;
use Modules\Core\Enums\AttachmentTypes;
use Dompdf\Dompdf;
use Illuminate\Support\Facades\URL;
use Illuminate\Validation\ValidationException;
use Modules\Core\Exports\ListExport;
Use Maatwebsite\Excel\Facades\Excel;
use Modules\Assessment\Services\AssessmentService;

class ApplicationController extends ApiController
{
    private $applicationService;
    private $transformer;
    private $applicationOwnership;
    private $attachmentService;
    private $assessmentService;

    public function __construct(
        ApplicationService $applicationService,
        ApplicationTransformer $transformer,
        AttachmentService $attachmentService,
        AssessmentService $assessmentService
    )
    {
        $this->applicationService = $applicationService;
        $this->transformer = $transformer;
        $this->applicationOwnership = new ApplicationOwnerProcess();
        $this->attachmentService = $attachmentService;
        $this->assessmentService = $assessmentService;
    }
    
    /**
     * Function to get list of all applications
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function index(Request $request)
    {   
        $filters = $request->only('start', 'application_no',
            'enterprise_name', 'payment_status', 'assessment_status','application_status',
            'rating', 'application_start_date', 'application_end_date',
            'assessment_start_date', 'assessment_end_date'
        );
        $filters['user_id'] = $request->user()->id;
        
        if(!empty(request()->get('is_excel'))){
            unset($filters['start']);
            $applications = $this->applicationService->lists($filters);
            $excelType = !empty(request()->get('excel_type')) ? request()->get('excel_type') : null;
            return Excel::download(new ListExport($applications, $excelType), 'vendor_applications.xlsx');
        }else{
            $applications = $this->applicationService->lists($filters);
        }
        
        $data = [
            'total'=>0,
            'applications'=>[]
        ];
        
        if(!empty($applications)){
            $data['total'] = $this->applicationService->lists($filters, true);
            foreach($applications as $application){
                $data['applications'][] = $this->transformer->transform($application); 
            }
        }

        $this->prepareApiResponse($data);
        return $this->sendApiResponse();
    }

    /**
     * Function to store an application
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function store(ApplicationCreateRequest $request)
    {
        $application = $this->applicationService->firstornew();
        $fillables = array_merge(['user_id'], $application->getFillable());
        $inputs = $request->only($fillables);
        if(!empty($inputs)){
            $inputs = $this->uploadCertificates($inputs, $application);
            $response = $this->applicationService->save($application, $inputs);
            if(!empty($response) ){
                if(!empty($response->vendorCertificate->id)){
                    $response->vendorCertificate->object_id = $response->id;
                    $response->vendorCertificate->save();
                }
                if(!empty($response->siteExteriorPicture->id)){
                    $response->siteExteriorPicture->object_id = $response->id;
                    $response->siteExteriorPicture->save();
                }
                if(!empty($response->incorporationCertificate->id)){
                    $response->incorporationCertificate->object_id = $response->id;
                    $response->incorporationCertificate->save();
                }
                if(!empty($response->udyamCertificate->id)){
                    $response->udyamCertificate->object_id = $response->id;
                    $response->udyamCertificate->save();
                }
                if(!empty($response->entrepreneurGSTCertificate->id)){
                    $response->entrepreneurGSTCertificate->object_id = $response->id;
                    $response->entrepreneurGSTCertificate->save();
                }
                
                $response = $this->transformer->transform($response, ['details'=>true]);
                $this->prepareApiResponse($response);
           }else{
                $this->httpCode = HttpStatusCode::HTTP_INTERNAL_SERVER_ERROR;
                $this->prepareApiResponse($response);
           }
        }
       return $this->sendApiResponse();
    }

    /**
     * Function to update an application
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function update(ApplicationCreateRequest $request, $applicationId)
    {
        $application = $this->applicationService->firstornew($applicationId);
        
        $fillables = $application->getFillable();
        $inputs = $request->only($fillables);
        if(!empty($inputs)){
            $inputs = $this->uploadCertificates($inputs, $application);
            $response = $this->applicationService->save($application, $inputs);
           if(!empty($response) ){
                $response = $this->transformer->transform($response, ['details'=>true]);
                $this->prepareApiResponse($response);
           }else{
                $this->httpCode = HttpStatusCode::HTTP_INTERNAL_SERVER_ERROR;
                $this->prepareApiResponse($response);
           }
        }
       return $this->sendApiResponse();
    }

    /**
     * Function to sync application ownerships
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function syncOwnership(ApplicationUpdateRequest $request, $applicationId)
    {
        $application = $this->applicationService->firstornew($applicationId);
        $fillables = $this->applicationOwnership->getFillable();
        $inputs = $request->only($fillables);
        if(!empty($inputs)){
            $response = $this->applicationService->syncApplicationOwnerships($application, $inputs);
            if(!empty($response) && $response instanceof Application){
                $response = $this->transformer->transform($response, ['details'=>true]);
                $this->prepareApiResponse($response);
            }else{
                $this->httpCode = HttpStatusCode::HTTP_INTERNAL_SERVER_ERROR;
                $this->prepareApiResponse($response);
            }
        }
       return $this->sendApiResponse();
    }

    /**
     * Function to submit an application
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function submitApplication($applicationId)
    {
        $application = $this->applicationService->firstornew($applicationId);
        if(!empty($application)){
           $response = $this->applicationService->submitApplication($application);
           if(!empty($response) ){
                $response = $this->transformer->transform($response);
                $this->prepareApiResponse($response);
           }else{
                $this->httpCode = HttpStatusCode::HTTP_INTERNAL_SERVER_ERROR;
                $this->prepareApiResponse($response);
           }
        }
        return $this->sendApiResponse();
    }

    /**
     * Function to schedule an Assessment
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function scheduleAssessment(AssessmentScheduleRequest $request, $applicationId)
    {
        $application = $this->applicationService->firstornew($applicationId);
        $inputs = $request->only('assessment_date');
        if(!empty($application)){
           if(!empty($application->assess_reschedule_count)){
            throw ValidationException::withMessages(['assessment' => __('validation.assessment_not_schedule')]);
           } 
           $response = $this->applicationService->scheduleAssessment($application, $inputs);
           if(!empty($response) ){
                // Start Assessment
                if(empty($application->assessment)){
                    $assessment = $this->assessmentService->firstornew();
                    $this->assessmentService->save($assessment, [
                        'application_id' => $application->id,
                        'application'=>$application
                    ]);
                }
                $response = $this->transformer->transform($response);
                $this->prepareApiResponse($response);
           }else{
                $this->httpCode = HttpStatusCode::HTTP_INTERNAL_SERVER_ERROR;
                $this->prepareApiResponse($response);
           }
        }
        return $this->sendApiResponse();
    }

    /**
     * Function to show a application
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function show($applicationId)
    {
        $application = $this->applicationService->firstornew($applicationId);
        if(!empty($application)){
            $data = $this->transformer->transform($application, [
                'details'=>true,
                'paymentOrder'=>true
            ]);
            $this->prepareApiResponse($data);
        } else {
            $this->prepareApiResponse(['error'=>HttpStatusCode::HTTP_NOT_FOUND]);
            $this->httpCode = HttpStatusCode::HTTP_NOT_FOUND;
        }

        return $this->sendApiResponse();
    }

    /**
     * Function to delete an application
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function destroy($applicationId)
    {
        $member = $this->applicationService->firstornew($applicationId);
        
        if(!empty($member)){
            $response = $this->applicationService->delete($member);
            $this->prepareApiResponse(['success'=>true]);
        } else {
            $this->prepareApiResponse(['error'=>HttpStatusCode::HTTP_NOT_FOUND]);
            $this->httpCode = HttpStatusCode::HTTP_NOT_FOUND;
        }

        return $this->sendApiResponse();
    }

    /**
     * Function to upload certificates
     */
    private function uploadCertificates($inputs =[], $application)
    {
        if(request()->file('vendor_certificate')){
            $previousVendorCertificate = !empty($application->vendor_certificate) ? $application->vendor_certificate : null;
            $inputs['vendor_certificate'] = $this->uploadPicture('vendor_certificate', $application, 'image', $previousVendorCertificate);
        }
        if(request()->file('entrepreneur_gst_certificate')){
            $previousGSTCertificate = !empty($application->entrepreneur_gst_certificate) ? $application->entrepreneur_gst_certificate : null;
            $inputs['entrepreneur_gst_certificate'] = $this->uploadPicture('entrepreneur_gst_certificate', $application, 'image', $previousGSTCertificate);
        }
        if(request()->file('udyam_certificate')){
            $previousUdyamCertificate = !empty($application->udyam_certificate) ? $application->udyam_certificate : null;
            $inputs['udyam_certificate'] = $this->uploadPicture('udyam_certificate', $application, 'image', $previousUdyamCertificate);
        }
        if(request()->file('incorporation_certificate')){
            $previousIncorpCertificate = !empty($application->incorporation_certificate) ? $application->incorporation_certificate : null;
            $inputs['incorporation_certificate'] = $this->uploadPicture('incorporation_certificate', $application, 'image', $previousIncorpCertificate);
        }
        if(request()->file('site_exterior_picture')){
            $previousSitePictureID = !empty($application->site_exterior_picture) ? $application->site_exterior_picture : null;
            $inputs['site_exterior_picture'] = $this->uploadPicture('site_exterior_picture', $application, 'image', $previousSitePictureID);
        }
        return $inputs;
    }

    /**
     * Function to upload mutliple pictures
     */
    private function uploadMultiplePictures($filename = '', Object $object, $fileType= 'image', $previousAttachID =null)
    {
        $files = request()->file($filename);
        if(is_array($files)){
            foreach($files as $file){
                $this->uploadPicture($file, $object, 'image', null);
            }
        }
        return true;
    }

    /**
     * Function to upload pictures
     */
    private function uploadPicture($filename = '', Object $object, $fileType= 'image', $previousAttachID =null)
    {
        $file = ($filename instanceof UploadedFile) ? $filename :request()->file($filename);
        //Upload file
        $attachment = null;
        if ($file instanceof UploadedFile) {
            $objectType = $this->getObjectType($object);
            $attachPath = $this->getApplicationFilePath($object);
            $attachment = $this->attachmentService->upload($file, $attachPath);
            if(!empty($attachment->id)){
                $objectId = !empty($object->id) ? $object->id : 0;
                $this->attachmentService->attach($attachment->id, $objectId, $objectType, $fileType);
            }
        }
        if(!empty($attachment->id) && !empty($previousAttachID)){
            $this->attachmentService->detach($previousAttachID);
        }
        return !empty($attachment->id) ? $attachment->id : null;
    }

    /**
     * Function to get object type
     */
    private function getObjectType($object)
    {
        if($object instanceof Application){
            return AttachmentTypes::APPLICATION;
        }elseif($object instanceof ApplicationOwnerProcess){
            return AttachmentTypes::APPLICATION_OWNERSHIP;
        }else{
            return get_class($object);
        }
    }

    /**
     * Function to get application pictures path
     */
    private function getApplicationFilePath($object)
    {
        $objectType  = $this->getObjectType($object);
        if($objectType == AttachmentTypes::APPLICATION){
            return "/application_".$object->id."/";
        }elseif($objectType == AttachmentTypes::APPLICATION_OWNERSHIP){
            return "/application_".$object->application_id."/";
        }else{
            return "/application_".$object->id."/";
        }
    }
}
