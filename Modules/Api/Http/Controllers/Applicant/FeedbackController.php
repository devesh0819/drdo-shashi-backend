<?php

namespace Modules\Api\Http\Controllers\Applicant;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Modules\Api\Http\Controllers\ApiController;
use Modules\Application\Services\ApplicationService;
use Modules\Api\Http\Requests\Application\FeedbackCreateRequest;
use Modules\Core\Enums\ApplicationStage;
use Modules\Core\Enums\HttpStatusCode;

class FeedbackController extends ApiController
{
    private $applicationService;

    public function __construct(
        ApplicationService $applicationService
    )
    {
        $this->applicationService = $applicationService;
    }
    
    /**
     * Submit a feedback to the application
     * @param Request $request
     * @return Renderable
     */
    public function submitFeedback(FeedbackCreateRequest $request, $applicationId)
    {
        $application = $this->applicationService->firstornew($applicationId);
        if(!empty($application->feedback->feedback_payload)){
            $feedVal = json_decode($application->feedback->feedback_payload, true);
            $inputs = [];
            $feedback_payload = [];
            if(!empty($feedVal)){
                foreach($feedVal as $key=>$val){
                    $feedback_payload[$key]=[
                        'uuid'=>$key,
                        'title'=>$val,
                        'rating'=>$request->get($key)
                    ];
                }
            }
            $inputs['feedback_payload'] = $feedback_payload;
            $inputs['feedback_comment'] = $request->get('feedback_comment');
            if(!empty($inputs) && $application->application_stage  == ApplicationStage::APP_ASSESS_COMPLETE){
                $response = $this->applicationService->saveFeedback($application, $inputs);
                $this->prepareApiResponse(['success'=>true,'message'=>__('api.feedback_submit_success')]);
            }else{
                $this->httpCode = HttpStatusCode::HTTP_BAD_REQUEST;
                $this->prepareApiResponse(['error'=>true,'message'=>__('api.feedback_submit_error')]);
            }
        }else{
            $this->httpCode = HttpStatusCode::HTTP_BAD_REQUEST;
            $this->prepareApiResponse(['error'=>true,'message'=>__('api.feedback_submit_error')]);
        }
        return $this->sendApiResponse();
    }

    /**
     * show a feedback to the application
     * @param Request $request
     * @return Renderable
     */
    public function show(Request $request, $applicationId)
    {
        $application = $this->applicationService->firstornew($applicationId);
        if(empty($application)){
            abort(404);
        }
        if(!empty($application->feedback->feedback_payload)){
            $data = json_decode($application->feedback->feedback_payload, true);
            $this->prepareApiResponse($data);
        } else {
            $this->prepareApiResponse(['error'=>HttpStatusCode::HTTP_NOT_FOUND]);
            $this->httpCode = HttpStatusCode::HTTP_NOT_FOUND;
        }

        return $this->sendApiResponse();
    }
}
