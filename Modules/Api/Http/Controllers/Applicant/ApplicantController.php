<?php

namespace Modules\Api\Http\Controllers\Applicant;

use Modules\Api\Http\Requests\User\UserCreateRequest;
use Modules\Api\Http\Controllers\ApiController;
use Modules\User\Services\UserService;
use Modules\Api\Transformers\UserProfileTransformer;
use Illuminate\Http\Request;
use Modules\Core\Enums\HttpStatusCode;
use Modules\Api\Http\Requests\User\UserUpdateRequest;

class ApplicantController extends ApiController
{
    private $userService;
    private $transformer;

    public function __construct(
        UserService $userService,
        UserProfileTransformer $transformer
    )
    {
        $this->userService = $userService;
        $this->transformer = $transformer;
    }
    
    /**
     * Function to sign up vendors into the system
     * @param Request $request
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function signup(UserCreateRequest $request)
    {
        $inputs = $request->only(
            'email', 'password', 'phone_number',
            'first_name', 'last_name', 'terms', 'roles');
        if(!empty($inputs)){
           $data = [];
           $user = $this->userService->firstornew();
           $response = $this->userService->save($user, $inputs);
           if(!empty($response['token']) && !empty($response['profile']->profile) ){
                $data['token'] = $response['token'];
                $data['user'] = $this->transformer->transform($response['profile']->profile);
                $this->prepareApiResponse($data);
           }else{
                $this->httpCode = HttpStatusCode::HTTP_INTERNAL_SERVER_ERROR;
                $this->prepareApiResponse($response);
           }
        }
       return $this->sendApiResponse();
    }

    /**
     * Function to update his profile
     * @param Request $request
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function updateProfile(UserUpdateRequest $request)
    {
        $inputs = $request->only(
            'email', 'phone_number',
            'first_name', 'last_name', 'alternate_email', 'alternate_phone_number');
        if(!empty($inputs)){
           $user = $this->userService->firstornew($request->user()->uuid);
           if(!empty($user)){
                $response = $this->userService->save($user, $inputs);
                if(!empty($response->profile) ){
                    $transformedResponse = $this->transformer->transform($response->profile);
                    $this->prepareApiResponse($response, $transformedResponse);
                }else{
                    $this->httpCode = HttpStatusCode::HTTP_INTERNAL_SERVER_ERROR;
                    $this->prepareApiResponse($response);
                }
           }
        }
       return $this->sendApiResponse();
    }
}
