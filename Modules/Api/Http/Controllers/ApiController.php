<?php

namespace Modules\Api\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;
use Modules\Core\Enums\HttpStatusCode;
use Modules\Core\Helpers\BasicHelper;

class ApiController extends Controller
{
    protected $httpCode;
    protected $apiResponse;

    /**
     * Send the response to client
     * @return type
     */
    public function sendApiResponse()
    {
        if (empty($this->httpCode)) {
            $this->httpCode = HttpStatusCode::HTTP_NO_CONTENT;
        }
        return response()->json($this->apiResponse, $this->httpCode);
    }

    /**
     * Resolve the API version
     * @return type
     */
    public function getApiVersion()
    {
        $apiAliases = config('api.version_alias');
        $version = request()->segment(2);
        return $apiAliases[$version];
    }

    /**
     * Prepare the response to send to the client
     * @param array $data
     * @return array
     */
    public function prepareApiResponse($serviceData = array(), $transformedData = array(), $total = false)
    {
        $response = array();
        if (BasicHelper::isException($serviceData)) {
            if($serviceData instanceof ValidationException){
                $response['errors'] = $serviceData->getMessage();
            }else{
                $response['errors'] = __('api.internal_server_error');
            }
            $this->httpCode = is_int($serviceData->getCode()) && !empty($serviceData->getCode()) ? $serviceData->getCode() : HttpStatusCode::HTTP_INTERNAL_SERVER_ERROR;
            $this->apiResponse = $response;
        } else {
            if ($total) {
                $response['total'] = $total;
            }
            $response['data'] = empty($transformedData) ? $serviceData : $transformedData;
            $this->httpCode = !empty($this->httpCode) ? $this->httpCode : HttpStatusCode::HTTP_OK;
            $this->apiResponse = $response;
        }
        return $response;
    }

}
