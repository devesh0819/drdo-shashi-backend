<?php

namespace Modules\Api\Http\Controllers\Drdo;

use Modules\Api\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use Modules\Application\Services\ApplicationService;
use Modules\Api\Transformers\ApplicationTransformer;
use Modules\Core\Enums\UserRole;
use Modules\Core\Enums\HttpStatusCode;
use Modules\Api\Http\Requests\Application\DrdoApprovalRequest;
use Modules\Core\Exports\ListExport;
Use Maatwebsite\Excel\Facades\Excel;

class ApplicationController extends ApiController
{
    private $applicationService;
    private $transformer;
    public function __construct(
        ApplicationService $applicationService,
        ApplicationTransformer $transformer
    )
    {
        $this->applicationService = $applicationService;
        $this->transformer = $transformer;
    }
    
    /**
     * Function to get list of all applications
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function index(Request $request)
    {   
        $filters = $request->only('start', 'application_no','application_status',
            'enterprise_name', 'payment_status', 'assessment_status','type_of_enterprise',
            'rating', 'application_start_date', 'application_end_date',
            'assessment_start_date', 'assessment_end_date', 'feedback_received', 'subsidy'
        );
        $filters['role'] = UserRole::DRDO;
        if(!empty(request()->get('is_excel'))){
            unset($filters['start']);
            $applications = $this->applicationService->lists($filters);
            $excelType = !empty(request()->get('excel_type')) ? request()->get('excel_type') : null;
            return Excel::download(new ListExport($applications, $excelType), 'drdo_application.xlsx');
        }else{
        $applications = $this->applicationService->lists($filters);
        }
        $data = [
            'total'=>0,
            'applications'=>[]
        ];
        
        if(!empty($applications)){
            $data['total'] = $this->applicationService->lists($filters, true);
            foreach($applications as $application){
                $data['applications'][] = $this->transformer->transform($application, ['details'=>true]); 
            }
        }

        $this->prepareApiResponse($data);
        return $this->sendApiResponse();
    }

    /**
     * Function to approve an application
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function approveApplication(DrdoApprovalRequest $request, $applicationId)
    {
        $application = $this->applicationService->firstornew($applicationId);
        $inputs = $request->only('application_stage', 'subsidy_approved');
        if(!empty($inputs)){
           $response = $this->applicationService->approveApplication($application, $inputs);
           if(!empty($response) ){
                $response = $this->transformer->transform($response);
                $this->prepareApiResponse($response);
           }else{
                $this->prepareApiResponse($response);
           }
        }
       return $this->sendApiResponse();
    }

    /**
     * Function to show an application
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function show($applicationId)
    {
        $application = $this->applicationService->firstornew($applicationId);
        
        if(!empty($application)){
            $data = $this->transformer->transform($application, ['details'=>true]);
            $this->prepareApiResponse($data);
        } else {
            $this->prepareApiResponse(['error'=>HttpStatusCode::HTTP_NOT_FOUND]);
            $this->httpCode = HttpStatusCode::HTTP_NOT_FOUND;
        }

        return $this->sendApiResponse();
    }
}
