<?php

namespace Modules\Api\Http\Controllers\Drdo;

use Modules\Api\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use Modules\Api\Transformers\QuestionnaireTransformer;
use Modules\Questionnaire\Services\QuestionnaireService;
use Modules\Core\Enums\HttpStatusCode;
use Modules\Attachment\Services\AttachmentService;
use Modules\Api\Http\Requests\Questionnaire\QuestionnaireApprovalRequest;
use Modules\Core\Enums\QuestionnaireStage;

class QuestionnaireController extends ApiController
{
    private $questionnaireService;
    private $transformer;
    private $attachmentService;

    public function __construct(
        QuestionnaireService $questionnaireService,
        QuestionnaireTransformer $transformer,
        AttachmentService $attachmentService
    )
    {
        $this->questionnaireService = $questionnaireService;
        $this->transformer = $transformer;
        $this->attachmentService = $attachmentService;
    }

    /**
     * Function to get list of all questionnaires
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function index(Request $request)
    {  
        $filters = [
            'start'=>$request->get('start'),
            'role'=>'drdo'
        ];
        $questionnaires = $this->questionnaireService->lists($filters);
        $data = [
            'total'=>0
        ];
        if(!empty($questionnaires)){
            $data['total'] = $this->questionnaireService->lists($filters, true);
            foreach($questionnaires as $questionnaire){
             $data['questionnaires'][] = $this->transformer->transform($questionnaire);
            }
        }
        $this->prepareApiResponse($data);
        return $this->sendApiResponse(); 

    }

    /**
     * Function to show the detail of a questionnaire
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function show(Request $request, $questionnaireID)
    {  
        if(!empty($questionnaireID)){
            $questionnaire = $this->questionnaireService->firstornew($questionnaireID);
        }
        $this->prepareApiResponse($questionnaire);
        return $this->sendApiResponse(); 
    }

    /**
     * Function to show audit detail of a questionnaire
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function showAudit(Request $request, $questionnaireID)
    {  
        if(!empty($questionnaireID)){
            $auditLogs = $this->questionnaireService->getAuditLogs($questionnaireID);
        }
        $this->prepareApiResponse($auditLogs);
        return $this->sendApiResponse(); 
    }

    /**
     * Function to show parameter detail of a questionnaire
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function showParameter(Request $request, $parameterID)
    {  
        if(!empty($parameterID)){
            $parameter = $this->questionnaireService->getParameter($parameterID);
        }
        $this->prepareApiResponse($parameter);
        return $this->sendApiResponse(); 
    }
    
    /**
     * Function to sent questionnaire for approval
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function checkApproval(QuestionnaireApprovalRequest $request, $questionnaireID)
    {
        if(!empty($questionnaireID)){
            $questionnaire = $this->questionnaireService->firstornew($questionnaireID);
            $inputs = $request->only('is_approved');
            if(!empty($questionnaire) && $questionnaire->state == QuestionnaireStage::PENDING_APPROVAL){
                $questionnaire = $this->questionnaireService->approveQuestionnaire($questionnaire, $inputs);
                $this->prepareApiResponse(['success'=>true, 'message'=>__('questionnaire_saved_success')]);
            }else{
                $this->httpCode = HttpStatusCode::HTTP_BAD_REQUEST;
                $this->prepareApiResponse(['error'=>true, 'message'=>__('questionnaire_saved_error')]);
            }
        }
        
        return $this->sendApiResponse();
    }
}
