<?php

namespace Modules\Api\Http\Controllers\Core;

use Modules\Api\Http\Controllers\ApiController;

use Modules\Attachment\Entities\Attachment;
use Response;
use Illuminate\Support\Facades\Storage;
use Modules\Attachment\Services\AttachmentService;
use Modules\Assessment\Entities\AssessmentParameterEvidence;
use Modules\Assessment\Entities\AssessmentSiteEvidense;

class FileController extends ApiController
{
    private $attachmentService;

    public function __construct(AttachmentService $attachmentService)
    {
        $this->attachmentService = $attachmentService;
    }

    /**
     * FUnction to download files by url 
     * 
     **/  
    public function showFile($attachmentID)
    {
        if(request()->get('type') == 'evidense'){
            $attachment = AssessmentParameterEvidence::where('evidense_uuid', request()->route('id'))->first();
        }elseif(request()->get('type')  == 'site_evidense'){
            $attachment = AssessmentSiteEvidense::where('evidense_uuid', request()->route('id'))->first();
        }else{
            $attachment = Attachment::where('attachment_uuid', $attachmentID)->first();
        }
        
        if(!empty($attachment)){
            return Storage::disk($attachment->disk)->download($attachment->path);
        }
        return false;
    }

    /**
     * FUnction to delete file by id
     * 
     **/  
    public function deleteFile($attachmentID)
    {
        $attachment = Attachment::where('attachment_uuid', $attachmentID)->first();
        if(!empty($attachment)){
            $this->attachmentService->detach($attachment->id);
            $this->prepareApiResponse(['message'=>__('api.attachment_delete_success')]);
        }else{
            $this->prepareApiResponse(['message'=>__('api.attachment_delete_error')]);
        }
        return $this->sendApiResponse();
    }
}
