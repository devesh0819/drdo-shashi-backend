<?php

namespace Modules\Api\Http\Controllers\Core;

use Modules\Api\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use Modules\Core\Entities\State;
use Modules\Agency\Entities\AgencyMeta;
use Modules\User\Entities\UserProfile;
use Illuminate\Support\Facades\DB;
use Modules\Application\Entities\Application;
use Modules\Core\Enums\Flag;
use Modules\Assessment\Entities\Assessment;
use Modules\Assessor\Entities\AssessorMeta;
use Modules\Core\Enums\AssessmentStage;
use Modules\Questionnaire\Entities\Questionnaire;
use Modules\Core\Entities\Country;

class CoreController extends ApiController
{
    private $coreService;
    private $transformer;

    public function __construct(
    )
    {
    }
    
    /**
     * Function to get master values
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function getMasterValues(Request $request)
    {
        $inputs = $request->only('type');
        $data = [];
        switch ($inputs['type']) {
            case "social_category":
                $data = $this->getSocialCategory();
                break;
            case "special_category":
                $data = $this->getSpecialCategory();
                break;    
            case "state":
                $data = $this->getStateData();
                break;
            case "country":
                $data = $this->getCountryData();
                break;    
            case "district":
                $stateId = request()->get('stateId');
                $data = $this->getDisrictData($stateId);
                break;
            case "organisation_type":
                $data = $this->getOrganisationTypes();
                break;
            case "enterprise_nature":
                $data = $this->getEnterpriseNature();
                break;
            case "agency":
                $data = $this->getAgency();
                break;
            case "assessor":
                $agencyId = request()->get('agencyId');
                $locationId = (int)request()->get('districtID');
                $data = $this->getAssessors($agencyId, $locationId);
                break;
            case "member":
                $data = $this->getMeetingMembers();
                break;
            case "assessments":
                $data = $this->getAssessmentsTobeReviewed();
                break;
            case "assessmentlist":
                $data = $this->getAssessmentsList();
                break;
            case "standards":
                $data = $this->getStandards();
                break;
            case "certified_vendors":
                $data = $this->getCertifiedVendors();
                break;         
            default:
                break;         
        }
        $response = [];
        if(in_array($inputs['type'], ['standards', 'certified_vendors'])){
            $response = $data;
        }elseif(!empty($data)){
            foreach($data as $key=>$value){
                array_push($response, [
                    'id'=>$key,
                    'title'=>$value
                ]);
            }
        }
        $this->prepareApiResponse($response);
        return $this->sendApiResponse();
    }

    /**
     * Function to get all social categories
     * @return Array $data
     * @author Daffodil Softwere
     */
    private function getSocialCategory()
    {
        return config('core.social_categories');
    }

    /**
     * Function to get all special categories
     * @return Array $data
     * @author Daffodil Softwere
     */
    private function getSpecialCategory()
    {
        return config('core.special_categories');
    }

    /**
     * Function to get all states
     * @return Array $data
     * @author Daffodil Softwere
     */
    private function getStateData()
    {
        return State::all()->pluck('title','id')->toArray();
    }

    /**
     * Function to get all country
     * @return Array $data
     * @author Daffodil Softwere
     */
    private function getCountryData()
    {
        return Country::all()->pluck('title','country_code')->toArray();
    }

    /**
     * Function to get all districts
     * @return Array $data
     * @author Daffodil Softwere
     */
    private function getDisrictData($stateId)
    {
        $state = State::with('districts')->where('id', $stateId)->first();
        return $state->districts->pluck('title', 'id')->toArray(); 
    }


    /**
     * Function to get all organisation types
     * @return Array $data
     * @author Daffodil Softwere
     */
    private function getOrganisationTypes()
    {
        return config('organisation_types');
    }


    /**
     * Function to get enterprise nature
     * @return Array $data
     * @author Daffodil Softwere
     */
    private function getEnterpriseNature()
    {
        return config('enterprise_nature');
    }

    /**
     * Function to get agency Lists
     * @return Array $data
     * @author Daffodil Softwere
     */
    private function getAgency()
    {
        return AgencyMeta::all()->pluck('title','user_id')->toArray();
    }

    /**
     * Function to get assessors Lists
     * @return Array $data
     * @author Daffodil Softwere
     */
    private function getAssessors($agencyId, $locationId=null)
    {
        $users = [];
            
        if(!empty($locationId) && is_int($locationId)){
            $assessorSameDistrict = AssessorMeta::with('user')
            ->where('assessor_status', 'active')
            ->where('agency_id', $agencyId)
            ->where('assessor_district', $locationId)
            ->get();
            $assessorDiffDistrict = AssessorMeta::with('user')
            ->where('assessor_status', 'active')
            ->where('agency_id', $agencyId)
            ->where('assessor_district', '!=', $locationId)
            ->get();
            if(!empty($assessorSameDistrict)){
                foreach($assessorSameDistrict as $assessor){
                    $users[$assessor->user_id] = !empty($assessor->user->profile) ? $assessor->user->profile->first_name." ".$assessor->user->profile->last_name."(".$assessor->user->email."), ".$assessor->assessorDistrict->title: "";
                }
            }
            if(!empty($assessorDiffDistrict)){
                foreach($assessorDiffDistrict as $assessor){
                    $users[$assessor->user_id] = !empty($assessor->user->profile) ? $assessor->user->profile->first_name." ".$assessor->user->profile->last_name."(".$assessor->user->email."), ".$assessor->assessorDistrict->title : "";
                }
            }
        }else{
            $assessors = AssessorMeta::with('user')
            ->where('assessor_status', 'active')
            ->where('agency_id', $agencyId)
            ->get();
            $users = [];
            if(!empty($assessors)){
                foreach($assessors as $assessor){
                    $users[$assessor->user_id] = !empty($assessor->user->profile) ? $assessor->user->profile->first_name." ".$assessor->user->profile->last_name."(".$assessor->user->email."), ".$assessor->assessorDistrict->title : "";
                }
            }
        }
        
        return $users;
    }

    /**
     * FUnction to get Meeting Members
     */
    private function getMeetingMembers()
    {
        $users = UserProfile::select("user_profiles.user_id", DB::raw("CONCAT(user_profiles.first_name, ' ', user_profiles.last_name) as full_username"));

        $users->join('model_has_roles', function ($roleJoin) {
            $roleJoin->on('user_profiles.user_id', '=', 'model_has_roles.model_id')->whereIn("model_has_roles.role_id", [FLAG::ADMIN, FLAG::MEMBER]);
        });
        return $users->get()->pluck('full_username','user_id')->toArray();
    }

    /**
     * Function to get assessments list
     */
    private function getAssessmentsTobeReviewed()
    {
        return Assessment::where('assessment_stage', AssessmentStage::ASSESS_SUBMIT)->get()->pluck('assessment_number','id')->toArray();
    }

    /**
     * Function to get standards for All vendors
     */
    private function getStandards()
    {
        $data = [];
        $data['small_questionnaire'] = Questionnaire::where([
            'status'=>Flag::IS_ACTIVE,
            'is_approved'=>Flag::IS_ACTIVE,
            'type'=>'S&M'
        ])->latest()->first()->toArray();
        $data['large_questionnaire'] = Questionnaire::where([
            'status'=>Flag::IS_ACTIVE,
            'is_approved'=>Flag::IS_ACTIVE,
            'type'=>'L'
        ])->latest()->first()->toArray();
        return $data;
    }

    /**
     * Function to get certified vendors list
     */
    private function getCertifiedVendors()
    {
        $assessments = Assessment::whereIn('assessment_stage', 
        [
            AssessmentStage::ASSESS_COMPLETE,
            AssessmentStage::ASSESS_FEEDBACK_SUBMIT
        ])->latest()->get();
        $data = [];
        if(!empty($assessments)){
            foreach($assessments as $assessment)
            {
                $assessExpiryDate = !empty($assessment->certification_expiry) ? date("Y-m-d", strtotime($assessment->certification_expiry)) :  null;
                $data[] = [
                    'application_number'=>$assessment->application->application_number,
                    'expiry_date'=>$assessExpiryDate,
                    'certification_level'=>__("api.".$assessment->certification_type."-certification"),
                    'unit_address'=>!empty($assessment->application->registered_address) ? $assessment->application->registered_address : null,
                    'entrepreneur_name'=>$assessment->application->entrepreneur_name,
                    'enterprise_name'=>$assessment->application->enterprise_name
                ];
            }
        }
        return $data;
    }
}
