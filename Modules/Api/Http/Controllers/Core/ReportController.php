<?php

namespace Modules\Api\Http\Controllers\Core;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Modules\Api\Http\Controllers\ApiController;
use Modules\Application\Entities\Application;
//use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Support\Facades\View;
use Modules\Application\Services\ApplicationService;
use Barryvdh\Snappy\Facades\SnappyPdf;
use Barryvdh\Snappy\Facades\SnappyImage;
use Modules\Assessment\Services\AssessmentParameterService;
use Modules\Assessment\Entities\AssessmentSiteEvidense;

class ReportController extends ApiController
{
    private $applicationService;
    private $parameterService;

    public function __construct(
        ApplicationService $applicationService,
        AssessmentParameterService $parameterService
    )
    {
        $this->applicationService = $applicationService;
        $this->parameterService = $parameterService;
    }

    /**
     * Function to download certificate for an application
     * @param Request $request
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function downloadCertificate($applicationID)
    {
        $application = $this->applicationService->firstornew($applicationID);
        $parameters = $application->assessment->parameters();
        $pdf = SnappyPdf::loadView('api::application.certificate', ['application'=>$application,'parameters'=>$parameters]);
        $pdf->setOption('enable-javascript', true);
        $pdf->setOption('javascript-delay', 5000);
        $pdf->setOption('enable-smart-shrinking', true);
        $pdf->setOption('no-stop-slow-scripts', true);
        return $pdf->download('certificate.pdf'); 
    }

    /**
     * Function to download Report for a application
     * @param Request $request
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function downloadReport($applicationID)
    {
        $application = $this->applicationService->firstornew($applicationID);
        $pages = config('api.applicationReportFormat.pages');
        $parameters = $application->assessment->parameters();
        $performanceData = [];
        $latLongData = [];
        if(!empty($parameters)){
            foreach($parameters as $parameter){
                if(!empty($parameter['discipline'])){
                    if(!empty($performanceData[$parameter['discipline']]['parameters'])){
                        array_push($performanceData[$parameter['discipline']]['parameters'],[
                            'title'=>$parameter['parameter'],
                            'score'=>$parameter['parameter_score']
                        ]);
                    }else{
                        $performanceData[$parameter['discipline']]['strength'] = [];
                        $performanceData[$parameter['discipline']]['ofi'] = [];
                        $performanceData[$parameter['discipline']]['parameters'] = [
                            [
                                'title'=>$parameter['parameter'],
                                'score'=>$parameter['parameter_score']
                            ]
                        ];
                    }
                    if(!empty($parameter->comments)){
                        foreach($parameter->comments as $comment){
                            if(!empty($comment->comment_type) && $comment->comment_type == 'strength'){
                                array_push($performanceData[$parameter['discipline']]['strength'], $comment);
                            }elseif(!empty($comment->comment_type) && $comment->comment_type == 'ofi'){
                                array_push($performanceData[$parameter['discipline']]['ofi'], $comment);
                            }
                        }
                    }
                    if(!empty($parameter->evidenses)){
                        foreach($parameter->evidenses as $evidense){
                            if(!empty($evidense->latitude) && !empty($evidense->longitude)){
                                $latLongData = [
                                    'latitude'=>$evidense->latitude,
                                    'longitude'=>$evidense->longitude
                                ];
                                break;
                            }
                        }
                    }

                }
            }
        }
        $assessmentEvidenses = AssessmentSiteEvidense::where('assessment_id', $application->assessment->id)
        ->whereIn('type', ['open_meet', 'close_meet'])
        ->get();
        $pdf = SnappyPdf::loadView('api::application.report', ['assessmentEvidenses'=>$assessmentEvidenses, 'latLongData'=>$latLongData, 'application'=>$application, 'pages'=>$pages, 'performanceData'=>$performanceData]);
        $pdf->setOption('enable-javascript', true);
        $pdf->setOption('javascript-delay', 5000);
        $pdf->setOption('enable-smart-shrinking', true);
        $pdf->setOption('no-stop-slow-scripts', true);
        return $pdf->download('report.pdf');      
    }
}
