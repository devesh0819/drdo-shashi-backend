<?php

namespace Modules\Api\Http\Controllers\Agency;

use Modules\Api\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use Modules\Api\Http\Requests\Assessment\AssessmentUpdateRequest;
use Modules\Assessment\Services\AssessmentService;
use Modules\Api\Transformers\AssessmentTransformer;

class AgencyAssessmentController extends ApiController
{
    private $assessmentService;
    private $transformer;

    public function __construct(
        AssessmentService $assessmentService,
        AssessmentTransformer $transformer
    )
    {
        $this->assessmentService = $assessmentService;
        $this->transformer = $transformer;
    }
    
    /**
     * Function to list all the Assessments assigned to agency
     * @param Request $request
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function getAssessments(Request $request)
    {
        $filters = [
            'agency_id'=>$request->user()->id
        ];
        $assessments = $this->assessmentService->lists($filters);
        $data = [
            'total'=>0,
            'assessments'=>[]
        ];
        
        if(!empty($assessments)){
            $data['total'] = $this->assessmentService->lists($filters, true);
            foreach($assessments as $assessment){
                $data['assessments'][] = $this->transformer->transform($assessment,['details'=>true]); 
            }
        }

        $this->prepareApiResponse($data);
        return $this->sendApiResponse();   
    }

    /**
     * Function to assign assessors to a assessment
     * @param Request $request
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function assignAssessors(AssessmentUpdateRequest $request, $assessmentid)
    {
        $inputs = $request->only("assessor_ids", "lead_assessor_id", "assessment_date");
        if(!empty($inputs)){
           $assessment = $this->assessmentService->firstornew($assessmentid);
           $response = $this->assessmentService->save($assessment, $inputs);
           if(!empty($response) ){
                $this->prepareApiResponse($response, $this->transformer->transform($response));
           }else{
                $this->prepareApiResponse($response);
           }
        }
       return $this->sendApiResponse();
    }

    /**
     * Function to show all assessments assigned to agency
     * @param Request $request
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function showAssessment(Request $request, $id)
    {
        $assessment = $this->assessmentService->firstornew($id);
        if(!empty($assessment->id) ){
            $this->prepareApiResponse($assessment, $this->transformer->transform($assessment,['details'=>true, 'assessor'=>true]));
       }else{
            $this->prepareApiResponse($assessment);
       }
        return $this->sendApiResponse();
    }
}
