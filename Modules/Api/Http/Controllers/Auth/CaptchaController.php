<?php

namespace Modules\Api\Http\Controllers\Auth;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Modules\Api\Http\Controllers\ApiController;
use Mews\Captcha\CaptchaController as CaptchaBaseController;
use Mews\Captcha\Captcha;

class CaptchaController extends ApiController
{
    private $captchaBase;
    private $captcha;
    public function __construct(CaptchaBaseController $captchaBase, Captcha $captcha)
    {
        $this->captchaBase = $captchaBase;
        $this->captcha = $captcha;
    }
    /**
     * Function to get captcha
     * @param Request $request
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function getCaptcha()
    {
        return  $this->captchaBase->getCaptchaApi($this->captcha);
    }
}
