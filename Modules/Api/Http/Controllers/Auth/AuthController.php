<?php

namespace Modules\Api\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Modules\Api\Http\Controllers\ApiController;
use Modules\Api\Http\Requests\Auth\LoginCreateRequest;
use Modules\Api\Http\Requests\Auth\ResetPasswordRequest;
use Modules\Api\Http\Requests\Auth\ChangePasswordRequest;
use Illuminate\Support\Facades\Auth;
use Modules\Api\Transformers\UserProfileTransformer;
use Modules\Core\Enums\HttpStatusCode;
use Modules\User\Services\UserService;
use Illuminate\Support\Facades\Password;
use Modules\Api\Http\Requests\Auth\ForgotPasswordRequest;
use Modules\Core\Enums\UserRole;
use Modules\Api\Http\Requests\Auth\EmailVerificationRequest;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Validation\ValidationException;
use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Support\Facades\Hash;

class AuthController extends ApiController
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use ThrottlesLogins;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';
    public $maxAttempts = 5; 
    private $passwordUpdated=false;
    private $transformer;
    private $userService;

    public function __construct(
        UserProfileTransformer $transformer,
        UserService $userService
    )
    {
        $this->transformer = $transformer;
        $this->userService = $userService;
        $this->middleware('throttle:6,1')->only('resend');
    }

     /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        $login = request()->input('email');
        $field = filter_var($login, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
        request()->merge([$field => $login]);
        return $field;
    }
    
    /**
     * Function to perform login operation for users
     * @param Request $request
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function login(LoginCreateRequest $request)
    {
        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if (method_exists($this, 'hasTooManyLoginAttempts') &&    
            $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }

        if(Auth::attempt([$this->username() => $request->email, 'password' => $request->password])){ 
            $this->authenticated($request, $request->user());
        } else{
            // If the login attempt was unsuccessful we will increment the number of attempts
            // to login and redirect the user back to the login form. Of course, when this
            // user surpasses their maximum number of attempts they will get locked out.
            $this->incrementLoginAttempts($request);
            return $this->sendFailedLoginResponse($request);
        } 
        return $this->sendApiResponse();
    }

    /**
     * Function to be executed after login attempt is successful
     * @param Request $request
     * @return Array $data
     * @author Daffodil Softwere
     */
    protected function authenticated(Request $request, $user)
    {
        $userRole = !empty($user->roles) ? $user->roles->pluck('name')->toArray() : [];
        $data = [];
        if($user->hasRole(UserRole::ASSESSOR)){
            $user->load('assessorMeta');
            if(!empty($user->assessorMeta) && $user->assessorMeta->assessor_status != 'active'){
                $this->logoutUser();
                throw ValidationException::withMessages([
                    $this->username() => [trans('auth.assessor_not_active')]
                ]);
            }
        }

        if (empty($user->status)) {
            $this->logoutUser();
            $this->httpCode = HttpStatusCode::HTTP_INTERNAL_SERVER_ERROR;
            $this->prepareApiResponse(['error'=>__('auth.inactive')]);;
        } elseif (
            !$user->hasRole(UserRole::SUPER_ADMIN)
            && !$user->hasRole(UserRole::ADMIN) 
            && !$user->hasRole(UserRole::VENDOR)
            && !$user->hasRole(UserRole::AGENCY)
            && !$user->hasRole(UserRole::ASSESSOR)
            && !$user->hasRole(UserRole::DRDO)
            ) {
            $this->logoutUser();
            $this->httpCode = HttpStatusCode::HTTP_INTERNAL_SERVER_ERROR;
            $this->prepareApiResponse(['error'=>__('auth.unauthorized')]);
        }else{
            $user = Auth::user();
            $profile = $user->load('profile');
            // Remove All Previous Sessions
            $tokens = $user->tokens;
            foreach($tokens as $token) {
                $token->revoke();   
            }
            $data['token'] = $user->createToken(config('auth.authTokenName'), config('rolespermissions.roles-permissions.'.$userRole[0]))->accessToken;
            $data['user'] =  $this->transformer->transform($profile->profile);
            $this->prepareApiResponse($data);
        }

        return $this->sendApiResponse();
    }

    /**
     * Function to Logout the user
     * @param Request $request
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function logout()
    {
        if($this->logoutUser()){
            $this->prepareApiResponse(['success'=>true]);
        }else{
            $this->httpCode = HttpStatusCode::HTTP_UNAUTHORIZED;
            $this->prepareApiResponse(['error'=>'Unauthorised']);
        }
        return $this->sendApiResponse();
    }

    /**
     * Function to Send Forget Password email
     * @param Request $request
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function sendForgotEmail(ForgotPasswordRequest $request)
    {
        $status = Password::sendResetLink(
            $request->only('email')
        );
        $this->prepareApiResponse(['message'=>__('auth.forgot_password_success')]);

        return $this->sendApiResponse();
    }

    /**
     * Get the broker to be used during password reset.
     *
     * @return \Illuminate\Contracts\Auth\PasswordBroker
     */
    public function broker()
    {
        return Password::broker();
    }
    
    /**
     * Function to Reset Password through password reset link
     * @param Request $request
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function resetPassword(ResetPasswordRequest $request)
    {
        $inputs = $request->only('email', 'password','password_confirmation', 'token');

        $user = $this->userService->findUserByEmail($inputs['email']);
        $this->passwordUpdated = false;       
        // Here we will attempt to reset the user's password. If it is successful we
        // will update the password on an actual user model and persist it to the
        // database. Otherwise we will parse the error and return the response.
        $response = $this->broker()->reset(
            $inputs, function ($user, $password) use ($inputs){
                $this->passwordUpdated = $this->userService->resetPassword($user, $inputs['password'], $inputs['token']);
            }
        );
        
        if(!empty($user) && $this->passwordUpdated){
            $this->prepareApiResponse(['success'=>true]);
        }else{
            $this->httpCode = HttpStatusCode::HTTP_INTERNAL_SERVER_ERROR;
            $this->prepareApiResponse(['message'=>__($response)]);
        }

        return $this->sendApiResponse();
    }

    /**
     * Function to Change User's Password
     * @param Request $request
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function changePassword(ChangePasswordRequest $request)
    {
        $inputs = $request->only('password', 'old_password');
        $user = Auth::User();
        
        if (Hash::check($inputs['old_password'], $user->password)) {
            if($this->userService->save($user, $inputs)){
                // Delete all other logged in users
                $this->prepareApiResponse(['success'=>true]);
            }else{
                $this->httpCode = HttpStatusCode::HTTP_INTERNAL_SERVER_ERROR;
                $this->prepareApiResponse(['error'=>'Something went wrong!']);
            }
        }else{
            $this->httpCode = HttpStatusCode::HTTP_INTERNAL_SERVER_ERROR;
            $this->prepareApiResponse(['message'=>'old_password_mismatch']);
        }
        

        return $this->sendApiResponse();
    }

    /**
     * Function to verify User OTP
     * @param Request $request
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function verifyOTP(EmailVerificationRequest $request)
    {
        $inputs = $request->only('otp');
        if ($request->user()->hasVerifiedEmail()) {
            $this->prepareApiResponse(['message'=>__('auth.email_verify_already')]);
        }elseif($this->userService->verifyOTP(Auth::User(), $inputs)){
            $request->user()->markEmailAsVerified();
            $this->prepareApiResponse(['message'=>__('auth.email_verify_success')]);
        }else{
            $this->httpCode = HttpStatusCode::HTTP_INTERNAL_SERVER_ERROR;
            $this->prepareApiResponse(['error'=>__('auth.email_verify_error')]);
        }

        return $this->sendApiResponse();
    }

    /**
     * Resend the email verification notification.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function resend(Request $request)
    {
        if ($request->user()->hasVerifiedEmail()) {
            $this->prepareApiResponse(['message'=>__('auth.email_verify_already')]);
        }else{
            $request->user()->sendEmailVerificationNotification();
            $this->prepareApiResponse(['message'=>__('auth.email_verification_sent')]);
        }
        return $this->sendApiResponse();
    }

    /**
     * Function to revoke user access token and logout
     * @param Request $request
     * @return Array $data
     * @author Daffodil Softwere
     */
    private function logoutUser()
    {
        $token = Auth::user()->token();
        if(!empty($token)){
            $token->revoke();
        }
        return true;
    }

    /**
     * Get the failed login response instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        
        $totalLoginAttempts = $this->limiter()->attempts($this->throttleKey($request)); 
        $attemptsLeft = ($this->maxAttempts())-$totalLoginAttempts;
        if($attemptsLeft >= 1){
            throw ValidationException::withMessages([
                $this->username() => [trans('auth.failed', ['attemptsLeft'=>$attemptsLeft])]
            ]);
        }else{
            return $this->sendLockoutResponse($request);
        }
        
    }
}
