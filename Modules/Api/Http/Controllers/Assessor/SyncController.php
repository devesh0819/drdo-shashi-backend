<?php

namespace Modules\Api\Http\Controllers\Assessor;

use Modules\Api\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use Modules\Assessment\Services\AssessmentService;
use Modules\Api\Transformers\AssessorSyncTransformer;
use Modules\Api\Transformers\AssessmentParameterTransformer;
use Modules\Assessment\Services\AssessmentParameterService;
use Modules\Attachment\Services\AttachmentService;

class SyncController extends ApiController
{
    private $assessmentEvidencePath;
    private $assessmentService;
    private $assessmentParameterService;
    private $transformer;
    private $parameterTransformer;
    private $attachmentService;

    public function __construct(
        AssessmentService $assessmentService,
        AssessorSyncTransformer $transformer,
        AssessmentParameterService $assessmentParameterService,
        AssessmentParameterTransformer $parameterTransformer,
        AttachmentService $attachmentService
    )
    {
        $this->assessmentService = $assessmentService;
        $this->transformer = $transformer;
        $this->assessmentParameterService = $assessmentParameterService;
        $this->parameterTransformer = $parameterTransformer;
        $this->attachmentService = $attachmentService;
    }
    
    /**
     * Function to Sync all data in which assessor is assigned
     * @param Request $request
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function syncData(Request $request)
    {
        $filters = [
            'assessor_id'=>request()->user()->id,
            'assessor_sync'=>true
        ];
        $assessments = $this->assessmentService->lists($filters);
        $data = [
            'total'=>0,
            'assessments'=>[]
        ];
        if(!empty($assessments)){
            $data['total'] = $this->assessmentService->lists($filters, true);
            $parameters = $this->assessmentParameterService->lists($filters);
            foreach($assessments as $assessment){
                $siteDetails = $this->assessmentService->getSiteDetails($assessment->id, $filters);
                $data['assessments'][] =  $this->transformer->transform($assessment, $parameters, $siteDetails);
            }
        }
        
        $this->prepareApiResponse($data);
        return $this->sendApiResponse();   
    }

    /**
     * Function to store details of any assigned assessment
     * @param Request $request
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function storeData(Request $request)
    {
        $inputs = $request->all();
        $assessor = request()->user();
        $assessment = $this->assessmentParameterService->syncAssessmentData($inputs, $assessor);
        if(!empty($assessment->id) ){
            $this->prepareApiResponse($assessment, $this->transformer->transform($assessment));
        }else{
            $this->prepareApiResponse($assessment);
        }
        return $this->sendApiResponse();
    }
}
