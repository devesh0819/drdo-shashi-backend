<?php

namespace Modules\Api\Http\Controllers\Assessor;

use Illuminate\Http\UploadedFile;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Modules\Api\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use Modules\Api\Http\Requests\Assessment\AssessmentParemeterCreateRequest;
use Modules\Assessment\Services\AssessmentService;
use Modules\Api\Transformers\AssessmentTransformer;
use Modules\Api\Transformers\AssessmentParameterTransformer;
use Modules\Assessment\Services\AssessmentParameterService;
use Modules\Attachment\Services\AttachmentService;
use Modules\Api\Http\Requests\Attachment\UploadRequest;
use Modules\Assessment\Entities\AssessmentParameter;
use Modules\Core\Exports\ListExport;
Use Maatwebsite\Excel\Facades\Excel;

class AssessorAssessmentController extends ApiController
{
    private $assessmentEvidencePath;
    private $assessmentService;
    private $assessmentParameterService;
    private $transformer;
    private $parameterTransformer;
    private $attachmentService;

    public function __construct(
        AssessmentService $assessmentService,
        AssessmentTransformer $transformer,
        AssessmentParameterService $assessmentParameterService,
        AssessmentParameterTransformer $parameterTransformer,
        AttachmentService $attachmentService
    )
    {
        $this->assessmentService = $assessmentService;
        $this->transformer = $transformer;
        $this->assessmentParameterService = $assessmentParameterService;
        $this->parameterTransformer = $parameterTransformer;
        $this->attachmentService = $attachmentService;
    }
    
    /**
     * Function to get all assessments in which assessor is assigned
     * @param Request $request
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function getAssessments(Request $request)
    {
        $filters = $request->only('start', 'type_of_enterprise', 'assessment_status',
        'certified', 'feedback_received', 'assessor_name',
       'application_start_date', 'application_end_date','assessment_start_date', 'assessment_end_date'
       );
       $filters['assessor_id'] = $request->user()->id;
       $filters['lead_assess_portal'] = true;
       
        if(!empty(request()->get('is_excel'))){
            unset($filters['start']);
            $assessments = $this->assessmentService->lists($filters);
            $excelType = !empty(request()->get('excel_type')) ? request()->get('excel_type') : null;
            return Excel::download(new ListExport($assessments, $excelType), 'assessorAssessment.xlsx');
        }else{
        $assessments = $this->assessmentService->lists($filters);
        }
        $data = [
            'total'=>0,
            'assessments'=>[]
        ];
        
        if(!empty($assessments)){
            $data['total'] = $this->assessmentService->lists($filters, true);
            foreach($assessments as $assessment){
                $data['assessments'][] = $this->transformer->transform($assessment); 
            }
        }

        $this->prepareApiResponse($data);
        return $this->sendApiResponse();   
    }

    /**
     * Function to show details of any assigned assessment
     * @param Request $request
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function showAssessment(Request $request, $assessmentID)
    {
        $assessment = $this->assessmentService->firstornew($assessmentID);
        if(!empty($assessment->id) ){
            $this->prepareApiResponse($assessment, $this->transformer->transform($assessment,['details'=>true, 'assessor'=>true, 'site_details'=>true]));
        }else{
            $this->prepareApiResponse($assessment);
        }
        return $this->sendApiResponse();
    }

    /**
     * Function to start assessment
     * @param Request $request
     * @return Array $data
     * @author Daffodil Softwere
     */    
    public function startAssessment($assessmentID)
    {
        $assessment = $this->assessmentService->firstornew($assessmentID);
        if(empty($assessment->id)){
            throw new ModelNotFoundException();
        }
        if(!empty($assessment->id)){
            $startAssessment = $this->assessmentService->startAssessment($assessment);
            if(!empty($startAssessment)){
                $this->prepareApiResponse(['success'=>true, 'message'=>__('api.assessment_start_success')]);    
            }else{
                $this->prepareApiResponse(['error'=>true, 'message'=>__('api.assessment_start_error')]);    
            }
        }
        return $this->sendApiResponse();
    }

    /**
     * Function to open assessment by assessor
     * @param Request $request
     * @return Array $data
     * @author Daffodil Softwere
     */    
    public function openAssessment($assessmentID)
    {
        $assessment = $this->assessmentService->firstornew($assessmentID);
        if(empty($assessment->id)){
            throw new ModelNotFoundException();
        }
        if(!empty($assessment->id)){
            $startAssessment = $this->assessmentService->openAssessment($assessment, request()->user());
            if(!empty($startAssessment)){
                $this->prepareApiResponse(['success'=>true, 'message'=>__('api.assessment_open_success')]);    
            }else{
                $this->prepareApiResponse(['error'=>true, 'message'=>__('api.assessment_open_error')]);    
            }
        }
        return $this->sendApiResponse();
    }

    /**
     * Function to submit assessment to QCI Admin
     * @param Request $request
     * @return Array $data
     * @author Daffodil Softwere
     */    
    public function submitAssessment($assessmentID)
    {
        $assessment = $this->assessmentService->firstornew($assessmentID);
        if(!empty($assessment->id)){
            $submitAssessment = $this->assessmentService->submitAssessment($assessment);
            if(!empty($submitAssessment)){
                $this->prepareApiResponse(['success'=>true, 'message'=>__('api.assessment_submit_success')]);    
            }else{
                $this->prepareApiResponse(['error'=>true, 'message'=>__('api.assessment_submit_error')]);    
            }
        }
        return $this->sendApiResponse();
    }

    /**
     * Function to get assessment Parameters
     * @param Request $request
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function getAssessmentParameters(Request $request, $assessmentID)
    {
        $assessment = $this->assessmentService->firstornew($assessmentID);
        if(!empty($assessment)){
            $filters = ['assessment_id'=>$assessment->id];
            if(!empty($assessment->lead_assessor_id) && $assessment->lead_assessor_id != request()->user()->id){
                $filters['assessor_id'] = request()->user()->id;
            }
            $parameters = $this->assessmentParameterService->lists($filters);
        }
        
        $data = [
            'total'=>0,
            'parameters'=>[]
        ];
        
        if(!empty($parameters)){
            $data['total'] = $this->assessmentParameterService->lists($filters, true);
            $data['parameters'] = $this->parameterTransformer->transform($parameters); 
        }

        $this->prepareApiResponse($data);
        return $this->sendApiResponse();
    }

    /**
     * Function to show details of any assigned parameter
     * @param Request $request
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function showParameter(Request $request, $parameterID)
    {
        $parameter = $this->assessmentParameterService->firstornew($parameterID);
        if(empty($parameter->id)){
            throw new ModelNotFoundException();
        }
        $this->prepareApiResponse($parameter);
        return $this->sendApiResponse();
    }

    /**
     * Function to assign assessors to Assessment's Paramters By Lead Assessor
     * @param Request $request
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function assignParametersAssessor(AssessmentParemeterCreateRequest $request, $parameterID)
    {
        $inputs = $request->only('assessor_id');
        
        if($parameterID == 'bulk'){
            $paramIds = $request->get('parameterIDs');
            $parameter = $this->assessmentParameterService->findById($paramIds);
        }else{
            $parameter = $this->assessmentParameterService->firstornew($parameterID); 
            if(empty($parameter->id)){
                throw new ModelNotFoundException();
            }
        }
        if($parameterID == 'bulk'){
            foreach($parameter as $param){
                $response = $this->assessmentParameterService->assignParametersAssessor($param, $inputs);
            }
        }else{
            $response = $this->assessmentParameterService->assignParametersAssessor($parameter, $inputs);
        }
        if(!empty($response) ){
            $this->prepareApiResponse(['message'=>'assessor_assign_sucsess']);
        }else{
            $this->prepareApiResponse(['message'=>'assessor_assign_error']);
        }
        return $this->sendApiResponse();
    }

    /**
     * Function to submit response on a parameter on Assessment date
     * @param Request $request
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function submitParametersResponse(AssessmentParemeterCreateRequest $request, $parameterID)
    {
        $inputs = $request->only('response', 'section_uuid');
        $parameter = $this->assessmentParameterService->firstornew($parameterID);
        if(empty($parameter->id)){
            throw new ModelNotFoundException();
        }

        $section = $this->assessmentParameterService->getSection($inputs['section_uuid']); 
        if(empty($section->id)){
            throw new ModelNotFoundException();
        }
        $response = $this->assessmentParameterService->submitSectionResponse($section, $inputs);
        if(!empty($response) ){
            $this->prepareApiResponse(['message'=>'assessor_response_sucsess']);
        }else{
            $this->prepareApiResponse(['message'=>'assessor_response_error']);
        }
        return $this->sendApiResponse();
    }

    /**
     * Function to get all comments added by assessor on a parameter
     * @param Request $request
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function getParameterComment(Request $request,  $parameterID)
    {
        $parameter = $this->assessmentParameterService->firstornew($parameterID); 
        if(empty($parameter->id)){
            throw new ModelNotFoundException();
        }
        $comments = $this->assessmentParameterService->getParameterComments($parameter);
        $data = [
            'total'=> 0,
            'data'=> []
        ];
        if(!empty($comments) ){
            $data['total'] = count($comments);
            foreach($comments as $comment){
                array_push($data['data'], $this->parameterTransformer->transformComment($comment));
            }
            $this->prepareApiResponse($data);
        }else{
            $this->prepareApiResponse($data);
        }
        return $this->sendApiResponse();
    }

    /**
     * Function to submit comment on a paramter
     * @param Request $request
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function submitParameterComment(AssessmentParemeterCreateRequest $request,  $parameterID)
    {
        $inputs = $request->only('comment', 'comment_type');
        $inputs['user_id'] = request()->user()->id;
        $parameter = $this->assessmentParameterService->firstornew($parameterID); 
        if(empty($parameter->id)){
            throw new ModelNotFoundException();
        }
        $response = $this->assessmentParameterService->submitParametersComment($parameter, $inputs);
        
        if(!empty($response) ){
            $this->prepareApiResponse(['message'=>'parameter_comment_sucsess']);
        }else{
            $this->prepareApiResponse(['message'=>'parameter_comment_error']);
        }
        return $this->sendApiResponse();
    }

    /**
     * Function to get all the evidences uploaded by assessor on a parameter
     * @param Request $request
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function getParameterEvidence(Request $request,  $parameterID)
    {
        $parameter = $this->assessmentParameterService->firstornew($parameterID); 
        if(empty($parameter->id)){
            throw new ModelNotFoundException();
        }
        $evidenses = $this->assessmentParameterService->getParameterEvidences($parameter);
        $data = [
            'total'=> 0,
            'data'=> []
        ];
        if(!empty($evidenses) ){
            $data['total'] = count($evidenses);
            foreach($evidenses as $evidense){
                array_push($data['data'], $this->parameterTransformer->transformEvidence($evidense));
            }
            $this->prepareApiResponse($data);
        }else{
            $this->prepareApiResponse($data);
        }
        return $this->sendApiResponse();
    }
    
    /**
     * Function to submit evidences on a parameter by assessor
     * @param Request $request
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function submitParameterEvidence(UploadRequest $request,  $parameterID)
    {
        $uploadedFile = $request->file('file');
        $parameter = $this->assessmentParameterService->firstornew($parameterID); 
        if(empty($parameter->id)){
            throw new ModelNotFoundException();
        }
        $response = $this->attachmentService->upload(
            $uploadedFile,
            $this->assessmentParameterService->getAssessmentEvidencePath($parameter),
            false
        );
        if(!empty($response['uploadedFile']) && $response['uploadedFile'] instanceof UploadedFile){
            $response['user_id'] = request()->user()->id;
            $response = array_merge($response, $request->only('latitude', 'longitude', 'file_date_time'));
            $this->assessmentParameterService->submitParametersEvidence($parameter, $response);
            $this->prepareApiResponse(['message'=>'parameter_evidence_sucsess']);
        }else{
            $this->prepareApiResponse(['message'=>'parameter_evidence_error']);
        }
        
        return $this->sendApiResponse();
    }

    /**
     * Function to get all the site evidences uploaded by assessor
     * @param Request $request
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function getSiteEvidence(Request $request,  $assessmentID)
    {
        $assessment = $this->assessmentService->firstornew($assessmentID); 
        if(empty($assessment->id)){
            throw new ModelNotFoundException();
        }
        $evidenses = $this->assessmentService->getSiteEvidences($assessment);
        $data = [
            'total'=> 0,
            'data'=> []
        ];
        if(!empty($evidenses) ){
            $data['total'] = count($evidenses);
            foreach($evidenses as $evidense){
                array_push($data['data'], $this->transformer->transformEvidence($evidense));
            }
            $this->prepareApiResponse($data);
        }else{
            $this->prepareApiResponse($data);
        }
        return $this->sendApiResponse();
    }
    
    /**
     * Function to submit site evidences on a assessment by assessor
     * @param Request $request
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function submitSiteEvidence(UploadRequest $request,  $assessmentId)
    {
        $uploadedFile = $request->file('file');
        $assessment = $this->assessmentService->firstornew($assessmentId); 
        if(empty($assessment->id)){
            throw new ModelNotFoundException();
        }
        $response = $this->attachmentService->upload(
            $uploadedFile,
            $this->assessmentService->getAssessmentEvidencePath($assessmentId),
            false
        );
        if(!empty($response['uploadedFile']) && $response['uploadedFile'] instanceof UploadedFile){
            $response['user_id'] = request()->user()->id;
            $response = array_merge($response, $request->only('latitude', 'longitude', 'file_date_time', 'file_caption', 'evidense_type'));
            $this->assessmentService->submitSiteEvidence($assessment, $response);
            $this->prepareApiResponse(['message'=>'parameter_evidence_sucsess']);
        }else{
            $this->prepareApiResponse(['message'=>'parameter_evidence_error']);
        }
        
        return $this->sendApiResponse();
    }
}
