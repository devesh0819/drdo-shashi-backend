<?php

namespace Modules\Api\Http\Controllers\Admin;

use Exception;
use Modules\Api\Http\Controllers\ApiController;
use Modules\User\Services\UserService;
use Modules\Api\Transformers\UserProfileTransformer;
use Modules\Api\Http\Requests\User\AssessorCreateRequest;
use Modules\Api\Http\Requests\User\AssessorUpdateRequest;
use Illuminate\Http\Request;
use Modules\Core\Enums\Flag;
use Modules\Core\Enums\HttpStatusCode;
use Modules\Core\Exports\ListExport;
Use Maatwebsite\Excel\Facades\Excel;

class AssessorController extends ApiController
{
    private $userService;
    private $transformer;

    public function __construct(
        UserService $userService,
        UserProfileTransformer $transformer
    )
    {
        $this->userService = $userService;
        $this->transformer = $transformer;
    }
    
    /**
     * Function to get list of all assessors
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function index(Request $request)
    {
        $filters = $request->only('start', 'work_domain', 'location');
        $filters['roles'] = [Flag::ASSESSOR];
        if(!empty(request()->get('is_excel'))){
            unset($filters['start']);
            $assessors = $this->userService->lists($filters);
            $excelType = !empty(request()->get('excel_type')) ? request()->get('excel_type') : null;
            return Excel::download(new ListExport($assessors, $excelType), 'assessors.xlsx');
        }else{
        $assessors = $this->userService->lists($filters);
        }
        $data = [
            'total'=>0,
            'assessors'=>[]
        ];
        
        if(!empty($assessors)){
            $data['total'] = $this->userService->lists($filters, true);
            foreach($assessors as $assessor){
                $data['assessors'][] = $this->transformer->transform($assessor->profile, ['assessorMeta'=>true]); 
            
            }
        }

        $this->prepareApiResponse($data);
        return $this->sendApiResponse();
    }

    /**
     * Function to store an assessor
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function store(AssessorCreateRequest $request)
    {
        $inputs = $request->only(
            'email', 'phone_number', 'first_name', 'last_name',
            'roles', 'agency_id', 'expertise','address',
            'assessor_state', 'assessor_district', 'assessor_certificate', 'assessor_status',
            'work_domain', 'years_of_experience', 'assessor_status_comment',
            'alternate_email', 'alternate_phone_number', 'agency_id'
        );
        $inputs['resume'] = $request->file('resume');
        if(!empty($inputs)){
           $data = [];
           $user = $this->userService->firstornew();
           $response = $this->userService->save($user, $inputs);
           
           if(!empty($response['profile']->profile) ){
                $data = $this->transformer->transform($response['profile']->profile);
                $this->prepareApiResponse($data);
           }else{
                $this->prepareApiResponse($response);
           }
        }
       return $this->sendApiResponse();
    }

    /**
     * Function to update an assessor
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function update(AssessorUpdateRequest $request, $assessorId)
    {
        $inputs = $request->only(
            'email', 'phone_number', 'first_name', 'last_name',
            'roles', 'agency_id', 'expertise','address',
            'assessor_state', 'assessor_district', 'assessor_certificate', 'assessor_status',
            'work_domain', 'years_of_experience', 'assessor_status_comment',
            'alternate_email', 'alternate_phone_number', 'agency_id'
        );
        $inputs['resume'] = $request->file('resume');
        if(!empty($inputs)){
           $data = [];
           $user = $this->userService->firstornew($assessorId);
           if(empty($user)){
                throw new Exception(HttpStatusCode::HTTP_NOT_FOUND);
           }

           $response = $this->userService->save($user, $inputs);
           if(!empty($response['profile']->profile) ){
                $data = $this->transformer->transform($response['profile']->profile);
                $this->prepareApiResponse($data);
           }else{
                $this->prepareApiResponse($response);
           }
        }
       return $this->sendApiResponse();
    }

    /**
     * Function to show an assessor
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function show($assessorId)
    {
        $user = $this->userService->firstornew($assessorId);
        if(!empty($user)){
           if(!empty($user->profile) ){
                $data = $this->transformer->transform($user->profile, ['assessorMeta'=>true]);
                $this->prepareApiResponse($data);
           }else{
                $this->prepareApiResponse($user);
           }
        }
       return $this->sendApiResponse();
    }

    /**
     * Function to delete a assessor
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function destroy($assessorId)
    {
        $assessor = $this->userService->firstornew($assessorId);
        
        if(!empty($assessor)){
            $response = $this->userService->delete($assessor);
            $this->prepareApiResponse(['success'=>true]);
        } else {
            $this->prepareApiResponse(['error'=>HttpStatusCode::HTTP_NOT_FOUND]);
            $this->httpCode = HttpStatusCode::HTTP_NOT_FOUND;
        }

        return $this->sendApiResponse();
    }
}