<?php

namespace Modules\Api\Http\Controllers\Admin;

use Modules\Api\Http\Controllers\ApiController;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Modules\Api\Http\Requests\Meeting\MeetingCreateRequest;
use Modules\Committee\Services\MeetingService;
use Modules\Api\Transformers\MeetingTransformer;
use Modules\Committee\Services\CommitteeService;
use Modules\Api\Http\Requests\Meeting\MeetingNoteCreateRequest;

class MeetingController extends ApiController
{
    private $meetingService;
    private $committeeService;
    private $transformer;

    public function __construct(
        MeetingService $meetingService,
        MeetingTransformer $transformer,
        CommitteeService $committeeService
    )
    {

        $this->meetingService = $meetingService;
        $this->transformer = $transformer;
        $this->committeeService = $committeeService;

    }
    /**
     * Function to get list of all Meetings
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function index(Request $request)
    {
        $meeting = $this->meetingService->lists();
        $this->prepareApiResponse($meeting);
        return $this->sendApiResponse();
    }

    /**
     * Function to create a new meeting
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function store(MeetingCreateRequest $request, $committeeId)
    {
        $inputs = $request->only('meeting_title', 'description',
         'meeting_date_time','meeting_type','meeting_link','meeting_address', 'meeting_assessments');
        $committee = $this->committeeService->firstornew($committeeId);
        if(empty($committee)){
            abort(404);
        }
        $inputs['committee_id'] = $committee->id;
        if(!empty($inputs)){
            $meeting = $this->meetingService->firstornew();
            $response = $this->meetingService->save($meeting, $inputs);
            $this->prepareApiResponse($response);
         }
        return $this->sendApiResponse();
    }

    /**
     * Function to show a meeting detail
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function show($id)
    {
        $response = $this->meetingService->firstornew($id);
        if(!empty($response) ){
            $data = $this->transformer->transform($response);
            $this->prepareApiResponse($data);
        }else{
            $this->prepareApiResponse($response);
        }
        return $this->sendApiResponse();   
    }

    /**
     * Function to update a meeting
     * @param Request $request
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function update(MeetingCreateRequest $request, $meetingId)
    {
        $inputs = $request->only('meeting_title', 'description',
         'meeting_date_time','meeting_link', 'meeting_members' ,'meeting_type', 'meeting_address', 'meeting_assessments');
        $meeting = $this->meetingService->firstornew($meetingId);
        if(empty($meeting)){
            abort(404);
        }
        if(!empty($inputs)){
            $response = $this->meetingService->save($meeting, $inputs);
            $this->prepareApiResponse($response);
         }
        return $this->sendApiResponse();
    }

    /**
     * Store a note to the meeting
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function storeNote(MeetingNoteCreateRequest $request, $meetingId)
    {
        $inputs = $request->only('meeting_notes');
        $inputs['user_id'] = request()->user()->id;
        $meeting = $this->meetingService->firstornew($meetingId);
        if(empty($meeting)){
            abort(404);
        }
        if(!empty($inputs)){
            $response = $this->meetingService->saveNote($meeting, $inputs);
            $this->prepareApiResponse($response);
         }
        return $this->sendApiResponse();
    }
}
