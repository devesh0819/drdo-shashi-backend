<?php

namespace Modules\Api\Http\Controllers\Admin;

use Modules\Api\Http\Controllers\ApiController;
use Modules\User\Services\UserService;
use Modules\Api\Transformers\UserProfileTransformer;
use Modules\Api\Http\Requests\User\AgencyCreateRequest;
use Modules\Api\Http\Requests\User\AgencyUpdateRequest;
use Illuminate\Http\Request;
use Modules\Core\Enums\Flag;
use Modules\Core\Enums\HttpStatusCode;
use Exception;

class AgencyController extends ApiController
{
    private $userService;
    private $transformer;

    public function __construct(
        UserService $userService,
        UserProfileTransformer $transformer
    )
    {
        $this->userService = $userService;
        $this->transformer = $transformer;
    }
    
    /**
     * Function to get list of all agency
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function index(Request $request)
    {   
        $filters = [
            'roles'=>[Flag::AGENCY],
            'start'=>$request->get('start')
        ];
        $agencies = $this->userService->lists($filters);
        $data = [
            'total'=>0,
            'agencies'=>[]

        ];
        
        if(!empty($agencies)){
            $data['total'] = $this->userService->lists($filters, true);
            foreach($agencies as $agency){
                $data['agencies'][] = $this->transformer->transform($agency->profile, ['agencyMeta'=>true]); 
            }
        }

        $this->prepareApiResponse($data);
        return $this->sendApiResponse();
    }

    /**
     * Function to store an agency
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function store(AgencyCreateRequest $request)
    {
        $inputs = $request->only('email', 'phone_number','first_name', 'last_name', 'roles', 'title', 'address');
        
        if(!empty($inputs)){
           $data = [];
           $user = $this->userService->firstornew();
           $response = $this->userService->save($user, $inputs);
           
           if(!empty($response['profile']->profile) ){
                $data = $this->transformer->transform($response['profile']->profile);
                $this->prepareApiResponse($data);
           }else{
                $this->prepareApiResponse($response);
           }
        }
       return $this->sendApiResponse();
    }

    /**
     * Function to show a agency
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function show($agencyId)
    {
        $user = $this->userService->firstornew($agencyId);

        if(!empty($user)){
           if(!empty($user->profile) ){
                $data = $this->transformer->transform($user->profile);
                $this->prepareApiResponse($data);
           }else{
                $this->prepareApiResponse($user);
           }
        }
       return $this->sendApiResponse();
    }

    /**
     * Function to update an agency
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function update(AgencyUpdateRequest $request, $agencyId)
    {
        $inputs = $request->only('phone_number', 'first_name', 'last_name', 'title', 'address');
        if(!empty($inputs)){
            $data = [];
            $user = $this->userService->firstornew($agencyId);
           
            if(empty($user)){
                throw new Exception(HttpStatusCode::HTTP_NOT_FOUND);
            }
            $response = $this->userService->save($user, $inputs);
            
            if(!empty($response['profile']->profile) ){
                $data = $this->transformer->transform($response['profile']->profile);
                $this->prepareApiResponse($data);
            }else{
                $this->prepareApiResponse($response);
            }
        }
       return $this->sendApiResponse();
    }

    /**
     * Function to delete an agency
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function destroy($agencyId)
    {
        $agency = $this->userService->firstornew($agencyId);
        
        if(!empty($agency)){
            $response = $this->userService->delete($agency);
            $this->prepareApiResponse(['success'=>true]);
        } else {
            $this->prepareApiResponse(['error'=>HttpStatusCode::HTTP_NOT_FOUND]);
            $this->httpCode = HttpStatusCode::HTTP_NOT_FOUND;
        }

        return $this->sendApiResponse();
    }

    /**
     * Function to get Payment configurations
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function getFeedbackConfig()
    {
        $settings = $this->applicationService->getFeedbackConfig();
        if(!empty($settings)){
            $this->prepareApiResponse($settings);
        }else{
            $this->prepareApiResponse(['error'=>HttpStatusCode::HTTP_BAD_REQUEST]);
            $this->httpCode = HttpStatusCode::HTTP_INTERNAL_SERVER_ERROR;
        }
        return $this->sendApiResponse();    
    }
}