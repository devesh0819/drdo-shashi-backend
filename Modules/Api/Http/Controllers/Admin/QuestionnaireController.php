<?php

namespace Modules\Api\Http\Controllers\Admin;

use Modules\Api\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use Modules\Api\Http\Requests\Questionnaire\QuestionnaireCreateRequest;
use Modules\Api\Http\Requests\Questionnaire\QuestionCreateRequest;
use Modules\Questionnaire\Entities\Questionnaire;
use Modules\Api\Transformers\QuestionnaireTransformer;
use Modules\Questionnaire\Services\QuestionnaireService;
use Illuminate\Support\Facades\Artisan;
use Modules\Core\Enums\HttpStatusCode;
use Modules\Questionnaire\Console\SyncQuestionnaire;
use Modules\Attachment\Services\AttachmentService;
use Modules\Core\Enums\QuestionnaireStage;

class QuestionnaireController extends ApiController
{
    private $questionnaireService;
    private $transformer;
    private $attachmentService;

    public function __construct(
        QuestionnaireService $questionnaireService,
        QuestionnaireTransformer $transformer,
        AttachmentService $attachmentService
    )
    {
        $this->questionnaireService = $questionnaireService;
        $this->transformer = $transformer;
        $this->attachmentService = $attachmentService;
    }

    /**
     * Function to get list of all questionnaires
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function index(Request $request)
    {  
        $filters = [
            'start'=>$request->get('start')
        ];
        $questionnaires = $this->questionnaireService->lists($filters);
        $data = [
            'total'=>0
        ];
        if(!empty($questionnaires)){
            $data['total'] = $this->questionnaireService->lists($filters, true);
            foreach($questionnaires as $questionnaire){
             $data['questionnaires'][] = $this->transformer->transform($questionnaire);
            }
        }
        $this->prepareApiResponse($data);
        return $this->sendApiResponse(); 

    }

    /**
     * Function to create/import questionnaire
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function import(QuestionCreateRequest $request)
    {
        $inputs = $request->only('file', 'type', 'title');
        if(!empty($inputs)){
            if(!empty($inputs['file'])){
                $questionnaireFile = $this->attachmentService->upload($inputs['file'], '/questionnaires/');
            }
            $filePath = storage_path('app/public/'.$questionnaireFile->path);
            $importQuestionnaire = Artisan::queue(SyncQuestionnaire::class, ['type' => $inputs['type'], 'title'=>$inputs['title'], '--filePath' => $filePath]);
            if(!empty($importQuestionnaire)){
                $this->prepareApiResponse([ 'success'=>true, 'message'=>__('api.questionnaire_import_success')]);
            }else{
                $this->httpCode = HttpStatusCode::HTTP_BAD_REQUEST;
                $this->prepareApiResponse([ 'error'=>true, 'message'=>__('api.questionnaire_import_error')]);
            }
        }
        return $this->sendApiResponse();
    }

    /**
     * Function to get detail of a questionnaire
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function show(Request $request, $questionnaireID)
    {  
        if(!empty($questionnaireID)){
            $questionnaire = $this->questionnaireService->firstornew($questionnaireID);
        }
        $this->prepareApiResponse($questionnaire);
        return $this->sendApiResponse(); 
    }

    /**
     * Function to Show Audit detail of a questionnaire
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function showAudit(Request $request, $questionnaireID)
    {  
        if(!empty($questionnaireID)){
            $auditLogs = $this->questionnaireService->getAuditLogs($questionnaireID);
        }
        $this->prepareApiResponse($auditLogs);
        return $this->sendApiResponse(); 
    }

    /**
     * Function to show parameter of a questionnaire
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function showParameter(Request $request, $parameterID)
    {  
        if(!empty($parameterID)){
            $parameter = $this->questionnaireService->getParameter($parameterID);
        }
        $this->prepareApiResponse($parameter);
        return $this->sendApiResponse(); 
    }

    /**
     * Function to send questionnaire for approval
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function sendApproval(Request $request, $questionnaireID)
    {
        if(!empty($questionnaireID)){
            $questionnaire = $this->questionnaireService->firstornew($questionnaireID);
            if(!empty($questionnaire) && $questionnaire->state == QuestionnaireStage::DRAFT){
                $questionnaire = $this->questionnaireService->submitQuestionnaire($questionnaire);
                $this->prepareApiResponse(['success'=>true, 'message'=>__('questionnaire_submit_success')]);
            }else{
                $this->httpCode = HttpStatusCode::HTTP_BAD_REQUEST;
                $this->prepareApiResponse(['error'=>true, 'message'=>__('questionnaire_submit_error')]);
            }
        }
        
        return $this->sendApiResponse();
    }
}
