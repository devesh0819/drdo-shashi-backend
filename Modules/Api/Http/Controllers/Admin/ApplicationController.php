<?php

namespace Modules\Api\Http\Controllers\Admin;

use Modules\Api\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use Modules\Application\Services\ApplicationService;
use Modules\Api\Transformers\ApplicationTransformer;
use Modules\Core\Enums\HttpStatusCode;
use Modules\Core\Enums\UserRole;
use Modules\Payment\Services\PaymentService;
use Modules\Core\Exports\ListExport;
Use Maatwebsite\Excel\Facades\Excel;

class ApplicationController extends ApiController
{
    private $applicationService;
    private $paymentService;
    private $transformer;

    public function __construct(
        ApplicationService $applicationService,
        ApplicationTransformer $transformer,
        PaymentService $paymentService
    )
    {
        $this->applicationService = $applicationService;
        $this->transformer = $transformer;
        $this->paymentService = $paymentService;
    }

    /**
     * Function to get list of all applications
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function index(Request $request)
    {   
        $filters = $request->only('start', 'application_no','application_status',
            'enterprise_name', 'payment_status', 'assessment_status','type_of_enterprise',
            'rating', 'application_start_date', 'application_end_date',
            'assessment_start_date', 'assessment_end_date', 'feedback_received', 'subsidy'
        );
        $filters['role'] = UserRole::ADMIN;
        if(!empty(request()->get('is_excel'))){
            unset($filters['start']);
            $applications = $this->applicationService->lists($filters);
            $excelType = !empty(request()->get('excel_type')) ? request()->get('excel_type') : null;
            return Excel::download(new ListExport($applications, $excelType), 'admin_applications.xlsx');
        }else{
            $applications = $this->applicationService->lists($filters);
        }
        $data = [
            'total'=>0,
            'applications'=>[]
        ];
        
        if(!empty($applications)){
            $data['total'] = $this->applicationService->lists($filters, true);
            foreach($applications as $application){
                $data['applications'][] = $this->transformer->transform($application, ['assessment'=>true]); 
            }
        }

        $this->prepareApiResponse($data);
        return $this->sendApiResponse();
    }

    /**
     * Function to show details of an application
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function show($applicationId)
    {
        $application = $this->applicationService->firstornew($applicationId);
        
        if(!empty($application)){
            $data = $this->transformer->transform($application, 
            [
                'details'=>true,
                 'paymentOrder'=>true,
                 'assessment'=>true
            ]);
            $this->prepareApiResponse($data);
        } else {
            $this->prepareApiResponse(['error'=>HttpStatusCode::HTTP_NOT_FOUND]);
            $this->httpCode = HttpStatusCode::HTTP_NOT_FOUND;
        }

        return $this->sendApiResponse();
    }

    /**
     * Function to approve payment of application
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function approvePayment(Request $request, $applicationId)
    {
        $inputs = $request->only('local_assessor');

        $application = $this->applicationService->firstornew($applicationId);
        
        if(!empty($application)){
            $approvePayment = $this->paymentService->approvePayment($application, $inputs);
            $data = $this->transformer->transformPayment($application,['paymentOrder'=>true]);
            $this->prepareApiResponse($data);
        } else {
            $this->prepareApiResponse(['error'=>HttpStatusCode::HTTP_NOT_FOUND]);
            $this->httpCode = HttpStatusCode::HTTP_NOT_FOUND;
        }

        return $this->sendApiResponse();
    }

    /**
     * Function to show payment Detail of a given application
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function paymentDetail($applicationId)
    {
        $application = $this->applicationService->firstornew($applicationId);
        
        if(!empty($application)){
            $data = $this->transformer->transformPayment($application,['paymentOrder'=>true]);
            $this->prepareApiResponse($data);
        } else {
            $this->prepareApiResponse(['error'=>HttpStatusCode::HTTP_NOT_FOUND]);
            $this->httpCode = HttpStatusCode::HTTP_NOT_FOUND;
        }

        return $this->sendApiResponse();
    }
}
