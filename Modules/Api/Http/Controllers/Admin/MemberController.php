<?php

namespace Modules\Api\Http\Controllers\Admin;

use Modules\Api\Http\Controllers\ApiController;
use Modules\User\Services\UserService;
use Modules\User\Entities\User;
use Modules\Api\Transformers\UserProfileTransformer;
use Modules\Api\Http\Requests\User\MemberCreateRequest;
use Modules\Api\Http\Requests\User\MemberUpdateRequest;
use Illuminate\Http\Request;
use Modules\Core\Enums\Flag;
use Modules\Core\Enums\HttpStatusCode;

class MemberController extends ApiController
{
    private $userService;
    private $transformer;

    public function __construct(
        UserService $userService,
        UserProfileTransformer $transformer
    )
    {
        $this->userService = $userService;
        $this->transformer = $transformer;
    }
    
    /**
     * Function to get list of all members
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function index(Request $request)
    {   
        $members = $this->userService->lists(['roles'=>[Flag::MEMBER]]);
        $data = [
            'total'=>0,
            'members'=>[]
        ];
        
        if(!empty($members)){
            $data['total'] = $this->userService->lists(['roles'=>[Flag::MEMBER]], true);
            foreach($members as $member){
                $data['members'][] = $this->transformer->transform($member->profile); 
            }
        }

        $this->prepareApiResponse($data);
        return $this->sendApiResponse();
    }

    /**
     * Function to store a member
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function store(MemberCreateRequest $request)
    {
        $inputs = $request->only(
            'email', 'phone_number', 'member_role',
            'first_name', 'last_name', 'roles', 'alternate_email', 'alternate_phone_number', "organisation", "designation");
        
        if(!empty($inputs)){
           $data = [];
           $user = $this->userService->firstornew();
           $response = $this->userService->save($user, $inputs);
           
           if(!empty($response['profile']->profile) ){
                $data = $this->transformer->transform($response['profile']->profile);
                $this->prepareApiResponse($data);
           }else{
                $this->prepareApiResponse($response);
           }
        }
       return $this->sendApiResponse();
    }

    /**
     * Function to update a member
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function update(MemberUpdateRequest $request, $memberId)
    {
        $inputs = $request->only('phone_number', 'first_name','member_role', 'last_name', 'alternate_email', 'alternate_phone_number', "organisation", "designation");
        
        if(!empty($inputs)){
           $data = [];
           $user = $this->userService->firstornew($memberId);
           $response = $this->userService->save($user, $inputs);
           
           if(!empty($response['profile']->profile) ){
                $data = $this->transformer->transform($response['profile']->profile);
                $this->prepareApiResponse($data);
           }else{
                $this->prepareApiResponse($response);
           }
        }
       return $this->sendApiResponse();
    }

    /**
     * Function to show a member
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function show($memberId)
    {
        $user = $this->userService->firstornew($memberId);

        if(!empty($user)){
           if(!empty($user->profile) ){
                $data = $this->transformer->transform($user->profile);
                $this->prepareApiResponse($data);
           }else{
                $this->prepareApiResponse($user);
           }
        }
       return $this->sendApiResponse();
    }

    /**
     * Function to delete a member
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function destroy($memberId)
    {
        $member = $this->userService->firstornew($memberId);
        
        if(!empty($member)){
            $response = $this->userService->delete($member);
            $this->prepareApiResponse(['success'=>true]);
        } else {
            $this->prepareApiResponse(['error'=>HttpStatusCode::HTTP_NOT_FOUND]);
            $this->httpCode = HttpStatusCode::HTTP_NOT_FOUND;
        }

        return $this->sendApiResponse();
    }
}
