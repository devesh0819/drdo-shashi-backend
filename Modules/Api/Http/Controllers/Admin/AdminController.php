<?php

namespace Modules\Api\Http\Controllers\Admin;

use Modules\Api\Http\Controllers\ApiController;
use Modules\User\Services\UserService;
use Modules\Api\Transformers\UserProfileTransformer;
use Modules\Api\Http\Requests\User\AdminCreateRequest;
use Modules\Api\Http\Requests\User\AdminUpdateRequest;
use Illuminate\Http\Request;
use Modules\Core\Enums\Flag;
use Modules\Core\Enums\HttpStatusCode;
use Exception;
use Modules\Api\Http\Requests\Payment\PaymentSettingCreateRequest;
use Modules\Api\Http\Requests\Application\FeedbackSettingCreateRequest;
use Modules\Api\Http\Requests\Application\FeedbackSettingUpdateRequest;
use Modules\Payment\Services\PaymentService;
use Modules\Application\Services\ApplicationService;
use Modules\Api\Transformers\PaymentPlanTransformer;
use Modules\Core\Entities\FeedbackQuestionSetting;

class AdminController extends ApiController
{
    private $userService;
    private $transformer;
    private $paymentService;
    private $playmentPlanTransformer;
    private $applicationService;

    public function __construct(
        UserService $userService,
        PaymentService $paymentService,
        UserProfileTransformer $transformer,
        PaymentPlanTransformer $playmentPlanTransformer,
        ApplicationService $applicationService
    )
    {
        $this->paymentService = $paymentService;
        $this->applicationService= $applicationService;
        $this->userService = $userService;
        $this->transformer = $transformer;
        $this->playmentPlanTransformer = $playmentPlanTransformer;
    }
    
    /**
     * Function to get list of all Admins
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function index(Request $request)
    {   
        $admins = $this->userService->lists(['roles'=>[Flag::ADMIN]]);
        $data = [
            'total'=>0,
            'admin'=>[]

        ];
        
        if(!empty($admins)){
            $data['total'] = $this->userService->lists(['roles'=>[Flag::ADMIN]], true);
            foreach($admins as $admin){
                $data['admin'][] = $this->transformer->transform($admin->profile); 
            }
        }

        $this->prepareApiResponse($data);
        return $this->sendApiResponse();
    }

    /**
     * Function to store an Admin
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function store(AdminCreateRequest $request)
    {
        
        $inputs = $request->only('email', 'phone_number','first_name', 'last_name', 'roles', 'title', 'address');
        
        if(!empty($inputs)){
           $data = [];
           $admin = $this->userService->firstornew();
           $response = $this->userService->save($admin, $inputs);
           
           if(!empty($response['profile']->profile) ){
                $data = $this->transformer->transform($response['profile']->profile);
                $this->prepareApiResponse($data);
           }else{
                $this->prepareApiResponse($response);
           }
        }
       return $this->sendApiResponse();
    }

    

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('api::edit');
    }

    /**
     * Function to update an admin
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function update(AdminUpdateRequest $request, $adminId)
    {
        $inputs = $request->only('phone_number', 'first_name', 'last_name', 'title', 'address');
        
        if(!empty($inputs)){
            $data = [];
            $admin = $this->userService->firstornew($adminId);
           
            if(!empty($admin)){
                throw new Exception(HttpStatusCode::HTTP_NOT_FOUND);
            }
            $response = $this->userService->save($admin, $inputs);
            die($response);
            if(!empty($response['profile']->profile) ){
                $data = $this->transformer->transform($response['profile']->profile);
                $this->prepareApiResponse($data);
            }else{
                $this->prepareApiResponse($response);
            }
        }
       return $this->sendApiResponse();
    }

    /**
     * Function to show an admin
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function show($adminId)
    {
        $user = $this->userService->firstornew($adminId);

        if(!empty($user)){
           if(!empty($user->profile) ){
                $data = $this->transformer->transform($user->profile);
                $this->prepareApiResponse($data);
           }else{
                $this->prepareApiResponse($user);
           }
        }
       return $this->sendApiResponse();
    }

    /**
     * Function to delete admin
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function destroy($adminId)
    {
        $admin = $this->userService->firstornew($adminId);
        
        if(!empty($admin)){
            $response = $this->userService->delete($admin);
            $this->prepareApiResponse(['success'=>true]);
        } else {
            $this->prepareApiResponse(['error'=>HttpStatusCode::HTTP_NOT_FOUND]);
            $this->httpCode = HttpStatusCode::HTTP_NOT_FOUND;
        }

        return $this->sendApiResponse();
    }

    /**
     * Function to store Payment configurations
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function savePaymentConfig(PaymentSettingCreateRequest $request)
    {
        $inputs = $request->only(
            'large_certification_fee_text', 'large_subsidy_text', 'large_assessor_fee_text', 'large_gst_text', 'large_tds_text', 'large_miscell_fee_text', 'large_miscell_discount_text',
            'large_certification_fee_value', 'large_subsidy_value', 'large_assessor_fee_value', 'large_gst_value', 'large_tds_value', 'large_miscell_fee_value', 'large_miscell_discount_value',
            'medium_certification_fee_text', 'medium_subsidy_text', 'medium_assessor_fee_text', 'medium_gst_text', 'medium_tds_text', 'medium_miscell_fee_text', 'medium_miscell_discount_text',
            'medium_certification_fee_value', 'medium_subsidy_value', 'medium_assessor_fee_value', 'medium_gst_value', 'medium_tds_value', 'medium_miscell_fee_value', 'medium_miscell_discount_value',
            'small_certification_fee_text', 'small_subsidy_text', 'small_assessor_fee_text', 'small_gst_text', 'small_tds_text', 'small_miscell_fee_text', 'small_miscell_discount_text',
            'small_certification_fee_value', 'small_subsidy_value', 'small_assessor_fee_value', 'small_gst_value', 'small_tds_value', 'small_miscell_fee_value', 'small_miscell_discount_value',
            'micro_certification_fee_text', 'micro_subsidy_text', 'micro_assessor_fee_text', 'micro_gst_text', 'micro_tds_text', 'micro_miscell_fee_text', 'micro_miscell_discount_text',
            'micro_certification_fee_value', 'micro_subsidy_value', 'micro_assessor_fee_value', 'micro_gst_value', 'micro_tds_value', 'micro_miscell_fee_value', 'micro_miscell_discount_value',
        );
        
        if(!empty($inputs)){
            $settings = $this->paymentService->saveConfig($inputs);
            if(!empty($settings)){
                $transformedData = [];
                foreach($settings as $setting){
                    array_push($transformedData, $this->playmentPlanTransformer->transform($setting));
                }
            }
            $this->prepareApiResponse($transformedData);
        }else {
            $this->prepareApiResponse(['error'=>HttpStatusCode::HTTP_BAD_REQUEST]);
            $this->httpCode = HttpStatusCode::HTTP_INTERNAL_SERVER_ERROR;
        }

        return $this->sendApiResponse();
    }

    /**
     * Function to get Payment configurations
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function getPaymentConfig()
    {
        $settings = $this->paymentService->getConfig();
        if(!empty($settings)){
            $transformedData = [];
            foreach($settings as $setting){
                array_push($transformedData, $this->playmentPlanTransformer->transform($setting));
            }
            $this->prepareApiResponse($transformedData);
        }else{
            $this->prepareApiResponse(['error'=>HttpStatusCode::HTTP_BAD_REQUEST]);
            $this->httpCode = HttpStatusCode::HTTP_INTERNAL_SERVER_ERROR;
        }
        return $this->sendApiResponse();    
    }

    /**
     * Function to store Feedback configuration
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function saveFeedbackConfig(FeedbackSettingCreateRequest $request)
    {
        
        $inputs = $request->only('title');
        if(!empty($inputs)){
            $feedback = $this->applicationService->firstornewFeedback();
            $settings = $this->applicationService->saveFeedbackConfig($feedback, $inputs);
            $this->prepareApiResponse($settings);
        }

        return $this->sendApiResponse();
    }

     /**
     * Function to update feedback Question
     * @param Request $request
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function updateFeedback(FeedbackSettingUpdateRequest $request, $Id)
    {
        $inputs = $request->only('title');
        if(!empty($inputs)){
            $feedback = $this->applicationService->firstornewFeedback($Id);
            $response = $this->applicationService->saveFeedbackConfig($feedback, $inputs);
            $this->prepareApiResponse($response);
         }
        return $this->sendApiResponse();   
    }


    /**
     * Function to delete feedback Question
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function deleteFeedback($Id)
    {
        $feedbackConfig = $this->applicationService->firstornewFeedback($Id);
        
        if(!empty($feedbackConfig)){
            $response = $this->applicationService->deleteFeedback($feedbackConfig);
            $this->prepareApiResponse(['success'=>true]);
        } else {
            $this->prepareApiResponse(['error'=>HttpStatusCode::HTTP_NOT_FOUND]);
            $this->httpCode = HttpStatusCode::HTTP_NOT_FOUND;
        }

        return $this->sendApiResponse();
    }

    /**
     * Function to get Payment configurations
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function getFeedbackConfig()
    {
        $settings = $this->applicationService->getFeedbackConfig();
        if(!empty($settings)){
            $this->prepareApiResponse($settings);
        }else{
            $this->prepareApiResponse(['error'=>HttpStatusCode::HTTP_BAD_REQUEST]);
            $this->httpCode = HttpStatusCode::HTTP_INTERNAL_SERVER_ERROR;
        }
        return $this->sendApiResponse();    
    }
}
