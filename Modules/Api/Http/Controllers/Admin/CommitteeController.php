<?php

namespace Modules\Api\Http\Controllers\Admin;

use Modules\Api\Http\Requests\Committee\CommitteeCreateRequest;
use Modules\Api\Http\Requests\Committee\CommitteeUpdateRequest;
use Modules\Api\Http\Controllers\ApiController;
use Modules\Api\Transformers\CommitteeTransformer;
use Illuminate\Http\Request;
use Modules\Core\Enums\HttpStatusCode;
use Modules\Committee\Services\CommitteeService;

class CommitteeController extends ApiController
{
    private $committeeService;
    private $transformer;

    public function __construct(
        CommitteeService $committeeService,
        CommitteeTransformer $transformer
    )
    {
        $this->committeeService = $committeeService;
        $this->transformer = $transformer;
    }

    /**
     * Function to list all the committies
     * @param Request $request
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function index(Request $request)
    {
        $filters = [
            'start'=>$request->get('start')
        ];
        $committees = $this->committeeService->lists($filters);
        $data = [
            'total'=>0,
            'committees'=>[]
        ];
        if(!empty($committees)){
            $data['total'] = $this->committeeService->lists($filters, true);
            foreach($committees as $committee){
                $data['committees'][] = $this->transformer->transform($committee);
            }
        }
        $this->prepareApiResponse($data);
        return $this->sendApiResponse();

    }

    /**
     * Function to create a new committee
     * @param Request $request
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function store(CommitteeCreateRequest $request)
    {
        $inputs = $request->only('title', 'members');

        if(!empty($inputs)){
            $committee = $this->committeeService->firstornew();
            $response = $this->committeeService->save($committee, $inputs);
            $this->prepareApiResponse($response);
        }
        return $this->sendApiResponse();
    }

    /**
     * Function to update a committee
     * @param Request $request
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function update(CommitteeUpdateRequest $request, $committeeId)
    {
        $inputs = $request->only('title', 'members' ,'addNew');
        if(!empty($inputs)){
            $committee = $this->committeeService->firstornew($committeeId);
            $response = $this->committeeService->save($committee, $inputs);
            if(!empty($response) ){
                 $this->prepareApiResponse($response, $this->transformer->transform($response));
            }else{
                 $this->prepareApiResponse($response);
            }
         }
        return $this->sendApiResponse();   
    }

    /**
     * Function to show Committee by ID
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function show(Request $request, $id)
    {
        $response = $this->committeeService->getCommittee($id);
        if(!empty($response['committee']->id) ){
            $data = $this->transformer->transform($response['committee'],['meetings'=>true, 'members'=>true, 'memberData'=>$response['membersData']]);
            $this->prepareApiResponse($data);
        }else{
            $this->prepareApiResponse($response);
        }
        return $this->sendApiResponse();   
    }

    /**
     * Function to delete a committee
     * @param Request $request
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function destroy($committeeId)
    {
        $committee = $this->committeeService->firstornew($committeeId);
        if(!empty($committee)){
            $response = $this->committeeService->delete($committee);
            $this->prepareApiResponse(['success'=>true]);
        } else {
            $this->prepareApiResponse(['error'=>HttpStatusCode::HTTP_NOT_FOUND]);
            $this->httpCode = HttpStatusCode::HTTP_NOT_FOUND;
        }
        return $this->sendApiResponse();
    }

}
