<?php

namespace Modules\Api\Http\Controllers\Admin;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Modules\Api\Http\Controllers\ApiController;
use Modules\Application\Services\ApplicationService;
use Modules\User\Services\UserService;
use Modules\Assessment\Services\AssessmentService;
use Modules\Core\Enums\UserRole;

class DashboardStatsController extends ApiController
{
    private $ApplicationService;
    

    public function __construct(
        ApplicationService $applicationService,
        UserService $userService,
        AssessmentService $assessmentService
        
    )
    {

        $this->ApplicationService = $applicationService;
        $this->UserService = $userService;
        $this->AssessmentService = $assessmentService;

    }
    /**
     * Function to Get All the stats for dashboard
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function getStats(Request $request)
    {
        $filters = [
            'role'=>UserRole::ADMIN,
            'start'=>$request->get('start')
        ];
        $applicationStats = $this->ApplicationService->getStats($filters);
        $this->prepareApiResponse($applicationStats);
        return $this->sendApiResponse();
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('api::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('api::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('api::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
