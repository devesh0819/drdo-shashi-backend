<?php

namespace Modules\Api\Http\Controllers\Admin;

use Modules\Api\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use Modules\Api\Http\Requests\Assessment\AssessmentCreateRequest;
use Modules\Api\Http\Requests\Assessment\AssessmentUpdateRequest;
use Modules\Assessment\Services\AssessmentService;
use Modules\Api\Transformers\AssessmentTransformer;
use Modules\Core\Enums\Flag;
use Modules\Core\Enums\HttpStatusCode;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Modules\Api\Http\Requests\Assessment\AssessmentParemeterCreateRequest;
use Illuminate\Http\UploadedFile;
use Modules\Api\Http\Requests\Attachment\UploadRequest;
use Modules\Api\Transformers\AssessmentParameterTransformer;
use Modules\Assessment\Services\AssessmentParameterService;
use Modules\Attachment\Services\AttachmentService;
use Illuminate\Validation\ValidationException;
use Modules\Core\Exports\ListExport;
Use Maatwebsite\Excel\Facades\Excel;

class AssessmentController extends ApiController
{
    private $assessmentService;
    private $transformer;
    private $assessmentParameterService;
    private $parameterTransformer;
    private $attachmentService;

    public function __construct(
        AssessmentService $assessmentService,
        AssessmentTransformer $transformer,
        AssessmentParameterService $assessmentParameterService,
        AssessmentParameterTransformer $parameterTransformer,
        AttachmentService $attachmentService
    )
    {
        $this->assessmentService = $assessmentService;
        $this->transformer = $transformer;
        $this->assessmentParameterService = $assessmentParameterService;
        $this->parameterTransformer = $parameterTransformer;
        $this->attachmentService = $attachmentService;
    }
    
    /**
     * Function to list all the Assessments
     * @param Request $request
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function index(Request $request)
    {
        $filters = $request->only('start', 'type_of_enterprise', 'assessment_status',
         'certified', 'feedback_received', 'assessor_name',
        'application_start_date', 'application_end_date','assessment_start_date', 'assessment_end_date'
        );
        $filters['roles'] = [Flag::ADMIN];
        if(!empty(request()->get('is_excel'))){
            unset($filters['start']);
            $assessments = $this->assessmentService->lists($filters);
            $excelType = !empty(request()->get('excel_type')) ? request()->get('excel_type') : null;
            return Excel::download(new ListExport($assessments, $excelType), 'assessments.xlsx');
        }else{
        $assessments = $this->assessmentService->lists($filters);
        }
        $data = [
            'total'=>0,
            'assessments'=>[]
        ];
        
        if(!empty($assessments)){
            $data['total'] = $this->assessmentService->lists($filters, true);
            foreach($assessments as $assessment){
                $data['assessments'][] = $this->transformer->transform($assessment,['details'=>true, 'assessor'=>true]);
            }
        }

        $this->prepareApiResponse($data);
        return $this->sendApiResponse();
    }

    /**
     * Function to create a new Assessment
     * @param Request $request
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function store(AssessmentCreateRequest $request, $applicationId)
    {
        $inputs = $request->only('questionnaire_id', 'agency_id', 'assessment_date', 'application_id');
        if(!empty($inputs)){
           $assessment = $this->assessmentService->firstornew();
           
           $response = $this->assessmentService->save($assessment, $inputs);
           if(!empty($response->id) ){
                $this->prepareApiResponse($response, $this->transformer->transform($response));
           }else{
                $this->prepareApiResponse($response);
           }
        }
       return $this->sendApiResponse();
    }

    /**
     * Function to update an Assessment
     * @param Request $request
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function update(AssessmentUpdateRequest $request, $assessmentID)
    {
        $inputs = $request->only("questionnaire_id", "agency_id", "lead_assessor_id", "assessment_date", "assessor_ids");
        if(!empty($inputs)){
            $assessment = $this->assessmentService->firstornew($assessmentID);
            $response = $this->assessmentService->save($assessment, $inputs);
            if(!empty($response) ){
                 $this->prepareApiResponse($response, $this->transformer->transform($response));
            }else{
                 $this->prepareApiResponse($response);
            }
         }
        return $this->sendApiResponse();   
    }

     /**
     * Function to show an Assessment
     * @param Request $request
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function show(Request $request, $id)
    {
        $response = $this->assessmentService->firstornew($id);
        if(!empty($response) ){
            $data = $this->transformer->transform($response, ['details'=>true, 'site_details'=>true]);
            $this->prepareApiResponse($data);
        }else{
            $this->prepareApiResponse($response);
        }
        return $this->sendApiResponse();   
    }

    /**
     * Function to get assessment Parameters
     * @param Request $request
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function getAssessmentParameters(Request $request, $assessmentID)
    {
        $assessment = $this->assessmentService->firstornew($assessmentID);
        if(!empty($assessment)){
            $filters = ['assessment_id'=>$assessment->id];
            $parameters = $this->assessmentParameterService->lists($filters);
        }
        
        $data = [
            'total'=>0,
            'parameters'=>[]
        ];
        
        if(!empty($parameters)){
            $data['total'] = $this->assessmentParameterService->lists($filters, true);
            $data['parameters'] = $this->parameterTransformer->transform($parameters); 
        }

        $this->prepareApiResponse($data);
        return $this->sendApiResponse();
    }

    /**
     * Function to get details of any assigned parameter
     * @param Request $request
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function showParameter(Request $request, $parameterID)
    {
        $parameter = $this->assessmentParameterService->firstornew($parameterID);
        if(empty($parameter->id)){
            throw new ModelNotFoundException();
        }
        $this->prepareApiResponse($parameter);
        return $this->sendApiResponse();
    }

    /**
     * Function to submit response on a parameter on Assessment date
     * @param Request $request
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function submitParametersResponse(AssessmentParemeterCreateRequest $request, $parameterID)
    {
        $inputs = $request->only('response', 'section_uuid');
        $parameter = $this->assessmentParameterService->firstornew($parameterID);
        if(empty($parameter->id)){
            throw new ModelNotFoundException();
        }
        if(empty($parameter->assessment()->assessment_review_date) || date("Y-m-d", strtotime($parameter->assessment()->assessment_review_date)) != date("Y-m-d")){
            throw ValidationException::withMessages(['assessment' => __('validation.admin_assessment_submit_error')]);
        }
        $section = $this->assessmentParameterService->getSection($inputs['section_uuid']); 
        if(empty($section->id)){
            throw new ModelNotFoundException();
        }
        $response = $this->assessmentParameterService->submitSectionResponse($section, $inputs);
        if(!empty($response) ){
            $this->prepareApiResponse(['message'=>'assessor_response_sucsess']);
        }else{
            $this->prepareApiResponse(['message'=>'assessor_response_error']);
        }
        return $this->sendApiResponse();
    }

    /**
     * Function to submit comment on a paramter
     * @param Request $request
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function submitParameterComment(AssessmentParemeterCreateRequest $request,  $parameterID)
    {
        $inputs = $request->only('comment', 'comment_type');
        $inputs['user_id'] = request()->user()->id;
        $parameter = $this->assessmentParameterService->firstornew($parameterID); 
        if(empty($parameter->id)){
            throw new ModelNotFoundException();
        }
        if(empty($parameter->assessment()->assessment_review_date) || date("Y-m-d", strtotime($parameter->assessment()->assessment_review_date)) != date("Y-m-d")){
            throw ValidationException::withMessages(['assessment' => __('validation.admin_assessment_submit_error')]);
        }
        $response = $this->assessmentParameterService->submitParametersComment($parameter, $inputs);
        if(!empty($response) ){
            $this->prepareApiResponse(['message'=>'parameter_comment_sucsess']);
        }else{
            $this->prepareApiResponse(['message'=>'parameter_comment_error']);
        }
        return $this->sendApiResponse();
    }

     /**
     * Function to complete an Assessment
     * @param Request $request
     * @return Array $data
     * @author Daffodil Softwere
     */
    public function completeAssessment(Request $request, $id)
    {
        $assessment = $this->assessmentService->firstornew($id);
        if(empty($assessment->id)){
            throw new ModelNotFoundException();
        }
        
        if(empty($assessment->assessment_review_date) || date("Y-m-d", strtotime($assessment->assessment_review_date)) != date("Y-m-d")){
            throw ValidationException::withMessages(['assessment' => __('validation.admin_assessment_submit_error')]);
        }
        if($this->assessmentService->completeAssessment($assessment)){
            $this->prepareApiResponse(['message'=>'assessment_complete_sucsess']);
        }else{
            $this->prepareApiResponse(['message'=>'assessment_complete_error']);
        }
        return $this->sendApiResponse();   
    }
}
