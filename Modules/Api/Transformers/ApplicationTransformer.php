<?php

namespace Modules\Api\Transformers;

use Modules\Application\Entities\ApplicationFinancial;
use Modules\Application\Entities\Interfaces\ApplicationInterface;
use Modules\Application\Entities\ApplicationOwnerProcess;
use Modules\Core\Enums\ApplicationStage;
use Modules\Core\Helpers\BasicHelper;

class ApplicationTransformer
{
    public function transform(ApplicationInterface $object, $options= [])
    {
        $data = [];
        if(!empty($options['details'])){
            $data = $object;
        }else{
            $data =  [
                'application_uuid'=>$object->getApplicationUUID(),
                'application_number'=>$object->getApplicationNumber(),
                'enterprise_name'=>$object->getEnterpriseName(),
                'assessment_date'=>$object->assessment_date,
                'last_update_time'=>$object->getUpdatedAt(),
                'status'=>$object->getApplicationStage(),
                'reject_reason'=>!empty($object->reject_reason) ? $object->reject_reason : null,
                'actions_allowed'=>$this->getAllowedActions($object->getApplicationStage()),
                'feedback'=>$object->feedback()->get(),
            ];
        }
        if(!empty($object->ownership)){
            $object->load('ownership');
            $data['ownership'] = $this->transformOwnership($object->ownership);
        }else{
            unset($data->ownership);
        }

        if(!empty($options['assessment'])){
            $assessment = $object->load('assessment');
            if(!empty($assessment)){
                $data['assessment'] = $assessment;
            }
        }
        if(!empty($options['paymentOrder']) && !empty($object->paymentOrder)){
            $data['order'] = [
                'order_number'=>$object->paymentOrder->order_number,
                'amount'=>$object->paymentOrder->amount,
                'discount'=>$object->paymentOrder->discount,
                'assessment_cost'=>$object->paymentOrder->assessment_cost
            ];
        }
        if(!empty($object->feedback)){
            $data['feedback'] = $object->feedback;
        }

        $data['payment_status'] = !empty($object->paymentOrder) ? $object->paymentOrder->status : "";
        $data['assessment_status'] = !empty($object->assessment->certification_type) ? 'Completed' : 'In Progress';
        $data['rating'] = !empty($object->assessment->certification_type) ? __("api.".$object->assessment->certification_type."-certification") : null;
        $data['application_date'] = !empty($object->created_at) ? $object->getCreatedAt() : null;
        $data['assessment_plan'] = !empty($object->assessmentPlan) ? $object->assessmentPlan : null;
        
        return $data;
    }

    public function transformOwnership(ApplicationOwnerProcess $object)
    {
        return !empty($object) ? $object->toArray() : [];
    }

    public function transformFinancial(ApplicationFinancial $object)
    {
        return !empty($object) ? $object->toArray() : [];
    }

    private function getAllowedActions($applicationStage)
    {
        $allowedActions = ['edit'];

        if(in_array($applicationStage, [
            ApplicationStage::APP_ASSESS_COMPLETE
        ])){
            array_push($allowedActions, 'download');
        }
        
        return $allowedActions;
    }

    public function transformPayment(ApplicationInterface $object, $options= [])
    {
        $data = [];
        if(!empty($options['paymentOrder']) && !empty($object->paymentOrder)){
            $data['paymentDetail'] = [
                'application_id'=>$object->paymentOrder->application_id,
                'order_number'=>$object->paymentOrder->order_number,
                'amount'=>$object->paymentOrder->amount,
                'discount'=>$object->paymentOrder->discount,
                'payment_status' => !empty($object->paymentOrder) ? $object->paymentOrder->status : ""
            ];
            
        }
        return $data;
    }
}
