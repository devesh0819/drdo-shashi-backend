<?php

namespace Modules\Api\Transformers;

use Modules\Questionnaire\Entities\Interfaces\QuestionnaireInterface;

class QuestionnaireTransformer
{
 public function transform(QuestionnaireInterface $object)
    {
        return [
            'id' => $object->getID(),
            'title' => $object->getTitle(),
            'applicability'=>$object->getType(),
            'version' =>$object->getVersion(),
            'status'=>$object->getStatus(),
            'state'=>$object->state,
            'created_at'=>$object->getCreatedAt(),
            'disciplines'=>$object->disciplines,
            'last_modified_by'=>$object->getLastModifiedBy(),
        ];
    }

}