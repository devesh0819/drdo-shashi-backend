<?php

namespace Modules\Api\Transformers;

use Modules\Assessment\Entities\Interfaces\AssessmentInterface;
use Modules\Api\Transformers\ApplicationTransformer;
use Modules\Assessment\Entities\AssessmentAssessor;
use Modules\Assessment\Entities\AssessmentParameter;
use Modules\Assessment\Entities\AssessmentSiteEvidense;

class AssessmentTransformer
{
    private $applicationTransformer;
    private $questionnaireTransformer;
    
    public function __construct(ApplicationTransformer $applicationTransformer)
    {
        $this->applicationTransformer = $applicationTransformer;

    }
    

    public function transform(AssessmentInterface $object, $options= [])
    {
        $assessStartDate = !empty($object->assessment_start_date) ? date("Y-m-d", strtotime($object->assessment_start_date)) :  date("Y-m-d");
        $assessSubmitDate = !empty($object->assessment_submit_date) ? date("Y-m-d", strtotime($object->assessment_submit_date)) :  date("Y-m-d");
        $duration = 1;
        $duration = $duration+ date_diff(date_create($assessStartDate), date_create($assessSubmitDate))->format("%R%a");
        $assessExpiryDate = !empty($object->certification_expiry) ? date("Y-m-d", strtotime($object->certification_expiry)) :  null;
        $data= [
            'uuid' => $object->getUUID(),
            'agency_id'=>$object->getAgencyID(),
            'questionnaire_id'=>$object->getQuestionnaireID(),
            'assessment_date'=>$object->getAssessmentDate(),
            'assessment_score'=>$this->getAssessmentScore($object),
            'assessment_expiry_date'=>$assessExpiryDate,
            'is_ongoing_assessment'=>!empty($object->is_ongoing_assessment) ? $object->is_ongoing_assessment : 'false',
            'progress'=>$object->assessment_stage,
            'number_of_days'=>$duration,
            'assessment_type'=>!empty($object->questionnaire->type) ? $object->questionnaire->type : '',
            'assessment_number'=>$object->assessment_number,
            'agency'=>$object->agency,
            'application'=> $this->applicationTransformer->transform($object->application, $options)
        ];
        $data['lead_assessor']=$object->leadAssessor;
        $data['assessors']=$object->assessors;
        if(!empty($options['site_details'])){
            $data['site_details'] = $object->siteDetails();
        }
        return $data;
    }

    public function getAssessmentScore($object){
        $parameters = AssessmentParameter::where('assessment_id', $object->id)->pluck('parameter_score')->toArray();
        return $assessmentScore = !empty($parameters) ? array_sum($parameters)/count($parameters) : 0;
    }

    /**
     * Function to transform parameter evidence
     */
    public function transformEvidence(AssessmentSiteEvidense $object)
    {
        return [
            'id' => $object->_id,
            'url' => config('attachment.evidence_base_url').'storage/' . $object->path,
            'type'=>!empty($object->type) ? $object->type : null,
            'caption'=>!empty($object->file_caption) ? $object->file_caption : null,
            'assessment_id' => $object->assessment_id
        ];
    }
}
