<?php

namespace Modules\Api\Transformers;

use Modules\Assessment\Entities\Interfaces\AssessmentInterface;
use Modules\Api\Transformers\ApplicationTransformer;
use Modules\Api\Transformers\AssessmentParameterTransformer;
use Modules\Core\Helpers\BasicHelper;

class AssessorSyncTransformer
{
    private $applicationTransformer;
    private $parameterTransformer;
    private $parameters = [];
    private $assessment;
    
    public function __construct(
        ApplicationTransformer $applicationTransformer,
        AssessmentParameterTransformer $parameterTransformer
    )
    {
        $this->applicationTransformer = $applicationTransformer;
        $this->parameterTransformer = $parameterTransformer;
    }

    public function transform(AssessmentInterface $object, $parametersData = [], $siteDetails = [])
    {
        $this->assessment = $object;
        $assessor = $object->assessors()->where([
                'assessor_id'=>request()->user()->id,
                'assessment_id'=>$object->id
            ])->first()->toArray();
        
        $assessment =  [
            "uuid" => $object->getUUID(),
            "progress"=>$this->calculatePercentage(),
            'status'=>$assessor['assessor_assess_status'],
            "assessment_date"=>$object->application->assessment_date,
            "assessment_number"=>$object->assessment_number,
            "assigned_date"=>!empty($assessor['created']) ? BasicHelper::showDate($assessor['created_at'], 'j M Y H:i') : null,
            "comments_count"=>0,
            "application"=>[
                "application_number"=>$object->application->application_number,
                "entrepreneur_name"=>$object->application->entrepreneur_name,
                "enterprise_name"=>$object->application->enterprise_name,
                "address"=>$object->application->registered_address
            ]
        ];

        if($object->lead_assessor_id == request()->user()->id){
            $assessment['opening_meeting'] = !empty($siteDetails['opening_meeting']) ? $siteDetails['opening_meeting'] : [];
            $assessment['closing_meeting'] = !empty($siteDetails['closing_meeting']) ? $siteDetails['closing_meeting'] : [];
        }
        
        $assessment['site_tour_evidenses'] = !empty($siteDetails['site_tour']['site_tour_evidenses']) ? $siteDetails['site_tour']['site_tour_evidenses'] : [];
        
        $disciplines = $this->formatDisciplines($parametersData);
        if(!empty($disciplines)){
            $assessment['disciplines'] = [];
            foreach($disciplines as $discipline){
                array_push($assessment['disciplines'], $discipline);
            }
        }
        return $assessment;
    }

    private function calculatePercentage()
    {
        return 0;
    }

    private function formatDisciplines($parametersData)
    {
        $disciplines = [
            'parameters'=>[]
        ];
        if(!empty($parametersData->toArray())){
            foreach($parametersData as $parameter){
                if($parameter->assessment_id == $this->assessment->id){
                    $disciplines[$parameter->discipline] = [
                        'title'=>$parameter->discipline,
                        "assigned_date"=>BasicHelper::showDate($parameter->created_at, 'j M Y H:i'),
                    ];
                    $paramData = [
                        "parameter_id"=>$parameter->id,
                        "discipline" => $parameter->discipline,
                        "title"=>$parameter->parameter,
                        "assigned_date"=>BasicHelper::showDate($parameter->created_at, 'j M Y H:i'),
                    ];
                    if(!empty($parameter->sections)){
                        $paramData['sections'] = [];
                        foreach($parameter->sections as $section){
                            $paramData['sections'] [] =[
                                'section_uuid'=>$section->section_uuid,
                                "type"=>$section->type,
	                            "options"=>json_decode($section->options, true),
	                            "optional"=>$section->optional,
	                            "response"=>$section->response
                            ];
                        }
                    }
                    if(!empty($parameter->comments)){
                        $paramData['comments'] = [];
                        foreach($parameter->comments as $comment){
                            $commentAuthor = ($comment->user_id == request()->user()->id) ? request()->user()->profile : $comment->user();
                            $paramData['comments'] [] =[
                                'comment_author'=>$commentAuthor->first_name." ".$commentAuthor->last_name,
                                'comment_uuid'=>$comment->comment_uuid,
                                'comment_type'=>$comment->comment_type,
                                'comment'=>!empty($comment->comment) ? $comment->comment : "",
                                'last_updated_at'=>!empty($comment->updated_at) ? BasicHelper::showDate($comment->updated_at, 'j M Y H:i')
                                : BasicHelper::showDate($comment->created_at, 'j M Y H:i'),
                            ];
                        }
                    }
                    
                    if(!empty($parameter->evidenses)){
                        $paramData['evidenses'] = [];
                        foreach($parameter->evidenses as $evidense){
                            $evidenseAuthor = ($evidense->user_id == request()->user()->id) ? request()->user()->profile : $evidense->user();
                            $paramData['evidenses'] [] =[
                                'evidenseAuthor'=>$evidenseAuthor->first_name." ".$evidenseAuthor->last_name,
                                'evidense_uuid'=>$evidense->evidense_uuid,
                                'latittude'=>!empty($evidense->latitude) ? $evidense->latitude : "",
                                'longitude'=>!empty($evidense->longitude) ? $evidense->longitude : "",
                                'file_date_time'=>!empty($evidense->file_date_time) ? BasicHelper::showDate($evidense->file_date_time, 'j M Y H:i') : "",
                                'file'=>!empty($evidense->path) ? $evidense->path : "",
                                'last_updated_at'=>!empty($evidense->updated_at) ? BasicHelper::showDate($evidense->updated_at, 'j M Y H:i')
                                : BasicHelper::showDate($evidense->created_at, 'j M Y H:i'),
                            ];
                        }
                    }
                    $disciplines['parameters'] []=  $paramData;

                }
            }

            if(!empty($disciplines['parameters'])){
                foreach($disciplines['parameters'] as $parameter){
                    $disciplines[$parameter['discipline']]['parameters'] []= $parameter;
                }
            }
        }
        unset($disciplines['parameters']);
        return $disciplines;
    }
 }
