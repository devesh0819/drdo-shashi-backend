<?php

namespace Modules\Api\Transformers;




use Modules\Committee\Entities\Interfaces\MeetingInterface;


class MeetingTransformer
{

    public function transform(MeetingInterface $object)
    {
        return [
            'uuid' => $object->meeting_uuid,
            'meeting_title' => $object->getMeetingTitle(),
            'description'=>$object->getDescription(),
            'meeting_date_time'=>$object->meeting_date_time,
            'meeting_type'=>$object->getMeetingType(),
            'meeting_link'=>$object->getMeetingLink(),
            //'meeting_type'=>$object->meeting_type,
            'meeting_address'=>$object->meeting_address,
            'meeting_assessments'=>$object->getMeetingAssessments(),
            'meeting_notes'=>$object->notes()->get(),
        ];
    }

}
