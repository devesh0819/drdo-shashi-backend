<?php

namespace Modules\Api\Transformers;

use Modules\Assessment\Entities\AssessmentParameterComment;
use Modules\Assessment\Entities\Interfaces\AssessmentParameterInterface;
use Modules\Assessment\Entities\Interfaces\AssessmentParameterCommentInterface;
use Modules\Assessment\Entities\Interfaces\AssessmentParameterEvidenceInterface;
use Modules\Core\Helpers\BasicHelper;

class AssessmentParameterTransformer
{
    private $data;

    public function transform($parameters)
    {
        $this->data = [];
        if(!empty($parameters)){
            $this->data = [
                'assessment_id'=>'',
                'application_id'=>'',
                'disciplines'=>[
                ]
            ];
            foreach($parameters as $key=>$val){
                $this->transformParameter($val);
            }
        }
        return $this->data;
    }

    public function transformParameter(AssessmentParameterInterface $object)
    {
        $this->data['assessment_id'] = $object->getAssessmentID();
        $this->data['application_id'] = $object->getApplicationID();
        
        if(!empty($object->getDiscipline())){
            $this->data['disciplines'] [$object->getDiscipline()]= [
                'title'=>$object->getDiscipline(),
                'parameters_count'=> !empty($this->data['disciplines'] [$object->getDiscipline()]['parameters_count']) 
                    ? $this->data['disciplines'] [$object->getDiscipline()]['parameters_count'] : 0,
                'comments_count'=>!empty($this->data['disciplines'] [$object->getDiscipline()]['comments_count']) 
                ? $this->data['disciplines'] [$object->getDiscipline()]['comments_count'] : 0,
                'assessors' =>!empty($this->data['disciplines'] [$object->getDiscipline()]['assessors']) 
                ? $this->data['disciplines'] [$object->getDiscipline()]['assessors'] : [],
                'parameters' =>!empty($this->data['disciplines'] [$object->getDiscipline()]['parameters']) 
                ? $this->data['disciplines'] [$object->getDiscipline()]['parameters'] : [],
            ];

            if(!empty($object->getParameter())){
                // Increase Paramter Count
                $this->data['disciplines'] [$object->getDiscipline()]['parameters_count']++;
                
                // Add Assessors
                if(!in_array($object->getAssessorID(), $this->data['disciplines'] [$object->getDiscipline()]['assessors'])){
                    array_push($this->data['disciplines'] [$object->getDiscipline()]['assessors'], $object->getAssessorID());
                }
                
                $paramData = [
                    'id'=>$object->getID(),
                    'parameter'=>$object->getParameter(),
                    'parameter_score'=>$object->parameter_score,
                    'parameter_completed'=>$object->sections_completed,
                    'assessor_id'=>$object->getAssessorID(),
                    'assessor_comments'=>$object->getAssessorComments(),
                    'sections'=>$object->sections
                ];
                if(!empty($object->comments)){
                    $paramData['comments'] = [];
                    foreach($object->comments as $comment){
                        $commentAuthor = ($comment->user_id == request()->user()->id) ? request()->user()->profile : $comment->user();
                        $paramData['comments'] [] =[
                            'comment_author'=>$commentAuthor->first_name." ".$commentAuthor->last_name,
                            'comment_uuid'=>$comment->comment_uuid,
                            'comment_type'=>$comment->comment_type,
                            'comment'=>!empty($comment->comment) ? $comment->comment : "",
                            'last_updated_at'=>!empty($comment->updated_at) ? BasicHelper::showDate($comment->updated_at, 'j M Y H:i')
                            : BasicHelper::showDate($comment->created_at, 'j M Y H:i'),
                        ];
                    }
                }
                
                if(!empty($object->evidenses)){
                    $paramData['evidenses'] = [];
                    foreach($object->evidenses as $evidense){
                        $evidenseAuthor = ($evidense->user_id == request()->user()->id) ? request()->user()->profile : $evidense->user();
                        $paramData['evidenses'] [] =[
                            'evidenseAuthor'=>$evidenseAuthor->first_name." ".$evidenseAuthor->last_name,
                            'evidense_uuid'=>$evidense->evidense_uuid,
                            'latitude'=>!empty($evidense->latitude) ? $evidense->latitude : "",
                            'longitude'=>!empty($evidense->longitude) ? $evidense->longitude : "",
                            'file_date_time'=>!empty($evidense->file_date_time) ? BasicHelper::showDate($evidense->file_date_time, 'j M Y H:i') : "",
                            'file'=>!empty($evidense->path) ? $evidense->path : "",
                            'last_updated_at'=>!empty($evidense->updated_at) ? BasicHelper::showDate($evidense->updated_at, 'j M Y H:i')
                            : BasicHelper::showDate($evidense->created_at, 'j M Y H:i'),
                        ];
                    }
                }
                $this->data['disciplines'] [$object->getDiscipline()]['parameters'] [] = $paramData;
                $disciplinesData = [];
                if(!empty($this->data['disciplines'])){
                    foreach($this->data['disciplines'] as $discipline){
                        array_push($disciplinesData, $discipline);
                    }
                }
                $this->data['disciplines'] = $disciplinesData;
            }
        }
    }

    /**
     * Function to transform parameter comment
     */
    public function transformComment(AssessmentParameterCommentInterface $object)
    {
        return [
            'id'=>$object->getID(),
            'comment'=>$object->getComment()
        ];
    }

    /**
     * Function to transform parameter evidence
     */
    public function transformEvidence(AssessmentParameterEvidenceInterface $object)
    {
        return [
            'id' => $object->getID(),
            'url' => $object->getUrl(),
            'parameter_id' => $object->getParameterID()
        ];
    }
}
