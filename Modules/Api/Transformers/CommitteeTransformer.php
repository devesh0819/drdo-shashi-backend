<?php

namespace Modules\Api\Transformers;

use Modules\Committee\Entities\Interfaces\CommitteeInterface;
use Modules\Api\Transformers\MeetingTransformer;
use Modules\Api\Transformers\UserProfileTransformer;
class CommitteeTransformer
{
    private $meetingTransformer;
    private $profileTransformer;

    public function __construct(
        MeetingTransformer $meetingTransformer,
        UserProfileTransformer $profileTransformer)
    {
        $this->meetingTransformer = $meetingTransformer;
        $this->profileTransformer = $profileTransformer;
    }

    public function transform(CommitteeInterface $object, $options =[])
    {
        $data =  [
            'uuid' => $object->committee_uuid,
            'title'=>$object->title,
            'meeting_counts'=>count($object->meetings),
            'members_count'=>$object->members_count()
        ];
        if(!empty($options['meetings'])){
            $meetings=$object->meetings;
            $data['meetings'] = [];
            if(!empty($meetings)){
                foreach($meetings as $meeting){
                    $data['meeting_counts']++;
                    array_push($data['meetings'], $this->meetingTransformer->transform($meeting));
                }
            }
        }
        if(!empty($options['members'])){
            $members=$options['memberData'];
            $data['members'] = [];
            if(!empty($members)){
                foreach($members as $member){
                    if(!empty($member->profile->id)){
                        $data['members_count']++;
                        array_push($data['members'], $this->profileTransformer->transform($member->profile));
                    }
                }
            }
        }
        return $data;
    }

}
