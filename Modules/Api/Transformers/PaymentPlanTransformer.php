<?php

namespace Modules\Api\Transformers;

use Modules\Core\Entities\AssessmentPlan;

class PaymentPlanTransformer
{

    public function transform(AssessmentPlan $object)
    {
        return [
           'enterprice_type'=>!empty($object->enterprice_type) ? $object->enterprice_type : null,
           'certification_fee'=>!empty($object->assessment_cost) ? $object->assessment_cost : null,
           'is_subsidized'=>!empty($object->is_subsidized) ? $object->is_subsidized : null,
           'subsidy_percent'=>!empty($object->subsidy_percent) ? $object->subsidy_percent : null,
           'payment_breakup'=>!empty($object->payment_breakup) ? json_decode($object->payment_breakup, true) : null,
        ];
    }

}
