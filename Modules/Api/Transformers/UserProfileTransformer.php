<?php

namespace Modules\Api\Transformers;

use Modules\User\Entities\Interfaces\UserProfileInterface;
use Modules\Agency\Entities\Interfaces\AgencyMetaInterface;

class UserProfileTransformer
{

    public function transform(UserProfileInterface $object, $options = [])
    {
        $data =  [
            'uuid' => $object->user->uuid,
            'id' => $object->id,
            'email'=>$object->user->email,
            'username'=>!empty($object->user->username) ? $object->user->username : null,
            'first_name' => $object->getFirstName(),
            'last_name'=>$object->getLastName(),
            'organisation'=>!empty($object->organisation) ? $object->organisation : $object->organisation,
            'designation'=>!empty($object->designation) ? $object->designation : $object->designation,
            'member_role'=>!empty($object->member_role) ? $object->member_role : $object->member_role,
            'phone_number'=>$object->getPhoneNumber(),
            'alternate_phone_number'=> $object->user->alternate_phone_number,
            'pan_number' => $object->getPanNumber(),
            'gst_number' => $object->getGSTNumber(),
            'roles'=>($object->user->roles) ? $object->user->roles->pluck('name') : '',
            'terms' => $object->getTerms()
        ];
        if(!empty($options['assessorMeta'])){
            $assessor = $object->user->assessorMeta;
            $data['assessorMeta'] = $assessor;
        }
        if(!empty($options['agencyMeta'])){
            $agency = $object->user->agencyMeta;
            if(!empty($agency) && $agency instanceof AgencyMetaInterface){
                $assessors = $agency->assessors;
                $data['agencyMeta'] = [
                    'user_id' => $agency->getUserId(),
                    'title'=> $agency->getTitle(),
                    'address'=>$agency->getAddress(),
                    'assessor_count'=>!empty($assessors->count()) ? $assessors->count() : 0,
                ];
                if(!empty($assessors)){
                    $data['agencyMeta']['assessors'] = [];
                    foreach($assessors as $assessor){
                        $data['agencyMeta']['assessors'][]=[
                            'name'=>$assessor->user->profile->getFirstName()." ".$assessor->user->profile->getLastName(),
                            'designation'=>$assessor->getExpertise(),
                            'address'=>$assessor->getAddress(),
                            'email'=>$assessor->user->email,
                            'id'=>$assessor->user->uuid
                        ];
                    }
                }
            }
        }
        return $data;
    }

}
