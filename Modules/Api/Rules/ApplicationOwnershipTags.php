<?php

namespace Modules\Api\Rules;

use Illuminate\Contracts\Validation\InvokableRule;
use Illuminate\Support\Facades\Validator;

class ApplicationOwnershipTags implements InvokableRule
{
    private $tagRules;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->tagRules = [
            'key_processes'=>[
                'maxLength'=> config('api.application.maxKeyProcessesCount'),
                'rules'=>[
                    'key_processes'=>'required|array'
                ]
            ],
            'processes_outsourced'=>[
                'maxLength'=> config('api.application.maxProcessesOutsourcedCount'),
                'rules'=>[
                    'processes_outsourced'=>'required|array'
                ]
            ],
            'energy_resources_used'=>[
                'maxLength'=> config('api.application.maxEnergyResourcesUsedCount'),
                'rules'=>[
                    'energy_resources_used'=>'required|array'
                ]
            ],
            'natural_resources_used'=>[
                'maxLength'=> config('api.application.maxNaturalResourcesUsedCount'),
                'rules'=>[
                    'natural_resources_used'=>'required|array'
                ]
            ],
            'environment_consents_name'=>[
                'maxLength'=> config('api.application.maxEnvironmentConsentsNameCount'),
                'rules'=>[
                    'environment_consents_name'=>'required|array'
                ]
            ]
        ];
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function __invoke($attribute, $values, $fail)
    {
        // validate Manufactured Products
        $response = $this->validateTags($attribute, $values);
        $errorMessage = $this->getErrorMessage($attribute, $response);
        if($errorMessage){
            $fail($errorMessage);
        }
    }

    /**
     * Function to customised error message
     */
    public function getErrorMessage($attribute, $response)
    {
        if( isset($response['validated'])
            && !$response['validated'] 
            && !empty($response['errorCode'])){
            return __("validation.custom.$attribute.".$response['errorCode'], ['maxLength'=>$this->tagRules[$attribute]['maxLength']]);
        };
        return false;
    }

    /**
     * Function to validate json input
     */
    private function validateTags($attribute, $values){
        $inputs[$attribute] = explode(",", $values);
        if(!empty($inputs[$attribute]) && is_array($inputs[$attribute])){
            if(count($inputs[$attribute]) > $this->tagRules[$attribute]['maxLength']){
                return ['validated'=>false, 'errorCode'=>'maxLength'];
            }
            if(!Validator::make($inputs, $this->tagRules[$attribute]['rules'])->passes()){
                return ['validated'=>false, 'errorCode'=>'invalid'];
            }
            return true;
        }else{
            return ['validated'=>false, 'errorCode'=>'invalid'];
        }
    }
}
