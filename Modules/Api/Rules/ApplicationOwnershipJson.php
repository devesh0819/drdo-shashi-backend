<?php

namespace Modules\Api\Rules;

use Illuminate\Contracts\Validation\InvokableRule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class ApplicationOwnershipJson implements InvokableRule
{
    private $jsonRules;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->jsonRules = [
            'product_manufactured'=>[
                'maxLength'=> config('api.application.maxProductManufactureCount'),
                'rules'=>[
                    'product_manufactured.*.productName'=>'required|string|min:3|max:55',
                    'product_manufactured.*.totalCapacityUnit'=>'required|string|min:2|max:55',
                    'product_manufactured.*.presentCapacityUnit'=>'required|string|min:2|max:55',
                    'product_manufactured.*.isProductExported'=>'required|boolean',
                    'product_manufactured.*.destinationCountries'=>'required',
                    'product_manufactured.*.percentageExportProduct'=>'required|string',
                    'product_manufactured.*.totalProductionCapacityPerYear'=>'required|string',
                    'product_manufactured.*.presentProductionCapacityPerYear'=>'required|string',
                    'product_manufactured.*.dateOfCommencement'=>'required|date'
                ]
            ],
            'key_domestic_customers'=>[
                'maxLength'=> config('api.application.maxKeyDomesticCustomerCount'),
                'rules'=>[
                    'key_domestic_customers.*.customerName'=>'required|alpha_spaces|min:2|max:55',
                    'key_domestic_customers.*.customerType'=>'required|string|min:1|max:100',
                    'key_domestic_customers.*.customerCountry'=>'required|string|min:1|max:100',
                ]
            ],
            'key_raw_materials'=>[
                'maxLength'=> config('api.application.maxKeyRawMaterialsCount'),
                'rules'=>[
                    'key_raw_materials.*.rawMaterialName'=>'required|string|min:3|max:55',
                    'key_raw_materials.*.rawMaterialPercentageImported'=>'nullable|string',
                    'key_raw_materials.*.rawMaterialCountryName'=>'nullable|string',
                    'key_raw_materials.*.rawMaterialSourceOfProcurement'=>'nullable|string',
                    'key_raw_materials.*.rawMaterialImported'=>'nullable|integer'
                ]
            ]
        ];
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function __invoke($attribute, $values, $fail)
    {
        // validate Manufactured Products
        $response = $this->validateJson($attribute, $values);
        $errorMessage = $this->getErrorMessage($attribute, $response);
        if($errorMessage){
            $fail($errorMessage);
        }
    }

    /**
     * Function to customised error message
     */
    public function getErrorMessage($attribute, $response)
    {
        if( isset($response['validated'])
            && !$response['validated'] 
            && !empty($response['errorCode'])){
            return __("validation.custom.$attribute.".$response['errorCode'], ['maxLength'=>$this->jsonRules[$attribute]['maxLength']]);
        };
        return false;
    }

    /**
     * Function to validate json input
     */
    private function validateJson($attribute, $values){
        $inputs[$attribute] = json_decode($values, true);
        if(!empty($inputs[$attribute]) && is_array($inputs[$attribute])){
            if(count($inputs[$attribute]) > $this->jsonRules[$attribute]['maxLength']){
                return ['validated'=>false, 'errorCode'=>'maxLength'];
            }
            $validator = Validator::make($inputs, $this->jsonRules[$attribute]['rules']); 
            if(!$validator->passes()){
                dd($validator->messages());
                return ['validated'=>false, 'errorCode'=>'invalid'];
            }
            return true;
        }else{
            return ['validated'=>false, 'errorCode'=>'invalid'];
        }
    }
}
