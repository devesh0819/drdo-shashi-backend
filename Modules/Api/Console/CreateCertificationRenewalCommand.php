<?php

namespace Modules\Api\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Exception;
use Modules\Core\Helpers\Logger;
use Modules\Application\Entities\Application;
use Modules\Assessment\Entities\Assessment;
use Modules\Notification\Notifications\SendAssessmentRenewalEmail;

class CreateCertificationRenewalCommand extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'send:renewal-command';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to send renewal certification notification to vendor.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $allAssessmentToBeExpired = Assessment::whereDate('certification_expiry', '=', date('Y-m-d', strtotime("+3 months")))->get();
        if(!empty($allAssessmentToBeExpired)){
            foreach($allAssessmentToBeExpired as $assessment){
                $assessment->application->user->user->notify(new SendAssessmentRenewalEmail($assessment->application));
            }
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
        ];
    }
}
