<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('applications')) {
            Schema::create('applications', function (Blueprint $table) {
                $table->id();
                $table->foreignId('user_id')
                    ->constrained()
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
                $table->string('application_uuid', 255)->unique();
                $table->string('application_number', 255)->unique();
                $table->enum('application_stage', [
                    'app_basic',
                    'app_financial',
                    'app_ownership',
                    'app_pending_approval',
                    'app_rejected',
                    'app_approved',
                    'app_payment_approved',
                    'app_payment_done',
                    'app_scheduled',
                    'app_assess_in_progress',
                    'app_assess_complete',
                    'app_feedback_complete'
                ])->default('app_basic');
                $table->timestamp('assessment_date')->nullable();
                $table->tinyInteger('assess_reschedule_count')->nullable();
                $table->tinyInteger('subsidy_approved')->default(0);
                $table->tinyInteger('local_assessment')->nullable();
                $table->tinyInteger('is_existing_vendor')->nullable();
                $table->unsignedInteger('vendor_certificate')->nullable();
                $table->string('entrepreneur_name', 255)->nullable();
                $table->enum('social_category', ['GN', 'SC','ST','OBC'])->default('GN');
                $table->enum('special_category', ['EXS', 'PWD','WOM','MIN', 'NONE'])->nullable();
                
                $table->string('enterprise_name', 255)->nullable();
                $table->text('enterprice_details')->nullable();
                $table->enum('enterprice_nature', [
                    'Prop',
                    'PL',
                    'Pub',
                    'LimPart',
                    'Part',
                    "Join"
                ])->nullable(); 
                $table->enum('business_nature', [
                    'manufacturer',
                    'fabricator',
                    'assembler'
                ])->nullable();
                $table->enum('enterprice_type', ['MIC',"SM","MED","L"])->default('MIC');
                $table->string('uam_number', 255)->nullable();
                $table->string('ncage_number', 255)->nullable();
                $table->string('cin_number', 255)->nullable();
                $table->unsignedInteger('organisation_type')->nullable();
                $table->text('registered_address')->nullable();
                $table->unsignedInteger('registered_state')->nullable();
                $table->unsignedInteger('registered_district')->nullable();
                $table->string('registered_pin', 255)->nullable();
                $table->string('registered_mobile_number', 255)->nullable();
                $table->string('registered_landline_number', 255)->nullable();
                $table->string('registered_email', 255)->nullable();
                $table->string('registered_alternate_email', 255)->nullable();
                $table->text('unit_address')->nullable();
                $table->unsignedInteger('unit_state')->nullable();
                $table->unsignedInteger('unit_district')->nullable();
                $table->string('unit_pin', 255)->nullable();
                $table->string('unit_lat', 255)->nullable();
                $table->string('unit_long', 255)->nullable();
                $table->string('unit_mobile_number', 255)->nullable();
                $table->string('unit_landline_number', 255)->nullable();
                $table->string('unit_email', 255)->nullable();
                $table->string('unit_alternate_email', 255)->nullable();
                $table->string('unit_web_url', 255)->nullable();
                $table->timestamp('enterprise_commencement_date')->nullable();
                $table->enum('cpcb_code', ['R',"O","G","W"])->nullable();
                $table->integer('persons_employed')->nullable();
                $table->tinyInteger('supplier_to_defence')->nullable();
                $table->tinyInteger('is_foreign_partner')->nullable();
                $table->string('police_jurisdiction', 255)->nullable();
                $table->string('police_jurisdiction_address', 255)->nullable();
                $table->tinyInteger('basic_details_confirmed')->nullable();
                $table->text('reject_reason', 255)->nullable();
                $table->tinyInteger('status')->default(1);    
                $table->unsignedInteger('last_modified_by')->nullable();
                $table->softDeletes();
                $table->timestamp('created_at')->useCurrent();
                $table->timestamp('updated_at')->nullable();
            });
        }

        if (!Schema::hasTable('application_financials')) {
            Schema::create('application_financials', function (Blueprint $table) {
                $table->id();
                $table->foreignId('application_id')
                    ->constrained()
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
                $table->string('enterprise_name', 255)->nullable();
                $table->string('ncage_number', 255)->nullable();    
                $table->enum('enterprice_type', ['MIC',"SM","MED","L"])->default('MIC');
                $table->string('entrepreneur_name', 255)->nullable();
                $table->string('entrepreneur_aadhar_number', 255)->nullable();
                $table->string('entrepreneur_gst_number', 255)->nullable();
                $table->string('tan_number', 255)->nullable();
                $table->tinyInteger('financial_details_confirmed')->nullable();
                // Bank Details
                $table->string('bank_name', 255)->nullable();
                $table->string('bank_account_number', 255)->nullable();
                $table->string('bank_ifsc_code', 255)->nullable();
                $table->string('bank_branch_name', 255)->nullable();
                $table->text('bank_full_address')->nullable();
                $table->unsignedInteger('bank_state')->nullable();
                $table->unsignedInteger('bank_district')->nullable();
                $table->string('bank_city', 255)->nullable();
                $table->unsignedInteger('ecs_form_id')->nullable();

                $table->timestamp('created_at')->useCurrent();
                $table->timestamp('updated_at')->nullable();
            });
        }

        if (!Schema::hasTable('application_owner_process')) {
            Schema::create('application_owner_process', function (Blueprint $table) {
                $table->id();
                $table->foreignId('application_id')
                    ->constrained()
                    ->onUpdate('cascade')
                    ->onDelete('cascade');

                $table->unsignedInteger('msme_representative_selfie')->nullable();
                $table->unsignedInteger('site_exterior_picture')->nullable();

                $table->string('work_area_covered', 255)->nullable();
                $table->tinyInteger('is_work_covered_leased')->nullable();

                $table->string('work_area_uncovered', 255)->nullable();
                $table->tinyInteger('is_work_uncovered_leased')->nullable();

                $table->string('work_area_bond_rooms', 255)->nullable();
                $table->tinyInteger('is_work_bond_leased')->nullable();

                $table->string('work_area_num_bond_rooms', 255)->nullable();
                $table->tinyInteger('is_work_numbond_leased')->nullable();

                $table->string('work_prod_area', 255)->nullable();
                $table->tinyInteger('is_work_prod_leased')->nullable();

                $table->string('work_testing_area', 255)->nullable();
                $table->tinyInteger('is_work_test_leased')->nullable();

                $table->string('admin_manpower_count', 55)->nullable();
                $table->text('admin_skill_set')->nullable();
                $table->string('tech_skilled_count', 55)->nullable();
                $table->text('tech_skill_set')->nullable();
                $table->string('tech_unskill_count', 55)->nullable();
                $table->tinyInteger('is_foreign_employees')->nullable();

                $table->text('financial_capital_outlay')->nullable();
                $table->text('financial_source')->nullable();
                $table->text('turnover_3_year')->nullable();
                $table->text('profit_3_year')->nullable();
                $table->text('acc_loss_3_year')->nullable();
                
                $table->tinyInteger('financial_audit_conducted')->nullable();
                
                $table->jsonb('product_manufactured')->nullable();
                $table->unsignedInteger('product_document')->nullable();
                
                $table->jsonb('key_domestic_customers')->nullable();
                $table->jsonb('key_raw_materials')->nullable();
                $table->jsonb('boughtout_parts')->nullable();
                $table->jsonb('key_vendors')->nullable();
                $table->jsonb('testing_method')->nullable();
                
                $table->text('acceptance_quality_level')->nullable();
                $table->tinyInteger('is_competent_banned')->nullable();
                $table->text('competent_banned_details')->nullable();
                $table->tinyInteger('is_enquiry_against_firm')->nullable();
                $table->text('enquiry_against_firm_detail')->nullable();

                $table->text('key_processes')->nullable();
                $table->text('processes_outsourced')->nullable();
                $table->text('energy_resources_used')->nullable();
                $table->text('natural_resources_used')->nullable();
                $table->text('environment_consents_name')->nullable();
                $table->text('third_party_cert')->nullable();
                
                /*
                $table->tinyInteger('is_imported_raw_material')->nullable();
                $table->tinyInteger('is_bought_out_parts')->nullable();
                $table->jsonb('imported_raw_materials')->nullable();
                $table->string('is_imported_boughtout_parts')->nullable();
                $table->string('parts_name')->nullable();
                $table->jsonb('external_testers')->nullable();
               */
              
                $table->unsignedInteger('last_modified_by')->nullable();
                $table->softDeletes();
                $table->timestamp('created_at')->useCurrent();
                $table->timestamp('updated_at')->nullable();
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applications');
        Schema::dropIfExists('application_financial');
        Schema::dropIfExists('application_owner_process');
    }
};
