<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('application_step_2')) {
            Schema::create('application_step_2', function (Blueprint $table) {
                $table->id();
                $table->foreignId('application_id')
                    ->constrained()
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
                $table->jsonb('product_manufactured')->nullable();
                $table->jsonb('key_domestic_customers')->nullable();
                $table->jsonb('key_raw_materials')->nullable();
                $table->string('admin_manpower_count', 55)->nullable();
                $table->tinyInteger('is_competent_banned')->nullable();
                $table->text('competent_banned_details')->nullable();
                $table->tinyInteger('is_enquiry_against_firm')->nullable();
                $table->text('enquiry_against_firm_detail')->nullable();
                $table->text('key_processes')->nullable();
                $table->text('processes_outsourced')->nullable();
                $table->text('third_party_cert')->nullable();
                
                $table->timestamp('created_at')->useCurrent();
                $table->timestamp('updated_at')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('application_step_2');
    }
};
