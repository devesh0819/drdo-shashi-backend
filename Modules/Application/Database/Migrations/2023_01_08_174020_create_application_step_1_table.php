<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('application_step_1')) {
            Schema::create('application_step_1', function (Blueprint $table) {
                $table->id();
                $table->foreignId('user_id')
                    ->constrained()
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
                $table->string('application_uuid', 255)->unique();
                $table->string('application_number', 255)->unique();
                $table->enum('application_stage', [
                    'app_basic',
                    'app_financial',
                    'app_ownership',
                    'app_pending_approval',
                    'app_rejected',
                    'app_approved',
                    'app_payment_approved',
                    'app_payment_done',
                    'app_scheduled',
                    'app_assess_in_progress',
                    'app_assess_complete',
                    'app_feedback_complete'
                ])->default('app_basic');
                $table->timestamp('assessment_date')->nullable();
                $table->tinyInteger('assess_reschedule_count')->nullable();
                $table->tinyInteger('subsidy_approved')->default(0);
                $table->tinyInteger('local_assessment')->nullable();
    
                /** Step-1 Basic Info */
                $table->tinyInteger('is_existing_vendor')->nullable();
                $table->unsignedInteger('vendor_certificate')->nullable();
                $table->string('enterprise_name', 255)->nullable();
                $table->enum('enterprice_nature', [
                    'Prop',
                    'PL',
                    'Pub',
                    'LimPart',
                    'Part',
                    "Join"
                ])->nullable(); 
                $table->enum('enterprice_type', ['MIC',"SM","MED","L"])->default('MIC');

                $table->text('registered_address')->nullable();
                $table->unsignedInteger('registered_state')->nullable();
                $table->unsignedInteger('registered_district')->nullable();
                $table->string('registered_pin', 255)->nullable();
                $table->string('registered_mobile_number', 255)->nullable();
                $table->string('registered_landline_number', 255)->nullable();
                $table->string('registered_email', 255)->nullable();
                $table->string('registered_alternate_email', 255)->nullable();
                $table->tinyInteger('unit_address_confirmed')->nullable();
                $table->text('unit_address')->nullable();
                $table->unsignedInteger('unit_state')->nullable();
                $table->unsignedInteger('unit_district')->nullable();
                $table->string('unit_pin', 255)->nullable();
                $table->string('unit_lat', 255)->nullable();
                $table->string('unit_long', 255)->nullable();
                $table->string('unit_mobile_number', 255)->nullable();
                $table->string('unit_landline_number', 255)->nullable();
                $table->string('unit_email', 255)->nullable();
                $table->string('unit_alternate_email', 255)->nullable();
                $table->timestamp('enterprise_commencement_date')->nullable();
               
                $table->string('drdo_registration_number', 255)->nullable();    
                $table->string('poc_name', 255)->nullable();    
                $table->string('poc_designation', 255)->nullable();    
                $table->string('poc_mobile_number', 255)->nullable();
                $table->timestamp('unit_commencement_date')->nullable();     

                $table->string('uam_number', 255)->nullable();
                $table->string('entrepreneur_gst_number', 255)->nullable();
                $table->unsignedInteger('entrepreneur_gst_certificate')->nullable();    
                $table->unsignedInteger('udyam_certificate')->nullable();
                $table->unsignedInteger('incorporation_certificate')->nullable();
                $table->unsignedInteger('site_exterior_picture')->nullable();
                
                

                $table->text('reject_reason', 255)->nullable();
                $table->tinyInteger('status')->default(1);    
                $table->unsignedInteger('last_modified_by')->nullable();
                $table->softDeletes();
                $table->timestamp('created_at')->useCurrent();
                $table->timestamp('updated_at')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('application_step_1');
    }
};
