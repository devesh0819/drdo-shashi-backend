<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('assessment_feedbacks')) {
            Schema::create('assessment_feedbacks', function (Blueprint $table) {
                $table->id();
                $table->unsignedInteger('application_id');
                $table->unsignedInteger('user_id');
                $table->jsonb('feedback_payload')->nullable();
                $table->text('feedback_comment')->nullable();
                $table->unsignedInteger('last_modified_by')->nullable();
                $table->softDeletes();
                $table->timestamp('created_at')->useCurrent();
                $table->timestamp('updated_at')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assessment_feedbacks');
    }
};
