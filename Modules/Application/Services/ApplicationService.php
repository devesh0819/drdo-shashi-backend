<?php

namespace Modules\Application\Services;

use Modules\Application\Repositories\Interfaces\ApplicationInterface;
use Exception;
use Modules\Core\Helpers\Logger;
use Modules\Application\Entities\Application;
use Modules\Core\Entities\FeedbackQuestionSetting;

class ApplicationService
{
    private $repository;

    /**
     * Constructor
     * @param ApplicationInterface $repository
     */
    public function __construct(ApplicationInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Function to find all applications list
     * @param Array $filters
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function lists($filters=[], $countOnly=false)
    {
        try {
            return $this->repository->lists($filters, $countOnly);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

    /**
     * Function to find all applications list
     * @param Array $filters
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function getStats($filters=[], $countOnly=false)
    {
        try {
            return $this->repository->getStats($filters, $countOnly);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

    /**
     * Function to get user details by id
     * @param type $id
     */
    public function firstornew($id = null)
    {
        try {
            return $this->repository->firstornew($id);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }


/**
     * Function to get Feedback Question details by id
     * @param type $id
     */
    public function firstornewFeedback($id = null)
    {
        try {
            return $this->repository->firstornewFeedback($id);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }


    /**
     * Function to save application
     * @param Application $application
     * @param array $data
     */
    public function save(Application $application, $data=[])
    {
        try {
            return $this->repository->save($application, $data);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

     /**
     * Function to save application ownership
     * @param Application $application
     * @param array $data
     */
    public function syncApplicationOwnerships(Application $application, Array $data)
    {
        try {
            return $this->repository->syncApplicationOwnerships($application, $data);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

    /**
     * Function to save application financial
     * @param Application $application
     * @param array $data
     */
    public function syncApplicationFinancials(Application $application, Array $data)
    {
        try {
            return $this->repository->syncApplicationFinancials($application, $data);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

    /**
     * Function to submit application
     * @param Application $application
     * @param array $data
     */
    public function submitApplication(Application $application)
    {
        try {
            return $this->repository->submitApplication($application);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

    /**
     * Function to schedule application's assessment
     * @param Application $application
     * @param array $data
     */
    public function scheduleAssessment(Application $application, array $data)
    {
        try {
            return $this->repository->scheduleAssessment($application, $data);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

     /**
     * FUnction to approve Application
     */
    public function approveApplication(Application $application, $data=[])
    {
        try {
            return $this->repository->approveApplication($application, $data);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

     /**
     * Function to save feedback
     * @param Application $capplication
     * @param array $data
     */
    public function saveFeedback(Application $application, $data=[])
    {
        try {
            return $this->repository->saveFeedback($application, $data);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

    /**
     * Function to save feedback Configuration
     * @param Array $filters
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function saveFeedbackConfig(FeedbackQuestionSetting $feedback, $data=[])
    {
        try {
            return $this->repository->saveFeedbackConfig($feedback, $data);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

    /**
     * Function to get payment configurations
     * @param Array $filters
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function getFeedbackConfig()
    {
        try {
            return $this->repository->getFeedbackConfig();
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }


    /**
     * Function to delete feedback question
     * @param FeedbackQuestionSetting $feedback
     * @param array $data
     */
    public function deleteFeedback(FeedbackQuestionSetting $feedback)
    {
        try {
            return $this->repository->deleteFeedback($feedback);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

    /**
     * Function to delete application
     * @param Application $application
     * @param array $data
     */
    public function delete(Application $application)
    {
        try {
            return $this->repository->delete($application);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }
}
