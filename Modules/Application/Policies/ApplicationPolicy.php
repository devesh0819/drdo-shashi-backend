<?php

namespace Modules\Application\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\AuthorizationException;
use Modules\User\Entities\User;
use Modules\Application\Services\ApplicationService;
use Route;
use Modules\Core\Enums\ApplicationStage;
use Modules\Core\Enums\UserRole;
use Modules\Application\Entities\Application;
use Illuminate\Validation\ValidationException;

class ApplicationPolicy
{
    use HandlesAuthorization;
    private $applicationService;
    private $application;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct(ApplicationService $applicationService)
    {
        $this->applicationService = $applicationService;

        if(!empty(request()->route('applicationID'))){
            $this->application = $this->applicationService->firstornew(request()->route('applicationID'));
            if(empty($this->application->id)){
                abort(404);
            }

            // Route Name
            $routeName = Route::current()->getName();
            if(str_contains($routeName, 'order-create') || str_contains($routeName, 'payment-initiate')){
                if(!($this->application->application_stage == ApplicationStage::APP_PAYMENT_APPROVED)){
                    throw new AuthorizationException();
                } 
            }elseif(str_contains($routeName, 'approvePayment')){
                if(!($this->application->application_stage == ApplicationStage::APP_APPROVED)){
                    throw new AuthorizationException();
                }
            }elseif(str_contains($routeName, 'schedule-assessment')){
                if(empty($this->application->assess_reschedule_count) && $this->application->application_stage == ApplicationStage::APP_SCHEDULED){
                    return true;
                }else{
                    if(!($this->application->application_stage == ApplicationStage::APP_PAYMENT_DONE)){
                        throw new AuthorizationException();
                    }
                } 
            }elseif (str_contains($routeName, 'drdo-approval')) { 
                if(!($this->application->application_stage == ApplicationStage::APP_PENDING_APPROVAL)){
                    throw new AuthorizationException();
                }    
            }elseif (str_contains($routeName, 'submit-application')) { 
                if(!($this->application->application_stage == ApplicationStage::APP_OWNERSHIP)){
                    throw new AuthorizationException();
                }    
            }elseif (str_contains($routeName, 'update-basic')) { 
                if(!(
                    $this->application->application_stage == ApplicationStage::APP_BASIC
                    || $this->application->application_stage == ApplicationStage::APP_FINANCIAL
                    || $this->application->application_stage == ApplicationStage::APP_OWNERSHIP
                )){
                    throw new AuthorizationException();
                }    
            }elseif(str_contains($routeName, 'update-financial')){
                if(!(
                    $this->application->application_stage == ApplicationStage::APP_BASIC
                    ||$this->application->application_stage == ApplicationStage::APP_FINANCIAL
                    || $this->application->application_stage == ApplicationStage::APP_OWNERSHIP
                )){
                    throw new AuthorizationException();
                }
            }elseif(str_contains($routeName, 'update-ownership')){
                if(!($this->application->application_stage == ApplicationStage::APP_BASIC
                    || $this->application->application_stage == ApplicationStage::APP_OWNERSHIP)){
                    throw new AuthorizationException();
                }
            }
        }
    }

    /**
     * Function to check if user is allowed to create application or not
     */
    public function create(User $user)
    {
        $application = Application::whereHas('assessment', function($que){
          $que->whereDate('certification_expiry', '>', date("Y-m-d", strtotime("+3 Months")));
        })->where('user_id', $user->id)->first();
        if(!empty($application->id)){
            throw ValidationException::withMessages([
                'application' => __('api.application_already_exist')
            ]);
        }
        return request()->user()->can('create-application');
    }

    /**
     * Function to check if user is allowed to update application or not
     */
    public function update(User $user)
    {
        if($user->hasAnyRole([UserRole::DRDO, UserRole::ADMIN])){
            return true;
        }
        return (request()->user()->can('update-application')) && ($user->id === $this->application->user_id);
    }

    /**
     * Function to check if user is allowed to view application or not
     */
    public function view(User $user)
    {
        if($user->hasAnyRole([UserRole::DRDO, UserRole::ADMIN])){
            return true;
        }
        return (request()->user()->can('view-application')) && (empty($this->application) || ($user->id === $this->application->user_id));
    }

    public function downloadReport(User $user)
    {
        if($user->hasAnyRole([UserRole::DRDO, UserRole::ADMIN])){
            return true;
        }
        return (request()->user()->can('view-application')) && (empty($this->application) || ($user->id === $this->application->user_id));
    }

    /**
     * Function to check if user is allowed to make payment for a application or not
     */
    public function payment(User $user)
    {
        return (request()->user()->can('update-application')) && ($user->id === $this->application->user_id);
    }

    /**
     * Function to check if user is allowed to delete application or not
     */
    public function delete(User $user)
    {
        return (request()->user()->can('delete-application')) && ($user->id === $this->application->user_id);
    }

    /**
     * Function to check if user is allowed to submit application feedback ot not
     */
    public function submitFeedback(User $user)
    {
        return ($user->id === $this->application->user_id);
    }

    /**
     * Function to check if user is allowed to view application feedback ot not
     */
    public function viewFeedback(User $user)
    {
        if($user->hasAnyRole([UserRole::DRDO, UserRole::ADMIN])){
            return true;
        }
        return ($user->id === $this->application->user_id);
    }
}
