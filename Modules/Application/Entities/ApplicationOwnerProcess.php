<?php

namespace Modules\Application\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Core\Helpers\BasicHelper;
use Modules\Attachment\Entities\Attachment;
use Modules\Core\Enums\AttachmentTypes;

class ApplicationOwnerProcess extends Model
{
    protected $table = "application_step_2";
    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with =[];

    protected $fillable = [
        "admin_manpower_count", "product_manufactured", "key_domestic_customers", "key_raw_materials",
        "key_processes", "processes_outsourced", "third_party_cert", "is_competent_banned",
        "competent_banned_details", "is_enquiry_against_firm", "enquiry_against_firm_detail",
        "app_data_confirmation", "app_acceptance_confirmation"
    ];
    
    protected $appends = [];
    
    protected $hidden = ['id', 'application_id',
    'last_modified_by', 'deleted_at', 'created_at', 'updated_at'];
    
    /**
     * Get the application that owns the ownserships.
     */
    public function application()
    {
        return $this->belongsTo('Modules\Application\Entities\Application');
    }
}
