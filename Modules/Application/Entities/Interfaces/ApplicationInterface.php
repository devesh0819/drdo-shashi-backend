<?php

namespace Modules\Application\Entities\Interfaces;

interface ApplicationInterface
{

    const ID = 'id';
    const USER_ID = 'user_id';
    const APPLICATION_UUID = 'application_uuid';
    const APPLICATION_STAGE='application_stage';
    const APPLICATION_NUMBER = 'application_number';

    const ENTREPRENEUR_NAME='entrepreneur_name';
    const SOCIAL_CATEGORY='social_category';
    const SPECIAL_CATEGORY='special_category';
    const ENTERPRISE_NAME='enterprise_name';
    const ENTERPRISE_DETAILS='enterprise_details';
    const ENTERPRISE_NATURE='enterprise_nature';
    const BUSINESS_NATURE = 'business_nature';
    const ENTERPRISE_TYPE = 'enterprice_type';
    const UAM_NUMBER = 'uam_number';
    const CIN_NUMBER = 'cin_number';
    const ORGANISATION_TYPE='organisation_type';
    
    const REGISTRATION_NUMBER_TYPE='registration_number_type';
    const REGISTRATION_NUMBER='registration_number';
    const REGISTERED_ADDRESS='registered_address';
    const REGISTERED_STATE='registered_state';
    const REGISTERED_DISTRICT='registered_district';
    const REGISTERED_PIN='registered_pin';
    const REGISTERED_MOBILE_NUMBER='registered_mobile_number';
    const REGISTERED_LANDLINE_NUMBER = 'registered_landline_number';
    const REGISTERED_EMAIL='registered_email';
    const REGISTERED_ALTERNATE_EMAIL='registered_alternate_email';
    
    const UNIT_ADDRESS='unit_address';
    const UNIT_STATE='unit_state';
    const UNIT_DISTRICT='unit_district';
    const UNIT_PIN='unit_pin';
    const UNIT_MOBILE_NUMBER='unit_mobile_number';
    const UNIT_LANDLINE_NUMBER='unit_landline_number';
    const UNIT_EMAIL='unit_email';
    const UNIT_ALTERNATE_EMAIL='unit_alternate_email';
    const UNIT_WEB_EMAIL = 'unit_web_url';
    
    const ENTERPRISE_COMMENCEMENT_DATE='enterprise_commencement_date';
    const CPCB_CODE='cpcb_code';
    const PERSONS_EMPLOYED='persons_employed';
    const SUPPLIER_TO_DEFENCE='supplier_to_defence';
    
    const POLICE_JURISDICTION = 'police_jurisdiction';
    const POLICE_JURISDICTION_ADDRESS = 'police_jurisdiction_address';

    const BASIC_DETAILS_CONFIRMED='basic_details_confirmed';
    const STATUS = 'status';

    const CREATED_DATETIME = 'created_at';
    const UPDATED_DATETIME = 'updated_at';
    const LAST_MODIFIED_BY = 'last_modified_by';
    const DELETED_AT = 'deleted_at';
    
    /**
     * Getters
     */
    public function getID();
    public function getUserID();
    public function getApplicationUUID();
    public function getApplicationStage();
    public function getApplicationNumber();
    
    public function getEntrepreneurName();
    public function getSocialCategory();
    public function getSpecialCategory();
    public function getEnterpriseName();
    
    public function getCreatedAt();
    public function getUpdatedAt();
    public function getLastModifiedBy();
    public function getDeletedAt();
}
