<?php

namespace Modules\Application\Entities\Interfaces;

interface ApplicationProcessInterface
{

    const ID = "id";
    const APPLICATION_ID = "application_id";
    const MSME_REPRESENTATIVE_SELFIE= "msme_representative_selfie";
    const SITE_EXTERIOR_PICTURE= "site_exterior_picture";
    const FINANCIAL_AUDIT_CONDUCTED= "financial_audit_conducted";
    const PRODUCT_MANUFACTURED= "product_manufactured";
    const KEY_DOMESTIC_CUSTOMERS= "key_domestic_customers";
    const KEY_INTERNATIONAL_CUSTOMERS= "key_international_customers";
    const IMPORTED_RAW_MATERIAL= "imported_raw_material";
    const RAW_MATERIALS_NAME= "raw_materials_name";
    const PROCESSES_OUTSOURCED= "processes_outsourced";
    const ENERGY_SOURCES= "energy_sources";
    const CREATED_DATETIME = "created_at";
    const UPDATED_DATETIME = "updated_at";
    const LAST_MODIFIED_BY= "last_modified_by";
    const DELETED_AT= "deleted_at";

    /**
     * Getters
     */
    public function getId();
    public function getApplicationID();
    public function getMsmeRepresentativeSelfie();
    public function getSiteExteriorPicture();
    public function getFinancialAuditConducted();
    public function getProductManufactured();
    public function getKeyDomesticCustomers();
    public function getKeyInternationalCustomers();
    public function getImportedRawMaterial();
    public function getRawMaterialsName();
    public function getProcessesOutsourced();
    public function getEnergySources();
    public function getCreatedAt();
    public function getUpdatedAt();
    public function getLastModifiedBy();
    public function getDeletedAt();
}
