<?php

namespace Modules\Application\Entities\Interfaces;

interface ApplicationOwnershipInterface
{

    const ID = "id";
    const APPLICATION_ID = "application_id";
    const KEY_CUSTOMERS = "key_customers";
    const KEY_PROCESSES= "key_processes";
    const KEY_RAW_MATERIAL= "key_raw_material";
    const KEY_VENDORS= "key_vendors";
    const TESTING_METHODOLOGY= "testing_methodology";
    const ACCEPTANCE_QUALITY_LEVEL= "acceptance_quality_level";
    const ENERGY_RESOURCES_USED= "energy_resources_used";
    const NATURAL_RESOURCES_USED= "natural_resources_used";
    const ENVIRONMENT_CONSENTS_NAME= "environment_consents_name";
    const TURNOVER_3_YEAR= "turnover_3_year";
    const PROFIT_3_YEAR= "profit_3_year";
    const INVENTORY_TURNOVER_RATIO= "inventory_turnover_ratio";
    const CREATED_DATETIME = "created_at";
    const UPDATED_DATETIME = "updated_at";
    const LAST_MODIFIED_BY= "last_modified_by";
    const DELETED_AT= "deleted_at";

    /**
     * Getters
     */
    public function getID();
    public function getApplicationID();
    public function getKeyCustomers();
    public function getKeyProcesses();
    public function getKeyRawMaterial();
    public function getKeyVendors();
    public function getTestingMethodology();
    public function getAcceptanceQualityLevel();
    public function getEnergyResourcesUsed();
    public function getNaturalResourcesUsed();
    public function getEnvironmentConsentsName();
    public function getTurnover3Year();
    public function getProfit3Year();
    public function getInventoryTurnoverRatio();
    public function getCreatedAt();
    public function getUpdatedAt();
    public function getLastModifiedBy();
    public function getDeletedAt();
}
