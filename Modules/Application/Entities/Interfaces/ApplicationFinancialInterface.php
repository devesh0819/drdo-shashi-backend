<?php

namespace Modules\Application\Entities\Interfaces;

interface ApplicationFinancialInterface
{

    const ID = "id";
    const APPLICATION_ID = "application_id";
    const ENTREPRENEUR_AADHAR_NUMBER = "entrepreneur_aadhar_number";
    const ENTREPRENEUR_GST_NUMBER = "entrepreneur_gst_number";
    const TAN_NUMBER = "tan_number";
    const FINANCIAL_DETAILS_CONFIRMED = "financial_details_confirmed";
    const CREATED_DATETIME = "created_at";
    const UPDATED_DATETIME = "updated_at";
    const LAST_MODIFIED_BY = "last_modified_by";
    const DELETED_AT = "deleted_at";

    /**
     * Getters
     */
    public function getId();
    public function getApplicationId();
    public function getEntrepreneurAadharNumber();
    public function getEntrepreneurGstNumber();
    public function getTanNumber(); 
    public function getFinancialDetailsConfirmed();
    public function getCreatedAt();
    public function getUpdatedAt();
    public function getLastModifiedBy();
    public function getDeletedAt();
}
