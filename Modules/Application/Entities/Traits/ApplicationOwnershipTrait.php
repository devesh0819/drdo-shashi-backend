<?php

namespace Modules\Application\Entities\Traits;

trait ApplicationOwnershipTrait
{

    public function getId()
    {
        return !empty($this->{$this::ID}) ? $this->{$this::ID} : null;
    }
    
    public function getApplicationId()
    {
        return !empty($this->{$this::APPLICATION_ID}) ? $this->{$this::APPLICATION_ID} : null;
    }

    public function getKeyCustomers()
    {
        return !empty($this->{$this::KEY_CUSTOMERS}) ? $this->{$this::KEY_CUSTOMERS} : null;
    }

    public function getKeyProcesses()
    {
        return !empty($this->{$this::KEY_PROCESSES}) ? $this->{$this::KEY_PROCESSES} : null;
    }

    public function getKeyRawMaterial()
    {
        return !empty($this->{$this::KEY_RAW_MATERIAL}) ? $this->{$this::KEY_RAW_MATERIAL} : null;
    }

    public function getKeyVendors()
    {
        return !empty($this->{$this::KEY_VENDORS}) ? $this->{$this::KEY_VENDORS} : null;
    }

    public function getTestingMethodology()
    {
        return !empty($this->{$this::TESTING_METHODOLOGY}) ? $this->{$this::TESTING_METHODOLOGY} : null;
    }

    public function getAcceptanceQualityLevel()
    {
        return !empty($this->{$this::ACCEPTANCE_QUALITY_LEVEL}) ? $this->{$this::ACCEPTANCE_QUALITY_LEVEL} : null;
    }

    public function getEnergyResourcesUsed()
    {
        return !empty($this->{$this::ENERGY_RESOURCES_USED}) ? $this->{$this::ENERGY_RESOURCES_USED} : null;
    }

    public function getNaturalResourcesUsed()
    {
        return !empty($this->{$this::NATURAL_RESOURCES_USED}) ? $this->{$this::NATURAL_RESOURCES_USED} : null;
    }

    public function getEnvironmentConsentsName()
    {
        return !empty($this->{$this::ENVIRONMENT_CONSENTS_NAME}) ? $this->{$this::ENVIRONMENT_CONSENTS_NAME} : null;
    }

    public function getTurnover3Year()
    {
        return !empty($this->{$this::TURNOVER_3_YEAR}) ? $this->{$this::TURNOVER_3_YEAR} : null;
    }

    public function getProfit3Year()
    {
        return !empty($this->{$this::PROFIT_3_YEAR}) ? $this->{$this::PROFIT_3_YEAR} : null;
    }

    public function getInventoryTurnoverRatio()
    {
        return !empty($this->{$this::INVENTORY_TURNOVER_RATIO}) ? $this->{$this::INVENTORY_TURNOVER_RATIO} : null;
    }

    public function getCreatedAt()
    {
        return !empty($this->{$this::CREATED_AT}) ? $this->{$this::CREATED_AT} : null;
    }

    public function getUpdatedAt()
    {
        return !empty($this->{$this::UPDATED_AT}) ? $this->{$this::UPDATED_AT} : null;
    }

    public function getLastModifiedBy()
    {
        return !empty($this->{$this::LAST_MODIFIED_BY}) ? $this->{$this::LAST_MODIFIED_BY} : null;
    }

    public function getDeletedAt()
    {
        return !empty($this->{$this::DELETED_AT}) ? $this->{$this::DELETED_AT} : null;
    }
}
