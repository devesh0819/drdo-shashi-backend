<?php

namespace Modules\Application\Entities\Traits;

trait ApplicationTrait
{

    /**
     * Getters
     */
    public function getID()
    {
        return !empty($this->{$this::ID}) ? $this->{$this::ID} : null;
    }

    public function getUserID()
    {
        return !empty($this->{$this::USER_ID}) ? $this->{$this::USER_ID} : null;
    }

    public function getApplicationUUID()
    {
        return !empty($this->{$this::APPLICATION_UUID}) ? $this->{$this::APPLICATION_UUID} : null;
    }

    public function getApplicationStage()
    {
        return !empty($this->{$this::APPLICATION_STAGE}) ? $this->{$this::APPLICATION_STAGE} : null;
    }

    public function getApplicationNumber()
    {
        return !empty($this->{$this::APPLICATION_NUMBER}) ? $this->{$this::APPLICATION_NUMBER} : null;
    }

    public function getEntrepreneurName(){
        return !empty($this->{$this::ENTREPRENEUR_NAME}) ? $this->{$this::ENTREPRENEUR_NAME} : null;
    }

    public function getSocialCategory()
    {
        return !empty($this->{$this::SOCIAL_CATEGORY}) ? $this->{$this::SOCIAL_CATEGORY} : null;
    }

    public function getSpecialCategory()
    {
        return !empty($this->{$this::SPECIAL_CATEGORY}) ? $this->{$this::SPECIAL_CATEGORY} : null;
    }

    public function getEnterpriseName()
    {
        return !empty($this->{$this::ENTERPRISE_NAME}) ? $this->{$this::ENTERPRISE_NAME} : null;
    }

    public function getCreatedAt()
    {
        return !empty($this->{$this::CREATED_AT}) ? $this->{$this::CREATED_AT} : null;
    }

    public function getUpdatedAt()
    {
        return !empty($this->{$this::UPDATED_AT}) ? $this->{$this::UPDATED_AT} : null;
    }

    public function getLastModifiedBy()
    {
        return !empty($this->{$this::LAST_MODIFIED_BY}) ? $this->{$this::LAST_MODIFIED_BY} : null;
    }

    public function getDeletedAt()
    {
        return !empty($this->{$this::DELETED_AT}) ? $this->{$this::DELETED_AT} : null;
    }
}
