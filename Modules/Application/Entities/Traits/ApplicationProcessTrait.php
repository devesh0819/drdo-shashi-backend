<?php

namespace Modules\Application\Entities\Traits;

trait ApplicationProcessTrait
{

    public function getId()
    {
        return !empty($this->{$this::ID}) ? $this->{$this::ID} : null;
    }
    
    public function getApplicationId()
    {
        return !empty($this->{$this::APPLICATION_ID}) ? $this->{$this::APPLICATION_ID} : null;
    }
    
    public function getMsmeRepresentativeSelfie()
    {
        return !empty($this->{$this::MSME_REPRESENTATIVE_SELFIE}) ? $this->{$this::MSME_REPRESENTATIVE_SELFIE} : null;
    }

    public function getSiteExteriorPicture()
    {
        return !empty($this->{$this::SITE_EXTERIOR_PICTURE}) ? $this->{$this::SITE_EXTERIOR_PICTURE} : null;
    }

    public function getFinancialAuditConducted()
    {
        return !empty($this->{$this::FINANCIAL_AUDIT_CONDUCTED}) ? $this->{$this::FINANCIAL_AUDIT_CONDUCTED} : null;
    }

    public function getProductManufactured()
    {
        return !empty($this->{$this::PRODUCT_MANUFACTURED}) ? $this->{$this::PRODUCT_MANUFACTURED} : null;
    }

    public function getKeyDomesticCustomers()
    {
        return !empty($this->{$this::KEY_DOMESTIC_CUSTOMERS}) ? $this->{$this::KEY_DOMESTIC_CUSTOMERS} : null;
    }

    public function getKeyInternationalCustomers()
    {
        return !empty($this->{$this::KEY_INTERNATIONAL_CUSTOMERS}) ? $this->{$this::KEY_INTERNATIONAL_CUSTOMERS} : null;
    }

    public function getImportedRawMaterial()
    {
        return !empty($this->{$this::IMPORTED_RAW_MATERIAL}) ? $this->{$this::IMPORTED_RAW_MATERIAL} : null;
    }

    public function getRawMaterialsName()
    {
        return !empty($this->{$this::RAW_MATERIALS_NAME}) ? $this->{$this::RAW_MATERIALS_NAME} : null;
    }

    public function getProcessesOutsourced()
    {
        return !empty($this->{$this::PROCESSES_OUTSOURCED}) ? $this->{$this::PROCESSES_OUTSOURCED} : null;
    }

    public function getEnergySources()
    {
        return !empty($this->{$this::ENERGY_SOURCES}) ? $this->{$this::ENERGY_SOURCES} : null;
    }
    
    public function getCreatedAt()
    {
        return !empty($this->{$this::CREATED_AT}) ? $this->{$this::CREATED_AT} : null;
    }

    public function getUpdatedAt()
    {
        return !empty($this->{$this::UPDATED_AT}) ? $this->{$this::UPDATED_AT} : null;
    }

    public function getLastModifiedBy()
    {
        return !empty($this->{$this::LAST_MODIFIED_BY}) ? $this->{$this::LAST_MODIFIED_BY} : null;
    }

    public function getDeletedAt()
    {
        return !empty($this->{$this::DELETED_AT}) ? $this->{$this::DELETED_AT} : null;
    }
}
