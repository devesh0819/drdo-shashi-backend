<?php

namespace Modules\Application\Entities;

use Illuminate\Database\Eloquent\Model;

use Modules\User\Entities\UserProfile;

class AssessFeedback extends Model
{
    protected $table = 'assessment_feedbacks';
    protected $with = ['profile'];
    protected $fillable = ["assessor_professinalism", "assessor_knowledge","overall_process", "assessment_usefulness"
    ];
    protected $hidden = ['id', 'last_modified_by','created_at', 'updated_at', 'deleted_at'];
    
    public function profile()
    {   
        return $this->hasOne(UserProfile::class, 'user_id');
    }
}
