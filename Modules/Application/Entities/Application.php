<?php

namespace Modules\Application\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Application\Entities\Interfaces\ApplicationInterface;
use Modules\Application\Entities\Traits\ApplicationTrait;
use Modules\Core\Entities\AssessmentPlan;
use Modules\Payment\Entities\PaymentOrder;
use Modules\Core\Enums\PaymentStage;
use Modules\Core\Entities\State;
use Modules\Core\Entities\District;
use Modules\Assessment\Entities\Assessment;
use Modules\User\Entities\UserProfile;
use Modules\Core\Helpers\BasicHelper;
use Modules\Attachment\Entities\Attachment;

class Application extends Model implements ApplicationInterface
{

    use ApplicationTrait;

    protected $table = "application_step_1";
    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = [
        'vendorCertificate', 'siteExteriorPicture', 'incorporationCertificate',
        'udyamCertificate', 'entrepreneurGSTCertificate', 'registeredState',
        'registeredDistrict', 'unitState', 'unitDistrict', 'feedback'
    ];
    
    protected $fillable = [
        "is_existing_vendor", "vendor_certificate", "enterprise_name", 
        "enterprice_nature", "enterprice_type", "enterprise_commencement_date",
        "registered_address", "registered_state", "registered_district", "registered_pin",
        "registered_mobile_number", "registered_landline_number", "registered_email", 
        "registered_alternate_email",
        "unit_address_confirmed", "unit_address", "unit_state", "unit_district", "unit_pin", "unit_mobile_number", "unit_landline_number",
        "unit_email","unit_alternate_email","unit_commencement_date",
        "drdo_registration_number", "poc_name", "poc_designation", "poc_mobile_number",
        "entrepreneur_gst_certificate", "uam_number", "udyam_certificate", "site_exterior_picture",
        "entrepreneur_gst_number", "incorporation_certificate"

    ];
    
    protected $appends = [
    'vendor_certificate_url', 'incorporation_certificate_url',
     'udyam_certificate_url', 'entrepreneur_gst_certi_url', 'site_exterior_picture_url'];
    
    
    protected $hidden = [
        'id', 'user_id', 'status', 'last_modified_by',
        'deleted_at', 'created_at', 'updated_at'
    ];
    
    public function user()
    {
        return $this->belongsTo(UserProfile::class, 'user_id', 'user_id');
    }
    
    /**
     * Get the assessment plan.
     */
    public function assessmentPlan()
    {
        return $this->hasOne(AssessmentPlan::class, 'enterprice_type', 'enterprice_type');
    } 

    /**
     * Get The Order
     */
    public function paymentOrder()
    {
        return $this->hasOne(PaymentOrder::class, 'application_id')->whereIn('status',[ PaymentStage::PENDING, PaymentStage::COMPLETE]);
    }

    public function registeredState()
    {
        return $this->belongsTo(State::class, 'registered_state');
    }

    public function registeredDistrict()
    {
        return $this->belongsTo(District::class, 'registered_district');
    }

    public function unitState()
    {
        return $this->belongsTo(State::class, 'unit_state');
    }

    public function unitDistrict()
    {
        return $this->belongsTo(District::class, 'unit_district');
    }

    public function assessment()
    {
        return $this->hasOne(Assessment::class, 'application_id', 'id');
    }
    public function feedback()
    {
        return $this->hasOne(AssessFeedback::class,'application_id');
    }

    /**
     * Get the application ownership.
     */
    public function ownership()
    {
        return $this->hasOne('Modules\Application\Entities\ApplicationOwnerProcess', 'application_id');
    }

    /**
     * FUnction to get EcsFormAttribute
     */
    public function getVendorCertificateUrlAttribute()
    {
        return !empty($this->vendorCertificate->path) ? BasicHelper::getFileURL($this->vendorCertificate) : null;
    }

    public function getIncorporationCertificateUrlAttribute()
    {
        return !empty($this->incorporationCertificate->path) ? BasicHelper::getFileURL($this->incorporationCertificate) : null;
    }

    public function getUdyamCertificateUrlAttribute()
    {
        return !empty($this->udyamCertificate->path) ? BasicHelper::getFileURL($this->udyamCertificate) : null;
    }

    public function getEntrepreneurGstCertiUrlAttribute()
    {
        return !empty($this->entrepreneurGSTCertificate->path) ? BasicHelper::getFileURL($this->entrepreneurGSTCertificate) : null;
    }

    public function getSiteExteriorPictureUrlAttribute()
    {
        return !empty($this->siteExteriorPicture->path) ? BasicHelper::getFileURL($this->siteExteriorPicture) : null;
    }

    public function vendorCertificate()
    {
        return $this->belongsTo(Attachment::class, 'vendor_certificate' , 'id');
    }

    public function incorporationCertificate()
    {
        return $this->belongsTo(Attachment::class, 'incorporation_certificate' , 'id');
    }

    public function udyamCertificate()
    {
        return $this->belongsTo(Attachment::class, 'udyam_certificate' , 'id');
    }

    public function entrepreneurGSTCertificate()
    {
        return $this->belongsTo(Attachment::class, 'entrepreneur_gst_certificate' , 'id');
    }
    
    public function siteExteriorPicture()
    {
        return $this->belongsTo(Attachment::class, 'site_exterior_picture' , 'id');
    }

}
