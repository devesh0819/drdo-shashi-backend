<?php

namespace Modules\Application\Repositories\Implementations;

use Modules\Application\Repositories\Interfaces\ApplicationInterface;
use Modules\Application\Entities\Application;
use Modules\Application\Entities\ApplicationOwnership;
use Modules\User\Entities\User;
use Illuminate\Support\Str;
use Modules\Application\Entities\ApplicationOwnerProcess;
use Modules\Core\Enums\ApplicationStage;
use Modules\Core\Enums\Flag;
use Modules\Core\Enums\UserRole;
use Modules\Core\Helpers\BasicHelper;
use Modules\Payment\Events\NewOrderCreated;
use Illuminate\Support\Facades\DB;
use Modules\Application\Entities\AssessFeedback;
use Modules\Core\Entities\FeedbackQuestionSetting;
use Modules\Core\Enums\ParameterResponses;
use Modules\Notification\Notifications\ApplicationSubmittedToDRDO;
use Modules\Payment\Entities\PaymentOrder;
use Modules\Core\Enums\PaymentStage;

class ApplicationRepository implements ApplicationInterface
{
    private $limit = 10;

    /**
     * Getter
     * @param type $property
     * @return type
     */
    public function __get($property)
    {
        return $this->$property;
    }

    /**
     * Setter
     * @param type $property
     * @param type $value
     * @return $this
     */
    public function __set($property, $value)
    {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
        return $this;
    }

    
    public function getStats($filters=[], $countOnly = false)
    {
        $data = [];
        $allApplications = Application::with('paymentOrder')->get();
        
        return $this->allStats($allApplications);
    }


    /**
     * Function to get amount stats 
     */
    private function allStats($allApplications)
    {
        $stats = [
            'amount_spent'=>[
                'amount_Alloted'=>0,
                'total_amount_spent'=>0,
                'micro_amount_spent'=>0,
                'small_amount_spent'=>0,
            ],
            'application_count'=>[
                'micro'=>0,
                'small'=>0,
                'medium'=>0,
                'large'=>0,
            ],
            'subsidised_count'=>[
                'micro'=>[
                    'total'=>0,
                    'subsidised'=>0,
                    'nonsubsidised'=>0,
                ],
                'small'=>[
                    'total'=>0,
                    'subsidised'=>0,
                    'nonsubsidised'=>0,
                ],
                'medium'=>[
                    'total'=>0,
                    'subsidised'=>0,
                    'nonsubsidised'=>0,
                ],
                'large'=>[
                    'total'=>0,
                    'subsidised'=>0,
                    'nonsubsidised'=>0,
                ]
            ],
            'certification_count'=>[
                'micro'=>[
                    'gold'=>0,
                    'bronze'=>0,
                    'silver'=>0,
                    'diamond'=>0,
                    'platinum'=>0
                ],
                'small'=>[
                    'gold'=>0,
                    'bronze'=>0,
                    'silver'=>0,
                    'diamond'=>0,
                    'platinum'=>0
                ],
                'medium'=>[
                    'gold'=>0,
                    'bronze'=>0,
                    'silver'=>0,
                    'diamond'=>0,
                    'platinum'=>0
                ],
                'large'=>[
                    'gold'=>0,
                    'bronze'=>0,
                    'silver'=>0,
                    'diamond'=>0,
                    'platinum'=>0
                ]
            ],
            'assessment_status'=>[
                'micro'=>[
                    'total'=>0,
                    'completed'=>0,
                    'pending'=>0,
                ],
                'small'=>[
                    'total'=>0,
                    'completed'=>0,
                    'pending'=>0,
                ],
                'medium'=>[
                    'total'=>0,
                    'completed'=>0,
                    'pending'=>0,
                ],
                'large'=>[
                    'total'=>0,
                    'completed'=>0,
                    'pending'=>0,
                ]
            ]
        ];
        
        if(!empty($allApplications)){
            foreach($allApplications as $application){
                if(!empty($application)){
                    if(!empty($application->paymentOrder)){
                        $amountAllocated = ($application->paymentOrder->amount*$application->paymentOrder->discount)/100;
                    }else{
                        $amountAllocated = 0;
                    }
                    $stats['amount_spent']['amount_Alloted']  = $stats['amount_spent']['amount_Alloted'] + $amountAllocated;
                    
                    if(!empty($application->assessment) && !in_array($application->application_stage,[ApplicationStage::APP_ASSESS_COMPLETE, ApplicationStage::APP_FEEDBACK_COMPLETE])){
                        if($application->enterprice_type == 'MIC'){
                            $stats['assessment_status']['micro']['total']++;
                            $stats['assessment_status']['micro']['pending']++;
                            $stats['application_count']['micro']++;
                        }elseif($application->enterprice_type == 'SM'){ 
                            $stats['assessment_status']['small']['total']++;
                            $stats['assessment_status']['small']['pending']++;
                            $stats['application_count']['small']++;
                        }elseif($application->enterprice_type == 'MED'){
                            $stats['assessment_status']['medium']['total']++;
                            $stats['assessment_status']['medium']['pending']++;
                            $stats['application_count']['medium']++;
                        }else{
                            $stats['assessment_status']['large']['total']++;
                            $stats['assessment_status']['large']['pending']++;
                            $stats['application_count']['large']++;
                        }
                    }
                    if(in_array($application->application_stage,[ApplicationStage::APP_ASSESS_COMPLETE, ApplicationStage::APP_FEEDBACK_COMPLETE])){
                        $stats['amount_spent']['total_amount_spent'] = $stats['amount_spent']['total_amount_spent']+ $amountAllocated;
                        
                        if($application->enterprice_type == 'MIC'){
                            $stats['amount_spent']['micro_amount_spent'] = $stats['amount_spent']['micro_amount_spent']+$amountAllocated;
                            $stats['application_count']['micro']++;
                            $stats['subsidised_count']['micro']['total']++;
                            if(!empty($application->subsidy_approved) && $amountAllocated > 0){
                                $stats['subsidised_count']['micro']['subsidised']++;
                            }else{
                                $stats['subsidised_count']['micro']['nonsubsidised']++;
                            }
                            if(!empty($application->assessment->certification_type)){
                                $stats['certification_count']['micro'][$application->assessment->certification_type]++;
                            }
                            $stats['assessment_status']['micro']['total']++;
                            $stats['assessment_status']['micro']['completed']++;
                        }elseif($application->enterprice_type == 'SM'){
                            $stats['amount_spent']['small_amount_spent'] = $stats['amount_spent']['small_amount_spent']+$amountAllocated;
                            $stats['application_count']['small']++;
                            
                            $stats['subsidised_count']['small']['total']++;
                            if(!empty($application->subsidy_approved) && $amountAllocated > 0){
                                $stats['subsidised_count']['small']['subsidised']++;
                            }else{
                                $stats['subsidised_count']['small']['nonsubsidised']++;
                            }
                            if(!empty($application->assessment->certification_type)){
                                $stats['certification_count']['small'][$application->assessment->certification_type]++;
                            }
                            $stats['assessment_status']['small']['total']++;
                            $stats['assessment_status']['small']['completed']++;
                        }elseif($application->enterprice_type == 'MED'){
                            $stats['application_count']['medium']++;
                            
                            $stats['subsidised_count']['medium']['total']++;
                            if(!empty($application->subsidy_approved) && $amountAllocated > 0){
                                $stats['subsidised_count']['medium']['subsidised']++;
                            }else{
                                $stats['subsidised_count']['medium']['nonsubsidised']++;
                            }

                            if(!empty($application->assessment->certification_type)){
                                $stats['certification_count']['medium'][$application->assessment->certification_type]++;
                            }
                            $stats['assessment_status']['medium']['total']++;
                            $stats['assessment_status']['medium']['completed']++;
                        }else{
                            $stats['application_count']['large']++;
                            
                            $stats['subsidised_count']['large']['total']++;
                            if(!empty($application->subsidy_approved) && $amountAllocated > 0){
                                $stats['subsidised_count']['large']['subsidised']++;
                            }else{
                                $stats['subsidised_count']['large']['nonsubsidised']++;
                            }

                            if(!empty($application->assessment->certification_type)){
                                $stats['certification_count']['large'][$application->assessment->certification_type]++;
                            }
                            $stats['assessment_status']['large']['total']++;
                            $stats['assessment_status']['large']['completed']++;
                        }
                    }
                }
            }
        }      
        
        return $stats;
    }

    /**
     * Function to find all application list
     * @param Array $filters
     * @return Array $data
     */
    public function lists($filters=[], $countOnly = false)
    {
        $applications = Application::with(['ownership']);
        if (!empty($filters['status'])) {
            $applications->where('status', $filters['status']);
        }
        if(!empty($filters['application_status'])){
            if($filters['application_status'] == 'in_progress'){
                $applications->whereIn('application_stage', [ApplicationStage::APP_BASIC, ApplicationStage::APP_FINANCIAL, ApplicationStage::APP_OWNERSHIP]);
            }else{
                $applications->where('application_stage', $filters['application_status']);
            }
        }

        if(!empty($filters['assessment_status'])){
            $applications->whereHas('assessment', function($que) use($filters){
                $que->where('assessment_stage', $filters['assessment_status']);
            });
        }

        if(!empty($filters['application_no'])){
            $applications->where('application_number', 'like', '%' . $filters['application_no']);
        }

        if (!empty($filters['enterprise_name'])) {
            $applications->where('enterprise_name', 'like', '%' . $filters['enterprise_name']);
        }
        
        if (!empty($filters['type_of_enterprise'])) {
            $applications->where('enterprice_type', $filters['type_of_enterprise']);
        }

        if(isset($filters['subsidy'])){
            if(!empty($filters['subsidy'])){
                $applications->where('subsidy_approved', $filters['subsidy']);
            }else{
                $applications->where('subsidy_approved', 0);
            }
        }

        if(isset($filters['feedback_received'])){
            if(!empty($filters['feedback_received'])){
                $applications->where('application_stage', ApplicationStage::APP_FEEDBACK_COMPLETE);
            }else{
                $applications->where('application_stage', '!=', ApplicationStage::APP_FEEDBACK_COMPLETE);
            }
        }

        if (!empty($filters['payment_status'])) {
            if($filters['payment_status'] == 'paid'){
                $applications->where(function($que){
                    $que->whereNotNull('assessment_date')->orWhere('application_stage', ApplicationStage::APP_PAYMENT_DONE);
                });
            }else{
                $applications->where(function($que){
                    $que->whereNull('assessment_date')->whereNot('application_stage', ApplicationStage::APP_PAYMENT_DONE);;
                });
            }
        }
        if(!empty($filters['assessment_status'])){
            $applications->where('application_stage', $filters['assessment_status']);
        }

        if(isset($filters['rating'])){
            if(!empty($filters['rating'])){
                $applications->whereHas('assessment', function($que) use($filters){
                    $que->where('certification_type', $filters['rating']);
                });
            }else{
                $applications->whereNotIn('application_stage', [ApplicationStage::APP_ASSESS_COMPLETE, ApplicationStage::APP_FEEDBACK_COMPLETE]);
            }
            
        }
        if(!empty($filters['application_start_date'])){
            $applications->whereDate('created_at', '>=',  date("Y-m-d", strtotime($filters['application_start_date'])));
        }

        if(!empty($filters['application_end_date'])){
            $applications->whereDate('created_at', '<=',  date("Y-m-d", strtotime($filters['application_end_date'])));
        }

        if(!empty($filters['assessment_start_date'])){
            $applications->whereDate('assessment_date', '>=',  date("Y-m-d", strtotime($filters['assessment_start_date'])));
        }

        if(!empty($filters['assessment_end_date'])){
            $applications->whereDate('assessment_date', '<=',  date("Y-m-d", strtotime($filters['assessment_end_date'])));
        }

        if(!empty($filters['role'])){
            if($filters['role'] == UserRole::DRDO){
                $applications->where('application_stage', ApplicationStage::APP_PENDING_APPROVAL);
            }elseif($filters['role'] == UserRole::ADMIN){
                $applications->whereIn('application_stage', [
                    ApplicationStage::APP_APPROVED,
                    ApplicationStage::APP_REJECTED,
                    ApplicationStage::APP_SCHEDULED,
                    ApplicationStage::APP_ASSESS_PROGRESS,
                    ApplicationStage::APP_ASSESS_COMPLETE,
                    ApplicationStage::APP_FEEDBACK_COMPLETE
                ]);
            }
        }
        if(!empty($filters['user_id'])){
            $applications->where('user_id', $filters['user_id']);
        }
        if ($countOnly) {
            return $applications->count();
        }elseif(!empty(request()->get('is_excel'))){
            return $applications->get();
        } else {
            $offset = !empty($filters['start']) ? ($filters['start']-1)*$this->limit : 0;
            return $applications->orderBy("updated_at", "desc")
            ->limit(!empty($filters['length']) ? $filters['length'] : $this->limit)
            ->offset($offset)
            ->get();
        }
    }

    /**
     * Function to save application
     * @param Application $application
     * @param array $data
     * @return Application $application
     */
    public function save(Application $application, array $data)
    {
        if(empty($application->id)){
            $application->user_id = $data['user_id'];
            $application->application_number = BasicHelper::getUniqueApplicationNumber($data);
            $application->application_stage = ApplicationStage::APP_BASIC;
            $application->application_uuid = (string) Str::orderedUuid();
            $application->status = Flag::IS_ACTIVE;
        }
       //$latLong = BasicHelper::getLatLongFromPincode($data['unit_pin']);
        $application->fill($data);
        $application->save();
        return $this->firstornew($application->getApplicationUUID());  
    }
    
    /**
     * Function sync application ownerships
     * 
     * @param Application $application
     * @param array $data
     * @return Application $application
     */
    public function syncApplicationOwnerships(Application $application, Array $data)
    {
        if(!empty($application->id)){
            $applicationOwnership = !empty($application->ownership) ? $application->ownership : new ApplicationOwnerProcess();
            $newRecord = false;
            if(empty($applicationOwnership->id)){
                $applicationOwnership->application_id = $application->id;
                $newRecord = true;
            }
            $applicationOwnership->fill($data);
            if($applicationOwnership->save()){
                if($newRecord){
                    $application->application_stage = ApplicationStage::APP_OWNERSHIP;
                    $application->save();
                }
            }
        }
        return $this->firstornew($application->getApplicationUUID());  
    }

    /**
     * Function sync application financials
     * 
     * @param Application $application
     * @param array $data
     * @return Application $application
     */
    public function syncApplicationFinancials(Application $application, Array $data)
    {
        if(!empty($application->id)){
            $applicationFinancials = !empty($application->financial) ? $application->financial : new ApplicationFinancial();
            $newRecord = false;
            if(empty($applicationFinancials->id)){
                $applicationFinancials->application_id = $application->id;
                $newRecord = true;
            }
            $applicationFinancials->fill($data);
            if($applicationFinancials->save()){
                if($newRecord){
                    $application->application_stage = ApplicationStage::APP_FINANCIAL;
                    $application->save();
                }
            }
        }
        return $this->firstornew($application->getApplicationUUID());
    }

    /**
     * public function submit application 
     */
    public function submitApplication(Application $application)
    {
        if(!empty($application->id)){
            $application->application_stage = ApplicationStage::APP_PENDING_APPROVAL;
            $application->save();
            $drdoAdminEmail = config('core.drdo_admin_email');
            if(!empty($drdoAdminEmail)){
                $drdoAdmin = User::where('email', $drdoAdminEmail)->first();
                if(!empty($drdoAdmin->email)){
                    $drdoAdmin->notify(new ApplicationSubmittedToDRDO($application));
                }
            }
        }
        return $application;
    }

    /**
     * Function to schedule application's assessment
     * @param Application $application
     * @param array $data
     */
    public function scheduleAssessment(Application $application, array $data)
    {
        if(!empty($application->id) && !empty($data['assessment_date'])){
            $application->assess_reschedule_count = !empty($application->assessment_date) ? 1 : null;
            $application->assessment_date = date("Y-m-d H:i:s", strtotime($data['assessment_date']));
            $application->application_stage = ApplicationStage::APP_SCHEDULED;
            $application->save();
            if(!empty($application->assessment)){
                $application->assessment->assessment_date = $application->assessment_date;
                $application->assessment->save();
            }
        }
        return $application;
    }

     /**
     * Function to save Feedback form
     * @param Application $application
     * @param array $data
     */
    public function saveFeedback(Application $application, $data=[])
    {
        if(!empty($application->feedback) && !empty($data['feedback_payload']) && !empty($data['feedback_comment'])){
           $application->feedback->feedback_payload = $data['feedback_payload'];
           $application->feedback->feedback_comment = $data['feedback_comment'];
           if($application->feedback->save()){
                $application->application_stage = ApplicationStage::APP_FEEDBACK_COMPLETE;
                $application->save();
           }
           return $application->feedback;
        }
        return false;
    }


    /**
     * Function to save Feedback configurations
     * @param Array $filters
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function saveFeedbackConfig(FeedbackQuestionSetting $feedback, array $data)
    {

        if(!empty($data)){
            if(empty($feedback->id)){
                $feedback->question_uuid = (string) Str::orderedUuid();
            }
           $feedback->title = $data['title'];
            
            $feedback->save();
            
            return $feedback;
        }
    }


    /**
     * Function to get Feedback configurations
     * @param Array $filters
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function getFeedbackConfig()
    {
        return FeedbackQuestionSetting::all();
    }

    /**
     * FUnction to approve Application
     */
    public function approveApplication(Application $application, $data=[])
    {
        if(!empty($application->id)){
            if(!empty($data['application_stage'])){
                $application->application_stage = $data['application_stage'];
            }
            if(isset($data['subsidy_approved'])){
                $application->subsidy_approved = $data['subsidy_approved'];
            }
            $application->save();
            if($application->getApplicationStage() == ApplicationStage::APP_APPROVED){
                NewOrderCreated::dispatch($application);
            }
        }
        return $application;
    }

    /**
     * Function to get user details by id
     * @param int $id
     * @return User $user
     */
    public function firstornew($id = null)
    {
        if (!empty($id)) {
            return Application::where('application_uuid', $id)->with('ownership')->first();
        }
        return new Application();
    }


    /**
     * Function to get feedback Question details by id
     * @param int $id
     * @return User $user
     */
    public function firstornewFeedback($id = null)
    {
        if(!empty($id)) {
            return FeedbackQuestionSetting::where('question_uuid', $id)->first();
        }
        return new FeedbackQuestionSetting();
    }

    /**
     * Function to delete Feedback Questions
     * @param FeedbackQuestionSetting $feedback
     * @param array $data
     */
    public function deleteFeedback(FeedbackQuestionSetting $feedback)
    {
        return $feedback->delete();
    }

    /**
     * Function to delete user
     * @param Application $user
     * @param array $data
     */
    public function delete(Application $application)
    {
        return $application->delete();
    }
}
