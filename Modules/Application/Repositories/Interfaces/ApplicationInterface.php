<?php

namespace Modules\Application\Repositories\Interfaces;

use Modules\Application\Entities\Application;

use Modules\Core\Entities\FeedbackQuestionSetting;

interface ApplicationInterface
{

    /**
     * Function to find all users list
     * @param Array $filters
     * @param Boolean $countOnly
     * 
     * @return Array $data
     */
    public function getStats($filters=[], $countOnly=false);
    
    /**
     * Function to find all users list
     * @param Array $filters
     * @param Boolean $countOnly
     * 
     * @return Array $data
     */
    public function lists($filters=[], $countOnly=false);

    /**
     * Function to save application
     * @param Application $application
     * @param array $data
     * @return Application $application
     */
    public function save(Application $application, array $data);

    /**
     * Function to delete application
     * @param Application $application
     * @param array $data
     */
    public function delete(Application $application);
    
    /**
     * Function to get application details by id
     * @param type $id
     */
    public function firstornew($id = null);

    /**
     * public function sync application ownerships
     */
    public function syncApplicationOwnerships(Application $application, Array $data);

    /**
     * public function sync application financials
     */
    public function syncApplicationFinancials(Application $application, Array $data);

    /**
     * public function submit application 
     */
    public function submitApplication(Application $application);

    /**
     * Function to schedule application's assessment
     * @param Application $application
     * @param array $data
     */
    public function scheduleAssessment(Application $application, array $data);
    

    /**
     * FUnction to approve Application
     */
    public function approveApplication(Application $application, $data=[]);

    /**
     * Function to save Feedback
     * @param Application $application
     * @param array $data
     */
    public function saveFeedback(Application $application, $data=[]);


   /**
     * Function to save payment configurations
     * @param Array $filters
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function saveFeedbackConfig(FeedbackQuestionSetting $feedback, array $data);


    /**
     * Function to get payment configurations
     * @param Array $filters
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function getFeedbackConfig();


     /**
     * Function to get feedback Question details by id
     * @param type $id
     */
    public function firstornewFeedback($id = null);


    /**
     * Function to delete feedback Question
     * @param Application $application
     * @param array $data
     */
    public function deleteFeedback(FeedbackQuestionSetting $feedback);

}
