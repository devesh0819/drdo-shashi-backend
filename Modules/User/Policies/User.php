<?php

namespace Modules\User\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;

class User
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
}
