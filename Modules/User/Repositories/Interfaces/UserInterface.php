<?php

namespace Modules\User\Repositories\Interfaces;

use Modules\User\Entities\User;

interface UserInterface
{

    /**
     * Function to find all users list
     * @param Array $filters
     * @param Boolean $countOnly
     * 
     * @return Array $data
     */
    public function lists($filters=[], $countOnly=false);

    /**
     * Function to get user details by email
     * @param type $id
     */
    public function findUserByEmail($email);

    /**
     * Function to save user
     * @param User $user
     * @param array $data
     */
    public function save(User $user, array $data);

    /**
     * Function to delete user
     * @param User $user
     * @param array $data
     */
    public function delete(User $user);
    
    /**
     * Function to reset password
     * @param User $user
     * @param String $password
     * @param String $token
     */
    public function resetpassword(User $user, $password, $token);

    /**
     * Function to get user details by id
     * @param type $id
     */
    public function firstornew($id = null);

    /**
     * Function to generate email verification otp
     * @param User $user
     * @param array $data
     */
    public function generateEmailVerifcationOTP(User $user);

    /**
     * Function to verify email verification otp
     * @param User $user
     * @param array $data
     */
    public function verifyOTP(User $user, Array $data);
}
