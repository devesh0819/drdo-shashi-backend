<?php

namespace Modules\User\Repositories\Implementations;

use Modules\User\Repositories\Interfaces\UserInterface;
use Modules\User\Entities\User;
use Illuminate\Support\Facades\Hash;
use Modules\User\Entities\UserProfile;
use Modules\Agency\Entities\AgencyMeta;
use Modules\Assessor\Entities\AssessorMeta;
use Illuminate\Support\Str;
use Illuminate\Auth\Events\PasswordReset;
use Modules\Core\Enums\UserRole;
use Modules\User\Entities\UserOtp;
use Modules\User\Events\NewUserCreated;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\UploadedFile;
use Modules\Attachment\Services\AttachmentService;

class UserRepository implements UserInterface
{
    private $limit = 10;
    private $attachmentService;

    public function __construct(AttachmentService $attachmentService)
    {
        $this->attachmentService = $attachmentService;
    }
    /**
     * Getter
     * @param type $property
     * @return type
     */
    public function __get($property)
    {
        return $this->$property;
    }

    /**
     * Setter
     * @param type $property
     * @param type $value
     * @return $this
     */
    public function __set($property, $value)
    {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
        return $this;
    }

    /**
     * Function to find all users list
     * @param Array $filters
     * @return Array $data
     */
    public function lists($filters=[], $countOnly = false)
    {
        $users = User::with(['profile', 'roles']);
        $users->join('user_profiles', function ($profileJoin) use ($filters) {
            $profileJoin->on('users.id', '=', 'user_profiles.user_id');
            if (!empty($filters['search']["value"])) {
                $profileJoin->where('first_name', 'like', '%' . $filters['search']["value"] . '%')
                        ->orWhere('last_name', 'like', '%' . $filters['search']["value"] . '%');
            }
        });
        if(!empty($filters['work_domain']) || !empty($filters['location'])){
            $users->join('assessor_meta', function ($assessorMeta) use ($filters) {
                $assessorMeta->on('users.id', '=', 'assessor_meta.user_id');
                if (!empty($filters["work_domain"])) {
                    $assessorMeta->where('work_domain', 'like', '%' . $filters["work_domain"] . '%');
                }
                if (!empty($filters["location"])) {
                    $assessorMeta->where('assessor_district', $filters["location"]);
                }
            });
        }
        if (!empty($filters['status'])) {
            $users->where('status', $filters['status']);
        }

        if (!empty($filters['roles'])) {
            $users->join('model_has_roles', function ($roleJoin) use ($filters) {
                $roleJoin->on('users.id', '=', 'model_has_roles.model_id')->where("model_has_roles.role_id", $filters['roles']);
            });
            
        }
           

        if (!empty($filters['from']) && !empty($filters['to'])) {
            $users->whereBetween('users.created_at', [$filters['from'], $filters['to']]);
        }
        if ($countOnly) {
            return $users->count();
        } 
        
        else {
            $offset = !empty($filters['start']) ? ($filters['start']-1)*$this->limit : 0;
            
            if($filters['roles']=='AGENCY'){
                $this->limit=50;
            }
            return $users->select(['users.email', 'users.status', 'users.created_at', 'users.id', 'user_profiles.first_name', 'user_profiles.last_name'])
            ->orderBy("created_at", "desc")
            ->limit(!empty($filters['length']) ? $filters['length'] : $this->limit)
            ->offset($offset)
            ->get();
        }
    }

    /**
     * Function to save user
     * @param User $user
     * @param array $data
     * @return User $user
     */
    public function save(User $user, array $data)
    {
        $newUser= false;
        if(empty($user->id)){
            $newUser = true;
            $user->uuid = (string) Str::orderedUuid();
            
            // Set User Password If Role is Other then Vendor
            $password = mt_rand(100000, 999999);
            $user->username = "SAMAR-".time().rand(0, 99);
            $user->password = Hash::make($password);
        }
        if(!empty($data['email']) && !empty($user->email) && $user->email != $data['email']){
            $user->email_verified_at = null;
        }
        $user->fill($data);
        // IF Role is Vendor Set User Password
        if (!empty($data['password'])) {
            $user->password = Hash::make($data['password']);
        }
        $user->save();
        // Update Unique Username
        if(!empty($user->id) && $newUser){
            $user->username = "SAMAR-".date("dmy")."-".$user->id;
            $user->save();
        }

        // Save profile
        $profile = !empty($user->profile) ? $user->profile : new UserProfile();
        $profile->user_id = $user->id;
        $profile->fill($data);
        $profile->save();

        // Save Roles
        if (!empty($data['roles'])) {
            $user->syncRoles([$data['roles']]);
        }
        
        //Sync Agency Meta
        if($user->hasRole(UserRole::AGENCY)){
            $agencyMeta = !empty($user->agencyMeta) ? $user->agencyMeta : new AgencyMeta();
            $agencyMeta->user_id = $user->id;
            $agencyMeta->fill($data);
            $agencyMeta->save();
        }

        //Sync Assessor Meta
        if($user->hasRole(UserRole::ASSESSOR)){
            if(!empty($data['resume'])){
                $previousResume = !empty($user->assessorMeta->assessorResume) ? $user->assessorMeta->assessorResume : null;
                $data['resume_id'] = $this->uploadResume('resume', $user, 'image', $previousResume);
            } 
            $assessorMeta = !empty($user->assessorMeta) ? $user->assessorMeta : new AssessorMeta();
            $assessorMeta->user_id = $user->id;
            $assessorMeta->fill($data);
            $assessorMeta->save();
        }
        if($newUser && !in_array($data['roles'], [UserRole::VENDOR, UserRole::MEMBER]) && !empty($password)){
            NewUserCreated::dispatch($user, $password);
        }else{
            $user->load('profile');
            if(!empty($data['roles']) && in_array($data['roles'], [UserRole::VENDOR])){
                if(!empty($data['password'])) {
                    NewUserCreated::dispatch($user, $data['password']);
                }
                event(new Registered($user));
            }
        }

        //Create Personal Access Tokens
        if(!empty($data['roles']) && $data['roles'] != UserRole::MEMBER){
            $token = $user->createToken(config('auth.authTokenName'), config('rolespermissions.roles-permissions.'.$data['roles']))->accessToken;
            return ['token'=>$token, 'profile'=>$user->load('profile')];
        }else{
            return $user->load('profile');
        }
    }

    /**
     * Function to upload resume
     */
    private function uploadResume($filename = '', Object $object, $fileType= 'image', $previousAttachID =null)
    {
        $file = ($filename instanceof UploadedFile) ? $filename :request()->file($filename);
        //Upload file
        $attachment = null;
        if ($file instanceof UploadedFile) {
            $objectType = 'user';
            $attachPath = "/assessor_".$object->id."/";
            $attachment = $this->attachmentService->upload($file, $attachPath);
            if(!empty($attachment->id)){
                $objectId = !empty($object->id) ? $object->id : 0;
                $this->attachmentService->attach($attachment->id, $objectId, $objectType, $fileType);
            }
        }
        if(!empty($attachment->id) && !empty($previousAttachID) && is_int($previousAttachID)){
            $this->attachmentService->detach($previousAttachID);
        }
        return !empty($attachment->id) ? $attachment->id : null;
    }
    /**
     * Function to get user details by email
     * @param type $id
     */
    public function findUserByEmail($email)
    {
        return User::where('email', $email)->first();
    }

    /**
     * Function to get user details by id
     * @param int $id
     * @return User $user
     */
    public function firstornew($id = null)
    {
        if (!empty($id)) {
            return User::where('uuid', $id)->with('profile')->first();
        }
        return new User();
    }

    /**
     * Function to generate email verification otp
     * @param User $user
     * @param array $data
     */
    public function generateEmailVerifcationOTP(User $user)
    {
        
    }

    /**
     * Function to reset password
     * @param User $user
     * @param String $password
     * @param String $token
     */
    public function resetpassword(User $user, $password, $token)
    {
        if(!empty($user) && !empty($password) && !empty($token)){
            $user->forceFill([
                'password' => Hash::make($password)
            ])->setRememberToken(Str::random(60));
 
            $user->save();
            event(new PasswordReset($user));
            return true;
        }
        return false;
    }

    /**
     * Function to delete user
     * @param User $user
     * @param array $data
     */
    public function delete(User $user)
    {
        return $user->delete();
    }

    /**
     * Function to verify email verification otp
     * @param User $user
     * @param array $data
     */
    public function verifyOTP(User $user, Array $data)
    {
        if(!empty($user->id) && !empty($data['otp'])){
            $otp =  UserOtp::where(['user_id'=>$user->id, 'otp'=>$data['otp']])
            ->notExpired()->first();
            if(!empty($otp)){
                $otp->delete();
                return true;
            }
        }   
        return false;
    }
}
