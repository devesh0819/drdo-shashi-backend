<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('user_profiles')) {
            Schema::create('user_profiles', function (Blueprint $table) {
                $table->id();
                $table->foreignId('user_id')
                    ->constrained()
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
                $table->string('first_name', 255)->nullable();
                $table->string('last_name', 255)->nullable();
                $table->string('alternate_email', 255)->nullable();
                $table->string('alternate_phone_number', 255)->nullable();
                $table->string('organisation', 255)->nullable();
                $table->string('designation', 255)->nullable();
                $table->string('member_role', 55)->nullable();
                $table->string('phone_number', 255)->nullable();
                $table->string('pan_number', 255)->nullable();
                $table->string('gst_number', 255)->nullable();
                $table->tinyInteger('terms')->default(0);
                $table->unsignedInteger('last_modified_by')->nullable();
                $table->softDeletes();
                $table->timestamp('created_at')->useCurrent();
                $table->timestamp('updated_at')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_profiles');
    }
};
