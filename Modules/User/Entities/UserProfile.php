<?php

namespace Modules\User\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\User\Entities\Interfaces\UserProfileInterface;
use Modules\User\Entities\Traits\UserProfileTrait;

class UserProfile extends Model implements UserProfileInterface
{

    use UserProfileTrait;

    protected $table = "user_profiles";
    
    protected $fillable = ["first_name", "last_name", "phone_number", "pan_number",'member_role',
     "gst_number", "terms", "alternate_email", "alternate_phone_number", "designation", "organisation"];
    
    protected $appends = [];
    
    protected $hidden = [];
    
        
    /**
     * Get the user that owns the profile.
     */
    public function user()
    {
        return $this->belongsTo('Modules\User\Entities\User');
    }

    public function getFullNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }
}
