<?php

namespace Modules\User\Entities\Traits;

trait UserProfileTrait
{

    public function getID()
    {
        return !empty($this->{$this::ID}) ? $this->{$this::ID} : null;
    }

    public function getUserId()
    {
        return !empty($this->{$this::USER_ID}) ? $this->{$this::USER_ID} : null;
    }

    public function getFirstName()
    {
        return !empty($this->{$this::FIRST_NAME}) ? $this->{$this::FIRST_NAME} : null;
    }

    public function getLastName()
    {
        return !empty($this->{$this::LAST_NAME}) ? $this->{$this::LAST_NAME} : null;
    }

    public function getPhoneNumber()
    {
        return !empty($this->{$this::PHONE_NUMBER}) ? $this->{$this::PHONE_NUMBER} : null;
    }

    public function getPanNumber()
    {
        return !empty($this->{$this::PAN_NUMBER}) ? $this->{$this::PAN_NUMBER} : null;
    }

    public function getGSTNumber()
    {
        return !empty($this->{$this::GST_NUMBER}) ? $this->{$this::GST_NUMBER} : null;
    }

    public function getTerms()
    {
        return !empty($this->{$this::TERMS}) ? $this->{$this::TERMS} : null;
    }

    public function getCreatedAt()
    {
        return !empty($this->{$this::CREATED_DATETIME}) ? date("Y-m-d H:i:s", strtotime($this->{$this::CREATED_DATETIME})) : null;
    }

    public function getUpdatedAt()
    {
        return !empty($this->{$this::UPDATED_DATETIME}) ? date("Y-m-d H:i:s", strtotime($this->{$this::UPDATED_DATETIME})) : null;
    }
}
