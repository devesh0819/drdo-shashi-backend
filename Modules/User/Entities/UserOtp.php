<?php

namespace Modules\User\Entities;

use Illuminate\Database\Eloquent\Model;

class UserOtp extends Model 
{
    protected $table = "user_otps";

    /**
     * Scope a query to only include inactive user.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeNotExpired($query)
    {
        return $query->where('expired_at', '>', now());
    }
}
