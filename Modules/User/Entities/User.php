<?php

namespace Modules\User\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Laravel\Passport\HasApiTokens;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Modules\User\Entities\UserProfile;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Modules\Agency\Entities\AgencyMeta;
use Modules\Assessor\Entities\AssessorMeta;
use Modules\User\Entities\UserOtp;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, HasFactory, Notifiable, HasRoles;

    protected $fillable = ['email', 'password','is_verified'];
    protected $guard_name = 'web';
    
    /**
     * Scope a query to only include active user.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('status', Flag::IS_ACTIVE);
    }

    /**
     * Scope a query to only include inactive user.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeInactive($query)
    {
        return $query->where('status', Flag::IS_INACTIVE);
    }

    public function profile()
    {
        return $this->hasOne(UserProfile::class);
    }
    
    public function agencyMeta()
    {
        return $this->hasOne(AgencyMeta::class);
    }

    public function assessorMeta()
    {
        return $this->hasOne(AssessorMeta::class);
    }

    public function validOtp()
    {
        return $this->hasOne(UserOtp::class)->latest();
    }
}
