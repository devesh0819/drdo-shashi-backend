<?php

namespace Modules\User\Entities\Interfaces;

interface UserProfileInterface
{

    const ID = 'id';
    const USER_ID = 'user_id';
    const FIRST_NAME = 'first_name';
    const LAST_NAME = 'last_name';
    const PHONE_NUMBER = 'phone_number';
    const PAN_NUMBER = 'pan_number';
    const GST_NUMBER = 'gst_number';
    const TERMS = 'terms';
    const CREATED_DATETIME = 'created_at';
    const UPDATED_DATETIME = 'updated_at';

    /**
     * Getters
     */
    public function getID();

    public function getUserId();

    public function getFirstName();

    public function getLastName();

    public function getPhoneNumber();

    public function getPanNumber();

    public function getGSTNumber();

    public function getTerms();

    public function getCreatedAt();

    public function getUpdatedAt();
}
