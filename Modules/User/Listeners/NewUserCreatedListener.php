<?php

namespace Modules\User\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\User\Entities\User;
use Modules\Core\Helpers\Logger;
use Modules\Notification\Notifications\NewUserCreated;
use Modules\User\Services\UserService;
use Modules\User\Events\NewUserCreated as NewUserCreatedEvent;
use Exception;

class NewUserCreatedListener implements ShouldQueue
{
    protected $userService;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(NewUserCreatedEvent $event)
    {
        try{
            if(!empty($event->user) && !empty($event->password)){
                $event->user->notify(new NewUserCreated($event->password));
            }
        }catch(Exception $ex){
            Logger::error($ex);
            return $ex;
        }
    }
}
