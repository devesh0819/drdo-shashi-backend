<?php

namespace Modules\User\Observers;

use Modules\User\Entities\User;
use Illuminate\Auth\Events\Registered;
use Modules\User\Entities\UserProfile;

class UserProfileObserver
{
    /**
     * Handle the User "created" event.
     *
     * @param  \Modules\User\Entities\User $user
     * @return void
     */
    public function created(UserProfile $profile)
    {
        
    }

    /**
     * Handle the User "updated" event.
     *
     * @param  \Modules\User\Entities\User $user
     * @return void
     */
    public function updated(UserProfile $user)
    {
    }

    /**
     * Handle the User "deleted" event.
     *
     * @param  \Modules\User\Entities\User $user
     * @return void
     */
    public function deleting(UserProfile $user)
    {
    }
}
