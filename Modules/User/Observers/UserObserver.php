<?php

namespace Modules\User\Observers;

use Modules\User\Entities\User;

class UserObserver
{
    /**
     * Handle the User "created" event.
     *
     * @param  \Modules\User\Entities\User $user
     * @return void
     */
    public function created(User $user)
    {
    }

    /**
     * Handle the User "updated" event.
     *
     * @param  \Modules\User\Entities\User $user
     * @return void
     */
    public function updated(User $user)
    {
    }

    /**
     * Handle the User "deleted" event.
     *
     * @param  \Modules\User\Entities\User $user
     * @return void
     */
    public function deleting(User $user)
    {
    }
}
