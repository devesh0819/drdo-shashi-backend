<?php

namespace Modules\User\Services;

use Modules\User\Repositories\Interfaces\UserInterface;
use Exception;
use Modules\Core\Helpers\Logger;
use Modules\User\Entities\User;

class UserService
{
    private $repository;

    /**
     * Constructor
     * @param UserInterface $repository
     */
    public function __construct(UserInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Function to find all users list
     * @param Array $filters
     * @param Boolean $countOnly
     * @return Array $data
     */
    public function lists($filters=[], $countOnly=false)
    {
        
        try {
            return $this->repository->lists($filters, $countOnly);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

    /**
     * Function to get user details by email
     * @param type $id
     */
    public function findUserByEmail($email)
    {
        try {
            return $this->repository->findUserByEmail($email);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

    /**
     * Function to get user details by id
     * @param type $id
     */
    public function firstornew($id = null)
    {
        try {
            return $this->repository->firstornew($id);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

    /**
     * Function to save user
     * @param User $user
     * @param array $data
     */
    public function save(User $user, $data=[])
    {
        try {
            return $this->repository->save($user, $data);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

    /**
     * Function to delete user
     * @param User $user
     * @param array $data
     */
    public function delete(User $user)
    {
        try {
            return $this->repository->delete($user);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

    /**
     * Function to reset password
     * @param User $user
     * @param String $password
     * @param String $token
     */
    public function resetpassword(User $user, $password, $token){
        try {
            return $this->repository->resetpassword($user, $password, $token);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }
    
    /**
     * Function to generate email verification otp
     * @param User $user
     * @param array $data
     */
    public function generateEmailVerifcationOTP(User $user)
    {
        try {
            return $this->repository->generateEmailVerifcationOTP($user);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }

    /**
     * Function to verify email verification otp
     * @param User $user
     * @param array $data
     */
    public function verifyOTP(User $user, Array $data)
    {
        try {
            return $this->repository->verifyOTP($user, $data);
        } catch (Exception $ex) {
            Logger::error($ex);
            return $ex;
        }
    }
}
