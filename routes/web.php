<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/forgot-password', function () {
    return ['message'=>'Sending Mail for Password'];
})->middleware('guest')->name('password.email');

Route::get('/reset-password', function () {
    return ['message'=>'Resetting Password'];
})->middleware('guest')->name('password.reset');

Route::get('/verify-email', function () {
    return ['message'=>'Verifying Email'];
}) ->middleware(['auth:api', 'throttle:6,1'])
->name('verification.verify');

Route::post('/email/verification-notification', function () {
    return ['message'=>'Sending Email Verification Link'];
}) ->middleware(['auth:api', 'throttle:6,1'])
->name('verification.send');