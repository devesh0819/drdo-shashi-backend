<?php

return [
    'default'   => [
        'length'    => 5,
        'width'     => 120,
        'height'    => 36,
        'quality'   => 90,
        'math'      => false,  //Enable Math Captcha
        'expire'    => 120,    //Stateless/API captcha expiration
    ],
    'disable'=>false
];