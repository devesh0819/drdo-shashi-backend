## QCI Backend

The Quality Council of India (QCI) is a pioneering experiment of the Government of India in setting up organizations in partnership with the Indian industry.

## Key Features :


## Coding Guidelines :
#### Branching Structure And Guidelines :
Following branching structure has to be followed:
- **master** : For deployment on production server
- **development** : For deployment on development server
- **feature** : For deployment on development server

#### Modules Creation Guidelines :
Following naming conventions have to be used for specified types:
- Class name : First letter capital
- Method name : camelCase
- Variable name : camelCase

#### DB Considerations :
- The table/column names of the database should be created with "snake case"naming convention.
 - Use the appropriate data types for columns. 
 -  Use referential integrity – Foreign keys and unique constraints should be applied

#### General Considerations  :
- One should follow PSR1 and PSR 2 coding standards.
- Variable names should be in camelCase.
- Apply try catch blocks wherever applicable.
- Proper documentation of code wherever required.
- One should ensure exception handling

